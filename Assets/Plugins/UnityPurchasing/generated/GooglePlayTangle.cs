#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class GooglePlayTangle
    {
        private static byte[] data = System.Convert.FromBase64String("mh8IWhBroJN/y2nrqiP6nyfmgYRHevH0v9YJugj+c2JJqZ1JI+UipgaeXobtYdUiuUjPGk9yxcLdBiDeZddUd2VYU1x/0x3TolhUVFRQVVadMepQPVJs+Jr7khwC52XCmgXdRS5JHWPpMn7jxoLCuXECo7p5ZyLnWJLpJzIrrhwWtn2kQuHp/AbE0Nh28cLbJHNB0C45lmpDmwdExoECQSMJDh0QjOhmWWpC6zXN00CiNT1f/QgpNFy9OMp13B60p0FmFBqr317tF6htk2IEyGSsZxnbhpz0LG1xNddUWlVl11RfV9dUVFXCGiU6+YGPekHgnzhOXFg6lX3IXLRaWUzCmWoNKACLXcPiXVVmSNH0lR9UPEWiBSVqYwsF2b6+/ldWVFVU");
        private static int[] order = new int[] { 7,4,8,7,10,9,9,8,9,9,12,12,13,13,14 };
        private static int key = 85;

        public static byte[] Data() {
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif

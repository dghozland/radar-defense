#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class AppleTangle
    {
        private static byte[] data = System.Convert.FromBase64String("EhteNxAdUE9YTlp4fSt6dW1jPw4KFxgXHR8KG14cB14fEAdeDh8MCs9OJpIkekzyFs3xY6AbDYEZIBvCS0xPSk5NSCRpc01LTkxOR0xPSk5eERheChYbXgoWGxBeHw4OEhcdH3p4bXwrLU9tTm94fSt6dG10Pw4OYe+lYDkulXuTIAf6U5VI3CkyK5J4fStjcHpoempVrhc56gh3gIoV8/EN/x64ZSV3UezMhjo2jh5G4GuLwIoN5ZCsGnG1BzFKptxAhwaBFbbV3Q/sOS0rv9FRP82GhZ0Os5jdMuvgBHLaOfUlqmhJTbW6cTOwahevWpyVr8kOoXE7n1m0jxMGk5nLaWnWogBcS7Rbq6dxqBWq3Fpdb4nf0gE/1uaHr7QY4loVb67dxZplVL1h9Wf3oIc1Eot51VxOfJZmQIYud60n2Xt3Amk+KG9gCq3J9V1FOd2rERcYFx0fChcREF4/CwoWEQwXCgdPWE5aeH0renVtYz8ODhIbXj0bDAoZ8XbKXom10lJeEQ7IQX9O8sk9sTsAYTIVLug/97oKHHVu/T/5TfT/Ul4dGwwKFxgXHR8KG14OERIXHQfJZcPtPFpsVLlxY8gz4iAdtjX+aSwbEhcfEB0bXhEQXgoWFw1eHRsMDhIbXj0bDAoXGBcdHwoXERBePwtQPtiJOTMBdiBOYXh9K2NdemZOaHmSA0f99S1erUa6z8HkMXQVgVWCe359/H9xfk78f3R8/H9/fprv13doTmp4fSt6fW1zPw4OEhteLBERCgdeHw0NCxMbDV4fHR0bDgofEB0bEBpeHREQGhcKFxEQDV4RGF4LDRsaS11rNWsnY83qiYji4LEuxL8mLrdnDIsjcKsBIeWMW33EK/EzI3OPeE5xeH0rY21/f4F6e059f3+BTmMMHx0KFx0bXg0KHwobExsQCg1QTmH7/ftl50M5SYzX5T7wUqrP7mymUU7/vXh2VXh/e3t5fHxO/8hk/83+alWuFznqCHeAihXzUD7YiTkzAV4fEBpeHRsMChcYFx0fChcREF4Oc3h3VPg2+Ilzf397e359/H9/fiJI5zJTBsmT8uWijQnljAisCU4xv078esVO/H3d3n18f3x8f3xOc3h3vh1NCYlEeVIolaRxX3CkxA1nMcs3pgjhTWob3wnqt1N8fX9+f938f05veH0renRtdD8ODhIbXjcQHVBPDhIbXiwREQpePT9OYGlzTkhOSkynSAG/+Sun2efHTDyFpqsP4ADfLAkJUB8ODhIbUB0RE1EfDg4SGx0fBE78fwhOcHh9K2Nxf3+Benp9fH8cEhteDQofEBofDBpeChsMEw1eH/x/fnh3VPg2+IkdGnt/Tv+MTlR4Q1gZXvRNFIlz/LGgld1Rhy0UJRoKFhEMFwoHT2hOanh9K3p9bXM/Dl49P078f1xOc3h3VPg2+Ilzf39/diBO/H9veH0rY156/H92Tvx/ek5NSCROHE91Tnd4fSt6eG18Ky1PbctE04pxcH7sdc9faFAKq0JzpRxodlV4f3t7eXx/aGAWCgoODURRUQlx40ONVTdWZLaAsMvHcKcgYqi1Q1T4NviJc39/e3t+ThxPdU53eH0rLtT0q6Sagq53eUnOCwtf");
        private static int[] order = new int[] { 3,39,26,7,41,26,15,30,55,50,24,18,53,54,17,16,52,18,24,21,25,44,59,38,41,31,51,41,53,40,43,31,44,40,46,49,37,44,45,47,48,43,57,43,50,52,58,50,53,57,58,56,56,54,54,55,56,57,59,59,60 };
        private static int key = 126;

        public static byte[] Data() {
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif

using UnityEngine;
using System.Collections;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class AchievementData : ScriptableObject
{
    [Tooltip("Achievement name ID for the localization")]
    [SerializeField]
    public string achievementNameID;

    [Tooltip("Achievement description ID for the localization")]
    [SerializeField]
    public string achievementDescID;

    //[SerializeField]
    //public Sprite achievementIcon;

    [SerializeField]
    public EAchievementType achievementType;

    [SerializeField]
    public float achievementGoal;
    
    [SerializeField]
    public RewardData achievementReward;

    [Tooltip("Achievement order in the profile tab")]
    [SerializeField]
    public int achievementOrder = 0;

    public enum EAchievementOverride
    {
        Progress,
        Replace,
        Larger,
        LargerOrEqual,
        Smaller,
        SmallerOrEqual,
    }

    public enum EAchievementType
    {
        None,
        Kills,
        Wave,
        Shots,
        Infantry,
        LAV,
        Tank,
        HeavyArmor,
        Convoy,
        Transporter,
        Helicopter,
        Fighter,
        Bomber,
        BallisticMissile,
        Jeep,
        Drone,
        AssaultHelicopter,
        Pulse,
        Repair,
        Ammo,
        Slow,
        OhShit,
        StackPulse,
        StackRepair,
        StackAmmo,
        StackSlow,
        StackOhShit,
        Buggy,
        Mission_1,
        Mission_2,
        Mission_3,
        Mission_4,
        Mission_5,
        Mission_6,
        Mission_7,
        Mission_8,
        Mission_9,
        Mission_10,
        Mission_11,
        Mission_12,
        Mission_13,
        Mission_14,
        Mission_15,
        Mission_16,
        Mission_17,
        Mission_18,
        Mission_19,
        Mission_20,
        Mission_21,
        Mission_22,
        Mission_23,
        Mission_24,
        LastStand_1,
        LastStand_2,
        LastStand_3
    }

#if UNITY_EDITOR
    [MenuItem("Assets/Create/Gameplay/Achievement")]
    public static void CreateAsset()
    {
        GameUtils.CreateAsset<AchievementData>();
    }
#endif
}

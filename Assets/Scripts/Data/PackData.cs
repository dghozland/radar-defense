using UnityEngine;
using System.Collections;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class PackData : ScriptableObject
{
    [Tooltip("Pack name ID for the localization")]
    [SerializeField]
    public string packNameID;

    [Tooltip("Pack order in the store")]
    [SerializeField]
    public int packOrder;

    [Tooltip("Pack type for the store visual")]
    [SerializeField]
    public EPackType packType;
    
    [SerializeField]
    public Sprite packImage;

    [SerializeField]
    public float packPrice;

    [SerializeField]
    public GameUtils.ECurrencyType currencyType;

    [SerializeField]
    public RewardData packRewardData;

    [SerializeField]
    public int packRewardAmount = 1;

    [HideInInspector]
    [Tooltip("The IAP product data")]
    [SerializeField]
    public ProductData productData;

    public enum EPackType
    {
        Credit,
        Gold,
        Bundles,
        Weapon,
        Support,
        Boosters
    }

#if UNITY_EDITOR
    [MenuItem("Assets/Create/Gameplay/Pack")]
    public static void CreateAsset()
    {
        GameUtils.CreateAsset<PackData>();
    }
#endif
}

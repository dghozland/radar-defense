﻿using UnityEngine;
using System.Collections;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class BuildingData : BaseData
{
    public Texture2D buildingDestroyedTexture;
    public GameUtils.EConsumable consumableProduction;
    public Sprite consumableImage;
    public float consumableLifeTime;
    public float consumableTimeProduction;

#if UNITY_EDITOR
    [MenuItem("Assets/Create/Gameplay/Building")]
    public static void CreateAsset()
    {
        GameUtils.CreateAsset<BuildingData>();
    }
#endif
}

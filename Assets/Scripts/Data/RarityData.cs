using UnityEngine;
using System.Collections.Generic;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class RarityData : ScriptableObject
{
    [SerializeField]
    public List<int> weaponCardsRarities = new List<int>();

    [SerializeField]
    public List<int> weaponUpgradeCardsRarities = new List<int>();

    [SerializeField]
    public List<int> consumableCardsRarities = new List<int>();

#if UNITY_EDITOR
    [MenuItem("Assets/Create/Gameplay/Rarity")]
    public static void CreateAsset()
    {
        GameUtils.CreateAsset<RarityData>();
    }
#endif
}

using UnityEngine;
using System.Collections;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class RewardData : ScriptableObject
{
    [Tooltip("Type of reward.")]
    [SerializeField]
    public ERewardType rewardType = ERewardType.Pack;

    [Tooltip("Amount of weapon upgrade cards for a reward pack.")]
    [SerializeField]
    public int upgradeCardAmount = 1;

    [Tooltip("Amount of consumables for a reward pack.")]
    [SerializeField]
    public int consumableAmount = 0;
    
    [Tooltip("Specific weapons. Having an array of 0 will take a random weapon.")]
    [SerializeField]
    public GameUtils.BaseID[] specificWeapons;

    [Tooltip("Specific weapon upgrade. Choosing 'COUNT' will take a random weapon upgrade.")]
    [SerializeField]
    public GameUtils.EWeaponUpgrade specificWeaponUpgrade = GameUtils.EWeaponUpgrade.COUNT;
        
    [Tooltip("Currency amount for a credit/gold reward.")]
    [SerializeField]
    public int currencyAmount = 0;

    [Tooltip("Amount of Hours the booster will be active.")]
    [SerializeField]
    public int boosterHours = 0;

    [Tooltip("The type of the booster")]
    [SerializeField]
    public BoosterController.EBoosterPackType boosterType = BoosterController.EBoosterPackType.None;

    [Tooltip("The type of the free URL")]
    [SerializeField]
    public EFreeURLType freeURLType = EFreeURLType.None;

    public enum ERewardType
    {
        Pack,
        Credit,
        Gold,
        Booster
    }

    public enum EFreeURLType
    {
        None,
        Facebook,
        Twitter,
        Review
    }

    public void DebugConsoleReward()
    {
        GameUtils.BaseID firstSpecificWeapon = specificWeapons != null && specificWeapons.Length > 0 ? specificWeapons[0] : GameUtils.BaseID.NONE;
        switch (rewardType)
        {
            case ERewardType.Pack:
                Debug.Log("Reward Data ::: " + rewardType + " / " + upgradeCardAmount + " / " + consumableAmount + " / " + firstSpecificWeapon + " / " + specificWeaponUpgrade);
                break;
            case ERewardType.Credit:
            case ERewardType.Gold:
                Debug.Log("Reward Data ::: " + rewardType + " / " + currencyAmount);
                break;
        }
    }

#if UNITY_EDITOR
    [MenuItem("Assets/Create/Gameplay/Reward")]
    public static void CreateAsset()
    {
        GameUtils.CreateAsset<RewardData>();
    }
#endif
}

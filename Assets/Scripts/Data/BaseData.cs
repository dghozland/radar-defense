﻿using UnityEngine;
using System.Collections;

public class BaseData : ScriptableObject
{
    public GameUtils.BaseID baseID = GameUtils.BaseID.NONE;

    [Tooltip("Base name ID for the localization")]
    public string baseNameID;

    public Sprite baseImage;

    public Color baseColor = Color.white;

    public float sightRadius = 1f;

    public int maxHealth = 50;
}

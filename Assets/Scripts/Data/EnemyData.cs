using UnityEngine;
using System.Collections;

#pragma warning disable 0649

#if UNITY_EDITOR
using UnityEditor;
#endif

public class EnemyData : ScriptableObject
{
    [SerializeField]
    public GameUtils.UnitType enemyType = GameUtils.UnitType.INFANTRY;

    [Tooltip("Enemy name ID for the localization")]
    [SerializeField]
    public string enemyNameID;

    [SerializeField]
    public bool isUnitAir = false;

    [SerializeField]
    public bool isUnitBonus = false;

    [SerializeField]
    public Sprite enemyNormalSprite;

    [SerializeField]
    public Texture2D enemyDestroyedTexture;

    [SerializeField]
    public Color enemyColor = Color.white;

    [SerializeField]
    public Color trailColor = Color.white;

    [SerializeField]
    public float colliderRadius = 10f;

    [SerializeField]
    private EnemyAttributes normalAttributes;

    [SerializeField]
    private EnemyAttributes hardAttributes;

    [SerializeField]
    private EnemyAttributes insaneAttributes;

    [SerializeField]
    private EnemyAttributes customAttributes;

    [SerializeField]
    public AudioClip enemyDeathSound;

    // new fields

    [SerializeField]
    public GameUtils.EMovementType movementType = GameUtils.EMovementType.FollowPathToBase;

    [SerializeField]
    public GameUtils.ETargetBaseType targetBaseType = GameUtils.ETargetBaseType.All;

    [SerializeField]
    public bool continueAfterTarget = false;

    [SerializeField]
    public GameUtils.UnitType spawnType = GameUtils.UnitType.NONE;

    [SerializeField]
    public float unitSpawnTimer = 20f;

    [SerializeField]
    public int unitSpawnCount = 1;

    [SerializeField]
    public bool destroyOnSpawn = false;

    [SerializeField]
    public bool returnFromBorder = false;

    [SerializeField]
    public GameUtils.EBossSpecial bossSpecial = GameUtils.EBossSpecial.None;

    [SerializeField]
    public float timeToDeploySpecial = 0f;

    [SerializeField]
    public float specialRepeatTime = 0f;

    [SerializeField]
    public float specialValue = 0f;


    public EnemyAttributes GetEnemyAttributes(GameUtils.EGameDifficulty difficulty)
    {
        switch(difficulty)
        {
            case GameUtils.EGameDifficulty.Normal:
                return normalAttributes;
            case GameUtils.EGameDifficulty.Hard:
                return hardAttributes;
            case GameUtils.EGameDifficulty.Insane:
                return insaneAttributes;
            case GameUtils.EGameDifficulty.Custom:
                return customAttributes;
            default:
                return null;
        }
    }


#if UNITY_EDITOR
    [MenuItem("Assets/Create/Gameplay/Enemy")]
    public static void CreateAsset()
    {
        GameUtils.CreateAsset<EnemyData>();
    }
#endif
}

﻿using UnityEngine;
using System.Collections;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class WeaponData : BaseData
{
    public Sprite weapon3DImage;
    
    [Tooltip("Level at which the weapon will be unlocked. (i.e Flak = 3 so after clearing Sector 003, it will be unlocked)")]
    public int unlockLvl;

    [Tooltip("Price for the unlock (put 0 if none)")]
    public int unlockPrice;

    public Sprite weaponImageTop;

    public Sprite weaponImageBottom;

    public Texture2D weaponTopDestroyedTexture;

    public Texture2D weaponBottomDestroyedTexture;
    
    [Tooltip("Base ground damage.")]
    public float groundPower = 1f;

    [Tooltip("Base air damage.")]
    public float airPower = 1f;

    [Tooltip("Damage multiplier which will be applied for each upgrade level for both ground and air.")]
    public float damageMultiplier = 1.4f;

    [Tooltip("Base explosion radius.")]
    public float radius = 2f;

    [Tooltip("Base projectile speed.")]
    public float speed = 20f;

    [Tooltip("Base ammo regen.")]
    public float regeneration = 5f;

    [Tooltip("Firing rate cooldown.")]
    public float cooldown = 3f;

    [Tooltip("Starting ammo count.")]
    public int startingAmmo = 5;

    [Tooltip("Health per upgrade level.")]
    public float healthPerUpgradeLvl = 5f;

    [Tooltip("Projectile speed per upgrade level.")]
    public float projectileSpeedPerUpgradeLvl = 1f;

    [Tooltip("Air damage per upgrade level.")]
    public float airDamagePerUpgradeLvl = 1f;

    [Tooltip("Ground damage per upgrade level.")]
    public float groundDamagePerUpgradeLvl = 1f;

    [Tooltip("Explosion radius per upgrade level.")]
    public float explosionRadiusPerUpgradeLvl = 0.2f;

    [Tooltip("Sight radius per upgrade level.")]
    public float sightRadiusPerUpgradeLvl = 0.2f;

    [Tooltip("Firing rate cooldown per upgrade level (needs to be negative).")]
    public float reloadSpeedPerUpgradeLvl = -0.1f;

    [Tooltip("Ammo regen speed per upgrade level (needs to be negative).")]
    public float regenSpeedPerUpgradeLvl = -0.1f;

    [Tooltip("Starting ammo count per upgrade level")]
    public float startingAmmoPerUpgradeLvl = 1f;
    
    [Tooltip("Sound when firing the weapon.")]
    public AudioClip fireSound;

    [Tooltip("Extra sound (currently being used only for the laser)")]
    public AudioClip extraSound;

    [Tooltip("Sound when firing is not possible (no more ammo or on cooldown)")]
    public AudioClip emptySound;

#if UNITY_EDITOR
    [MenuItem("Assets/Create/Gameplay/Weapon")]
    public static void CreateAsset()
    {
        GameUtils.CreateAsset<WeaponData>();
    }
#endif
}

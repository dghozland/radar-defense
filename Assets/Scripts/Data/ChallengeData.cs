using UnityEngine;
using System.Collections;
using System;

#if UNITY_EDITOR
using UnityEditor;
#endif

#pragma warning disable 0649

public class ChallengeData : ScriptableObject
{
    [Tooltip("Challenge name ID for the localization")]
    [SerializeField]
    public string challengeNameID;

    [SerializeField]
    private string normalChallengeNumber;

    [SerializeField]
    private string hardChallengeNumber;

    [SerializeField]
    private string insaneChallengeNumber;

    [SerializeField]
    public string sceneName;

    [SerializeField]
    public Sprite challengeMap;

    [SerializeField]
    public EChallengeType challengeType;

    public enum EChallengeType
    {
        Mission,
        LastStand,
        Operation,
        Event
    }

    [Serializable]
    public class EnemyInfo
    {
        [SerializeField]
        public GameUtils.UnitType type;
        
        [Tooltip("Amount of enemy unit to spawn. (Used for Campaign)")]
        [SerializeField]
        public int amount;

        [Tooltip("Spawn percentage for the enemy unit to spawn, between 0 and 100. (Used for Last Stands)")]
        [Range(0, 100)]
        [SerializeField]
        public int spawnPercentage;
    }

    [Serializable]
    public class WaveData2
    {
        public bool EnemyOrientationNorth = true;
        public bool EnemyOrientationEast = true;
        public bool EnemyOrientationSouth = true;
        public bool EnemyOrientationWest = true;

        [Tooltip("Total amount of enemies to spawn for the wave. (Used for Last Stands")]
        public int enemyAmount;

        public EnemyInfo[] enemyInfo;
                
        public float GetWaveDuration(GameUtils.EGameDifficulty difficulty, EChallengeType challengeType)
        {
            float ttk = GameUtils.GetTTK(difficulty, challengeType);
            int enemyCount = 0;

            if (challengeType == EChallengeType.LastStand)
            {
                enemyCount = enemyAmount;
            }
            else if (challengeType == EChallengeType.Mission)
            {
                for (int i = 0; i < enemyInfo.Length; ++i)
                {
                    enemyCount += enemyInfo[i].amount;
                }
            }
            else if (challengeType == EChallengeType.Operation)
            {
                for (int i = 0; i < enemyInfo.Length; ++i)
                {
                    enemyCount += enemyInfo[i].amount;
                }
            }
            return enemyCount * ttk;
        }
    }

    [SerializeField]
    public WaveData2[] normalWaves;

    [SerializeField]
    public WaveData2[] hardWaves;

    [SerializeField]
    public WaveData2[] insaneWaves;

    [SerializeField]
    public float timeBetweenWaves = 10f;

    [Serializable]
    public class GrindCardRarity
    {
        public GameUtils.BaseID weaponID;
        public GameUtils.EWeaponUpgrade weaponUpgrade;
        public int rarity;
    }

    [SerializeField]
    public GrindCardRarity[] normalGrindCardRarities;

    [SerializeField]
    public GrindCardRarity[] hardGrindCardRarities;

    [SerializeField]
    public GrindCardRarity[] insaneGrindCardRarities;

    [SerializeField]
    public int weaponLvlLimit = -1;


    public WaveData2[] GetWavesData(GameUtils.EGameDifficulty difficulty)
    {
        switch (difficulty)
        {
            case GameUtils.EGameDifficulty.Normal:
                return normalWaves;
            case GameUtils.EGameDifficulty.Hard:
                if (hardWaves.Length > 0)
                {
                    return hardWaves;
                }
                break;
            case GameUtils.EGameDifficulty.Insane:
                if (insaneWaves.Length > 0)
                {
                    return insaneWaves;
                }
                break;
            case GameUtils.EGameDifficulty.Custom:
                break;
        }

        return normalWaves;
    }

    public GrindCardRarity[] GetGrindCardRarities(GameUtils.EGameDifficulty difficulty)
    {
        switch (difficulty)
        {
            case GameUtils.EGameDifficulty.Normal:
                return normalGrindCardRarities;
            case GameUtils.EGameDifficulty.Hard:
                return hardGrindCardRarities;
            case GameUtils.EGameDifficulty.Insane:
                return insaneGrindCardRarities;
            case GameUtils.EGameDifficulty.Custom:
                break;
        }

        return normalGrindCardRarities;
    }

    public string GetChallengeNumber(GameUtils.EGameDifficulty difficulty)
    {
        switch (difficulty)
        {
            case GameUtils.EGameDifficulty.Insane:
                return insaneChallengeNumber;
            case GameUtils.EGameDifficulty.Hard:
                return hardChallengeNumber;
            case GameUtils.EGameDifficulty.Normal:
            default:
                return normalChallengeNumber;
        }
    }
    

#if UNITY_EDITOR
    [MenuItem("Assets/Create/Gameplay/Challenge")]
    public static void CreateAsset()
    {
        GameUtils.CreateAsset<ChallengeData>();
    }
#endif
}

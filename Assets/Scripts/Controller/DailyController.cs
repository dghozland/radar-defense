using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class DailyController : UnitySingleton<DailyController>
{ 
    [Serializable]
    public class DailyData
    {
        private Dictionary<RewardData.ERewardType, DateTime> openedDailys;
        private Dictionary<BoosterController.EBoosterPackType, DateTime> activeBoosterPacks;
        private int lastDaily;
        private int currentDaily;
        private bool currentDailyCompleted;
        private DateTime lastDailyDate;
        private DateTime currentDailyDate;
        private bool wentToFacebook;
        private bool wentToReview;
        private bool wentToTwitter;

        public DailyData()
        {
            openedDailys = new Dictionary<RewardData.ERewardType, DateTime>();
            activeBoosterPacks = new Dictionary<BoosterController.EBoosterPackType, DateTime>();
            lastDaily = -1;
            currentDaily = -1;
            currentDailyCompleted = false;
            currentDailyDate = DateTime.Now;
            lastDailyDate = DateTime.Now;
            wentToFacebook = false;
            wentToReview = false;
            wentToTwitter = false;
        }

        public DailyData(DailyData copy)
        {
            openedDailys = copy.openedDailys;
            activeBoosterPacks = copy.activeBoosterPacks;
            lastDaily = copy.lastDaily;
            currentDaily = copy.currentDaily;
            currentDailyCompleted = copy.currentDailyCompleted;
            currentDailyDate = copy.currentDailyDate;
            lastDailyDate = copy.lastDailyDate;
            wentToFacebook = copy.wentToFacebook;
            wentToReview = copy.wentToReview;
            wentToTwitter = copy.wentToTwitter;
        }

        public Dictionary<RewardData.ERewardType, DateTime> OpenedDailys
        {
            get { return openedDailys; }
            set { openedDailys = value; }
        }

        public Dictionary<BoosterController.EBoosterPackType, DateTime> ActiveBoosterPacks
        {
            get { return activeBoosterPacks; }
            set { activeBoosterPacks = value; }
        }

        public int LastDaily
        {
            get { return lastDaily; }
            set { lastDaily = value; }
        }

        public int CurrentDaily
        {
            get { return currentDaily; }
            set { currentDaily = value; }
        }

        public bool CurrentDailyCompleted
        {
            get { return currentDailyCompleted; }
            set { currentDailyCompleted = value; }
        }

        public DateTime LastDailyDate
        {
            get { return lastDailyDate; }
            set { lastDailyDate = value; }
        }

        public DateTime CurrentDailyDate
        {
            get { return currentDailyDate; }
            set { currentDailyDate = value; }
        }

        public bool WentToFacebook
        {
            get { return wentToFacebook; }
            set { wentToFacebook = value; }
        }

        public bool WentToReview
        {
            get { return wentToReview; }
            set { wentToReview = value; }
        }

        public bool WentToTwitter
        {
            get { return wentToTwitter; }
            set { wentToTwitter = value; }
        }
    }

    [SerializeField]
    private int baseDailyReward = 25000;
    [SerializeField]
    private int inactiveDaysCredits = 5000;
    [SerializeField]
    private int maxInactiveDaysCounted = 10;

    private static bool isInitialized = false;
    private DailyData dynamicData;
    private DateTime wentToURLTime;
    private PackData recentPack;

    public DailyData DynamicData
    {
        get { return dynamicData; }
        set { dynamicData = value; }
    }

    public DailyController()
    {
        dynamicData = new DailyData();
    }

    public override void Awake()
    {
        base.Awake();
        if (!isInitialized)
        {
            // Always keep the controller loaded through the scene changes
            DontDestroyOnLoad(gameObject);
            isInitialized = true;
        }
    }

#region Free URL Clicks
    public void ViewPageAndReward(PackData packData)
    {
        switch(packData.packRewardData.freeURLType)
        {
            case RewardData.EFreeURLType.Facebook:
                {
                    wentToURLTime = DateTime.Now;
                    recentPack = packData;
#if UNITY_ANDROID && !UNITY_EDITOR
                    if (GameUtils.CheckPackageAppIsPresent("com.facebook.katana"))
                    {
                        //there is Facebook app installed so let's use it
                        Application.OpenURL("fb://page/1530965673878732"); 
                    }
                    else
#endif
                    {
                        // no Facebook app - use built-in web browser
                        Application.OpenURL("https://www.facebook.com/AdageGames/"); 
                    }

                }
                break;
            case RewardData.EFreeURLType.Twitter:
                {
                    wentToURLTime = DateTime.Now;
                    recentPack = packData;
#if UNITY_ANDROID && !UNITY_EDITOR
                    if (GameUtils.CheckPackageAppIsPresent("com.twitter.android"))
                    {
                        //there is Facebook app installed so let's use it
                        Application.OpenURL("twitter://user?screen_name=AdageGames"); 
                    }
                    else
#endif
                    {
                        // no Twitter app - use built-in web browser
                        Application.OpenURL("https://twitter.com/AdageGames/");
                    }

                }
                break;
            case RewardData.EFreeURLType.Review:
                {
                    wentToURLTime = DateTime.Now;
                    recentPack = packData;
#if UNITY_ANDROID && !UNITY_EDITOR
                    if (GameUtils.CheckPackageAppIsPresent("com.twitter.android"))
                    {
                        //there is play store app installed so let's use it
                        Application.OpenURL("market://details?id=com.AdageGames.RadarWarfare"); 
                    }
                    else
                    {
                        //  app Play Store App - use built-in web browser
                        Application.OpenURL("https://play.google.com/store/apps/details?id=com.AdageGames.RadarWarfare&hl=en");
                    }
#elif UNITY_IOS && !UNITY_EDITOR
                    {
                        // on iOS no need to test, the app store is always there
                        Application.OpenURL("itms-apps://itunes.apple.com/app/id1142834664");
                    }
#elif !UNITY_EDITOR
                    {
                        //  app Play Store App - use built-in web browser
                        Application.OpenURL("https://play.google.com/store/apps/details?id=com.AdageGames.RadarWarfare&hl=en");
                    }
#endif
                }
                break;
        }
    }

    private void OnApplicationFocus(bool pauseStatus)
    {
        if (pauseStatus)
        {
            // the app is back from loosing the focus
            TimeSpan diff = DateTime.Now - wentToURLTime;
            if (diff.TotalSeconds > 3 && diff.TotalMinutes < 5 && recentPack != null)
            {
                // check has it already
                if (HasRewardAlready(recentPack.packRewardData.freeURLType)) return;
                // save the item
                SaveURLReward(recentPack.packRewardData.freeURLType);

                // user was on URL and did some stuff, let's reward him
                PackController.Instance.RewardPackFromFreeURL(recentPack);
            }
            // reset stuff
            wentToURLTime = DateTime.MinValue;
            recentPack = null;
        }
    }

    private void SaveURLReward(RewardData.EFreeURLType type)
    {
        switch (type)
        {
            case RewardData.EFreeURLType.Facebook:
                dynamicData.WentToFacebook = true;
                break;
            case RewardData.EFreeURLType.Twitter:
                dynamicData.WentToTwitter = true;
                break;
            case RewardData.EFreeURLType.Review:
                dynamicData.WentToReview = true;
                break;
        }
        SaveDailyData(DataController.SAVEPRIORITY.INSTANT);
    }

    private bool HasRewardAlready(RewardData.EFreeURLType type)
    {
        switch(type)
        {
            case RewardData.EFreeURLType.Facebook:
                return dynamicData.WentToFacebook;
            case RewardData.EFreeURLType.Twitter:
                return dynamicData.WentToTwitter;
            case RewardData.EFreeURLType.Review:
                return dynamicData.WentToReview;
        }
        // if not the right type assume the user has it
        return true;
    }

    public bool HasAllURLItems()
    {
        return dynamicData.WentToFacebook && dynamicData.WentToTwitter && dynamicData.WentToReview;
    }

    public bool HasURLItem(RewardData.EFreeURLType type)
    {
        switch (type)
        {
            case RewardData.EFreeURLType.Facebook:
                return dynamicData.WentToFacebook;
            case RewardData.EFreeURLType.Twitter:
                return dynamicData.WentToTwitter;
            case RewardData.EFreeURLType.Review:
                return dynamicData.WentToReview;
        }
        return false;
    }
#endregion // Free URL Clicks


#region Daily Mission
    public bool IsDailyMission(int challengeIndex)
    {
        // make sure a daily is set
        // change the daily if it's a new day
        if (dynamicData.CurrentDaily == -1 || dynamicData.CurrentDailyDate.Date != DateTime.Now.Date)
        {
            SelectDailyMission();
        }

        // return true if this is the current daily
        if (dynamicData.CurrentDaily == challengeIndex)
        {
            return !dynamicData.CurrentDailyCompleted;
        }

        // return false if not
        return false;
    }

    public bool IsDailyComplete()
    {
        return dynamicData.CurrentDailyCompleted;
    }

    public bool CompleteDaily(int challengeIndex)
    {
        if (dynamicData.CurrentDaily == challengeIndex && dynamicData.CurrentDailyCompleted == false)
        {
            dynamicData.CurrentDailyCompleted = true;
            SaveDailyData(DataController.SAVEPRIORITY.INSTANT);
            // return true if unlocked the reward now
            return true;
        }
        // do not give reward again
        return false;
    }

    public int GetDailyMissionReward()
    {
        int creditsReward = 0;
        // how many days since last daily
        TimeSpan diff = DateTime.Now.Subtract(dynamicData.LastDailyDate);
        // max days counted
        int maxDays = Math.Min((int)diff.TotalDays, maxInactiveDaysCounted + 1) - 1;
        maxDays = Math.Max(maxDays, 0);
        // multiply the amount of days
        creditsReward += inactiveDaysCredits * maxDays;
        // add the base
        creditsReward += baseDailyReward;

        return creditsReward;
    }

    private void SelectDailyMission()
    {
        int lastChallengeComplete = ChallengeController.Instance.GetChallengeProgressionIndex();
        dynamicData.LastDailyDate = dynamicData.CurrentDailyDate;
        dynamicData.LastDaily = dynamicData.CurrentDaily;
        if (lastChallengeComplete > 1)
        {
            // more than first mission available
            dynamicData.CurrentDaily = -1;
            while (dynamicData.CurrentDaily < 1 )
            {
                int newDailyMap = UnityEngine.Random.Range(1, lastChallengeComplete + 1);
                if (newDailyMap != dynamicData.LastDaily)
                {
                    // found a mission that was not the same as the last one
                    dynamicData.CurrentDaily = newDailyMap;
                    dynamicData.CurrentDailyDate = DateTime.Now;
                    dynamicData.CurrentDailyCompleted = false;
                }
            }
        }
        else
        {
            // only first mission available
            dynamicData.CurrentDaily = 1;
            dynamicData.CurrentDailyDate = DateTime.Now;
            dynamicData.CurrentDailyCompleted = false;
        }
        SaveDailyData(DataController.SAVEPRIORITY.INSTANT);
    }
#endregion // Daily Mission

#region Daily Packs
    public bool CanOpenDailyForRewardType(RewardData.ERewardType type)
    {
        if (dynamicData.OpenedDailys.ContainsKey(type))
        {
            DateTime lastSeen = dynamicData.OpenedDailys[type];
            DateTime nextDay = new DateTime(lastSeen.Year, lastSeen.Month, lastSeen.Day, 0, 0, 1);
            nextDay = nextDay.AddDays(1);
            if (nextDay > DateTime.Now)
            {
                return false;
            }
        }
        return true;
    }

    public int TimeUntilNextDaily(RewardData.ERewardType type, GameUtils.ETimeType timeType)
    {
        if (dynamicData.OpenedDailys.ContainsKey(type))
        {
            DateTime now = DateTime.Now;
            // get the dateTime from the save
            DateTime openedTime = dynamicData.OpenedDailys[type];
            // add day to last time opened the daily
            DateTime nextDay = openedTime.AddDays(1);
            // set that time to 12:00:01 AM (0:00:01h in the morning)
            nextDay = new DateTime(nextDay.Year, nextDay.Month, nextDay.Day, 0, 0, 1);
            // if we are before that date, return a time value
            // if we are after return 0
            if (nextDay > now)
            {
                System.TimeSpan diff = nextDay.Subtract(now);
                if (timeType == GameUtils.ETimeType.Hours)
                {
                    return diff.Hours;
                }
                else if (timeType == GameUtils.ETimeType.Minutes)
                {
                    return diff.Minutes;
                }
                else if (timeType == GameUtils.ETimeType.Seconds)
                {
                    return diff.Seconds;
                }
            }
        }
        return 0;
    }

    public void OpenDailyPack(RewardData.ERewardType type)
    {
        System.DateTime time = System.DateTime.Now;
        if (dynamicData.OpenedDailys.ContainsKey(type))
        {
            dynamicData.OpenedDailys[type] = time;
        }
        else
        {
            dynamicData.OpenedDailys.Add(type, time);
        }
        SaveDailyData(DataController.SAVEPRIORITY.INSTANT);
    }
#endregion // Daily Packs

#region Booster Packs
    public bool CanOpenBoosterForBoosterType(BoosterController.EBoosterPackType type)
    {
        if (dynamicData.ActiveBoosterPacks.ContainsKey(type))
        {
            DateTime endDate = dynamicData.ActiveBoosterPacks[type];
            if (endDate > DateTime.Now)
            {
                return false;
            }
        }
        return true;
    }

    public void AddBooster(BoosterController.EBoosterPackType type, int boosterHours)
    {
        if (dynamicData.ActiveBoosterPacks.ContainsKey(type))
        {
            if (dynamicData.ActiveBoosterPacks[type] < DateTime.Now)
            {
                dynamicData.ActiveBoosterPacks[type] = DateTime.Now;
            }
            dynamicData.ActiveBoosterPacks[type] = dynamicData.ActiveBoosterPacks[type].AddHours(boosterHours);
        }
        else
        {
            dynamicData.ActiveBoosterPacks[type] = DateTime.Now.AddHours(boosterHours);
        }
        SaveDailyData(DataController.SAVEPRIORITY.INSTANT);
    }

    public int TimeUntilBoosterEnds(BoosterController.EBoosterPackType type, GameUtils.ETimeType timeType)
    {
        if (dynamicData.ActiveBoosterPacks.ContainsKey(type))
        {
            DateTime now = DateTime.Now;
            // get the dateTime from the save
            DateTime openedTime = dynamicData.ActiveBoosterPacks[type];

            // if we are before that date, return a time value
            // if we are after return 0
            if (openedTime > now)
            {
                System.TimeSpan diff = openedTime.Subtract(now);
                if (timeType == GameUtils.ETimeType.Hours)
                {
                    return (int)diff.TotalHours;
                }
                else if (timeType == GameUtils.ETimeType.Minutes)
                {
                    return diff.Minutes;
                }
                else if (timeType == GameUtils.ETimeType.Seconds)
                {
                    return diff.Seconds;
                }
            }
        }
        return 0;
    }
#endregion // Booster Packs


    public void SaveDailyData(DataController.SAVEPRIORITY priority)
    {
        DataController.Instance.SaveDailyData(priority);
    }

    public void LoadDailyData(DailyData remoteDailyData)
    {
        if (remoteDailyData == null)
        {
            System.Exception ex = JsonUtils<DailyData>.Load(GameUtils.DAILY_JSON, out dynamicData, GameUtils.ELoadType.DynamicData);
            if (ex != null && ex.GetType() == typeof(FileLoadException))
            {
                ResetDailyData();
            }
        }
        else
        {
            dynamicData = remoteDailyData;
        }
        CleanDailyData();
    }

    private void CleanDailyData()
    {
        // add the dictionaries
        if (dynamicData.OpenedDailys == null)
        {
            dynamicData.OpenedDailys = new Dictionary<RewardData.ERewardType, DateTime>();
        }
        if (dynamicData.ActiveBoosterPacks == null)
        {
            dynamicData.ActiveBoosterPacks = new Dictionary<BoosterController.EBoosterPackType, DateTime>();
        }
        // check the data and remove data if already in the past  
        if (dynamicData.OpenedDailys.Count > 0)
        {
            List<RewardData.ERewardType> removeKeys = new List<RewardData.ERewardType>();
            foreach (var daily in dynamicData.OpenedDailys)
            {
                if (CanOpenDailyForRewardType(daily.Key))
                {
                    removeKeys.Add(daily.Key);
                }
            }
            for(int i = 0; i < removeKeys.Count; ++i)
            {
                dynamicData.OpenedDailys.Remove(removeKeys[0]);
            }
        }
        if (dynamicData.ActiveBoosterPacks.Count > 0)
        {
            List<BoosterController.EBoosterPackType> removeKeys = new List<BoosterController.EBoosterPackType>();
            foreach (var booster in dynamicData.ActiveBoosterPacks)
            {
                if (CanOpenBoosterForBoosterType(booster.Key))
                {
                    removeKeys.Add(booster.Key);
                }
            }
            for (int i = 0; i < removeKeys.Count; ++i)
            {
                dynamicData.ActiveBoosterPacks.Remove(removeKeys[0]);
            }
        }
    }

    public void ResetDailyData()
    {
        if (dynamicData == null)
        {
            dynamicData = new DailyData();
        }
        // reset daily Packs
        if (dynamicData.OpenedDailys == null)
        {
            dynamicData.OpenedDailys = new Dictionary<RewardData.ERewardType, DateTime>();
        }
        else
        {
            dynamicData.OpenedDailys.Clear();
        }
        // reset booster Packs
        if (dynamicData.ActiveBoosterPacks == null)
        {
            dynamicData.ActiveBoosterPacks = new Dictionary<BoosterController.EBoosterPackType, DateTime>();
        }
        else
        {
            dynamicData.ActiveBoosterPacks.Clear();
        }
        dynamicData.LastDaily = -1;
        dynamicData.CurrentDaily = -1;
        dynamicData.CurrentDailyCompleted = false;
        dynamicData.CurrentDailyDate = DateTime.Now;
        dynamicData.LastDailyDate = DateTime.Now;
        dynamicData.WentToFacebook = false;
        dynamicData.WentToReview = false;
        dynamicData.WentToTwitter = false;
        SaveDailyData(DataController.SAVEPRIORITY.QUEUE);
    }
}

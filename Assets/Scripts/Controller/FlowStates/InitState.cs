public class InitState : BaseState
{
    bool initialized = false;

    public override void EnterState()
    {
        base.EnterState();
    }

    public override void EnterTransition()
    {
        base.EnterTransition();
    }

    public override void UpdateState()
    {
        if (!initialized)
        {
            initialized = true;
            FlowStateController.Instance.SetState(FlowStateController.FlowStates.Intro);
        }
    }

    public override void ExitState()
    {

    }

    public override int GetStateType()
    {
        return FlowStateController.FlowStates.Init;
    }
}
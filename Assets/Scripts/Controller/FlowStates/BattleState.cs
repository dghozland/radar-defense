using UnityEngine.SceneManagement;

public class BattleState : BaseState
{
    private bool initialized = false;
    private UnityEngine.AsyncOperation loadScene = null;
    private string battleScene;

    public void SetBattleScene(string sceneName)
    {
        battleScene = sceneName;
    }

    public override void EnterState()
    {
        FlowStateController.Instance.LoadingScreen(true);
        base.EnterState();
        initialized = false;
        loadScene = SceneManager.LoadSceneAsync(battleScene, UnityEngine.SceneManagement.LoadSceneMode.Single);
    }

    public override void EnterTransition()
    {
        if (DataController.Instance.AllLoaded)
        {
            base.EnterTransition();
        }
    }

    public override void UpdateState()
    {
        if ((loadScene != null && loadScene.isDone && !initialized) || (loadScene == null && !initialized))
        {
            initialized = true;

            // play music
            AudioManager.Instance.PlayBattleMusic();

            FlowStateController.Instance.LoadingScreen(false);
        }
    }

    public override void ExitState()
    {
        DataController.Instance.SaveData();
    }

    public override int GetStateType()
    {
        return FlowStateController.FlowStates.Battle;
    }
}
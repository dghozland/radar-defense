using UnityEngine.UI;
using UnityEngine;

#pragma warning disable 0649

public class IntroState : BaseState
{
    private CanvasGroup canvasGroup;
    private float fadeInValue = 0f;
    private float fadeOutValue = 1f;
    private float waitValue = 2f;
    private float fadeSpeed = 0.8f;
    private bool finished = false;

    public void Awake()
    {
        SetAlphaValue(0f);
    }

    public override void EnterState()
    {
        base.EnterState();
        canvasGroup = GameObject.Find("InitUI").GetComponent<CanvasGroup>();
    }

    public override void EnterTransition()
    {
        base.EnterTransition();
    }

    public override void UpdateState()
    {        
        if (fadeInValue <= 1f)
        {
            // fade in
            fadeInValue += Time.deltaTime * fadeSpeed;            
            canvasGroup.alpha = Mathf.Lerp(0f, 1f, fadeInValue);
        }
        else if (fadeInValue > 1f && waitValue > 0f)
        {
            // fade out
            waitValue -= Time.deltaTime;
        }
        else if (waitValue <= 0f && fadeInValue > 1f && fadeOutValue > 0f)
        {
            // fade out
            fadeOutValue -= Time.deltaTime * fadeSpeed;

            SetAlphaValue(Mathf.Lerp(0f, 1f, fadeOutValue));
        }
        else
        {
            if (!finished)
            {
                finished = true;
                FlowStateController.Instance.LoadingScreen(true);
            }
            if (!DataController.Instance.AllLoaded) return;

            // Check if tutorial has been done
            if (ChallengeController.Instance.GetChallengeAt(0).stars == 3)
            {
                FlowStateController.Instance.SetState(FlowStateController.FlowStates.Menu);
            }
            else
            {
                ChallengeController.Instance.EnterTutorial();
            }
        }
    }

    public override void ExitState()
    {

    }
    public override int GetStateType()
    {
        return FlowStateController.FlowStates.Intro;
    }

    private void SetAlphaValue(float alpha)
    {
        canvasGroup.alpha = alpha;
    }
}
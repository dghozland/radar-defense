using UnityEngine.SceneManagement;

public class MenuState : BaseState
{
    bool initialized = true;
    UnityEngine.AsyncOperation loadScene;

    public override void EnterState()
    {
        FlowStateController.Instance.LoadingScreen(true);
        base.EnterState();
        // load main menu scene
        initialized = false;
        loadScene = SceneManager.LoadSceneAsync(FlowStateController.MENU_SCENE_NAME, UnityEngine.SceneManagement.LoadSceneMode.Single);
    }

    public override void EnterTransition()
    {
        if (DataController.Instance.AllLoaded)
        {
            AudioManager.Instance.PlayMenuMusic();
            base.EnterTransition();
        }
    }

    public override void UpdateState()
    {
		if (loadScene != null && loadScene.isDone && !initialized)
        {
            FlowStateController.Instance.RegisterBackButton(FlowStateController.Instance.ShowQuitInfoPanel);
            initialized = true;
        }
    }

    public override void ExitState()
    {
        FlowStateController.Instance.UnregisterBackButton(FlowStateController.Instance.ShowQuitInfoPanel);
        DataController.Instance.SaveData();
    }

    public override int GetStateType()
    {
        return FlowStateController.FlowStates.Menu;
    }
}
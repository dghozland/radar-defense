using UnityEngine;
using System.Collections;
using System;
using System.IO;
using UnityEngine.UI;
#if UNITY_ANDROID
using GooglePlayGames;
#endif

public class CurrencyController : UnitySingleton<CurrencyController>
{
    private static bool isInitialized = false;
    private static bool loaded = false;
    private static bool setup = false;

    [Serializable]
    public class CurrencyData
    {
        public CurrencyData()
        {
            credits = 0;
            gold = 0;
            creditsSpent = 0;
        }

        public CurrencyData(CurrencyData copy)
        {
            credits = copy.credits;
            gold = copy.gold;
            creditsSpent = copy.creditsSpent;
        }

        public int credits;
        public int gold;
        public int creditsSpent;
    }

    [SerializeField]
    private static CurrencyData currencyData;

    // UI elements
    private Text creditsText;
    private Text goldText;
    private Button debugMaxCurrencyButton;

    public CurrencyData Currency
    {
        get { return currencyData; }
        set { currencyData = value; }
    }

    public override void Awake()
    {
        base.Awake();
        if (!isInitialized)
        {
            // Always keep the controller loaded through the scene changes
            DontDestroyOnLoad(gameObject);
            isInitialized = true;
        }
        setup = false;
    }

    public override bool FixedUpdate()
    {
        if (!base.FixedUpdate()) return false;

        if (DataController.Instance.CurrencyDataLoaded && !loaded)
        {
            loaded = true;
        }
        if (!loaded) return false;

        if (FlowStateController.Instance.IsInMenuScene())
        {
            if (!setup)
            {
                setup = true;
                GameObject currencyPanel = GameObject.Find("prefab_CurrencyPanel");

                creditsText = GameUtils.GetChildGameObject(currencyPanel, "CreditsText", true).GetComponent<Text>();
                goldText = GameUtils.GetChildGameObject(currencyPanel, "GoldText", true).GetComponent<Text>();

                debugMaxCurrencyButton = GameUtils.GetChildGameObject(currencyPanel, "BtnDebugMaxCurrency", true).GetComponent<Button>();

#if UNITY_EDITOR || DEBUG_MOBILE
                debugMaxCurrencyButton.onClick.AddListener(delegate { DebugMaxCurrencies(); });
#else
                debugMaxCurrencyButton.gameObject.SetActive(false);
#endif
            }
            RefreshCreditsUI();
            RefreshGoldUI();
        }
        return true;
    }

    #region Currency UI
    private void RefreshCreditsUI()
    {
        // Update UI if existing
        if (creditsText)
        {
            creditsText.text = currencyData.credits.ToString();
        }
    }

    private void RefreshGoldUI()
    {
        // Update UI if existing
        if (goldText)
        {
            goldText.text = currencyData.gold.ToString();
        }
    }
    #endregion

    public void ModifyCreditsAmount(int amount, GameUtils.CreditsSource source, DataController.SAVEPRIORITY priority)
    {
        currencyData.credits += amount;
        if (amount < 0)
        {
            // remember how much credits were spent
            currencyData.creditsSpent += -amount;


#if UNITY_ANDROID
            if (PlayGamesPlatform.Instance.IsAuthenticated())
            {
                if (currencyData.creditsSpent >= GameUtils.GOOGLE_ACHIEVEMENTS_INVESTOR)
                {
                    Social.ReportProgress(GPGSConstants.achievement_investor, 100.0f, (bool success) =>
                    {
                        // handle success or failure
                        Debug.Log("Unlocked Investor Achievement");
                    });
                }
            }
#elif UNITY_IOS
            if (Social.localUser.authenticated)
            {
                if (currencyData.creditsSpent >= GameUtils.GOOGLE_ACHIEVEMENTS_INVESTOR)
                {
                    Social.ReportProgress(GCConstants.achievement_investor, 100.0f, (bool success) =>
                    {
                        // handle success or failure
                        Debug.Log("Unlocked Investor Achievement");
                    });
                }
            }
#endif
        }

        SaveCurrencies(priority);
        RefreshCreditsUI();
    }

    public void ModifyGoldAmount(int amount, GameUtils.GoldSource source, DataController.SAVEPRIORITY priority)
    {
        currencyData.gold += amount;
        SaveCurrencies(priority);
        RefreshGoldUI();
    }

#if UNITY_EDITOR || DEBUG_MOBILE
    private void DebugMaxCurrencies()
    {
        ModifyCreditsAmount(999999, GameUtils.CreditsSource.Debug, DataController.SAVEPRIORITY.NORMAL);
        ModifyGoldAmount(999999, GameUtils.GoldSource.Debug, DataController.SAVEPRIORITY.NORMAL);
    }
#endif

    public bool HasEnoughCurrency(int currencyNeeded, GameUtils.ECurrencyType currencyType)
    {
        int currencyAmount = 0;

        if (currencyType == GameUtils.ECurrencyType.Credit)
        {
            currencyAmount = Currency.credits;
        }
        else if (currencyType == GameUtils.ECurrencyType.Gold)
        {
            currencyAmount = Currency.gold;           
        }

        if (currencyAmount >= currencyNeeded)
        {
            return true;
        }
        else
        {
            // Show popup
            MenuUIController.Instance.ShowNotEnoughCurrencyPopup((currencyNeeded - currencyAmount), currencyType);

            return false;
        }
    }

#region Data Handling
    public void SaveCurrencies(DataController.SAVEPRIORITY priority)
    {
        DataController.Instance.SaveCurrencyData(priority);
    }

    public void LoadCurrencies(CurrencyData remoteCurrency)
    {
        if (remoteCurrency == null)
        {
            System.Exception ex = JsonUtils<CurrencyData>.Load(GameUtils.CURRENCY_JSON, out currencyData, GameUtils.ELoadType.DynamicData);
            if (ex != null
                && ex.GetType() == typeof(FileLoadException))
            {
                ResetCurrencies();
            }
        }
        else
        {
            currencyData = remoteCurrency;
        }
    }

    public void ResetCurrencies()
    {
        // Note :: Needs to be initialized otherwise it crashes
        currencyData = new CurrencyData();
        currencyData.credits = 0;
        currencyData.gold = 0;

        SaveCurrencies(DataController.SAVEPRIORITY.QUEUE);
    }
#endregion
}
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Linq;
using System.IO;
using Newtonsoft.Json;
using UnityEngine.SceneManagement;
using System;
#if UNITY_ANDROID
using GooglePlayGames;
#endif

#pragma warning disable 0414

// REFACTOR :: Move some specific weapon code into WeaponObject
public class WeaponController : UnitySingleton<WeaponController>
{
    // List of weapons data coming from the resources folder
    private WeaponData[] weaponsData;
    private bool loaded = false;
    private bool pressed = false;
    private bool canFire = false;
    private float weapon_click_timer;
    private float weapon_pressed_timer;

    private const float WEAPON_UNSELECTED_ALPHA = 0.70f;
    private const float WEAPON_SELECTED_ALPHA = 1f;
    private const float WEAPON_COOLDOWN_ALPHA = 0.5f;
    private const float QUICK_AMMO_REGEN_DURATION = 1f;
    private const float AMMO_TEXT_QUICK_REGEN_SIZE = 1.3f;
    private const float WEAPON_CLICK_TIME = 0.6f;
    private const float WEAPON_PRESSED_SHOWUP_TIMER = 0.2f;

    #region Exposed Values
    [Tooltip("How many starting ammo amount to give when using the reload consumable.")]
    [SerializeField]
    private int ammoCountMultiplierForReload = 2;
    
    [Tooltip("How long before the weapon AI tries to find a new target.")]
    [SerializeField]
    private float weaponAIUpdateTargetDelay = 0.5f;
    
    [Tooltip("Lock-on duration for the weapon AI before shooting.")]
    [SerializeField]
    private float weaponAILockOnDuration = 2f;

    [Tooltip("Delay between weapon AI shots.")]
    [SerializeField]
    private float weaponAIShotDelay = 0.5f;

    [Tooltip("Alpha value of the enemy needed for weapon AI to shot.")]
    [SerializeField]
    private float weaponAIEnemyAlphaThreshold = 0.75f;

    [Tooltip("Multiplier to tweak the weapon AI range.")]
    [SerializeField]
    private float weaponAIRangeMultiplier = 2f;

    [Tooltip("Weight value divider when a weapon is specialized vs air/ground units.")]
    [SerializeField]
    private float weaponAIDamageSpecializationDivider = 3f;

    [Tooltip("Weight value divider when a weapon is being targeted by an enemy unit.")]
    [SerializeField]
    private float weaponAIBeingTargetedDivider = 3f;

    [Tooltip("The distance compare to where the weapon is vs where the closest enemy is.")]
    [SerializeField]
    private float weaponSelectionVsFireDistance = 100f;
    #endregion

    // active weapons
    private List<WeaponPositionObject> weaponPositions;
    private Dictionary<GameUtils.BaseID, WeaponObject> activeWeapons;

    private Dictionary<GameUtils.BaseID, float> weaponRegenerations;
    private Dictionary<GameUtils.BaseID, float> weaponRegenerationTimeRatioTrackers;      // To track the current regen time
    private Dictionary<GameUtils.BaseID, int> weaponQuickRegenerationAmountTrackers;      // To track the amount of ammo during the quick regen
    private Dictionary<GameUtils.BaseID, float> weaponCooldowns;
    private Dictionary<GameUtils.BaseID, float> weaponSelectionCooldowns;

    private static List<GameUtils.BaseID> unlockedWeapons;

    private int weaponLvlLimit;

    // pooling for weapon trails
    public Queue<GameObject> inactiveWeaponTrails;

    // QUESTION :: Why are enemy trails in WeaponController?
    public Queue<GameObject> inactiveBonusTrails;
    public Queue<GameObject> inactiveEnemyTrails;

    private Dictionary<GameUtils.BaseID, Queue<GameObject>> inactiveProjectiles = new Dictionary<GameUtils.BaseID, Queue<GameObject>>();
    private Queue<GameObject> inactiveExplosions = new Queue<GameObject>();  
    private Queue<GameObject> inactiveTargetMarkers = new Queue<GameObject>();
    private Queue<GameObject> inactiveTargetAIMarkers = new Queue<GameObject>();
    private GameObject projectileContainer;
    private GameObject explosionContainer;
    private GameObject targetMarkerContainer;
    private GameObject targetAIMarkerContainer;
    private GameObject weaponContainer;

    private GameObject targetPressedMarker;

    [HideInInspector]
    public List<GameUtils.BaseID> UnlockedWeapons
    {
        get { return unlockedWeapons; }
        set { unlockedWeapons = value; }
    }

    // Which weapon is currently used
    private GameUtils.BaseID selectedWeaponID;
    
    private GameObject weaponSelectionPanel;
    private RectTransform weaponSelectionRect;
    private RectTransform weaponSelectionLabelRect;

    private bool weaponPositionsInitialized;

    public Dictionary<GameUtils.BaseID, WeaponObject> ActiveWeapons
    {
        get { return activeWeapons; }
    }

    public GameUtils.BaseID SelectedWeaponID
    {
        get { return selectedWeaponID; }
        set
        {
            if (selectedWeaponID == value)
                return;

            GameUtils.BaseID lastSelectedWeaponID = selectedWeaponID;

            // Unselect the last weapon
            if (selectedWeaponID != GameUtils.BaseID.NONE
                && activeWeapons.ContainsKey(selectedWeaponID))
            {
                SetWeaponAlpha(activeWeapons[selectedWeaponID], WEAPON_UNSELECTED_ALPHA);

                // Play the weapon unselection anim
                GameUtils.GetChildGameObject(activeWeapons[selectedWeaponID].gameObject, "WeaponButton", true).GetComponent<Animator>().SetTrigger("WeaponUnselect");                
            }

            selectedWeaponID = value;

            // Select the current weapon
            if (selectedWeaponID != GameUtils.BaseID.NONE)
            {
                SetWeaponAlpha(activeWeapons[selectedWeaponID], WEAPON_SELECTED_ALPHA);

                // Play the weapon selection anim
                GameUtils.GetChildGameObject(activeWeapons[selectedWeaponID].gameObject, "WeaponButton", true).GetComponent<Animator>().SetTrigger("WeaponSelect");

                // Don't play a sound if it's the weapon placement step
                if (lastSelectedWeaponID != GameUtils.BaseID.NONE)
                {
                    // Sound hook - Weapon select
                    AudioManager.Instance.PlaySound(AudioManager.ESoundType.WeaponSelect);
                }
            }                
        }
    }

    public GameObject WeaponSelectionPanel
    {
        get { return weaponSelectionPanel; }
    }

#if ENABLE_WEAPON_AI
    public float WeaponAIUpdateTargetDelay
    {
        get { return weaponAIUpdateTargetDelay; }
    }
    
    public float WeaponAIDamageSpecializationDivider
    {
        get { return weaponAIDamageSpecializationDivider; }
    }

    public float WeaponAILockOnDuration
    {
        get { return weaponAILockOnDuration; }
    }

    public float WeaponAIBeingTargetedDivider
    {
        get { return weaponAIBeingTargetedDivider; }
    }

    public float WeaponAIShotDelay
    {
        get { return weaponAIShotDelay; }
    }

    public float WeaponAIEnemyAlphaThreshold
    {
        get { return weaponAIEnemyAlphaThreshold; }
    }
#endif

    public override void Awake()
    {
        base.Awake();

        if (FlowStateController.Instance.IsInBattleScene())
        {
            projectileContainer = new GameObject("Projectiles");
            explosionContainer = new GameObject("Explosions");
            targetMarkerContainer = new GameObject("TargetMarkers");
            targetAIMarkerContainer = new GameObject("TargetAIMarkers");
            weaponContainer = new GameObject("Weapons");
            GameObject mask = GameObject.Find("RadarScanMask");
            explosionContainer.transform.parent = mask.transform;
            targetMarkerContainer.transform.parent = mask.transform;
            targetAIMarkerContainer.transform.parent = mask.transform;
            weaponContainer.transform.parent = mask.transform;

            RegisterDependency(ChallengeController.Instance);

            targetPressedMarker = (GameObject)Instantiate(Resources.Load("TargetPressedMarker"));
            targetPressedMarker.transform.parent = targetMarkerContainer.transform;
            targetPressedMarker.SetActive(false);
            weapon_click_timer = 0;
            weapon_pressed_timer = 0;
        }

        PopulateWeaponsData();

        weaponPositions = new List<WeaponPositionObject>();
        activeWeapons = new Dictionary<GameUtils.BaseID, WeaponObject>();

        weaponRegenerations = new Dictionary<GameUtils.BaseID, float>()
            {
                { GameUtils.BaseID.MISSILE, 0 },
                { GameUtils.BaseID.ARTILLERY, 0 },
                { GameUtils.BaseID.LASER, 0 },
                { GameUtils.BaseID.RAILGUN, 0 },
                { GameUtils.BaseID.ROCKETS, 0 },
                { GameUtils.BaseID.FLAK, 0 },
            };

        weaponRegenerationTimeRatioTrackers = new Dictionary<GameUtils.BaseID, float>()
            {
                { GameUtils.BaseID.MISSILE, 0 },
                { GameUtils.BaseID.ARTILLERY, 0 },
                { GameUtils.BaseID.LASER, 0 },
                { GameUtils.BaseID.RAILGUN, 0 },
                { GameUtils.BaseID.ROCKETS, 0 },
                { GameUtils.BaseID.FLAK, 0 },
            };

        weaponQuickRegenerationAmountTrackers = new Dictionary<GameUtils.BaseID, int>()
            {
                { GameUtils.BaseID.MISSILE, 0 },
                { GameUtils.BaseID.ARTILLERY, 0 },
                { GameUtils.BaseID.LASER, 0 },
                { GameUtils.BaseID.RAILGUN, 0 },
                { GameUtils.BaseID.ROCKETS, 0 },
                { GameUtils.BaseID.FLAK, 0 },
            };

        weaponCooldowns = new Dictionary<GameUtils.BaseID, float>()
            {
                { GameUtils.BaseID.MISSILE, 0 },
                { GameUtils.BaseID.ARTILLERY, 0 },
                { GameUtils.BaseID.LASER, 0 },
                { GameUtils.BaseID.RAILGUN, 0 },
                { GameUtils.BaseID.ROCKETS, 0 },
                { GameUtils.BaseID.FLAK, 0 },
            };

        weaponSelectionCooldowns = new Dictionary<GameUtils.BaseID, float>()
            {
                { GameUtils.BaseID.MISSILE, 0 },
                { GameUtils.BaseID.ARTILLERY, 0 },
                { GameUtils.BaseID.LASER, 0 },
                { GameUtils.BaseID.RAILGUN, 0 },
                { GameUtils.BaseID.ROCKETS, 0 },
                { GameUtils.BaseID.FLAK, 0 },
            };

        inactiveWeaponTrails = new Queue<GameObject>();
        inactiveBonusTrails = new Queue<GameObject>();
        inactiveEnemyTrails = new Queue<GameObject>();

        SelectedWeaponID = GameUtils.BaseID.NONE;
        weaponPositionsInitialized = false;
    }

    public bool WeaponsPlaced()
    {
        return weaponPositions.Count == 0 && activeWeapons.Count > 0;
    }

    public override bool Update()
    {
        if (!base.Update()) return false;
        if (DataController.Instance.WeaponDataLoaded && !loaded)
        {
            loaded = true;
            
            // HACK :: Now using the weapon controller for the menu scene
            // which doesn't contain a wave controller.
            // SOLUTION :: Create a WeaponDataController and rename this one WeaponBattleController
            // Could also create a WeaponMenuController to ease out the MenuUIController.
            if (FlowStateController.Instance.IsInBattleScene())
            {
                RegisterDependency(WaveController.Instance);

                // Assigning the weapon lvl limit if it's a Last Stand only
                // Safeguard
                if (ChallengeController.Instance.GetCurrentChallenge() != null
                    && ChallengeController.Instance.GetCurrentChallenge().GetChallengeType() == ChallengeData.EChallengeType.LastStand)
                {
                    weaponLvlLimit = ChallengeController.Instance.GetCurrentChallengeData().weaponLvlLimit;
                }
                else
                {
                    weaponLvlLimit = -1;
                }
            }
            RegisterDependency(UpgradeController.Instance);
        }
        if (!loaded) return false;

        // HACK :: Now using the weapon controller for the menu scene
        // which doesn't contain a wave controller.
        // SOLUTION :: Create a WeaponDataController and rename this one WeaponBattleController
        // Could also create a WeaponMenuController to ease out the MenuUIController.
        if (FlowStateController.Instance.IsInBattleScene())
        {
            if (WaveController.Instance.Paused)
                return false;

            // don't start the game before weapons are placed
            GetAllWeaponPositions();
            // place weapon selection
            UpdateWeaponSelection();
            // weapon targeting
            UpdateWeaponClicked();
            // weapon cooldowns
            UpdateWeaponTimers();
        }        

        return true;
    }

    private void UpdateWeaponClicked()
    {
        if (SelectedWeaponID != GameUtils.BaseID.NONE)
        {
            if (SettingsController.Instance.GetDragAndRelease())
            {
                // If holding
                if (Input.GetMouseButtonDown(0) ||              // when mouse down OR
                    (pressed && !Input.GetMouseButtonUp(0)))    // when pressed and not mouse up
                {
                    if (!pressed)
                    {
                        weapon_pressed_timer = WEAPON_PRESSED_SHOWUP_TIMER;
                        pressed = true;
                    }
                    canFire = ShowWeaponPressed(SelectedWeaponID);
                }
                // if released
                else if (Input.GetMouseButtonUp(0) && canFire && pressed) // shoot when all conditions are ok
                {
                    pressed = false;
                    canFire = false;
                    PlayerFireWeapon(SelectedWeaponID);
                }
                else if (Input.GetMouseButtonUp(0)) // reset if the mouse has been released and not shot
                {
                    pressed = false;
                    canFire = false;
                }
            }
            else
            {
                if (Input.GetMouseButtonUp(0))
                {
                    PlayerFireWeapon(SelectedWeaponID);
                }
            }
        }
    }

    private bool CheckAboveUIObject()
    {
//#if UNITY_EDITOR
        // If clicked on a UI element 
        // in unity editor
        if (UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
        {
            return true;
        }
/*#else
        // on mobile devices
        Touch[] touches = Input.touches;
        foreach (Touch touch in touches)
        {
            if (UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject(touch.fingerId))
            {
                return true;
            }
        }
#endif*/
        return false;
    }

    private bool IsWeaponDead(GameUtils.BaseID weaponID)
    {
        if (activeWeapons.ContainsKey(weaponID))
        {
            if (activeWeapons[weaponID].Health > 0)
            {
                return false;
            }
        }
        return true;
    }

    private void UpdateWeaponSelection()
    {
        if (!AllWeaponPositionsScaled())
            return;

        // If there's only one weapon position
        // and one weapon unlocked, place it immediately
        if (IsAutomaticWeaponPlacement())
        {
            WeaponSelectionPressed(GameUtils.BaseID.ARTILLERY);
            return;            
        }

        if (weaponPositions.Count > 0)
        {
            // play animation
            Animator anim = weaponPositions[0].GetComponent<Animator>();
            if (!anim.GetCurrentAnimatorStateInfo(0).IsName("ActivePosition"))
            {
                anim.Play("ActivePosition", 0, 0);
            }

            // Enable weapon selection
            if (!weaponSelectionPanel.activeInHierarchy)
            {
                weaponSelectionPanel.SetActive(true);
            }

            // move weapon selection to weapon position
            MoveWeaponSelectionPanel();
            return;
        }
        else
        {
            // disable weapon selection when all weapons placed
            if (weaponSelectionPanel.activeInHierarchy)
            {
                weaponSelectionPanel.SetActive(false);
            }
        }
    }

    private bool AllWeaponPositionsScaled()
    {
        for (int i = 0; i < weaponPositions.Count; ++i)
        {
            if (!weaponPositions[i].GetComponent<ScaleObject>().IsScaled) return false;
        }
        return true;
    }

    public void RegisterWeapon(GameUtils.BaseID baseID, GameObject weapon, UnityEngine.Events.UnityAction functionPointer )
    {
        WeaponObject weaponObject = weapon.GetComponent<WeaponObject>();

        // Enable the weapon now
        weaponObject.SetVisibility(true);

        // Init the weapon from the weapon data
        weaponObject.InitWeaponData(FindWeaponData(baseID));

        // add to active list
        activeWeapons.Add(baseID, weaponObject);

        // add button listeners
        Button weaponButton = weapon.GetComponentInChildren<Button>();
        weaponButton.onClick.AddListener(functionPointer);
        
        // To refresh ammo text
        AddAmmo(baseID, 0);

        // Set the regen
        weaponRegenerations[baseID] = GetWeaponInGameRegeneration(baseID);

        // Set the rank icon
        Image weaponRankIcon = GameUtils.GetChildGameObject(weapon, "WeaponRankIcon").GetComponent<Image>();
        weaponRankIcon.sprite = UpgradeController.Instance.GetWeaponRankIcon(baseID, weaponLvlLimit);

        // Select the last weapon placed by default
        if (WeaponsPlaced())
        {
            SelectWeapon(baseID);
        }
    }
    
    public void UnregisterWeapon(GameUtils.BaseID baseID)
    {
        // remove button listeners
        Button weaponButton = activeWeapons[baseID].gameObject.GetComponentInChildren<Button>();
        weaponButton.onClick.RemoveAllListeners();

        // Remove from the active list
        activeWeapons.Remove(baseID);

        // select the first weapon that is still active
        if (activeWeapons.Count > 0)
        {
            SelectWeapon(activeWeapons.ElementAt(0).Key);
        }
    }
    
    public void SelectWeapon(GameUtils.BaseID weaponID)
    {
        if (weaponSelectionCooldowns[weaponID] <= 0)
        {
            SelectedWeaponID = weaponID;
            weaponSelectionCooldowns[weaponID] = GameUtils.WEAPON_SELECTION_COOLDOWN;
        }
    }

    public bool IsWeaponUnlocked(GameUtils.BaseID weaponID)
    {
        if (unlockedWeapons.Contains(weaponID))
        {
            return true;
        }
        return false;
    }

    public void UnlockWeapon(GameUtils.BaseID weaponID, bool showPopup = true)
    {
        // If already unlocked
        if (unlockedWeapons.Contains(weaponID))
            return;
        
        if (showPopup)
        {
            // Show unlock popup
            MenuUIController.Instance.ShowUnlockedWeaponPopup(weaponID, FindWeaponData(weaponID).unlockLvl);
        }        

        unlockedWeapons.Add(weaponID);
        SaveWeaponController(DataController.SAVEPRIORITY.QUEUE);

#if UNITY_ANDROID
        if (PlayGamesPlatform.Instance.IsAuthenticated())
        {
            Social.ReportProgress(GPGSConstants.achievement_fire_power, 100.0f, (bool success) =>
            {
                // handle success or failure
                Debug.Log("Unlocked FirePower Achievement");
            });

            if (unlockedWeapons.Count >= (int)GameUtils.BaseID.CITY)
            {
                Social.ReportProgress(GPGSConstants.achievement_arsenal, 100.0f, (bool success) =>
                {
                    // handle success or failure
                    Debug.Log("Unlocked Arsenal Achievement");
                });
            }
        }
#elif UNITY_IOS
        if (Social.localUser.authenticated)
        {
            Social.ReportProgress(GCConstants.achievement_fire_power, 100.0f, (bool success) =>
            {
                // handle success or failure
                Debug.Log("Unlocked FirePower Achievement");
            });

            if (unlockedWeapons.Count >= (int)GameUtils.BaseID.CITY)
            {
                Social.ReportProgress(GCConstants.achievement_arsenal, 100.0f, (bool success) =>
                {
                    // handle success or failure
                    Debug.Log("Unlocked Arsenal Achievement");
                });
            }
        }
#endif
    }

    public void UnlockWeaponByLevel(int level)
    {
        if (level >= FindWeaponData(GameUtils.BaseID.ARTILLERY).unlockLvl)
        {
            // Make sure Artillery is unlocked
            UnlockWeapon(GameUtils.BaseID.ARTILLERY);
        }
        if (level >= FindWeaponData(GameUtils.BaseID.FLAK).unlockLvl)
        {
            // Make sure Flak is unlocked
            UnlockWeapon(GameUtils.BaseID.FLAK);
        }
        if (level >= FindWeaponData(GameUtils.BaseID.MISSILE).unlockLvl)
        {
            // Make sure Missile is unlocked
            UnlockWeapon(GameUtils.BaseID.MISSILE);
        }
        if (level >= FindWeaponData(GameUtils.BaseID.LASER).unlockLvl)
        {
            // Make sure Laser is unlocked
            UnlockWeapon(GameUtils.BaseID.LASER);
        }
        if (level >= FindWeaponData(GameUtils.BaseID.RAILGUN).unlockLvl)
        {
            // Make sure Railgun is unlocked
            UnlockWeapon(GameUtils.BaseID.RAILGUN);
        }
        if (level >= FindWeaponData(GameUtils.BaseID.ROCKETS).unlockLvl)
        {
            // Make sure Rockets is unlocked
            UnlockWeapon(GameUtils.BaseID.ROCKETS);
        }
    }

    public void UnlockStandardWeaponByLevel(int level)
    {
        // Note :: For the standard weapons only.

        if (level >= FindWeaponData(GameUtils.BaseID.ARTILLERY).unlockLvl)
        {
            // Make sure Artillery is unlocked
            UnlockWeapon(GameUtils.BaseID.ARTILLERY);
        }
        if (level >= FindWeaponData(GameUtils.BaseID.FLAK).unlockLvl)
        {
            // Make sure Flak is unlocked
            UnlockWeapon(GameUtils.BaseID.FLAK);
        }
        if (level >= FindWeaponData(GameUtils.BaseID.MISSILE).unlockLvl)
        {
            // Make sure Missile is unlocked
            UnlockWeapon(GameUtils.BaseID.MISSILE);
        }
    }

    public void ShowUnlockAdvancedWeaponByLevel(int level)
    {
        if (level >= FindWeaponData(GameUtils.BaseID.LASER).unlockLvl)
        {
            MenuUIController.Instance.ShowUnlockedWeaponPopup(GameUtils.BaseID.LASER, FindWeaponData(GameUtils.BaseID.LASER).unlockLvl);
        }
        if (level >= FindWeaponData(GameUtils.BaseID.RAILGUN).unlockLvl)
        {
            MenuUIController.Instance.ShowUnlockedWeaponPopup(GameUtils.BaseID.RAILGUN, FindWeaponData(GameUtils.BaseID.RAILGUN).unlockLvl);
        }
        if (level >= FindWeaponData(GameUtils.BaseID.ROCKETS).unlockLvl)
        {
            MenuUIController.Instance.ShowUnlockedWeaponPopup(GameUtils.BaseID.ROCKETS, FindWeaponData(GameUtils.BaseID.ROCKETS).unlockLvl);
        }
    }
    
    private void GetAllWeaponPositions()
    {
        if (weaponPositionsInitialized) return;

        weaponPositionsInitialized = true;

        weaponPositions.AddRange(FindObjectsOfType<WeaponPositionObject>());
        for(int i = 0; i < weaponPositions.Count; ++i)
        {
            GameObject weaponPositionGO = GameUtils.GetChildGameObject(weaponPositions[i].gameObject, "BaseName", true);
            weaponPositionGO.GetComponent<Text>().text = LocalizationManager.Instance.GetLocalizedString(LocalizationManager.WEAPON_POSITION_NAME) + (i + 1).ToString();
        }

        GameObject battleUI = GameObject.Find("BattleUICanvas");
        weaponSelectionPanel = GameUtils.GetChildGameObject(battleUI, "WeaponSelectionPanel", true);
        weaponSelectionRect = weaponSelectionPanel.GetComponent<RectTransform>();
        GameObject label = GameUtils.GetChildGameObject(weaponSelectionPanel, "WeaponSelectLabel", true);
        weaponSelectionLabelRect = label.GetComponent<RectTransform>();
        weaponSelectionPanel.SetActive(false);

        UpdateWeaponSelectionPanel(true);
    }

    private bool IsAutomaticWeaponPlacement()
    {
        // If there's only one weapon position and one unlocked weapon
        return weaponPositions.Count == 1 && unlockedWeapons.Count == 1;
    }

    private void UpdateWeaponSelectionPanel(bool init)
    {
        int unlockedWeaponsCount = 0;
        float buttonWidth = 150f;
        Button[] weaponButton = weaponSelectionPanel.GetComponentsInChildren<Button>();
        for (int i = 0; i < weaponButton.Length; ++i)
        {
            GameUtils.BaseID weaponID = (GameUtils.BaseID)System.Enum.Parse(typeof(GameUtils.BaseID), weaponButton[i].name);
            
            if (IsWeaponUnlocked(weaponID) && weaponButton[i].IsInteractable())
            {
                if (init)
                {
                    weaponButton[i].onClick.AddListener(delegate { WeaponSelectionPressed(weaponID); });
                }

                // Show the weapon rank
                GameUtils.GetChildGameObject(weaponButton[i].gameObject, "IconWeaponRank").GetComponent<Image>().sprite = UpgradeController.Instance.GetWeaponRankIcon(weaponID, weaponLvlLimit);
                GameUtils.GetChildGameObject(weaponButton[i].gameObject, "TextWeaponRank").GetComponent<Text>().text = " Lv. " + UpgradeController.Instance.GetWeaponRankLevel(weaponID, weaponLvlLimit);

                // Move the button to the left
                RectTransform buttonRect = weaponButton[i].GetComponent<RectTransform>();
                Vector3 position = buttonRect.anchoredPosition;
                buttonWidth = buttonRect.rect.width;
                position.x = (buttonWidth * unlockedWeaponsCount) + 90;
                buttonRect.anchoredPosition = position;
                unlockedWeaponsCount++;
            }
            else
            {
                weaponButton[i].gameObject.SetActive(false);
            }
        }

        RectTransform rectTransform = weaponSelectionPanel.GetComponent<RectTransform>();
        Vector2 size = rectTransform.sizeDelta;
        size.x = (buttonWidth * unlockedWeaponsCount) + 40;
        rectTransform.sizeDelta = size;
    }

    private void WeaponSelectionPressed(GameUtils.BaseID weaponID)
    {
        // Spawn the weapon at the right position
        GameObject weaponPrefab = (GameObject)Instantiate(Resources.Load("WeaponPrefab"), weaponPositions[0].transform.position, Quaternion.identity);
        weaponPrefab.name = weaponID.ToString().ToUpper();
        weaponPrefab.transform.parent = weaponContainer.transform;
        Canvas weaponCanvas = GameUtils.GetChildGameObject(weaponPrefab, "WeaponCanvas", true).GetComponent<Canvas>();
        // to make it clickable again
        weaponCanvas.overrideSorting = true;
        weaponCanvas.sortingOrder = 11;

        // Important to set the weapon ID immediately
        WeaponObject weaponObject = weaponPrefab.GetComponent<WeaponObject>();
        weaponObject.BaseID = weaponID;
        weaponObject.Node = weaponPositions[0].Node;

        // Hide now since the weapon data is coming one frame after
        // Note :: This is to avoid a one frame visual glitch
        // where the weapon is not all set.
        weaponObject.SetVisibility(false);

        // Disable the weapon position game object
        weaponPositions[0].gameObject.SetActive(false);
        weaponPositions.RemoveAt(0);

        // Disable the selected weapon in the weapon selection panel
        Button weaponButton = GameUtils.GetChildGameObject(weaponSelectionPanel, weaponID.ToString()).GetComponent<Button>();
        weaponButton.onClick.RemoveAllListeners();
        weaponButton.interactable = false;
        UpdateWeaponSelectionPanel(false);

        // Set the alpha to unselect
        SetWeaponAlpha(weaponObject, WEAPON_UNSELECTED_ALPHA);
    }
    
    private void SetWeaponAlpha(WeaponObject weaponObject, float alphaValue)
    {           
        Color color1 = weaponObject.WeaponImageTop.material.color;
        color1.a = alphaValue;
        weaponObject.WeaponImageTop.material.color = color1;
        
        Color color2 = weaponObject.WeaponImageBottom.material.color;
        color2.a = alphaValue;
        weaponObject.WeaponImageBottom.material.color = color2;
    }

    private void MoveWeaponSelectionPanel()
    {
        if (weaponPositions.Count > 0)
        {
            Vector3 position = weaponPositions[0].transform.position;
            float heightHalf = ((PlayfieldController.Instance.TopRight.y - PlayfieldController.Instance.BottomLeft.y) / 2f) + PlayfieldController.Instance.BottomLeft.y;
            float labelHeightHalf = weaponSelectionLabelRect.rect.height / 2f * PlayfieldController.Instance.HeightScale;
           
            float panelHeightHalfNormal = weaponSelectionRect.rect.height / 2f;
            float panelWidthHalfNormal = weaponSelectionRect.rect.width / 2f;
            float panelHeightHalfScaled = panelHeightHalfNormal * PlayfieldController.Instance.HeightScale + labelHeightHalf;
            float panelWidthHalfScaled = panelWidthHalfNormal * PlayfieldController.Instance.WidthScale;

            if (position.y > heightHalf)
            {
                position -= new Vector3(0f, panelHeightHalfScaled, 0f);
                weaponSelectionLabelRect.anchoredPosition = new Vector3(0, -panelHeightHalfNormal - labelHeightHalf, 0);
            }
            else
            {
                position += new Vector3(0f, panelHeightHalfScaled, 0f);
                weaponSelectionLabelRect.anchoredPosition = new Vector3(0, panelHeightHalfNormal + labelHeightHalf, 0);
            }

            if (position.x - panelWidthHalfScaled < PlayfieldController.Instance.BottomLeft.x)
            {
                position.x += PlayfieldController.Instance.BottomLeft.x - (position.x - panelWidthHalfScaled);
            }
            else if (position.x + panelWidthHalfScaled > PlayfieldController.Instance.TopRight.x)
            {
                position.x += PlayfieldController.Instance.TopRight.x - (position.x + panelWidthHalfScaled);
            }
            weaponSelectionPanel.transform.position = position;
        }
    }

    private bool CheckCanFireWeaponConditions(GameUtils.BaseID weaponID, Vector3 pos)
    {
        // make sure we are not trying to click UI
        if (CheckAboveUIObject())
            return false;

        // check if the weapon is available
        if (!activeWeapons.ContainsKey(weaponID))
            return false;

        // make sure the weapon is not dead
        if (IsWeaponDead(weaponID))
            return false;

        // check if the weapon is not on cooldown
        if (!IsWeaponReady(weaponID))
        {
            // Sound hook - Empty sound
            if (weapon_click_timer <= 0f)
            {
                AudioManager.Instance.PlaySound(activeWeapons[weaponID].EmptySound);
                weapon_click_timer = WEAPON_CLICK_TIME;
            }
            return false;
        }

        // Player needs to click inside the playfield
        if (!PlayfieldController.Instance.IsInsidePlayArea(pos))
            return false;

        // check if this is maybe a weapon selection
        if (CheckForWeaponSelection(pos))
            return false;

        // possible fire of the weapon
        return true;
    }

    private bool ShowWeaponPressed(GameUtils.BaseID weaponID)
    {
        // reduce timer
        weapon_pressed_timer -= Time.deltaTime;

        // record the click position
        Vector3 firePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        // Do not deal with depth
        firePos.z = 0;

        // check if the weapon is available
        if (!CheckCanFireWeaponConditions(weaponID, firePos))
        {
            // disable the taget marker
            targetPressedMarker.SetActive(false);
            return false;
        }

        if (weapon_pressed_timer < 0)
        {
            // show target marker
            targetPressedMarker.SetActive(true);
            targetPressedMarker.transform.position = firePos;
        }
        return true;
    }


    private void PlayerFireWeapon(GameUtils.BaseID weaponID)
    {
        targetPressedMarker.SetActive(false);

        // record the click position
        Vector3 firePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        // Do not deal with depth
        firePos.z = 0;

        // check if the weapon is available
        if (!CheckCanFireWeaponConditions(weaponID, firePos))
        {
            return;
        }

        // Now fire the weapon
        FireWeapon(weaponID, firePos);

        // Target marker
        if (weaponID != GameUtils.BaseID.LASER && weaponID != GameUtils.BaseID.MISSILE)
        {
            GameObject targetMarker = GetTargetMarkerFromPool();
            targetMarker.transform.position = firePos;
            targetMarker.GetComponent<SpawnTargetObject>().Init();
        }

        // Set cooldown
        weaponCooldowns[weaponID] = GetWeaponCooldown(weaponID);
    }

    private bool CheckForWeaponSelection(Vector3 position)
    {
        // don't do this check when in pressed status
        if (SettingsController.Instance.GetDragAndRelease() && pressed) return false;

        // max distance
        float maxDistSqr = weaponSelectionVsFireDistance * weaponSelectionVsFireDistance * PlayfieldController.Instance.WidthScale;
        float closestWeaponDistSqr = float.PositiveInfinity;
        float closestEnemyDistSqr = float.PositiveInfinity;
        GameUtils.BaseID closestWeaponIndex = GameUtils.BaseID.NONE;
        int closestEnemyIndex = -1;

        // find closest weapon
        for (int i = 0; i < activeWeapons.Count; ++i)
        {
            float distSqr = Vector3.SqrMagnitude(activeWeapons.ElementAt(i).Value.gameObject.transform.position - position);
            if  (distSqr < closestWeaponDistSqr && distSqr < maxDistSqr)
            {
                closestWeaponIndex = activeWeapons.ElementAt(i).Key;
                closestWeaponDistSqr = distSqr;
            }
        }

        // no weapon close enough, fire
        if (closestWeaponIndex == GameUtils.BaseID.NONE) return false;

        // find closest enemy
        for (int i = 0; i < EnemyController.Instance.AliveEnemies.Count; ++i)
        {
            float distSqr = Vector3.SqrMagnitude(EnemyController.Instance.AliveEnemies[i].transform.position - position);
            if (distSqr < closestEnemyDistSqr && distSqr < maxDistSqr)
            {
                closestEnemyIndex = i;
                closestWeaponDistSqr = distSqr;
            }
        }

        // no enemy near, means weapon selection
        if (closestEnemyIndex == -1 && closestWeaponIndex != selectedWeaponID)
        {
            SelectedWeaponID = closestWeaponIndex;
            return true;
        }

        // weapon is closer than enemy, means weapon selection
        if (closestWeaponDistSqr < closestEnemyIndex && closestWeaponIndex != selectedWeaponID)
        {
            SelectedWeaponID = closestWeaponIndex;
            return true;
        }

        // all other cases it's a normal fire
        return false;
    }

#if ENABLE_WEAPON_AI
    public void AIFireWeapon(GameUtils.BaseID weaponID, Vector3 firePos)
    {
        if (!activeWeapons.ContainsKey(weaponID))
            return;

        if (IsWeaponDead(weaponID))
            return;

        if (IsWeaponReady(weaponID))
        {
            // Do not deal with depth
            firePos.z = 0;

            // Safegaurd :: Fire position needs to be inside the playfield
            if (!PlayfieldController.Instance.IsInsidePlayArea(firePos))
                return;

            FireWeapon(weaponID, firePos);

            // Set cooldown
            weaponCooldowns[weaponID] = GetWeaponCooldown(weaponID);
        }
    }
#endif
    
    private void FireWeapon(GameUtils.BaseID weaponID, Vector3 firePos)
    {
        // call to tell the wave controller that the player is not idleing
        WaveController.Instance.FiredWeapon();
         
        WeaponObject weaponObject = activeWeapons[weaponID];
        
        // Perform a rotation on the weapon
        RotateWeapon(weaponID, firePos);
        
        // we need to fire 5 rockets, other weapons only do 1
        int count = weaponID == GameUtils.BaseID.ROCKETS ? GameUtils.ROCKETS_COUNT : 1;

        // create the shells/rockets/missiles/etc
        for (int i = 0; i < count; ++i)
        {
            GameObject weapon = GetProjectileFromPool(weaponID);
            MovingWeaponObject movingObject = weapon.GetComponent<MovingWeaponObject>();
            movingObject.WeaponID = weaponID;

            // positions
            movingObject.TargetPosition = SpreadPosition(weaponID, firePos, i);
            Vector3 startPosition = BaseController.Instance.Bases[weaponID].transform.position;
            if (weaponID == GameUtils.BaseID.ROCKETS)
            {
                Vector3 direction = (movingObject.TargetPosition - startPosition).normalized;
                Vector3 right = Quaternion.AngleAxis(90f, Vector3.forward) * direction;
                startPosition += right * (-15f + (i * 7.5f)) * PlayfieldController.Instance.WidthScale;
            }
            movingObject.StartPosition = startPosition;

            // stats
            movingObject.SetSpeed = GetWeaponInGameSpeed(weaponID);
            movingObject.GroundPower = GetWeaponInGameGroundDamage(weaponID);
            movingObject.AirPower = GetWeaponInGameAirDamage(weaponID);
            movingObject.Radius = GetWeaponInGameExplosionRadius(weaponID);

            // initialize
            movingObject.Init();
        }

        // reduce the ammo of the weapon
        AddAmmo(weaponID, -1);
                
        // Sound hook - Fire sound
        AudioManager.Instance.PlaySound(weaponObject.FireSound);

        AchievementController.Instance.UpdateAchievementProgression(AchievementData.EAchievementType.Shots, 1, AchievementData.EAchievementOverride.Progress);
    }

    public void RotateWeapon(GameUtils.BaseID weaponID, Vector3 firePos)
    {
        firePos.z = 0;
        
        // Rotate the weapon
        activeWeapons[weaponID].WeaponImageTop.transform.rotation = Quaternion.LookRotation(Vector3.forward, firePos - activeWeapons[weaponID].WeaponImageTop.transform.position);
    }

    private Vector3 SpreadPosition(GameUtils.BaseID weaponID, Vector3 position, int shellNumber)
    {
        // first shell goes to target
        if (shellNumber == 0) return position;

        // other shells need adjustment
        Vector3 newPosition = position;
        // try 5 times max to find a random position that is valid
        for (int i = 0; i < 5; ++i)
        {
            // max distance from original position
            float radius = UnityEngine.Random.Range(GameUtils.ROCKETS_AREA_RADIUS * GameUtils.RADIUS_FACTOR * PlayfieldController.Instance.WidthScale / 4f, GameUtils.ROCKETS_AREA_RADIUS * GameUtils.RADIUS_FACTOR * PlayfieldController.Instance.WidthScale);
            float angle = UnityEngine.Random.Range(0f, 360f);
            Vector3 adjust = Quaternion.AngleAxis(angle, Vector3.forward) * Vector3.up * radius;
            newPosition += adjust;

            if (PlayfieldController.Instance.IsInsidePlayArea(newPosition))
            {
                // fpund valid position
                break;
            }
            else
            {
                // try again or use target position
                newPosition = position;
            }
        }
        return newPosition;
    }

    public void PutProjectileInPool(GameUtils.BaseID weaponID, GameObject projectile)
    {
        projectile.SetActive(false);

        if (!inactiveProjectiles.ContainsKey(weaponID))
        {
            inactiveProjectiles.Add(weaponID, new Queue<GameObject>());
        }

        inactiveProjectiles[weaponID].Enqueue(projectile);
    }

    // returns a new object or an object from the pool
    public GameObject GetProjectileFromPool(GameUtils.BaseID weaponID)
    {
        GameObject projectile;
        if (inactiveProjectiles.ContainsKey(weaponID))
        {
            if (inactiveProjectiles[weaponID].Count > 0)
            {
                projectile = inactiveProjectiles[weaponID].Dequeue();
                projectile.SetActive(true);
                return projectile;
            }
        }
        
        switch (weaponID)
        {
            case GameUtils.BaseID.MISSILE:
                projectile = (GameObject)Instantiate(Resources.Load("Projectiles/MissileOne")); break;
            case GameUtils.BaseID.FLAK:
                projectile = (GameObject)Instantiate(Resources.Load("Projectiles/FlakOne")); break;
            case GameUtils.BaseID.RAILGUN:
                projectile = (GameObject)Instantiate(Resources.Load("Projectiles/RailgunOne")); break;
            case GameUtils.BaseID.ROCKETS:
                projectile = (GameObject)Instantiate(Resources.Load("Projectiles/RocketOne")); break;
            case GameUtils.BaseID.LASER:
                projectile = (GameObject)Instantiate(Resources.Load("Projectiles/LaserOne")); break;
            case GameUtils.BaseID.ARTILLERY:
            default:
                projectile = (GameObject)Instantiate(Resources.Load("Projectiles/ArtilleryOne")); break;
        }

        // put it into the container
        projectile.transform.parent = projectileContainer.transform;

        return projectile;
    }

    public void PutTargetMarkerToPool(GameObject targetMarker)
    {
        targetMarker.SetActive(false);

        inactiveTargetMarkers.Enqueue(targetMarker);
    }

    public GameObject GetTargetMarkerFromPool()
    {
        GameObject targetMarker;
        if (inactiveTargetMarkers.Count > 0)
        {
            targetMarker = inactiveTargetMarkers.Dequeue();
            targetMarker.SetActive(true);
            
            return targetMarker;
        }

        targetMarker = (GameObject)Instantiate(Resources.Load("ProjectileTargetMarker"));
        targetMarker.transform.parent = targetMarkerContainer.transform;

        return targetMarker;
    }

#if ENABLE_WEAPON_AI
    public void PutTargetAIMarkerToPool(GameObject targetAIMarker)
    {
        targetAIMarker.SetActive(false);

        inactiveTargetAIMarkers.Enqueue(targetAIMarker);
    }

    public GameObject GetTargetAIMarkerFromPool()
    {
        GameObject targetAIMarker;
        if (inactiveTargetAIMarkers.Count > 0)
        {
            targetAIMarker = inactiveTargetAIMarkers.Dequeue();
            targetAIMarker.SetActive(true);
            
            return targetAIMarker;
        }

        targetAIMarker = (GameObject)Instantiate(Resources.Load("ProjectileTargetAIMarker"));
        targetAIMarker.transform.parent = targetAIMarkerContainer.transform;

        return targetAIMarker;
    }
#endif

    public void PutExplosionToPool(GameObject explosion)
    {
        explosion.SetActive(false);

        inactiveExplosions.Enqueue(explosion);
    }

    private GameObject GetExplosionFromPool()
    {
        GameObject explosion;
        if (inactiveExplosions.Count > 0)
        {
            explosion = inactiveExplosions.Dequeue();
            explosion.SetActive(true);
            return explosion;
        }

        // instantiate
        explosion = (GameObject)Instantiate(Resources.Load("Explosion"));

        // put it into the container
        explosion.transform.parent = explosionContainer.transform;

        return explosion;
    }

    private void UpdateWeaponTimers()
    {
       for (int i = 0; i < activeWeapons.Count; ++ i)
       { 
            // Only perform update on regen and cooldown when all weapons are placed
            if (WeaponsPlaced())
            {
                UpdateWeaponRegeneration(activeWeapons.ElementAt(i).Key);
                UpdateWeaponCooldown(activeWeapons.ElementAt(i).Key);
            }
        }
        weapon_click_timer -= Time.deltaTime;
    }
    
    private void UpdateWeaponRegeneration(GameUtils.BaseID weaponID)
    {
        // Do the normal regen
        if (weaponRegenerations[weaponID] > 0f)
        {
            if (activeWeapons[weaponID].CurrentAmmo == 0
                && !IsWeaponInQuickAmmoRegen(weaponID))
            {
                weaponRegenerations[weaponID] -= GameUtils.AMMO_REGEN_MULTIPLIER_AT_ZERO * Time.deltaTime;
            }
            else
            {
                weaponRegenerations[weaponID] -= Time.deltaTime;
            }            

            // If the weapon is in quick ammo regen
            // and the weapon has no more quick ammo to regen
            if (IsWeaponInQuickAmmoRegen(weaponID) && weaponQuickRegenerationAmountTrackers[weaponID] == 0)
            {
                // Wait until the regen came back to the tracked ratio
                if (GetAmmoRegenRatioTime(weaponID) <= weaponRegenerationTimeRatioTrackers[weaponID])
                {
                    // Set the regen tracked before using the reload consumable
                    weaponRegenerations[weaponID] = weaponRegenerationTimeRatioTrackers[weaponID] * GetWeaponInGameRegeneration(weaponID);

                    // Reset time tracker
                    weaponRegenerationTimeRatioTrackers[weaponID] = 0f;

                    // Set the ammo text size back to normal
                    SetAmmoTextSize(weaponID);
                }
            }
        }
        else
        {
            // Regen one ammo
            AddAmmo(weaponID, 1);
                
            if (weaponQuickRegenerationAmountTrackers[weaponID] > 0)
            {
                // Update the quick ammo regen tracker
                weaponQuickRegenerationAmountTrackers[weaponID]--;

                // Redo the quick regen timer
                weaponRegenerations[weaponID] += GetWeaponQuickRegeneration(weaponID);
            }                
            else
            {
                // Redo the normal regen timer
                weaponRegenerations[weaponID] += GetWeaponInGameRegeneration(weaponID);
            }                
        }

        SetRegenRadialBar(weaponID);
    }

    private void UpdateWeaponCooldown(GameUtils.BaseID weaponID, bool isForced = false)
    {
        // Update the reload cooldown
        if (weaponCooldowns[weaponID] > 0f
            || isForced)
        {
            // TEMP :: Missing visuals, will enable soon!
            //// If damaged, make the cooldown longer
            //if (activeWeapons[weaponID].isDamaged)
            //{
            //    weaponCooldowns[weaponID] -= (Time.deltaTime / GameUtils.DAMAGED_WEAPON_COOLDOWN_MULTIPLIER);
            //}
            //else
            {
                weaponCooldowns[weaponID] -= Time.deltaTime;
            }            

            // If still in cooldown
            if (weaponCooldowns[weaponID] > 0f)
            {
                SetWeaponAlpha(activeWeapons[weaponID], WEAPON_COOLDOWN_ALPHA);
            }
            // No more cooldown
            else
            {
                SetWeaponAlpha(activeWeapons[weaponID], weaponID == selectedWeaponID ? WEAPON_SELECTED_ALPHA : WEAPON_UNSELECTED_ALPHA);
            }            

            SetReloadRadialBar(weaponID);
        }        

        // Update the weapon selection cooldown
        if (weaponSelectionCooldowns[weaponID] > 0f)
        {
            weaponSelectionCooldowns[weaponID] -= Time.deltaTime;
        }
    }
    
    public void AddAmmo(GameUtils.BaseID weaponID, int ammoToAdd)
    {
        if (!activeWeapons.ContainsKey(weaponID)) return;
        
        activeWeapons[weaponID].CurrentAmmo += ammoToAdd;
    }

    public void PerformQuickFullAmmoRegen()
    {
        // For each weapons
        for (int i = (int)GameUtils.BaseID.MISSILE; i <= (int)GameUtils.BaseID.FLAK; ++i)
        {
            GameUtils.BaseID weaponID = (GameUtils.BaseID)i;

            // Skip all none existant weapons
            if (!activeWeapons.ContainsKey(weaponID)) continue;
            
            // If there's an active quick regen
            if (IsWeaponInQuickAmmoRegen(weaponID))
            {
                // Just add more to the amount of quick ammo regen to do
                weaponQuickRegenerationAmountTrackers[weaponID] += GetWeaponAmmoCountForReload(weaponID);

                continue;
            }

            // Track where the current ammo regen is at before setting the quick regen
            weaponRegenerationTimeRatioTrackers[weaponID] = weaponRegenerations[weaponID] / GetWeaponInGameRegeneration(weaponID);

            // Track the amount of quick ammo regen to do
            weaponQuickRegenerationAmountTrackers[weaponID] = GetWeaponAmmoCountForReload(weaponID);

            // Set a faster weapon regen
            weaponRegenerations[weaponID] = weaponRegenerationTimeRatioTrackers[weaponID] * GetWeaponQuickRegeneration(weaponID);

            // Set the ammo text size bigger
            SetAmmoTextSize(weaponID);
        }
    }
        
    private void SetReloadRadialBar(GameUtils.BaseID weaponID)
    {
        if (weaponCooldowns[weaponID] <= 0f)
        {
            // Reset fill amount if no more cooldown
            activeWeapons[weaponID].CooldownRadialBar.fillAmount = 0f;
        }
        else
        {
            activeWeapons[weaponID].CooldownRadialBar.fillAmount = 1 - (weaponCooldowns[weaponID] / GetWeaponCooldown(weaponID));
        }
    }

    private void SetRegenRadialBar(GameUtils.BaseID weaponID)
    {
        activeWeapons[weaponID].AmmoRegenRadialBar.fillAmount = 1 - GetAmmoRegenRatioTime(weaponID); 
    }

    private void SetAmmoTextSize(GameUtils.BaseID weaponID)
    {
        activeWeapons[weaponID].AmmoText.rectTransform.localScale = IsWeaponInQuickAmmoRegen(weaponID) ? new Vector2(AMMO_TEXT_QUICK_REGEN_SIZE, AMMO_TEXT_QUICK_REGEN_SIZE) : new Vector2(1f, 1f);
    }

    private bool IsWeaponInQuickAmmoRegen(GameUtils.BaseID weaponID)
    {
        return weaponRegenerationTimeRatioTrackers[weaponID] > 0f;
    }

    private float GetAmmoRegenRatioTime(GameUtils.BaseID weaponID)
    {
        if (IsWeaponInQuickAmmoRegen(weaponID))
        {
            return weaponRegenerations[weaponID] / GetWeaponQuickRegeneration(weaponID);
        }
        else
        {
            return weaponRegenerations[weaponID] / GetWeaponInGameRegeneration(weaponID);
        }
    }
    
    private bool IsWeaponReady(GameUtils.BaseID weaponID)
    {
        return !IsWeaponOutOfAmmo(weaponID) && !IsWeaponInCooldown(weaponID);
    }

    private bool IsWeaponOutOfAmmo(GameUtils.BaseID weaponID)
    {
        return activeWeapons[weaponID].CurrentAmmo <= 0;
    }

    private bool IsWeaponInCooldown(GameUtils.BaseID weaponID)
    {
        return weaponCooldowns[weaponID] > 0f;
    }

#if ENABLE_WEAPON_AI
    public bool IsEnemyAlreadyTargeted(GameObject enemyGO)
    {
        MovingEnemyObject enemyObject = enemyGO.GetComponent<MovingEnemyObject>();

        // For each weapons
        for (int i = (int)GameUtils.BaseID.MISSILE; i <= (int)GameUtils.BaseID.FLAK; ++i)
        {
            GameUtils.BaseID weaponID = (GameUtils.BaseID)i;

            // Skip all none existant weapons
            if (!activeWeapons.ContainsKey(weaponID))
                continue;

            if (activeWeapons[weaponID].CurrentEnemyTarget == enemyObject)
                return true;
        }

        return false;
    }
#endif

#region Weapon Attribute Getters
    public float GetWeaponBaseStat(GameUtils.BaseID weaponID, GameUtils.EWeaponUpgrade weaponUpgrade)
    {
        switch (weaponUpgrade)
        {
            case GameUtils.EWeaponUpgrade.AIR:
                return GetWeaponBaseAirDamage(weaponID, weaponLvlLimit);
            case GameUtils.EWeaponUpgrade.EXPLOSION_RADIUS:
                return GetWeaponBaseExplosionRadius(weaponID, weaponLvlLimit);
            case GameUtils.EWeaponUpgrade.GROUND:
                return GetWeaponBaseGroundDamage(weaponID, weaponLvlLimit);
            case GameUtils.EWeaponUpgrade.HEALTH:
                return GetWeaponBaseHealth(weaponID, weaponLvlLimit);
            case GameUtils.EWeaponUpgrade.REGEN:
                return GetWeaponBaseRegeneration(weaponID, weaponLvlLimit);
            case GameUtils.EWeaponUpgrade.RELOAD:
                return GetWeaponBaseStartingAmmo(weaponID, weaponLvlLimit);
            case GameUtils.EWeaponUpgrade.SPEED:
                return GetWeaponBaseSpeed(weaponID, weaponLvlLimit);
            case GameUtils.EWeaponUpgrade.VISIBILITY_RADIUS:
                return GetWeaponBaseSightRadius(weaponID, weaponLvlLimit);
            default:
                return 0f;
        }
    }

    public string GetWeaponBaseStatWithString(GameUtils.BaseID weaponID, GameUtils.EWeaponUpgrade weaponUpgrade, float boost)
    {
        switch (weaponUpgrade)
        {
            case GameUtils.EWeaponUpgrade.AIR:
                if (weaponID == GameUtils.BaseID.ROCKETS)
                {
                    return GetWeaponBaseAirDamage(weaponID) * 5f + " DMG";
                }
                else
                {
                    return GetWeaponBaseAirDamage(weaponID) + " DMG";
                }
            case GameUtils.EWeaponUpgrade.EXPLOSION_RADIUS:
                float explosionRadius = GetWeaponBaseExplosionRadius(weaponID);
                explosionRadius = (float)System.Math.Round(explosionRadius, 3);
                if (weaponID == GameUtils.BaseID.LASER)
                {
                    return explosionRadius / 10 + " M";
                }
                else
                {
                    return explosionRadius * 10 + " M";
                }                
            case GameUtils.EWeaponUpgrade.GROUND:
                if (weaponID == GameUtils.BaseID.ROCKETS)
                {
                    return GetWeaponBaseGroundDamage(weaponID) * 5f + " DMG";
                }
                else
                {
                    return GetWeaponBaseGroundDamage(weaponID) + " DMG";
                }                
            case GameUtils.EWeaponUpgrade.HEALTH:
                return Math.Ceiling(GetWeaponBaseHealth(weaponID)) + "";
            case GameUtils.EWeaponUpgrade.REGEN:
                float ammoRegen = GetWeaponBaseRegeneration(weaponID);
                ammoRegen = (float)System.Math.Round(ammoRegen, 2);
                return ammoRegen + " SEC";
            case GameUtils.EWeaponUpgrade.RELOAD:
                return GetWeaponBaseStartingAmmo(weaponID) + "";
            case GameUtils.EWeaponUpgrade.SPEED:
                if (weaponID == GameUtils.BaseID.LASER)
                {
                    return GetWeaponBaseSpeed(weaponID) + " SEC";
                }
                return GetWeaponBaseSpeed(weaponID) * 10 + " M/S";
            case GameUtils.EWeaponUpgrade.VISIBILITY_RADIUS:
                return GetWeaponBaseSightRadius(weaponID) * 100 + " M";
            default:
                return "NULL";
        }
    }

    private float GetWeaponBaseHealth(GameUtils.BaseID weaponID, int lvlLimit = -1)
    {
        // Base
        WeaponData weaponData = FindWeaponData(weaponID);
        float health = weaponData.maxHealth;

        // Upgrade
        int healthLvl = UpgradeController.Instance.GetWeaponUpgradeLevel(weaponID, GameUtils.EWeaponUpgrade.HEALTH);

        // If there's a level limit
        if (lvlLimit >= 0)
        {
            // Take the lowest between the limit and the current upgrade level
            healthLvl = Mathf.Min(lvlLimit, healthLvl);
        }

        float healthImprovement = weaponData.healthPerUpgradeLvl * healthLvl;
        health += healthImprovement;

        if (BoosterController.Instance.BoosterTypeActive(BoosterController.EBoosterPackType.Defensive))
        {
            health *= BoosterController.Instance.health;
        }

        return health;
    }
    
    private float GetWeaponBaseSpeed(GameUtils.BaseID weaponID, int lvlLimit = -1)
    {
        // Base
        WeaponData weaponData = FindWeaponData(weaponID);
        float speed = weaponData.speed;

        // Upgrade
        int projectileSpeedLvl = UpgradeController.Instance.GetWeaponUpgradeLevel(weaponID, GameUtils.EWeaponUpgrade.SPEED);

        // If there's a level limit
        if (lvlLimit >= 0)
        {
            // Take the lowest between the limit and the current upgrade level
            projectileSpeedLvl = Mathf.Min(lvlLimit, projectileSpeedLvl);
        }

        float speedImprovement = weaponData.projectileSpeedPerUpgradeLvl * projectileSpeedLvl;
        speed += speedImprovement;

        if (weaponID != GameUtils.BaseID.LASER)
        {
            if (BoosterController.Instance.BoosterTypeActive(BoosterController.EBoosterPackType.Offensive))
            {
                speed *= BoosterController.Instance.weaponSpeed;
            }
        }
        else
        {
            if (BoosterController.Instance.BoosterTypeActive(BoosterController.EBoosterPackType.Offensive))
            {
                speed /= BoosterController.Instance.weaponSpeed;
            }
        }

        return speed;
    }

    private float GetWeaponInGameSpeed(GameUtils.BaseID weaponID)
    {
        float speed = 0f;

        WeaponObject weaponObject = BaseController.Instance.Bases[weaponID].GetComponent<WeaponObject>();
        if (weaponObject)
        {
            speed = GetWeaponBaseSpeed(weaponID, weaponLvlLimit);

            if (weaponID != GameUtils.BaseID.LASER)
            {
                // Adjust speed with factor
                speed *= GameUtils.SPEED_FACTOR;

                // Adjust speed to screen size
                speed *= PlayfieldController.Instance.WidthScale;

                // Boost - Projectile Speed
                bool isProjectileSpeedBoosted = BoosterController.Instance.CurrentBattleBoost == BoosterController.EBoostType.ProjectileSpeed;
                speed *= isProjectileSpeedBoosted ? BoosterController.Instance.BattleBoostMultiplier : 1f;
            }
            else
            {
                // LASER is only weapon which does not have speed, but uses the upgrade value for reducing the 8 rge time!

                // Boost - Projectile Speed
                bool isProjectileSpeedBoosted = BoosterController.Instance.CurrentBattleBoost == BoosterController.EBoostType.ProjectileSpeed;
                speed /= isProjectileSpeedBoosted ? BoosterController.Instance.BattleBoostMultiplier : 1f;
            }
        }

        return speed;
    }

    private float GetWeaponBaseGroundDamage(GameUtils.BaseID weaponID, int lvlLimit = -1)
    {
        // Base
        WeaponData weaponData = FindWeaponData(weaponID);
        float groundDamage = weaponData.groundPower;

        // Upgrade
        int groundDamageLvl = UpgradeController.Instance.GetWeaponUpgradeLevel(weaponID, GameUtils.EWeaponUpgrade.GROUND);

        // If there's a level limit
        if (lvlLimit >= 0)
        {
            // Take the lowest between the limit and the current upgrade level
            groundDamageLvl = Mathf.Min(lvlLimit, groundDamageLvl);
        }

        for (int i = 0; i < groundDamageLvl; ++i)
        {
            float upgradedGroundDamage = groundDamage * weaponData.damageMultiplier;

            // Round to nearest 5
            groundDamage = (float)System.Math.Round(upgradedGroundDamage / 5f, System.MidpointRounding.AwayFromZero) * 5f;            
        }

        // Booster Pack
        if (BoosterController.Instance.BoosterTypeActive(BoosterController.EBoosterPackType.Offensive))
        {
            groundDamage *= BoosterController.Instance.groundDamage;
        }

        return groundDamage;
    }

    private float GetWeaponInGameGroundDamage(GameUtils.BaseID weaponID)
    {
        float groundDamage = GetWeaponBaseGroundDamage(weaponID, weaponLvlLimit);

        // Boost - Damage
        bool isDamageBoosted = BoosterController.Instance.CurrentBattleBoost == BoosterController.EBoostType.Damage;
        groundDamage *= isDamageBoosted ? BoosterController.Instance.BattleBoostMultiplier : 1f;

        return groundDamage;
    }

    private float GetWeaponBaseAirDamage(GameUtils.BaseID weaponID, int lvlLimit = -1)
    {
        // Base 
        WeaponData weaponData = FindWeaponData(weaponID);
        float airDamage = weaponData.airPower;
        
        // Upgrade
        int airDamageLvl = UpgradeController.Instance.GetWeaponUpgradeLevel(weaponID, GameUtils.EWeaponUpgrade.AIR);

        // If there's a level limit
        if (lvlLimit >= 0)
        {
            // Take the lowest between the limit and the current upgrade level
            airDamageLvl = Mathf.Min(lvlLimit, airDamageLvl);
        }

        for (int i = 0; i < airDamageLvl; ++i)
        {
            float upgradedAirDamage = airDamage * weaponData.damageMultiplier;

            // Round to nearest 5
            airDamage = (float)System.Math.Round(upgradedAirDamage / 5f, System.MidpointRounding.AwayFromZero) * 5f;
        }

        // Booster Pack
        if (BoosterController.Instance.BoosterTypeActive(BoosterController.EBoosterPackType.Offensive))
        {
            airDamage *= BoosterController.Instance.airDamage;
        }

        return airDamage;
    }

    private float GetWeaponInGameAirDamage(GameUtils.BaseID weaponID)
    {
        float airDamage = GetWeaponBaseAirDamage(weaponID, weaponLvlLimit);

        // Boost - Damage
        bool isDamageBoosted = BoosterController.Instance.CurrentBattleBoost == BoosterController.EBoostType.Damage;
        airDamage *= isDamageBoosted ? BoosterController.Instance.BattleBoostMultiplier : 1f;

        return airDamage;
    }

    private float GetWeaponBaseExplosionRadius(GameUtils.BaseID weaponID, int lvlLimit = -1)
    {
        // Base 
        WeaponData weaponData = FindWeaponData(weaponID);
        float explosionRadius = weaponData.radius;

        // Upgrade
        int explosionRadiusLvl = UpgradeController.Instance.GetWeaponUpgradeLevel(weaponID, GameUtils.EWeaponUpgrade.EXPLOSION_RADIUS);

        // If there's a level limit
        if (lvlLimit >= 0)
        {
            // Take the lowest between the limit and the current upgrade level
            explosionRadiusLvl = Mathf.Min(lvlLimit, explosionRadiusLvl);
        }

        float explosionRadiusImprovement = weaponData.explosionRadiusPerUpgradeLvl * explosionRadiusLvl;
        explosionRadius += explosionRadiusImprovement;

        // Booster Pack
        if (BoosterController.Instance.BoosterTypeActive(BoosterController.EBoosterPackType.Offensive))
        {
            explosionRadius *= BoosterController.Instance.explosionRadius;
        }

        return explosionRadius;
    }

    private float GetWeaponInGameExplosionRadius(GameUtils.BaseID weaponID)
    {
        float explosionRadius = GetWeaponBaseExplosionRadius(weaponID, weaponLvlLimit);

        // Apply scale factor
        explosionRadius *= GameUtils.RADIUS_FACTOR;

        // Apply Scaling
        explosionRadius /= PlayfieldController.Instance.WidthScale;

        return explosionRadius;
    }

    private float GetWeaponBaseRegeneration(GameUtils.BaseID weaponID, int lvlLimit = -1)
    {
        // Base 
        WeaponData weaponData = FindWeaponData(weaponID);
        float regeneration = weaponData.regeneration;

        // Upgrade
        int regenerationLvl = UpgradeController.Instance.GetWeaponUpgradeLevel(weaponID, GameUtils.EWeaponUpgrade.REGEN);

        // If there's a level limit
        if (lvlLimit >= 0)
        {
            // Take the lowest between the limit and the current upgrade level
            regenerationLvl = Mathf.Min(lvlLimit, regenerationLvl);
        }

        float regenerationImprovement = weaponData.regenSpeedPerUpgradeLvl * regenerationLvl;
        regeneration += regenerationImprovement;

        // Booster Pack
        if (BoosterController.Instance.BoosterTypeActive(BoosterController.EBoosterPackType.Defensive))
        {
            regeneration /= BoosterController.Instance.regeneration;
        }

        return regeneration;
    }

    private float GetWeaponInGameRegeneration(GameUtils.BaseID weaponID)
    {
        float regeneration = GetWeaponBaseRegeneration(weaponID, weaponLvlLimit);

        // Boost - Regen Speed
        bool isRegenSpeedBoosted = BoosterController.Instance.CurrentBattleBoost == BoosterController.EBoostType.RegenSpeed;
        regeneration /= isRegenSpeedBoosted ? BoosterController.Instance.BattleBoostMultiplier : 1f;

        return regeneration;
    }

    private float GetWeaponQuickRegeneration(GameUtils.BaseID weaponID)
    {
        float ammoRegenTime = QUICK_AMMO_REGEN_DURATION / GetWeaponAmmoCountForReload(weaponID);
        return ammoRegenTime;
    }

    private int GetWeaponAmmoCountForReload(GameUtils.BaseID weaponID)
    {
        return FindWeaponData(weaponID).startingAmmo * ammoCountMultiplierForReload;
    }
    
    private float GetWeaponCooldown(GameUtils.BaseID weaponID)
    {
        WeaponData weaponData = FindWeaponData(weaponID);
        return weaponData.cooldown;
    }
    
    private int GetWeaponBaseStartingAmmo(GameUtils.BaseID weaponID, int lvlLimit = -1)
    {
        // Base 
        WeaponData weaponData = FindWeaponData(weaponID);
        int startingAmmo = weaponData.startingAmmo;

        // Upgrade
        int startingAmmoLvl = UpgradeController.Instance.GetWeaponUpgradeLevel(weaponID, GameUtils.EWeaponUpgrade.RELOAD);

        // If there's a level limit
        if (lvlLimit >= 0)
        {
            // Take the lowest between the limit and the current upgrade level
            startingAmmoLvl = Mathf.Min(lvlLimit, startingAmmoLvl);
        }

        int startingAmmoImprovement = Mathf.RoundToInt(weaponData.startingAmmoPerUpgradeLvl * startingAmmoLvl);
        startingAmmo += startingAmmoImprovement;

        if (BoosterController.Instance.BoosterTypeActive(BoosterController.EBoosterPackType.Defensive))
        {
            startingAmmo = (int)(startingAmmo * BoosterController.Instance.startingAmmo);
        }

        return startingAmmo;
    }

    private float GetWeaponBaseSightRadius(GameUtils.BaseID weaponID, int lvlLimit = -1)
    {
        // Base 
        WeaponData weaponData = FindWeaponData(weaponID);
        float sightRadius = weaponData.sightRadius;

        // Upgrade
        int sightRadiusLvl = UpgradeController.Instance.GetWeaponUpgradeLevel(weaponID, GameUtils.EWeaponUpgrade.VISIBILITY_RADIUS);

        // If there's a level limit
        if (lvlLimit >= 0)
        {
            // Take the lowest between the limit and the current upgrade level
            sightRadiusLvl = Mathf.Min(lvlLimit, sightRadiusLvl);
        }

        float sightRadiusImprovement = weaponData.sightRadiusPerUpgradeLvl * sightRadiusLvl;
        sightRadius += sightRadiusImprovement;

        // Booster Pack
        if (BoosterController.Instance.BoosterTypeActive(BoosterController.EBoosterPackType.Defensive))
        {
            sightRadius *= BoosterController.Instance.sightRadius;
        }

        return sightRadius;
    }

    private float GetWeaponInGameSightRadius(GameUtils.BaseID weaponID)
    {
        float sightRadius = GetWeaponBaseSightRadius(weaponID, weaponLvlLimit);

        return sightRadius;
    }

#if ENABLE_WEAPON_AI
    public float GetWeaponAIRange(GameUtils.BaseID weaponID)
    {
        WeaponData weaponData = FindWeaponData(weaponID);
        float sightRadius = weaponData.sightRadius;
        
        float sightRadiusImprovement = weaponData.sightRadiusPerUpgradeLvl * 10;
        sightRadius += sightRadiusImprovement;

        // Adjustments
        sightRadius *= GameUtils.SIGHT_RADIUS_FACTOR;
        sightRadius *= PlayfieldController.Instance.WidthScale;
        sightRadius *= weaponAIRangeMultiplier;

        return sightRadius;
    }
#endif

    public void SpawnExplosion(Vector3 position, float groundPower, float airPower, float radius)
    {
        GameObject explosionGO = GetExplosionFromPool();
        Explosion explosion = explosionGO.GetComponent<Explosion>();
        explosion.Init(groundPower, airPower, radius);
        explosionGO.transform.position = position;
    }
#endregion

#region Data Handling
    private void PopulateWeaponsData()
    {
        weaponsData = Resources.LoadAll<WeaponData>("Weapons");
    }

    public WeaponData FindWeaponData(GameUtils.BaseID weaponID)
    {
        for (int i = 0; i < weaponsData.Length; ++i)
        {
            if (weaponID == weaponsData[i].baseID)
            {
                return weaponsData[i];
            }
        }

        return null;
    }
    
    public void SaveWeaponController(DataController.SAVEPRIORITY priority)
    {
        DataController.Instance.SaveWeaponData(priority);
    }

    public void LoadWeaponController(List<GameUtils.BaseID> remoteWeapons)
    {
        if (remoteWeapons == null)
        {
            System.Exception ex = JsonUtils<List<GameUtils.BaseID>>.Load(GameUtils.WEAPON_CTR_JSON, out unlockedWeapons, GameUtils.ELoadType.DynamicData);
            if (ex != null
                && ex.GetType() == typeof(FileLoadException))
            {
                ResetWeaponController();
            }
        }
        else
        {
            unlockedWeapons = remoteWeapons;
        }
    }

    public void ResetWeaponController()
    {
        if (unlockedWeapons == null)
        {
            unlockedWeapons = new List<GameUtils.BaseID>();
        }
        unlockedWeapons.Clear();
        // Artillery is always unlocked!!!
        unlockedWeapons.Add(GameUtils.BaseID.ARTILLERY);
        SaveWeaponController(DataController.SAVEPRIORITY.QUEUE);
    }
#endregion
}


using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
#if UNITY_IOS
using UnityEngine.SocialPlatforms.GameCenter;
#endif
#if UNITY_ANDROID
using GooglePlayGames;
#endif

#pragma warning disable 0649

public class MenuUIController : UnitySingleton<MenuUIController>
{
    #region Menu UI elements
    [SerializeField]
    private Button playButton;
    [SerializeField]
    private Button storeButton;
    [SerializeField]
    private Button upgradesButton;
    [SerializeField]
    private Button profileButton;
    [SerializeField]
    private Button optionsButton;
	[SerializeField]
	private Button quitButton;
	[SerializeField]
	private Button exitGameButton;

    //private GameObject playButtonNewAmountContainer;
    //private GameObject storeButtonNewAmountContainer;
    private GameObject upgradesButtonNewAmountContainer;
    private GameObject profileButtonNewAmountContainer;
    //private GameObject optionsButtonNewAmountContainer;

    [SerializeField]
    private GameObject playActiveTab;
    [SerializeField]
    private GameObject storeActiveTab;
    [SerializeField]
    private GameObject upgradesActiveTab;
    [SerializeField]
    private GameObject profileActiveTab;
    [SerializeField]
    private GameObject optionsActiveTab;

    //private GameObject playActiveTabNewAmountContainer;
    //private GameObject storeActiveTabNewAmountContainer;
    private GameObject upgradesActiveTabNewAmountContainer;
    private GameObject profileActiveTabNewAmountContainer;
    //private GameObject optionsActiveTabNewAmountContainer;

    [SerializeField]
    private GameObject playFolder;
    [SerializeField]
    private GameObject storeFolder;
    [SerializeField]
    private GameObject upgradesFolder;
    [SerializeField]
    private GameObject profileFolder;
    [SerializeField]
    private GameObject optionsFolder;

    [SerializeField]
    private Button moreCreditsButton;
    [SerializeField]
    private Button moreGoldButton;
    #endregion

    #region Section Play UI elements
    [SerializeField]
    private GameObject campaignSubFolder;
    [SerializeField]
    private GameObject lastStandSubFolder;
    [SerializeField]
    private GameObject eventsSubFolder;

    [SerializeField]
    private Button campaignButton;
    [SerializeField]
    private Button lastStandButton;
    [SerializeField]
    private Button eventsButton;

    [SerializeField]
    private GameObject campaignActiveTab;
    [SerializeField]
    private GameObject lastStandActiveTab;
    [SerializeField]
    private GameObject eventsActiveTab;

    [SerializeField]
    private GameObject dailyMarkerOn;
    [SerializeField]
    private GameObject dailyMarkerOff;

    [SerializeField]
    private GameObject bossInfoPanel;
    [SerializeField]
    private Button dismissBossInfoButton;
    [SerializeField]
    private Button closeBossInfoButton;
    [SerializeField]
    private GameObject lastStandInfoPanel;
    [SerializeField]
    private Button dismissLastStandInfoButton;
    [SerializeField]
    private Button closeLastStandInfoButton;

    [SerializeField]
    private GameObject quitInfoPanel;
    [SerializeField]
    private Button dismissQuitInfoButton;
    [SerializeField]
    private Button closeQuitInfoButton;
    [SerializeField]
    private Button abortQuitInfoButton;

    #endregion

    #region Section Store UI elements
    [SerializeField]
    private GameObject standardPacksSubFolder;
    [SerializeField]
    private GameObject creditPacksSubFolder;
    [SerializeField]
    private GameObject goldPacksSubFolder;    

    [SerializeField]
    private Button standardPacksButton;
    [SerializeField]
    private Button creditPacksButton;
    [SerializeField]
    private Button goldPacksButton;
    
    [SerializeField]
    private GameObject standardPacksActiveTab;
    [SerializeField]
    private GameObject creditPacksActiveTab;
    [SerializeField]
    private GameObject goldPacksActiveTab;

    [SerializeField]
    private GameObject notEnoughCurrencyPanel;
    [SerializeField]
    private Button dismissNotEnoughCurrencyButton;
    [SerializeField]
    private Button goToCreditStoreButton;
    [SerializeField]
    private Button goToGoldStoreButton;
    [SerializeField]
    private Button closeNotEnoughCurrencyButton;
    #endregion

    #region Section Upgrades UI elements
    [SerializeField]
    private GameObject weaponSubFolder;
    [SerializeField]
    private GameObject consumableSubFolder;

    [SerializeField]
    private Button weaponButton;
    [SerializeField]
    private Button consumableButton;

    [SerializeField]
    private GameObject weaponActiveTab;
    [SerializeField]
    private GameObject consumableActiveTab;

    // Weapons
    [SerializeField]
    private Button artilleryButton;
    [SerializeField]
    private Button flakButton;
    [SerializeField]
    private Button missileButton;
    [SerializeField]
    private Button laserButton;
    [SerializeField]
    private Button railgunButton;
    [SerializeField]
    private Button rocketsButton;    
    [SerializeField]
    private Button unlockWeaponButton;
    [SerializeField]
    private Button buyUpgradePackButton;

    private Button debugMaxUpgradesButton;
    private Button debugMaxCardsButton;
    private Button debugResetUpgradesButton;

    // Consumables
    [SerializeField]
    private Button repairButton;
    [SerializeField]
    private Button ammoButton;
    [SerializeField]
    private Button pulseButton;
    [SerializeField]
    private Button slowButton;
    [SerializeField]
    private Button ohShitButton;
    [SerializeField]
    private Button buySupportPackButton;

    // Unlocked item panel
    [SerializeField]
    private GameObject unlockedItemPanel;
    [SerializeField]
    private Text sectorText;
    [SerializeField]
    private Image unlockedItemImage;
    [SerializeField]
    private Text unlockedItemText;
    [SerializeField]
    private Button continueButton;

    private Button debugMaxConsumablesButton;    
    #endregion

    #region Section Profile UI elements
    [SerializeField]
    private GameObject achievementSubFolder;

    [SerializeField]
    private Button achievementButton;

    [SerializeField]
    private GameObject achievementActiveTab;
    #endregion

    #region Section Options UI elements
    [SerializeField]
    private Button settingsButton;

    [SerializeField]
    private GameObject settingsActiveTab;

    [SerializeField]
    private GameObject optionsSubFolder;

    [SerializeField]
    private Dropdown languageSelector;
    [SerializeField]
    private Slider musicSlider;
    [SerializeField]
    private Slider soundSlider;
    [SerializeField]
    private Toggle dragAndReleaseToggle;
    [SerializeField]
    private Toggle performanceToggle;
    [SerializeField]
    private Toggle normalToggle;
    [SerializeField]
    private Toggle qualityToggle;
    [SerializeField]
    private Button creditsButton;
    [SerializeField]
    private Button creditsBackButton;
    [SerializeField]
    private GameObject creditsPopup;
    [SerializeField]
    private Button resetButton;
    [SerializeField]
    private Button gpgsButton;
    [SerializeField]
    private Button gcButton;
    #endregion

    private GameObject activeMenuFolder;
    private GameObject activePlaySubFolder;
    private GameObject activeStoreSubFolder;
    private GameObject activeUpgradeSubFolder;
    private GameObject activeProfileSubFolder;
    private GameObject activeOptionsSubFolder;

    private GameObject menuCanvas;
    
    private GameUtils.BaseID selectedWeaponID;
    private GameUtils.EConsumable selectedConsumableID;
    private const float updateSpan = 0.2f;
    private float updateTimer = 0f;
    private static bool setup = false;
	private KeyCode key = KeyCode.Escape;

    public override void Awake()
    {
        base.Awake();
        RegisterDependency(FlowStateController.Instance);
        RegisterDependency(CardsController.Instance);
        RegisterDependency(UpgradeController.Instance);
        RegisterDependency(LocalizationManager.Instance);
        RegisterDependency(PackController.Instance);
        RegisterDependency(ChallengeController.Instance);

        InitMenuUI();
        
        setup = false;

        SceneManager.sceneUnloaded += OnSceneUnloaded;
    }

    // Note :: This is used to detect when the open pack anim is done.
    private void OnSceneUnloaded(Scene scene)
    {
        // If it's not the open prize scene being unloaded, return now
        if (scene.name != FlowStateController.OPEN_PRIZE_SCENE_NAME) return;

        if (activeMenuFolder == upgradesFolder)
        {
            if (weaponActiveTab.activeInHierarchy)
            {
                RefreshSelectedWeaponUI();
            }
            else if (consumableActiveTab.activeInHierarchy)
            {
                RefreshSelectedConsumableUI();
            }
        }
    }

    private void InitMenuUI()
    {
        menuCanvas = GameObject.Find("MenuUICanvas");

        // menu
        playButton.onClick.AddListener(delegate { ActivateMenuFolder(playFolder); });
        storeButton.onClick.AddListener(delegate { ActivateMenuFolder(storeFolder); });
        upgradesButton.onClick.AddListener(delegate { ActivateMenuFolder(upgradesFolder); });
        profileButton.onClick.AddListener(delegate { ActivateMenuFolder(profileFolder); });
        optionsButton.onClick.AddListener(delegate { ActivateMenuFolder(optionsFolder); });
		//button to exit game
		exitGameButton.onClick.AddListener(delegate { quitInfoPanel.SetActive(true); });

        //playButtonNewAmountContainer = GameUtils.GetChildGameObject(playButton.gameObject, "prefab_newAmountIcon", true);
        //storeButtonNewAmountContainer = GameUtils.GetChildGameObject(storeButton.gameObject, "prefab_newAmountIcon", true);
        upgradesButtonNewAmountContainer = GameUtils.GetChildGameObject(upgradesButton.gameObject, "prefab_newAmountIcon", true);
        profileButtonNewAmountContainer = GameUtils.GetChildGameObject(profileButton.gameObject, "prefab_newAmountIcon", true);
        //optionsButtonNewAmountContainer = GameUtils.GetChildGameObject(optionsButton.gameObject, "prefab_newAmountIcon", true);

        //playActiveTabNewAmountContainer = GameUtils.GetChildGameObject(playActiveTab, "prefab_newAmountIcon", true);
        //storeActiveTabNewAmountContainer = GameUtils.GetChildGameObject(storeActiveTab, "prefab_newAmountIcon", true);
        upgradesActiveTabNewAmountContainer = GameUtils.GetChildGameObject(upgradesActiveTab, "prefab_newAmountIcon", true);
        profileActiveTabNewAmountContainer = GameUtils.GetChildGameObject(profileActiveTab, "prefab_newAmountIcon", true);
        //optionsActiveTabNewAmountContainer = GameUtils.GetChildGameObject(optionsActiveTab, "prefab_newAmountIcon", true);

        moreCreditsButton.onClick.AddListener(delegate { OpenCreditPacksTab(); });
        moreGoldButton.onClick.AddListener(delegate { OpenGoldPacksTab(); });

        // Play tabs
        campaignButton.onClick.AddListener(delegate { ActivatePlaySubFolder(campaignSubFolder); });
        lastStandButton.onClick.AddListener(delegate { ActivatePlaySubFolder(lastStandSubFolder); });
        eventsButton.onClick.AddListener(delegate { ActivatePlaySubFolder(eventsSubFolder); });
        activePlaySubFolder = campaignSubFolder;

        // Boss info panel
        bossInfoPanel.SetActive(false);
        dismissBossInfoButton.onClick.AddListener(delegate { ShowBossInfoPopup(false); });
        closeBossInfoButton.onClick.AddListener(delegate { ShowBossInfoPopup(false); });

        quitInfoPanel.SetActive(false);
        dismissQuitInfoButton.onClick.AddListener(delegate { ShowQuitInfoPopup(false); });
        closeQuitInfoButton.onClick.AddListener(delegate { ShowQuitInfoPopup(false); });
        abortQuitInfoButton.onClick.AddListener(delegate { ShowQuitInfoPopup(false); });
        quitButton.onClick.AddListener(delegate { FlowStateController.Instance.Quit(); });

        // Last Stand Info Panel
        lastStandInfoPanel.SetActive(false);

        // Store tabs
        standardPacksButton.onClick.AddListener(delegate { ActivateStoreSubFolder(standardPacksSubFolder); });
       // goldPacksButton.onClick.AddListener(delegate { ActivateStoreSubFolder(goldPacksSubFolder); });
        creditPacksButton.onClick.AddListener(delegate { ActivateStoreSubFolder(creditPacksSubFolder); });
        activeStoreSubFolder = standardPacksSubFolder;

        // Not enough currency panel
        notEnoughCurrencyPanel.SetActive(false);
        dismissNotEnoughCurrencyButton.onClick.AddListener(delegate { HideNotEnoughCurrencyPopup(); });
        goToCreditStoreButton.onClick.AddListener(delegate { HideNotEnoughCurrencyPopup(GameUtils.ECurrencyType.Credit); });
       // goToGoldStoreButton.onClick.AddListener(delegate { HideNotEnoughCurrencyPopup(GameUtils.ECurrencyType.Gold); });
        closeNotEnoughCurrencyButton.onClick.AddListener(delegate { HideNotEnoughCurrencyPopup(); });

        // Upgrade tabs
        weaponButton.onClick.AddListener(delegate { ActivateUpgradeSubFolder(weaponSubFolder); });
        consumableButton.onClick.AddListener(delegate { ActivateUpgradeSubFolder(consumableSubFolder); });
        activeUpgradeSubFolder = weaponSubFolder;

        // Weapons
        artilleryButton.onClick.AddListener(delegate { SelectWeapon(GameUtils.BaseID.ARTILLERY); });
        flakButton.onClick.AddListener(delegate { SelectWeapon(GameUtils.BaseID.FLAK); });
        missileButton.onClick.AddListener(delegate { SelectWeapon(GameUtils.BaseID.MISSILE); });
        railgunButton.onClick.AddListener(delegate { SelectWeapon(GameUtils.BaseID.RAILGUN); });
        rocketsButton.onClick.AddListener(delegate { SelectWeapon(GameUtils.BaseID.ROCKETS); });
        laserButton.onClick.AddListener(delegate { SelectWeapon(GameUtils.BaseID.LASER); });
        unlockWeaponButton.onClick.AddListener(delegate { UnlockSelectedWeapon(); });
        buyUpgradePackButton.onClick.AddListener(delegate { BuyUpgradePack(); });

        debugMaxUpgradesButton = GameUtils.GetChildGameObject(weaponSubFolder, "BtnDebugMaxUpgrades", true).GetComponent<Button>();
        debugMaxCardsButton = GameUtils.GetChildGameObject(weaponSubFolder, "BtnDebugMaxCards", true).GetComponent<Button>();
        debugResetUpgradesButton = GameUtils.GetChildGameObject(weaponSubFolder, "BtnDebugResetUpgrades", true).GetComponent<Button>();

#if UNITY_EDITOR || DEBUG_MOBILE
        debugMaxUpgradesButton.onClick.AddListener(delegate { SetWeaponUpgradesLevel(GameUtils.MAX_UPGRADE_LEVEL); });
        debugMaxCardsButton.onClick.AddListener(delegate { MaxWeaponCards(); });
        debugResetUpgradesButton.onClick.AddListener(delegate { SetWeaponUpgradesLevel(0); });
#else
        debugMaxUpgradesButton.gameObject.SetActive(false);
        debugMaxCardsButton.gameObject.SetActive(false);
        debugResetUpgradesButton.gameObject.SetActive(false);
#endif

        // Consumables
        repairButton.onClick.AddListener(delegate { SelectConsumable(GameUtils.EConsumable.Repair); });
        ammoButton.onClick.AddListener(delegate { SelectConsumable(GameUtils.EConsumable.Reload); });
        pulseButton.onClick.AddListener(delegate { SelectConsumable(GameUtils.EConsumable.Pulse); });
        slowButton.onClick.AddListener(delegate { SelectConsumable(GameUtils.EConsumable.Slower); });
        ohShitButton.onClick.AddListener(delegate { SelectConsumable(GameUtils.EConsumable.OhShit); });
        buySupportPackButton.onClick.AddListener(delegate { BuySupportPack(); });

        debugMaxConsumablesButton = GameUtils.GetChildGameObject(consumableSubFolder, "DebugMaxConsumablesButton", true).GetComponent<Button>();

#if UNITY_EDITOR || DEBUG_MOBILE
        debugMaxConsumablesButton.onClick.AddListener(delegate { SetConsumableAmount(999); });
#else
        debugMaxConsumablesButton.gameObject.SetActive(false);
#endif

        // Unlocked item panel
        unlockedItemPanel.SetActive(false);
        continueButton.onClick.AddListener(delegate { HideUnlockedItemPopup(); });

        // weapon upgrades
        WeaponUpgradeCallBacks();
                
        // Profile tabs
        achievementButton.onClick.AddListener(delegate { ActivateProfileSubFolder(achievementSubFolder); });
        activeProfileSubFolder = achievementSubFolder;

        // Options tabs
        settingsButton.onClick.AddListener(delegate { ActivateOptionsSubFolder(optionsSubFolder); });
        languageSelector.onValueChanged.AddListener(delegate { LanguageValueChanged(); });
        musicSlider.onValueChanged.AddListener(delegate { MusicSliderValueChanged(); });
        soundSlider.onValueChanged.AddListener(delegate { SoundSliderValueChanged(); });
        dragAndReleaseToggle.onValueChanged.AddListener(delegate { DragAndReleaseValueChanged(); });
        performanceToggle.onValueChanged.AddListener(delegate { GraphicSettingValueChanged(SettingsController.EGraphicSetting.Performance); });
        normalToggle.onValueChanged.AddListener(delegate { GraphicSettingValueChanged(SettingsController.EGraphicSetting.Normal); });
        qualityToggle.onValueChanged.AddListener(delegate { GraphicSettingValueChanged(SettingsController.EGraphicSetting.Quality); });
        creditsButton.onClick.AddListener(delegate { SetCreditsPopup(true); });
        creditsBackButton.onClick.AddListener(delegate { SetCreditsPopup(false); });
        activeOptionsSubFolder = optionsSubFolder;

        GameUtils.GetChildGameObject(optionsSubFolder, "TextVersion", true).GetComponent<Text>().text = "Version: " + Application.version;

#if UNITY_ANDROID
        // show the google play service button
        gpgsButton.gameObject.SetActive(true);
        gpgsButton.onClick.AddListener(delegate { GPGSButtonClicked(); });
        gcButton.gameObject.SetActive(false);
#elif UNITY_IOS
        if (!Social.localUser.authenticated)
        {
            // only show the button if can login
            gcButton.gameObject.SetActive(true);
            gcButton.onClick.AddListener(delegate { GCButtonClicked(); });
        }
        else
        {
            // cannot logout
            gcButton.gameObject.SetActive(false);
        }
        gpgsButton.gameObject.SetActive(false);
#else
        // dont show the buttons if not android or ios
        gpgsButton.gameObject.SetActive(false);
        gcButton.gameObject.SetActive(false);
#endif


#if UNITY_EDITOR || RELEASE_BETA
        resetButton.onClick.AddListener(delegate { ResetButtonClicked(); });
#else
        resetButton.gameObject.SetActive(false);
#endif

        creditsPopup.SetActive(false);
        selectedWeaponID = GameUtils.BaseID.ARTILLERY;
        selectedConsumableID = GameUtils.EConsumable.Repair;

        dailyMarkerOn.SetActive(!DailyController.Instance.IsDailyComplete());
        dailyMarkerOff.SetActive(!DailyController.Instance.IsDailyComplete());
    }

    private void WeaponUpgradeCallBacks()
    {
        for (int i = 0; i < (int)GameUtils.EWeaponUpgrade.COUNT; ++i)
        {
            // Re-order the weapon upgrades for the UI            
            GameUtils.EWeaponUpgrade currentWeaponUpgrade = GetOrderedWeaponUpgrade(i);

            Button WeaponUpgradeButton = GameUtils.GetChildGameObject(weaponSubFolder, "prefab_upgradeInfo (" + i + ")").GetComponent<Button>();
            WeaponUpgradeButton.onClick.AddListener(delegate { WeaponUpgradeButtonClicked(currentWeaponUpgrade); });
        }
    }

	public void Update()
	{
		if (Input.GetKeyDown (key)) 
		{
			quitInfoPanel.SetActive(true);
		}
	}

    public override bool FixedUpdate()
    {
        if (!base.FixedUpdate()) return false;
        if (!DataController.Instance.AllLoaded) return false;
        if (!setup) SetupScene();

        UpdatePlay();
        UpdateStore();
        UpdateUpgrades();
        UpdateProfile();
        UpdateOptions();

        updateTimer -= Time.deltaTime;
        if (updateTimer < 0)
        {
            updateTimer = updateSpan;
            SlowUpdate();
        }

        return true;
    }

    private void SetupScene()
    {
        FlowStateController.Instance.LoadingScreen(false);

        // don't switch to the play folder
        // if we are coming from tutorial which already set a folder!
        if (activeMenuFolder == null)
        {
            // Switch to the right play tab
            switch (ChallengeController.Instance.CurrentChallengeTab)
            {
                case ChallengeData.EChallengeType.LastStand:
                    activePlaySubFolder = lastStandSubFolder;
                    break;
                case ChallengeData.EChallengeType.Operation:
                    //TODO :: OPERATION TAB
                    //activePlaySubFolder = lastStandSubFolder;
                    break;
                case ChallengeData.EChallengeType.Event:
                    activePlaySubFolder = eventsSubFolder;
                    break;
                case ChallengeData.EChallengeType.Mission:
                default:
                    activePlaySubFolder = campaignSubFolder;
                    break;
            }

            ActivateMenuFolder(playFolder);
        }

        SetOptionsValue();

        setup = true;
    }

    private void SlowUpdate()
    {
        // Update the new amount icon
        //UpdateNewAmountIcon(playButtonNewAmountContainer, playActiveTabNewAmountContainer, 0); // DESIGN :: No need for now
        //UpdateNewAmountIcon(storeButtonNewAmountContainer, storeActiveTabNewAmountContainer, 0); // DESIGN :: No need for now
        UpdateNewAmountIcon(upgradesButtonNewAmountContainer, upgradesActiveTabNewAmountContainer, GetAllWeaponUpgradeReadyAmount());
        UpdateNewAmountIcon(profileButtonNewAmountContainer, profileActiveTabNewAmountContainer, AchievementController.Instance.GetNewAchievementAmount());
        //UpdateNewAmountMenuIcon(optionsButtonNewAmountContainer, optionsActiveTabNewAmountContainer, 0); // DESIGN :: No need for now

#if !UNITY_EDITOR && UNITY_ANDROID
        if (PlayGamesPlatform.Instance.IsAuthenticated())
        {
            gpgsButton.GetComponentInChildren<LocalizedText>().LocalizedTextID = LocalizationManager.GOOGLE_LOGOUT_TEXT;
        }
        else
        {
            gpgsButton.GetComponentInChildren<LocalizedText>().LocalizedTextID = LocalizationManager.GOOGLE_LOGIN_TEXT;
        }
#endif
    }

    private void UpdateNewAmountIcon(GameObject buttonNewAmountContainer, GameObject activeTabNewAmountContainer, int amount)
    {
        bool hasNew = amount > 0;

        buttonNewAmountContainer.SetActive(hasNew);
        activeTabNewAmountContainer.SetActive(hasNew);

        if (hasNew)
        {
            buttonNewAmountContainer.GetComponentInChildren<Text>().text = amount.ToString();
            activeTabNewAmountContainer.GetComponentInChildren<Text>().text = amount.ToString();
        }
    }

#region Update Play Section
    private void UpdatePlay()
    {
        if (activeMenuFolder == playFolder)
        {
            if (activePlaySubFolder == campaignSubFolder) UpdateMissions();
            if (activePlaySubFolder == lastStandSubFolder) UpdateLastStand();
            if (activePlaySubFolder == eventsSubFolder) UpdateEvents();
        }
    }

    private void UpdateMissions()
    {
    }

    private void UpdateLastStand()
    {
    }

    private void UpdateEvents()
    {
    }

    public void ShowQuitInfoPopup(bool show)
    {
        quitInfoPanel.SetActive(show);
        if (!show)
        {
            FlowStateController.Instance.RegisterBackButton(FlowStateController.Instance.ShowQuitInfoPanel);
            FlowStateController.Instance.UnregisterBackButton(FlowStateController.Instance.Quit);
        }
        else
        {
            FlowStateController.Instance.UnregisterBackButton(FlowStateController.Instance.ShowQuitInfoPanel);
            FlowStateController.Instance.RegisterBackButton(FlowStateController.Instance.Quit);
        }
    }

    public void ShowBossInfoPopup(bool show)
    {
        bossInfoPanel.SetActive(show);
    }

    public void ShowLastStandInfoPopup(bool show, int id)
    {
        lastStandInfoPanel.SetActive(show);
        if (show)
        {
            GameObject bossDesc = GameUtils.GetChildGameObject(lastStandInfoPanel, "BossDesc", true);
#if UNITY_ANDROID
            bossDesc.GetComponent<LocalizedText>().LocalizedTextID = LocalizationManager.LASTSTAND_POPUP_GP_DESC;
#elif UNITY_IOS
            bossDesc.GetComponent<LocalizedText>().LocalizedTextID = LocalizationManager.LASTSTAND_POPUP_GC_DESC;
#endif

            dismissLastStandInfoButton.onClick.AddListener(delegate { ShowLastStandInfoPopup(false, id); });
            closeLastStandInfoButton.onClick.AddListener(delegate { ShowLastStandInfoPopup(false, id); });
        }
        else
        {
            ChallengeController.Instance.LastStandButtonClicked(id);
        }
    }
#endregion

#region Update Store Section
    private void UpdateStore()
    {
        if (activeMenuFolder == storeFolder)
        {
            if (activeStoreSubFolder == standardPacksSubFolder) UpdatePacks();
            if (activeStoreSubFolder == goldPacksSubFolder) UpdateBoosters();
            if (activeStoreSubFolder == creditPacksSubFolder) UpdateCurrencies();
        }
    }

    private void UpdatePacks()
    {
    }

    private void UpdateBoosters()
    {
    }

    private void UpdateCurrencies()
    {
    }

    public void ShowNotEnoughCurrencyPopup(int currencyMissing, GameUtils.ECurrencyType currencyType)
    {
        notEnoughCurrencyPanel.SetActive(true);

        bool isMissingGold = currencyType == GameUtils.ECurrencyType.Gold;

        goToGoldStoreButton.gameObject.SetActive(isMissingGold);
        goToCreditStoreButton.gameObject.SetActive(!isMissingGold);
        GameObject goldAmountGO = GameUtils.GetChildGameObject(notEnoughCurrencyPanel, "goldAmount", true);
        goldAmountGO.SetActive(isMissingGold);
        goldAmountGO.GetComponent<Text>().text = currencyMissing.ToString();
        GameObject creditAmountGO = GameUtils.GetChildGameObject(notEnoughCurrencyPanel, "creditAmount", true);
        creditAmountGO.SetActive(!isMissingGold);
        creditAmountGO.GetComponent<Text>().text = currencyMissing.ToString();
        GameUtils.GetChildGameObject(notEnoughCurrencyPanel, "goldIcon", true).SetActive(isMissingGold);
        GameUtils.GetChildGameObject(notEnoughCurrencyPanel, "creditIcon", true).SetActive(!isMissingGold);
    }

    private void HideNotEnoughCurrencyPopup(GameUtils.ECurrencyType currencyType = GameUtils.ECurrencyType.None)
    {
        notEnoughCurrencyPanel.SetActive(false);

        if (currencyType == GameUtils.ECurrencyType.Gold)
        {
            OpenGoldPacksTab();
        }
        else if (currencyType == GameUtils.ECurrencyType.Credit)
        {
            OpenCreditPacksTab();
        }
    }
#endregion

#region Update Upgrade Section
    private void UpdateUpgrades()
    {
        if (activeMenuFolder == upgradesFolder)
        {
            if (activeUpgradeSubFolder == weaponSubFolder) UpdateWeaponUpgrades();
            if (activeUpgradeSubFolder == consumableSubFolder) UpdateConsumables();
        }
    }

    private void UpdateWeaponUpgrades()
    {
    }

    private void UpdateConsumables()
    {
    }

#region Weapon UI
    private void RefreshSelectedWeaponUI()
    {
        // Get the weapon data
        WeaponData weaponData = WeaponController.Instance.FindWeaponData(selectedWeaponID);

        // Get the weapon rank
        int weaponRank = UpgradeController.Instance.GetWeaponRankLevel(selectedWeaponID);
        GameObject weaponRankContainer = GameUtils.GetChildGameObject(weaponSubFolder, "IconWeaponRankContainer", true);
        List<Transform> listWeaponRankIcons = new List<Transform>();
        weaponRankContainer.transform.GetComponentsInChildren(true, listWeaponRankIcons);
        for (int i = 0; i < listWeaponRankIcons.Count; ++i)
        {
            // Note :: GetComponentsInChildren will add the WeaponRankContainer object
            if (listWeaponRankIcons[i].gameObject.name.StartsWith("IconWeaponRankLv"))
            {
                bool isWeaponRankIconName = listWeaponRankIcons[i].gameObject.name == ("IconWeaponRankLv" + weaponRank);
                listWeaponRankIcons[i].gameObject.SetActive(isWeaponRankIconName);
            }            
        }
        GameUtils.GetChildGameObject(weaponSubFolder, "TextWeaponRank").GetComponent<Text>().text = "Lv. " + weaponRank;
        GameObject sliderWeaponRankGO = GameUtils.GetChildGameObject(weaponSubFolder, "SliderWeaponRank", true);
        sliderWeaponRankGO.SetActive(weaponRank < 10);
        sliderWeaponRankGO.GetComponent<Slider>().value = UpgradeController.Instance.GetWeaponRankProgression(selectedWeaponID);

        GameUtils.GetChildGameObject(weaponSubFolder, "TextWeaponName").GetComponent<Text>().text = GameUtils.GetWeaponLocalizedName(selectedWeaponID).ToUpper();
        GameUtils.GetChildGameObject(weaponSubFolder, "TextWeaponDesc").GetComponent<Text>().text = GameUtils.GetWeaponLocalizedDesc(selectedWeaponID);
        GameUtils.GetChildGameObject(weaponSubFolder, "ImageWeapon").GetComponent<Image>().sprite = weaponData.weapon3DImage;
        
        // Is the weapon locked
        bool isWeaponLocked = !WeaponController.Instance.IsWeaponUnlocked(selectedWeaponID);
        GameObject weaponLockedContainer = GameUtils.GetChildGameObject(weaponSubFolder, "WeaponLockedContainer", true);
        weaponLockedContainer.SetActive(isWeaponLocked);        
        if (isWeaponLocked)
        {
            // Determine whether to show the unlock button
            GameObject textUnlock = GameUtils.GetChildGameObject(weaponLockedContainer, "UnlockLabel", true);
            GameObject unlockBtn = GameUtils.GetChildGameObject(weaponLockedContainer, "UnlockBtn", true);            
            int currentLvlProgression = ChallengeController.Instance.GetChallengeProgressionIndex();
            bool hasClearedUnlockLvl = currentLvlProgression > weaponData.unlockLvl;
            // If the player has cleared the lvl unlock requirement and if there's an unlock price
            bool showUnlockButton = hasClearedUnlockLvl && weaponData.unlockPrice > 0;
            textUnlock.SetActive(showUnlockButton);
            unlockBtn.SetActive(showUnlockButton);
            if (showUnlockButton)
            {
                // Set the price
                unlockBtn.GetComponentInChildren<Text>().text = weaponData.unlockPrice.ToString("0");
            }

            // Show the unlock lvl for all weapons if the player did not clear the unlock lvl
            GameObject unlockLvlText = GameUtils.GetChildGameObject(weaponLockedContainer, "UnlockLvlText", true);
            unlockLvlText.SetActive(!hasClearedUnlockLvl);
            if (!hasClearedUnlockLvl)
            {
                unlockLvlText.GetComponent<LocalizedText>().LocalizedFullText = LocalizationManager.UNLOCK_LVL_TEXT + " " + ChallengeController.Instance.ConvertSectorNumberToChallengeNumber(weaponData.unlockLvl);
            }            
        }


        // Upgrade pack
        GameObject upgradePack = GameUtils.GetChildGameObject(weaponSubFolder, "UpgradePack", true);
        upgradePack.SetActive(!isWeaponLocked);

        // Note :: Check if the pack controller is setup.
        // It won't be during the tutorial step where
        // it goes straight into the upgrade tab.
        if (!isWeaponLocked
            && PackController.Instance.isSceneSetup)
        {
            GameUtils.GetChildGameObject(upgradePack, "WeaponPackImage").GetComponent<Image>().sprite = PackController.Instance.GetWeaponPackImage(selectedWeaponID);
            GameUtils.GetChildGameObject(upgradePack, "TextCost").GetComponent<Text>().text = PackController.Instance.GetWeaponPackPrice(selectedWeaponID).ToString("0");
        }

        // Upgrade panels
        for (int i = 0; i < (int)GameUtils.EWeaponUpgrade.COUNT; ++i)
        {
            // Re-order the weapon upgrades for the UI            
            GameUtils.EWeaponUpgrade currentWeaponUpgrade = GetOrderedWeaponUpgrade(i);

            int upgradeLvl = UpgradeController.Instance.GetWeaponUpgradeLevel(selectedWeaponID, currentWeaponUpgrade);
            GameObject upgradeGO = GameUtils.GetChildGameObject(weaponSubFolder, "prefab_upgradeInfo (" + i + ")");
            GameUtils.GetChildGameObject(upgradeGO, "textUpgradeName").GetComponent<Text>().text = GameUtils.GetWeaponUpgradeLocalizedName(selectedWeaponID, currentWeaponUpgrade);
            Text valueText = GameUtils.GetChildGameObject(upgradeGO, "textUpgradeValue").GetComponent<Text>();
            
            float boost = BoosterController.Instance.IsStatUpgraded(currentWeaponUpgrade);
            valueText.text = WeaponController.Instance.GetWeaponBaseStatWithString(selectedWeaponID, currentWeaponUpgrade, boost);
            if (boost > 1f)
            {
                valueText.color = GameUtils.COLOR_STAT_BOOSTED;
            }

            GameUtils.GetChildGameObject(upgradeGO, "textUpgradeLevel").GetComponent<Text>().text = "Lv. " + upgradeLvl;
            int nbWeaponUpgradeCards = CardsController.Instance.GetWeaponCards(selectedWeaponID, currentWeaponUpgrade);
            int nbWeaponUpgradeCardsNeeded = UpgradeController.Instance.GetAmountOfCardsPerLvl(upgradeLvl);
            bool isUpgradeMax = upgradeLvl == GameUtils.MAX_UPGRADE_LEVEL;
            bool isUpgradeReady = nbWeaponUpgradeCards >= nbWeaponUpgradeCardsNeeded && !isUpgradeMax && WeaponController.Instance.IsWeaponUnlocked(selectedWeaponID);
            upgradeGO.GetComponent<Button>().interactable = isUpgradeReady;
            GameUtils.GetChildGameObject(upgradeGO, "CardProgressPanel", true).SetActive(!isUpgradeReady);
            GameUtils.GetChildGameObject(upgradeGO, "UpgradePricePanel", true).SetActive(isUpgradeReady);
            if (!isUpgradeMax)
            {
                GameUtils.GetChildGameObject(upgradeGO, "cardsQty", true).GetComponent<Text>().text =
                LocalizationManager.Instance.GetLocalizedString(LocalizationManager.CARDS_TEXT) + " "
                + nbWeaponUpgradeCards + "/" + nbWeaponUpgradeCardsNeeded;
                // BUG :: Only one slider doesn't work, no idea why.
                // Stacking 2 sliders to work around the bug.
                GameUtils.GetChildGameObject(upgradeGO, "cardProgressBar (1)", true).GetComponent<Slider>().value = (float)nbWeaponUpgradeCards / (float)nbWeaponUpgradeCardsNeeded;
                GameUtils.GetChildGameObject(upgradeGO, "cardProgressBar", true).GetComponent<Slider>().value = (float)nbWeaponUpgradeCards / (float)nbWeaponUpgradeCardsNeeded;
            }
            else
            {
                GameUtils.GetChildGameObject(upgradeGO, "cardsQty", true).GetComponent<Text>().text = "MAX";
                // BUG :: Only one slider doesn't work, no idea why.
                // Stacking 2 sliders to work around the bug.
                GameUtils.GetChildGameObject(upgradeGO, "cardProgressBar (1)", true).GetComponent<Slider>().value = 1;
                GameUtils.GetChildGameObject(upgradeGO, "cardProgressBar", true).GetComponent<Slider>().value = 1;
            }            
            GameUtils.GetChildGameObject(upgradeGO, "textPrice", true).GetComponent<Text>().text = UpgradeController.Instance.GetCreditCostPerUpgradeLvl(upgradeLvl) + "";
        }


        // Refresh weapon selectors
        UpdateAllWeaponSelectors();
    }

    private GameUtils.EWeaponUpgrade GetOrderedWeaponUpgrade(int index)
    {
        GameUtils.EWeaponUpgrade currentWeaponUpgrade = GameUtils.EWeaponUpgrade.COUNT;
        switch (index)
        {
            case 0:
                currentWeaponUpgrade = GameUtils.EWeaponUpgrade.GROUND;
                break;
            case 1:
                currentWeaponUpgrade = GameUtils.EWeaponUpgrade.AIR;
                break;
            case 2:
                currentWeaponUpgrade = GameUtils.EWeaponUpgrade.SPEED;
                break;
            case 3:
                currentWeaponUpgrade = GameUtils.EWeaponUpgrade.EXPLOSION_RADIUS;
                break;
            case 4:
                currentWeaponUpgrade = GameUtils.EWeaponUpgrade.RELOAD;
                break;
            case 5:
                currentWeaponUpgrade = GameUtils.EWeaponUpgrade.REGEN;
                break;
            case 6:
                currentWeaponUpgrade = GameUtils.EWeaponUpgrade.VISIBILITY_RADIUS;
                break;
            case 7:
                currentWeaponUpgrade = GameUtils.EWeaponUpgrade.HEALTH;
                break;
        }

        return currentWeaponUpgrade;
    }

    private int GetAllWeaponUpgradeReadyAmount()
    {
        int allWeaponUpgradeReadyAmount = 0;
        // For each weapon
        for (int i = (int)GameUtils.BaseID.MISSILE; i < (int)GameUtils.BaseID.CITY; ++i)
        {
            // If weapon is unlocked
            if (WeaponController.Instance.IsWeaponUnlocked((GameUtils.BaseID)i))
            {
                allWeaponUpgradeReadyAmount += GetWeaponUpgradeReadyAmount((GameUtils.BaseID)i);
            }            
        }
        return allWeaponUpgradeReadyAmount;
    }

    private int GetWeaponUpgradeReadyAmount(GameUtils.BaseID weaponID)
    {
        int weaponUpgradeReadyAmount = 0;
        // For each weapon upgrades
        for (int i = 0; i < (int)GameUtils.EWeaponUpgrade.COUNT; ++i)
        {
            int upgradeLvl = UpgradeController.Instance.GetWeaponUpgradeLevel(weaponID, (GameUtils.EWeaponUpgrade)i);
            int nbWeaponUpgradeCards = CardsController.Instance.GetWeaponCards(weaponID, (GameUtils.EWeaponUpgrade)i);
            int nbWeaponUpgradeCardsNeeded = UpgradeController.Instance.GetAmountOfCardsPerLvl(upgradeLvl);
            bool isUpgradeMax = upgradeLvl == GameUtils.MAX_UPGRADE_LEVEL;
            bool isUpgradeReady = nbWeaponUpgradeCards >= nbWeaponUpgradeCardsNeeded && !isUpgradeMax;
            if (isUpgradeReady)
            {
                weaponUpgradeReadyAmount++;
            }
        }
        return weaponUpgradeReadyAmount;
    }

    private void UpdateAllWeaponSelectors()
    {
        // For each weapon
        for (int i = (int)GameUtils.BaseID.MISSILE; i < (int)GameUtils.BaseID.CITY; ++i)
        {
            GameUtils.BaseID weaponID = (GameUtils.BaseID)i;
            UpdateWeaponSelector(weaponID);            
        }
    }

    private void UpdateWeaponSelector(GameUtils.BaseID weaponID)
    {
        // Note :: This is needed because the BaseID enum is not
        // the same order as the UI elements from the menu
        int selectorUpgradeIndex = 0;
        switch (weaponID)
        {
            case GameUtils.BaseID.ARTILLERY:
                selectorUpgradeIndex = 0;
                break;
            case GameUtils.BaseID.FLAK:
                selectorUpgradeIndex = 1;
                break;
            case GameUtils.BaseID.MISSILE:
                selectorUpgradeIndex = 2;
                break;
            case GameUtils.BaseID.LASER:
                selectorUpgradeIndex = 3;
                break;
            case GameUtils.BaseID.RAILGUN:
                selectorUpgradeIndex = 4;
                break;
            case GameUtils.BaseID.ROCKETS:
                selectorUpgradeIndex = 5;
                break;
        }

        // Update selector visual
        GameObject selectorUpgradeGO = GameUtils.GetChildGameObject(weaponSubFolder, "prefab_selectorUpgrade (" + selectorUpgradeIndex + ")");
        Image leftSide = GameUtils.GetChildGameObject(selectorUpgradeGO, "leftSide").GetComponent<Image>();
        Image rightSide = GameUtils.GetChildGameObject(selectorUpgradeGO, "rightSide").GetComponent<Image>();
        leftSide.enabled = rightSide.enabled = (selectedWeaponID == weaponID);

        // Update the weapon upgrade ready amount
        UpdateWeaponUpgradeReadyAmount(weaponID, selectorUpgradeGO);

        // Update if locked
        bool isWeaponLocked = !WeaponController.Instance.IsWeaponUnlocked(weaponID);
        GameUtils.GetChildGameObject(selectorUpgradeGO, "Locked", true).SetActive(isWeaponLocked);
    }
    
    private void UpdateWeaponUpgradeReadyAmount(GameUtils.BaseID weaponID, GameObject selectorUpgradeGO)
    {
        int weaponUpgradeReadyAmount = GetWeaponUpgradeReadyAmount(weaponID);
        
        GameObject weaponUpgradeReadyAmountGO = GameUtils.GetChildGameObject(selectorUpgradeGO, "prefab_newAmountIcon", true);
        bool hasWeaponUpgradeReady = weaponUpgradeReadyAmount > 0;
        weaponUpgradeReadyAmountGO.SetActive(hasWeaponUpgradeReady);
        if (hasWeaponUpgradeReady)
        {
            weaponUpgradeReadyAmountGO.GetComponentInChildren<Text>().text = weaponUpgradeReadyAmount + "";
        }
    }
    
    // Note :: For tutorial
    public void UpgradeArtilleryStat(GameUtils.EWeaponUpgrade weaponUpgradeID)
    {
        WeaponUpgradeButtonClicked(weaponUpgradeID);
    }

    private void WeaponUpgradeButtonClicked(GameUtils.EWeaponUpgrade weaponUpgradeID)
    {
        UpgradeController.Instance.UpgradeWeapon(selectedWeaponID, weaponUpgradeID, false);

        // Refresh weapon UI
        RefreshSelectedWeaponUI();
    }
#endregion // Weapon UI

#region Consumable UI
    private void RefreshSelectedConsumableUI()
    {
        GameUtils.GetChildGameObject(consumableSubFolder, "TextConsumableName").GetComponent<Text>().text = GameUtils.GetConsumableLocalizedName(selectedConsumableID).ToUpper();
        GameUtils.GetChildGameObject(consumableSubFolder, "TextConsumableDesc").GetComponent<Text>().text = GameUtils.GetConsumableLocalizedDesc(selectedConsumableID);
        GameUtils.GetChildGameObject(consumableSubFolder, "ImageConsumable").GetComponent<Image>().sprite = GameUtils.GetConsumableIcon(selectedConsumableID, false);
        GameUtils.GetChildGameObject(consumableSubFolder, "TextConsumableQty").GetComponent<Text>().text = "x " + ConsumableController.Instance.GetConsumableAmount(selectedConsumableID);
        // REFACTOR :: Separate all the localized texts
        GameUtils.GetChildGameObject(consumableSubFolder, "TextConsumableStats").GetComponent<Text>().text = GameUtils.GetConsumableLocalizedStats(selectedConsumableID);

        // Is the consumable locked
        bool isConsumableLocked = !ConsumableController.Instance.IsConsumableUnlocked(selectedConsumableID);
        GameObject consumableLockedContainer = GameUtils.GetChildGameObject(consumableSubFolder, "ConsumableLockedContainer", true);
        consumableLockedContainer.SetActive(isConsumableLocked);
        if (isConsumableLocked)
        {
            // Show the unlock lvl for all consumables if the player did not clear the unlock lvl
            GameObject unlockLvlText = GameUtils.GetChildGameObject(consumableLockedContainer, "UnlockLvlText", true);
            int levelUnlock = ConsumableController.Instance.GetConsumableLvlUnlock(selectedConsumableID);
            unlockLvlText.GetComponent<LocalizedText>().LocalizedFullText = LocalizationManager.UNLOCK_LVL_TEXT + " " + ChallengeController.Instance.ConvertSectorNumberToChallengeNumber(levelUnlock);
        }

        // Support pack
        GameObject supportPack = GameUtils.GetChildGameObject(consumableSubFolder, "SupportPack");
        GameObject creditAmountGO = GameUtils.GetChildGameObject(supportPack, "creditAmount", true);
        GameObject goldAmountGO = GameUtils.GetChildGameObject(supportPack, "goldAmount", true);
        // Set the currency reward image and amount
        if (PackController.Instance.UpgradeSupportPack.currencyType == GameUtils.ECurrencyType.Credit)
        {
            creditAmountGO.SetActive(true);
            creditAmountGO.GetComponent<Text>().text = PackController.Instance.UpgradeSupportPack.packPrice.ToString("0");

            goldAmountGO.SetActive(false);

            GameUtils.GetChildGameObject(supportPack, "creditIcon", true).SetActive(true);
            GameUtils.GetChildGameObject(supportPack, "goldIcon", true).SetActive(false);
        }
        else if (PackController.Instance.UpgradeSupportPack.currencyType == GameUtils.ECurrencyType.Gold)
        {
            goldAmountGO.SetActive(true);
            goldAmountGO.GetComponent<Text>().text = PackController.Instance.UpgradeSupportPack.packPrice.ToString("0");

            creditAmountGO.SetActive(false);

            GameUtils.GetChildGameObject(supportPack, "goldIcon", true).SetActive(true);
            GameUtils.GetChildGameObject(supportPack, "creditIcon", true).SetActive(false);
        }

        UpdateAllConsumableSelectors();
    }

    private void UpdateAllConsumableSelectors()
    {
        // For each consumable
        for (int i = (int)GameUtils.EConsumable.Slower; i < (int)GameUtils.EConsumable.Count; ++i)
        {
            GameUtils.EConsumable consumableID = (GameUtils.EConsumable)i;
            UpdateConsumableSelector(consumableID);
        }
    }

    private void UpdateConsumableSelector(GameUtils.EConsumable consumableID)
    {
        // Note :: This is needed because the EConsumable enum is not
        // the same order as the UI elements from the menu
        int selectorUpgradeIndex = 0;
        switch (consumableID)
        {
            case GameUtils.EConsumable.Repair:
                selectorUpgradeIndex = 0;
                break;
            case GameUtils.EConsumable.Reload:
                selectorUpgradeIndex = 1;
                break;
            case GameUtils.EConsumable.Pulse:
                selectorUpgradeIndex = 2;
                break;
            case GameUtils.EConsumable.Slower:
                selectorUpgradeIndex = 3;
                break;
            case GameUtils.EConsumable.OhShit:
                selectorUpgradeIndex = 4;
                break;
        }

        // Update the selector visual
        GameObject selectorConsumableGO = GameUtils.GetChildGameObject(consumableSubFolder, "prefab_selectorConsummable (" + selectorUpgradeIndex + ")");
        Image leftSide = GameUtils.GetChildGameObject(selectorConsumableGO, "leftSide").GetComponent<Image>();
        Image rightSide = GameUtils.GetChildGameObject(selectorConsumableGO, "rightSide").GetComponent<Image>();
        leftSide.enabled = rightSide.enabled = (selectedConsumableID == consumableID);

        // Update if locked
        bool isConsumableLocked = !ConsumableController.Instance.IsConsumableUnlocked(consumableID);
        GameUtils.GetChildGameObject(selectorConsumableGO, "Locked", true).SetActive(isConsumableLocked);
    }
#endregion

    public void ShowUnlockedWeaponPopup(GameUtils.BaseID weaponID, int unlockLvl)
    {
        unlockedItemPanel.SetActive(true);
        FlowStateController.Instance.RegisterBackButton(HideUnlockedItemPopup);

        // Populate the info for the weapon unlock
        sectorText.text = LocalizationManager.Instance.GetLocalizedString(LocalizationManager.LEVEL_TEXT) + " " + unlockLvl.ToString("000");
        unlockedItemImage.sprite = WeaponController.Instance.FindWeaponData(weaponID).baseImage;
        unlockedItemText.text = GameUtils.GetWeaponLocalizedName(weaponID) + " " + LocalizationManager.Instance.GetLocalizedString(LocalizationManager.WEAPON_UNLOCKED_TEXT);

        selectedWeaponID = weaponID;
    }

    public void ShowUnlockedConsumablePopup(GameUtils.EConsumable consumableID, int unlockLvl)
    {
        unlockedItemPanel.SetActive(true);
        FlowStateController.Instance.RegisterBackButton(HideUnlockedItemPopup);

        // Populate the info for the weapon unlock
        sectorText.text = LocalizationManager.Instance.GetLocalizedString(LocalizationManager.LEVEL_TEXT) + " " + unlockLvl.ToString("000");
        unlockedItemImage.sprite = GameUtils.GetConsumableIcon(consumableID, true);
        unlockedItemText.text = GameUtils.GetConsumableLocalizedName(consumableID) + " " + LocalizationManager.Instance.GetLocalizedString(LocalizationManager.CONSUMABLE_UNLOCKED_TEXT);

        selectedConsumableID = consumableID;
    }

    private void HideUnlockedItemPopup()
    {
        unlockedItemPanel.SetActive(false);
        FlowStateController.Instance.UnregisterBackButton(HideUnlockedItemPopup);

        // Activate the upgrade tab
        ActivateMenuFolder(upgradesFolder);

        // Activate the weapon or consumable sub tab
        if (selectedWeaponID != GameUtils.BaseID.ARTILLERY)
        {
            ActivateUpgradeSubFolder(weaponSubFolder);
        }
        else if (selectedConsumableID != GameUtils.EConsumable.Repair)
        {
            ActivateUpgradeSubFolder(consumableSubFolder);
        }
    }
#endregion

#region Update Profile Section
    private void UpdateProfile()
    {
        if (activeMenuFolder == profileFolder)
        {
            if (activeProfileSubFolder == achievementSubFolder) UpdateAchievements();
        }
    }

    private void UpdateAchievements()
    {    
    }
#endregion

#region Update Options Section
    private void UpdateOptions()
    {
        if (activeMenuFolder == optionsFolder)
        {
            if (activeOptionsSubFolder == optionsSubFolder) UpdateSettings();
        }
    }

    private void UpdateSettings()
    {
    }

    private void SetOptionsValue()
    {
        SetLanguageValue();
        SetAudioValues();
        SetGraphicSettingToggle();
        SetDragAndReleaseToggle();
    }

    private void SetLanguageValue()
    {
        switch (LocalizationManager.Instance.CurrentLanguage)
        {
            case SystemLanguage.English:
                languageSelector.value = 0;
                break;
            case SystemLanguage.French:
                languageSelector.value = 1;
                break;
            case SystemLanguage.German:
                languageSelector.value = 2;
                break;
            default:
                languageSelector.value = 0;
                break;
        }
    }

    private void LanguageValueChanged()
    {
        switch (languageSelector.value)
        {
            case 0:
                if (LocalizationManager.Instance.CurrentLanguage != SystemLanguage.English)
                {
                    LocalizationManager.Instance.CurrentLanguage = SystemLanguage.English;
                }
                break;
            case 1:
                if (LocalizationManager.Instance.CurrentLanguage != SystemLanguage.French)
                {
                    LocalizationManager.Instance.CurrentLanguage = SystemLanguage.French;
                }
                break;
            case 2:
                if (LocalizationManager.Instance.CurrentLanguage != SystemLanguage.German)
                {
                    LocalizationManager.Instance.CurrentLanguage = SystemLanguage.German;
                }
                break;
        }
    }

    private void SetAudioValues()
    {
        musicSlider.value = AudioManager.Instance.GetMusicVolumeJSON();
        soundSlider.value = AudioManager.Instance.GetSoundVolumeJSON();
    }

    private void MusicSliderValueChanged()
    {
        GameUtils.GetChildGameObject(musicSlider.transform.parent.gameObject, "TextPourcentage").GetComponent<Text>().text = ((int)(musicSlider.value * 100)) + "%";
        AudioManager.Instance.SetMusicVolume(musicSlider.value);
    }

    private void SoundSliderValueChanged()
    {
        GameUtils.GetChildGameObject(soundSlider.transform.parent.gameObject, "TextPourcentage").GetComponent<Text>().text = ((int)(soundSlider.value * 100)) + "%";
        AudioManager.Instance.SetSoundVolume(soundSlider.value);
    }

    private void SetGraphicSettingToggle()
    {
        if (SettingsController.Instance.GetGraphicSettings() == SettingsController.EGraphicSetting.Performance)
        {
            performanceToggle.isOn = true;
            qualityToggle.isOn = false;
            normalToggle.isOn = false;
        }
        else if (SettingsController.Instance.GetGraphicSettings() == SettingsController.EGraphicSetting.Normal)
        {
            normalToggle.isOn = true;
            performanceToggle.isOn = false;
            qualityToggle.isOn = false;
        }
        else
        {
            normalToggle.isOn = false;
            performanceToggle.isOn = false;
            qualityToggle.isOn = true;
        }
    }

    private void SetDragAndReleaseToggle()
    {
        dragAndReleaseToggle.isOn = SettingsController.Instance.GetDragAndRelease();
    }

    private void DragAndReleaseValueChanged()
    {
        SettingsController.Instance.SetDragAndRelease(dragAndReleaseToggle.isOn);
    }

    private void GraphicSettingValueChanged(SettingsController.EGraphicSetting newGraphicSetting)
    {
        // Only set the graphic settings for the toggle that has been turned on
        // Note :: When toggling, this function get called twice, one for the 
        // toggle on event and one for the other toggle off event.
        if ((newGraphicSetting == SettingsController.EGraphicSetting.Performance && performanceToggle.isOn)
            || (newGraphicSetting == SettingsController.EGraphicSetting.Quality && qualityToggle.isOn) 
            || (newGraphicSetting == SettingsController.EGraphicSetting.Normal && normalToggle.isOn))
        {
            SettingsController.Instance.SetGraphicSettings(newGraphicSetting);
        }        
    }

    private void SetCreditsPopup(bool enable)
    {
        // Note :: Needed since the credits popup is disabled on awake
        GameUtils.GetChildGameObject(menuCanvas, "CreditsPopup", true).SetActive(enable);
    }
#endregion
    
    // Note :: For tutorial
    public void OpenUpgrade()
    {
        ActivateMenuFolder(upgradesFolder);
    }

    // Note :: For tutorial
    public void OpenPlay()
    {
        ActivateMenuFolder(playFolder);
    }

    private void OpenCreditPacksTab()
    {
        ActivateMenuFolder(storeFolder);
        ActivateStoreSubFolder(creditPacksSubFolder);
    }

    private void OpenGoldPacksTab()
    {
        ActivateMenuFolder(storeFolder);
        ActivateStoreSubFolder(goldPacksSubFolder);
    }
    
    private void ActivateMenuFolder(GameObject activeFolder)
    {
        // Manage folders
        playFolder.SetActive(false);
        storeFolder.SetActive(false);
        upgradesFolder.SetActive(false);
        profileFolder.SetActive(false);
        optionsFolder.SetActive(false);

        this.activeMenuFolder = activeFolder;
        activeFolder.SetActive(true);

        // Manage tabs
        playButton.gameObject.SetActive(true);
        storeButton.gameObject.SetActive(true);
        upgradesButton.gameObject.SetActive(true);
        profileButton.gameObject.SetActive(true);
        optionsButton.gameObject.SetActive(true);
		exitGameButton.gameObject.SetActive (true);
        playActiveTab.SetActive(false);
        storeActiveTab.SetActive(false);
        upgradesActiveTab.SetActive(false);
        profileActiveTab.SetActive(false);
        optionsActiveTab.SetActive(false);
        if (activeFolder == playFolder)
        {
            playActiveTab.SetActive(true);
            playButton.gameObject.SetActive(false);

            ActivatePlaySubFolder(activePlaySubFolder);
        }
        else if (activeFolder == storeFolder)
        {
            storeActiveTab.SetActive(true);
            storeButton.gameObject.SetActive(false);

            ActivateStoreSubFolder(activeStoreSubFolder);
        }
        else if (activeFolder == upgradesFolder)
        {
            upgradesActiveTab.SetActive(true);
            upgradesButton.gameObject.SetActive(false);

            ActivateUpgradeSubFolder(activeUpgradeSubFolder);         
        }
        else if (activeFolder == profileFolder)
        {
            profileActiveTab.SetActive(true);
            profileButton.gameObject.SetActive(false);

            ActivateProfileSubFolder(activeProfileSubFolder);

            AchievementController.Instance.UpdateAchievementUI();
        }
        else if (activeFolder == optionsFolder)
        {
            optionsActiveTab.SetActive(true);
            optionsButton.gameObject.SetActive(false);

            ActivateOptionsSubFolder(activeOptionsSubFolder);
        }
    }
        
    private void ActivatePlaySubFolder(GameObject activePlayFolder)
    {
        // Manage screens
        campaignSubFolder.SetActive(false);
        lastStandSubFolder.SetActive(false);
        eventsSubFolder.SetActive(false);

        this.activePlaySubFolder = activePlayFolder;
        activePlayFolder.SetActive(true);

        // Manage tabs
        campaignButton.gameObject.SetActive(true);
        //campaignButton.GetComponentInChildren<LocalizedText>().LocalizedTextID = LocalizationManager.CAMPAIGN_TAB;
        lastStandButton.gameObject.SetActive(true);
        //lastStandButton.GetComponentInChildren<LocalizedText>().LocalizedTextID = LocalizationManager.LAST_STAND_TAB;
        eventsButton.gameObject.SetActive(true);
        //eventsButton.GetComponentInChildren<LocalizedText>().LocalizedTextID = LocalizationManager.EVENTS_TAB;
        campaignActiveTab.SetActive(false);
        //campaignActiveTab.GetComponentInChildren<LocalizedText>().LocalizedTextID = LocalizationManager.CAMPAIGN_TAB;
        lastStandActiveTab.SetActive(false);
        //lastStandActiveTab.GetComponentInChildren<LocalizedText>().LocalizedTextID = LocalizationManager.LAST_STAND_TAB;
        eventsActiveTab.SetActive(false);
        //eventsActiveTab.GetComponentInChildren<LocalizedText>().LocalizedTextID = LocalizationManager.EVENTS_TAB;
        if (activePlayFolder == campaignSubFolder)
        {
            campaignActiveTab.SetActive(true);
            campaignButton.gameObject.SetActive(false);

            ChallengeController.Instance.CurrentChallengeTab = ChallengeData.EChallengeType.Mission;
        }
        else if (activePlayFolder == lastStandSubFolder)
        {
            lastStandActiveTab.SetActive(true);
            lastStandButton.gameObject.SetActive(false);

            ChallengeController.Instance.CurrentChallengeTab = ChallengeData.EChallengeType.LastStand;
        }
        // TODO :: OPERATIONS TAB
        else if (activePlayFolder == eventsSubFolder)
        {
            eventsActiveTab.SetActive(true);
            eventsButton.gameObject.SetActive(false);

            ChallengeController.Instance.CurrentChallengeTab = ChallengeData.EChallengeType.Event;
        }
    }

    private void ActivateStoreSubFolder(GameObject storeFolder)
    {
        // Manage screens
        standardPacksSubFolder.SetActive(false);
        goldPacksSubFolder.SetActive(false);
        creditPacksSubFolder.SetActive(false);

        activeStoreSubFolder = storeFolder;
        activeStoreSubFolder.SetActive(true);

        // Manage tabs
        standardPacksButton.gameObject.SetActive(true);
        //packsButton.GetComponentInChildren<LocalizedText>().LocalizedTextID = LocalizationManager.PACKS_TAB;
        goldPacksButton.gameObject.SetActive(true);
        //boostersButton.GetComponentInChildren<LocalizedText>().LocalizedTextID = LocalizationManager.PACKS_TAB;
        creditPacksButton.gameObject.SetActive(true);
        //currenciesButton.GetComponentInChildren<LocalizedText>().LocalizedTextID = LocalizationManager.PACKS_TAB;
        standardPacksActiveTab.SetActive(false);
        //packsActiveTab.GetComponentInChildren<LocalizedText>().LocalizedTextID = LocalizationManager.PACKS_TAB;
        goldPacksActiveTab.SetActive(false);
        //boostersActiveTab.GetComponentInChildren<LocalizedText>().LocalizedTextID = LocalizationManager.PACKS_TAB;
        creditPacksActiveTab.SetActive(false);
        //currenciesActiveTab.GetComponentInChildren<LocalizedText>().LocalizedTextID = LocalizationManager.PACKS_TAB;
        if (activeStoreSubFolder == standardPacksSubFolder)
        {
            standardPacksActiveTab.SetActive(true);
            standardPacksButton.gameObject.SetActive(false);
        }
        else if (activeStoreSubFolder == goldPacksSubFolder)
        {
            goldPacksActiveTab.SetActive(true);
            goldPacksButton.gameObject.SetActive(false);
        }
        else if (activeStoreSubFolder == creditPacksSubFolder)
        {
            creditPacksActiveTab.SetActive(true);
            creditPacksButton.gameObject.SetActive(false);
        }
    }

    private void ActivateUpgradeSubFolder(GameObject upgradeSubFolder)
    {
        // Manage screens
        weaponSubFolder.SetActive(false);
        consumableSubFolder.SetActive(false);

        activeUpgradeSubFolder = upgradeSubFolder;
        activeUpgradeSubFolder.SetActive(true);

        // Manage sub-tabs
        weaponButton.gameObject.SetActive(true);
        consumableButton.gameObject.SetActive(true);
        weaponActiveTab.SetActive(false);
        consumableActiveTab.SetActive(false);

        if (activeUpgradeSubFolder == weaponSubFolder)
        {
            weaponActiveTab.SetActive(true);
            weaponButton.gameObject.SetActive(false);

            RefreshSelectedWeaponUI();
        }
        else if (activeUpgradeSubFolder == consumableSubFolder)
        {
            consumableActiveTab.SetActive(true);
            consumableButton.gameObject.SetActive(false);

            RefreshSelectedConsumableUI();
        }
    }

    private void ActivateProfileSubFolder(GameObject profileSubFolder)
    {
        // Manage screens
        activeProfileSubFolder = profileSubFolder;
        activeProfileSubFolder.SetActive(true);

        // Manage sub-tabs
        achievementActiveTab.SetActive(false);
        if (activeProfileSubFolder == achievementSubFolder)
        {
            achievementActiveTab.SetActive(true);
            achievementButton.gameObject.SetActive(false);
        }
    }

    private void ActivateOptionsSubFolder(GameObject optionsFolder)
    {
        // Manage screens
        activeOptionsSubFolder = optionsFolder;
        activeOptionsSubFolder.SetActive(true);

        // Manage sub-tabs
        settingsActiveTab.SetActive(false);
        //settingsActiveTab.GetComponentInChildren<LocalizedText>().LocalizedTextID = LocalizationManager.OPTIONS_TAB;
        if (activeOptionsSubFolder == optionsSubFolder)
        {
            settingsActiveTab.SetActive(true);
            settingsButton.gameObject.SetActive(false);
        }
    }
    
    private void SelectWeapon(GameUtils.BaseID weaponID)
    {
        selectedWeaponID = weaponID;
        RefreshSelectedWeaponUI();
    }

    private void UnlockSelectedWeapon()
    {
        int unlockPrice = WeaponController.Instance.FindWeaponData(selectedWeaponID).unlockPrice;
        if (CurrencyController.Instance.HasEnoughCurrency(unlockPrice, GameUtils.ECurrencyType.Credit))
        {
            // Decrease credits
            CurrencyController.Instance.ModifyCreditsAmount(-unlockPrice, GameUtils.CreditsSource.WeaponUnlocked, DataController.SAVEPRIORITY.QUEUE);

            // Unlock weapon and save
            WeaponController.Instance.UnlockWeapon(selectedWeaponID, false);

            // Update the weapon packs interactibility in the store
            PackController.Instance.RefreshWeaponPacksInteractibility();

            // Refresh UI
            RefreshSelectedWeaponUI();
        }
    }

    private void BuyUpgradePack()
    {
        PackController.Instance.WeaponPackBuyButtonClicked(selectedWeaponID);
    }

#if UNITY_EDITOR || DEBUG_MOBILE
    private void SetWeaponUpgradesLevel(int upgradeLevel)
    {
        UpgradeController.Instance.SetWeaponUpgradesLevel(selectedWeaponID, upgradeLevel);

        // Refresh UI
        RefreshSelectedWeaponUI();
    }

    private void MaxWeaponCards()
    {
        CardsController.Instance.MaxWeaponCards(selectedWeaponID);

        // Refresh UI
        RefreshSelectedWeaponUI();
    }
#endif

    private void SelectConsumable(GameUtils.EConsumable consumableType)
    {
        selectedConsumableID = consumableType;
        RefreshSelectedConsumableUI();
    }

    private void BuySupportPack()
    {
        PackController.Instance.SupportPackBuyButtonClicked();
    }

#if UNITY_EDITOR || DEBUG_MOBILE
    private void SetConsumableAmount(int consumableAmount)
    {
        ConsumableController.Instance.ModifyAllConsumablesAmount(consumableAmount, DataController.SAVEPRIORITY.NORMAL);

        // Refresh UI
        RefreshSelectedConsumableUI();
    }
#endif

#if UNITY_EDITOR || RELEASE_BETA
    private void ResetButtonClicked()
    {
        DataController.Instance.ResetAllData();

#if UNITY_IOS
        if (Social.localUser.authenticated)
        {
            GameCenterPlatform.ResetAllAchievements((resetResult) =>
            {
                Debug.Log((resetResult) ? "Reset done." : "Reset failed.");
            });
        }
#endif
    }
#endif

#if UNITY_ANDROID
    private void GPGSButtonClicked()
    {
        if (PlayGamesPlatform.Instance.IsAuthenticated())
        {
            PlayGamesPlatform.Instance.SignOut();
        }
        else
        {
            PlayGamesPlatform.Instance.Authenticate(FlowStateController.Instance.OnAuthenticationCallbackAndroid);
        }
    }
#endif

#if UNITY_IOS
    private void GCButtonClicked()
    {
        if (!Social.localUser.authenticated)
        {
            Social.localUser.Authenticate(FlowStateController.Instance.OnAuthenticationCallbackiOS);
        }
    }
#endif

    public void EnableMenuUI(bool isEnable)
    {
        menuCanvas.GetComponent<CanvasGroup>().interactable = isEnable;
        menuCanvas.GetComponent<CanvasGroup>().blocksRaycasts = isEnable;
    }

    public override void OnDestroy()
    {
        base.OnDestroy();
        SceneManager.sceneUnloaded -= OnSceneUnloaded;
    }
}

using GooglePlayGames;
using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using Steamworks;

public class LeaderboardController : UnitySingleton<LeaderboardController>
{
    private static bool isInitialized = false;

    [Serializable]
    public class LeaderboardData
    {
        [Serializable]
        public class LeaderboardEntry
        {
            public int bestScore;
            public int lastScore;

            public LeaderboardEntry()
            {
                bestScore = 0;
                lastScore = 0;
            }

            public LeaderboardEntry(LeaderboardEntry copy)
            {
                bestScore = copy.bestScore;
                lastScore = copy.lastScore;
            }
        }

        public Dictionary<string, LeaderboardEntry> boards;

        public LeaderboardData()
        {
            boards = new Dictionary<string, LeaderboardEntry>();
        }

        public LeaderboardData(LeaderboardData copy)
        {
            boards = new Dictionary<string, LeaderboardEntry>(copy.boards);
        }
    }

    private LeaderboardData leaderboards;
    public LeaderboardData Leaderboards
    {
        get { return leaderboards; }
    }

    public override void Awake()
    {
        base.Awake();
        if (!isInitialized)
        {
            // Always keep the controller loaded through the scene changes
            DontDestroyOnLoad(gameObject);
            isInitialized = true;
        }
    }

    public void ShowLeaderboard(string leaderBoardNameID)
    {
#if UNITY_ANDROID
        if (PlayGamesPlatform.Instance.IsAuthenticated())
        {
            PlayGamesPlatform.Instance.ShowLeaderboardUI(leaderBoardNameID);
        }
        else
        {
            PlayGamesPlatform.Instance.Authenticate(FlowStateController.Instance.OnAuthenticationCallbackAndroid);
        }
#elif UNITY_IOS
        if (Social.localUser.authenticated)
        {
            Social.ShowLeaderboardUI();
        }
#endif

		SteamAPICall_t hSteamAPICall = SteamUserStats.FindLeaderboard(leaderBoardNameID);
    }

    public void PostScoreToLeaderboard(GameUtils.EGameDifficulty difficulty, int score)
    {
        string leaderBoardNameID = GetLeaderboardNameID(difficulty);

        // only really post on live builds
#if UNITY_ANDROID && RELEASE_LIVE && ENABLE_LEADERBOARDS
        if (PlayGamesPlatform.Instance.IsAuthenticated())
        {
            Social.ReportScore(score, leaderBoardNameID, ScorePostedCallback);
            Debug.Log("LeaderboardController:PostScoreToLeaderboard() Leaderboard: " + leaderBoardNameID + " score: " + score);
        }
#elif UNITY_IOS && RELEASE_LIVE && ENABLE_LEADERBOARDS
        if (Social.localUser.authenticated)
        {
            Social.ReportScore(score, leaderBoardNameID, ScorePostedCallback);
            Debug.Log("LeaderboardController:PostScoreToLeaderboard() Leaderboard: " + leaderBoardNameID + " score: " + score);
        }
#endif
        UpdateScore(leaderBoardNameID, score);
    }

    private void UpdateScore(string leaderBoardNameID, int score)
    {
        if (leaderboards != null)
        {
            // If it's the first score for this leaderboard
            if (!leaderboards.boards.ContainsKey(leaderBoardNameID))
            {
                LeaderboardData.LeaderboardEntry entry = new LeaderboardData.LeaderboardEntry();
                entry.bestScore = score;
                leaderboards.boards.Add(leaderBoardNameID, entry);
                SaveLeaderboardController(DataController.SAVEPRIORITY.NORMAL);
            }
            // If there's already a score for this leaderboard
            else
            {
                // Update the last score
                leaderboards.boards[leaderBoardNameID].lastScore = score;

                // Update the best score if it's the case
                if (leaderboards.boards[leaderBoardNameID].bestScore < score)
                {
                    leaderboards.boards[leaderBoardNameID].bestScore = score;
                }
                SaveLeaderboardController(DataController.SAVEPRIORITY.NORMAL);
            }
        }
    }

    public int GetLastScore(string leaderBoardNameID)
    {
        if (leaderboards != null && leaderboards.boards.ContainsKey(leaderBoardNameID))
        {
            return leaderboards.boards[leaderBoardNameID].lastScore;
        }
        return 0;
    }

    public int GetBestScore(string leaderBoardNameID)
    {
        if (leaderboards != null && leaderboards.boards.ContainsKey(leaderBoardNameID))
        {
            return leaderboards.boards[leaderBoardNameID].bestScore;
        }
        return 0;
    }

    private void ScorePostedCallback(bool success)
    {
        if (success)
        {
            Debug.Log("LeaderboardController:ScorePostedCallback() Successfully posted score");
        }
        else
        {
            Debug.Log("LeaderboardController:ScorePostedCallback() Failed to post score");
        }
    }

    public string GetLeaderboardNameID(GameUtils.EGameDifficulty difficulty)
    {
#if UNITY_ANDROID
        switch (difficulty)
        {
            case GameUtils.EGameDifficulty.Insane:
                return GPGSConstants.leaderboard_onslaught_board;
            case GameUtils.EGameDifficulty.Hard:
                return GPGSConstants.leaderboard_raid_board;
            case GameUtils.EGameDifficulty.Normal:
            default:
                return GPGSConstants.leaderboard_skirmish_board;
        }
#elif UNITY_IOS
        switch (difficulty)
        {
            case GameUtils.EGameDifficulty.Insane:
                return GCConstants.leaderboard_onslaught_board;
            case GameUtils.EGameDifficulty.Hard:
                return GCConstants.leaderboard_raid_board;
            case GameUtils.EGameDifficulty.Normal:
            default:
                return GCConstants.leaderboard_skirmish_board;
        }
#else
        // Note :: Just to have it compile for Windows build
        return "";
#endif
    }

#region Data Handling
    public void SaveLeaderboardController(DataController.SAVEPRIORITY priority)
    {
        DataController.Instance.SaveLeaderboardData(priority);
    }

    public void LoadLeaderboardData(LeaderboardData remoteData)
    {
        if (remoteData == null)
        {
            Exception ex = JsonUtils<LeaderboardData>.Load(GameUtils.LEADERBOARD_DATA_JSON, out leaderboards, GameUtils.ELoadType.DynamicData);
            if (ex != null && ex.GetType() == typeof(FileLoadException))
            {
                ResetLeaderboardController();
            }
        }
        else
        {
            leaderboards = remoteData;
        }
    }

    public void ResetLeaderboardController()
    {
        if (leaderboards == null)
        {
            leaderboards = new LeaderboardData();
        }
        leaderboards.boards.Clear();

        SaveLeaderboardController(DataController.SAVEPRIORITY.QUEUE);
    }
#endregion
}

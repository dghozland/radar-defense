using UnityEngine;
using System.Collections.Generic;
using System.IO;

#pragma warning disable 0649

public class AdvertismentController : UnitySingleton<AdvertismentController>
{
    private System.Action<System.Exception> smartAdException;
    private static Dictionary<RewardData.ERewardType, System.DateTime> watchedAds;
    private static bool isInitialized = false;

    [HideInInspector]
    public Dictionary<RewardData.ERewardType, System.DateTime> WatchedAds
    {
        get { return watchedAds; }
        set { watchedAds = value; }
    }

    public override void Awake()
    {
        base.Awake();
        if (!isInitialized)
        {
            DontDestroyOnLoad(gameObject);
            isInitialized = true;
        }
        smartAdException += RewardAdException;
    }

    public void RequestAdvertisment(System.Action<bool, PackData> advertismentFinished, string decisionPoint, PackData pack)
    {
//#if UNITY_EDITOR
        Debug.LogWarning("Ads disabled in Editor!");
        advertismentFinished(true, pack);
    }

    private void OnRewardAdClosedHandler(bool finishedWatching)
    {

    }

    private void OnRewardedAdOpenedHandler()
    {
        Debug.Log("Rewarded Ad opened its ad.");
    }

    private void OnRewardedAdFailedToOpenHandler(string reason)
    {
        Debug.Log("Rewarded Ad failed to open its ad: " + reason);;
    }		

    private void RewardAdException(System.Exception exception)
    {
        Debug.Log("Engage encountered an error: " + exception.Message);
    }

    public bool AdvertismentReady()
    {
//#if UNITY_EDITOR
        return true;
    }

    public void SaveAdvertismentsWatched(DataController.SAVEPRIORITY priority)
    {
        DataController.Instance.SaveAdvertismentData(priority);
    }

    public bool CanWatchAdForRewardType(RewardData.ERewardType type)
    {
        if (watchedAds.ContainsKey(type))
        {
            System.DateTime lastSeen = watchedAds[type];
            if (lastSeen <= System.DateTime.UtcNow)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        return true;
    }

    public int TimeUntilAdCanBeWatched(RewardData.ERewardType type, GameUtils.ETimeType timeType)
    {
        if (watchedAds.ContainsKey(type))
        {
            System.DateTime lastSeen = watchedAds[type];
            System.TimeSpan diff = lastSeen.Subtract(System.DateTime.UtcNow);
            if (timeType == GameUtils.ETimeType.Minutes)
            {
                return diff.Minutes;
            }
            else if (timeType == GameUtils.ETimeType.Seconds)
            {
                return diff.Seconds;
            }
        }
        return 0;
    }

    public void WatchedAd(RewardData.ERewardType type)
    {
        System.DateTime time = System.DateTime.UtcNow;
        time = time.AddMinutes(GameUtils.WATCHED_AD_COOLDOWN_MINUTES);
        if (watchedAds.ContainsKey(type))
        {
            watchedAds[type] = time;
        }
        else
        {
            watchedAds.Add(type, time);
        }
        SaveAdvertismentsWatched(DataController.SAVEPRIORITY.INSTANT);
    }

    public void LoadAdvertismentsWatched(Dictionary<RewardData.ERewardType, System.DateTime> remoteAdvertisments)
    {
        if (remoteAdvertisments == null)
        {
            System.Exception ex = JsonUtils<Dictionary<RewardData.ERewardType, System.DateTime>>.Load(GameUtils.WATCHED_ADS_JSON, out watchedAds, GameUtils.ELoadType.DynamicData);
            if (ex != null
                && ex.GetType() == typeof(FileLoadException))
            {
                ResetAdvertismentsWatched();
            }
        }
        else
        {
            watchedAds = remoteAdvertisments;
        }
    }

    public void ResetAdvertismentsWatched()
    {
        if (watchedAds == null)
        {
            watchedAds = new Dictionary<RewardData.ERewardType, System.DateTime>();
        }
        else
        {
            watchedAds.Clear();
        }
        SaveAdvertismentsWatched(DataController.SAVEPRIORITY.QUEUE);
    }
}

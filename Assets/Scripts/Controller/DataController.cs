using UnityEngine;
using System.Collections.Generic;
using System.IO;
#if UNITY_ANDROID
using GooglePlayGames.BasicApi.SavedGame;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
#endif
using System;

public class DataController : UnitySingleton<DataController>
{
    private static bool achievementDataDirty = false;
    private static bool challengeDataDirty = false;
    private static bool lastStandDataDirty = false;
    private static bool operationDataDirty = false;
    private static bool consumableDataDirty = false;
    private static bool currencyDataDirty = false;
    private static bool cardDataDirty = false;
    private static bool upgradeDataDirty = false;
    private static bool advertismentDataDirty = false;
    private static bool weaponDataDirty = false;
    private static bool packDataDirty = false;
    private static bool leaderboardDirty = false;
    private static bool dailyDirty = false;

    private static bool achievementDataLoaded = false;
    private static bool challengeDataLoaded = false;
    private static bool lastStandDataLoaded = false;
    private static bool operationDataLoaded = false;
    private static bool consumableDataLoaded = false;
    private static bool currencyDataLoaded = false;
    private static bool cardDataLoaded = false;
    private static bool upgradeDataLoaded = false;
    private static bool advertismentDataLoaded = false;
    private static bool weaponDataLoaded = false;
    private static bool packDataLoaded = false;
    private static bool leaderboardLoaded = false;
    private static bool dailyLoaded = false;

    public bool AchievementDataLoaded { get { return achievementDataLoaded; } }
    public bool ChallengeDataLoaded { get { return challengeDataLoaded; } }
    public bool LastStandDataLoaded { get { return lastStandDataLoaded; } }
    public bool OperationDataLoaded { get { return operationDataLoaded; } }
    public bool ConsumableDataLoaded { get { return consumableDataLoaded; } }
    public bool CurrencyDataLoaded { get { return currencyDataLoaded; } }
    public bool CardDataLoaded { get { return cardDataLoaded; } }
    public bool UpgradeDataLoaded { get { return upgradeDataLoaded; } }
    public bool AdvertismentDataLoaded { get { return advertismentDataLoaded; } }
    public bool WeaponDataLoaded { get { return weaponDataLoaded; } }
    public bool PackDataLoaded { get { return packDataLoaded; } }
    public bool LeaderboardLoaded { get { return leaderboardLoaded; } }
    public bool DailyLoaded { get { return dailyLoaded; } }

    private const float LOCAL_SAVE_MAX_INTERVAL = 60f; // 1 min
    private const float LOCAL_SAVE_QUEUE_INTERVAL = 5f; // 5 sec
    private static bool isInitialized = false;
    private static float maxSaveTimer;
    private static float maxQueueTimer;
    private static SAVEPRIORITY savePriority = SAVEPRIORITY.NORMAL;
    private static bool isSaving = false;
#if UNITY_ANDROID && ENABLE_CLOUD_SAVE
    private static ISavedGameMetadata openSaveGameMetadata = null;
#endif

    private LocalInfo localInfo;

    [Serializable]
    private class LocalInfo
    {
        private TimeSpan totalPlayedTime;
        private DateTime lastSaved;
        private string gpgsUserId;

        public LocalInfo()
        {
            totalPlayedTime = TimeSpan.Zero;
            lastSaved = DateTime.UtcNow;
            gpgsUserId = string.Empty;
        }

        public void Setup()
        {
#if UNITY_ANDROID
            if (PlayGamesPlatform.Instance.IsAuthenticated())
            {
                gpgsUserId = PlayGamesPlatform.Instance.GetUserId();
            }
#endif
        }

        public TimeSpan TotalPlayedTime
        {
            get { return totalPlayedTime; }
            set { totalPlayedTime = value; }
        }

        public DateTime LastSaved
        {
            get { return lastSaved; }
            set { lastSaved = value; }
        }

        public string GpgsUserId
        {
            get { return gpgsUserId; }
            set { gpgsUserId = value; }
        }

        public string TotalPlayedTimeString()
        {
            return string.Format("Days: {0:D2} - {1:D2}:{2:D2}:{3:D2}", totalPlayedTime.Days, totalPlayedTime.Hours, totalPlayedTime.Minutes, totalPlayedTime.Seconds);
        }
    }

    [Serializable]
    public class SaveDataBundle
    {
        private List<AchievementController.Achievement> achievement;
        private List<ChallengeController.ChallengeJSON> challenge;
        private List<ChallengeController.LastStandJSON> lastStand;
        private List<ChallengeController.OperationJSON> operation;
        private ConsumableController.ConsumableData consumable;
        private CurrencyController.CurrencyData currency;
        private Dictionary<GameUtils.BaseID, Dictionary<GameUtils.EWeaponUpgrade, int>> card;
        private Dictionary<GameUtils.BaseID, Dictionary<GameUtils.EWeaponUpgrade, int>> upgrade;
        private Dictionary<RewardData.ERewardType, DateTime> advertisment;
        private List<GameUtils.BaseID> weapon;
        private PackController.PackSaveData pack;
        private LeaderboardController.LeaderboardData leaderboard;
        private DailyController.DailyData daily;

        public List<AchievementController.Achievement> Achievement { get { return achievement; } }
        public List<ChallengeController.ChallengeJSON> Challenge { get { return challenge; } }
        public List<ChallengeController.LastStandJSON> LastStand { get { return lastStand; } }
        public List<ChallengeController.OperationJSON> Operation { get { return operation; } }
        public ConsumableController.ConsumableData Consumable { get { return consumable; } }
        public CurrencyController.CurrencyData Currency { get { return currency; } }
        public Dictionary<GameUtils.BaseID, Dictionary<GameUtils.EWeaponUpgrade, int>> Card { get { return card; } }
        public Dictionary<GameUtils.BaseID, Dictionary<GameUtils.EWeaponUpgrade, int>> Upgrade { get { return upgrade; } }
        public Dictionary<RewardData.ERewardType, DateTime> Advertisment { get { return advertisment; } }
        public List<GameUtils.BaseID> Weapon { get { return weapon; } }
        public PackController.PackSaveData Pack { get { return pack; } }
        public LeaderboardController.LeaderboardData Leaderboard { get { return leaderboard; } }
        public DailyController.DailyData Daily { get { return daily; } }

        public SaveDataBundle()
        {
            achievement = new List<AchievementController.Achievement>(AchievementController.Instance.Achievements);
            challenge = new List<ChallengeController.ChallengeJSON>(ChallengeController.Instance.ChallengesJSON);
            lastStand = new List<ChallengeController.LastStandJSON>(ChallengeController.Instance.LastStandsJSON);
            operation = new List<ChallengeController.OperationJSON>(ChallengeController.Instance.OperationsJSON);
            consumable = new ConsumableController.ConsumableData(ConsumableController.Instance.Consumables);
            currency = new CurrencyController.CurrencyData(CurrencyController.Instance.Currency);
            card = new Dictionary<GameUtils.BaseID, Dictionary<GameUtils.EWeaponUpgrade, int>>(CardsController.Instance.WeaponCards);
            upgrade = new Dictionary<GameUtils.BaseID, Dictionary<GameUtils.EWeaponUpgrade, int>>(UpgradeController.Instance.WeaponUpgrades);
            advertisment = new Dictionary<RewardData.ERewardType, DateTime>(AdvertismentController.Instance.WatchedAds);
            weapon = new List<GameUtils.BaseID>(WeaponController.Instance.UnlockedWeapons);
            pack = new PackController.PackSaveData(PackController.Instance.PackSave);
            leaderboard = new LeaderboardController.LeaderboardData(LeaderboardController.Instance.Leaderboards);
            daily = new DailyController.DailyData(DailyController.Instance.DynamicData);
        }

        public static SaveDataBundle FromByteArray(Byte[] array)
        {

            if (array == null || array.Length == 0)
            {
                Debug.LogWarning("Serialization of zero sized array!");
                return null;
            }

            using (var stream = new System.IO.MemoryStream(array))
            {
                try
                {
                    var formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                    SaveDataBundle bundle = (SaveDataBundle)formatter.Deserialize(stream);
                    return bundle;
                }
                catch (Exception e)
                {
                    Debug.LogError("Error when reading stream: " + e.Message);
                }
            }
            return null;
        }

        public static byte[] ToByteArray(SaveDataBundle bundle)
        {
            var formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
            using (var stream = new System.IO.MemoryStream())
            {
                formatter.Serialize(stream, bundle);
                return stream.ToArray();
            }
        }
    }

    public override void Awake()
    {
        base.Awake();
        if (!isInitialized)
        {
            DontDestroyOnLoad(gameObject);
            maxSaveTimer = LOCAL_SAVE_MAX_INTERVAL;
            maxQueueTimer = LOCAL_SAVE_QUEUE_INTERVAL;
            isInitialized = true;
            LoadLocalInfo();
        }
    }

#region Save
    public enum SAVEPRIORITY
    {
        NORMAL = 0,
        QUEUE = 1,
        INSTANT = 2
    }

    public void SaveAchievementData(SAVEPRIORITY priority)
    {
        achievementDataDirty = true;
        if (priority > savePriority)
        {
            savePriority = priority;
        }
    }

    public void SaveChallengeData(SAVEPRIORITY priority)
    {
        challengeDataDirty = true;
        if (priority > savePriority)
        {
            savePriority = priority;
        }
    }

    public void SaveLastStandData(SAVEPRIORITY priority)
    {
        lastStandDataDirty = true;
        if (priority > savePriority)
        {
            savePriority = priority;
        }
    }

    public void SaveOperationData(SAVEPRIORITY priority)
    {
        operationDataDirty = true;
        if (priority > savePriority)
        {
            savePriority = priority;
        }
    }
    public void SaveConsumableData(SAVEPRIORITY priority)
    {
        consumableDataDirty = true;
        if (priority > savePriority)
        {
            savePriority = priority;
        }
    }

    public void SaveCurrencyData(SAVEPRIORITY priority)
    {
        currencyDataDirty = true;
        if (priority > savePriority)
        {
            savePriority = priority;
        }
    }

    public void SaveCardData(SAVEPRIORITY priority)
    {
        cardDataDirty = true;
        if (priority > savePriority)
        {
            savePriority = priority;
        }
    }

    public void SaveUpgradeData(SAVEPRIORITY priority)
    {
        upgradeDataDirty = true;
        if (priority > savePriority)
        {
            savePriority = priority;
        }
    }

    public void SaveAdvertismentData(SAVEPRIORITY priority)
    {
        advertismentDataDirty = true;
        if (priority > savePriority)
        {
            savePriority = priority;
        }
    }

    public void SaveWeaponData(SAVEPRIORITY priority)
    {
        weaponDataDirty = true;
        if (priority > savePriority)
        {
            savePriority = priority;
        }
    }

    public void SavePackData(SAVEPRIORITY priority)
    {
        packDataDirty = true;
        if (priority > savePriority)
        {
            savePriority = priority;
        }
    }

    public void SaveLeaderboardData(SAVEPRIORITY priority)
    {
        leaderboardDirty = true;
        if (priority > savePriority)
        {
            savePriority = priority;
        }
    }

    public void SaveDailyData(SAVEPRIORITY priority)
    {
        dailyDirty = true;
        if (priority > savePriority)
        {
            savePriority = priority;
        }
    }

    private bool DataDirty()
    {
        return (achievementDataDirty ||
                challengeDataDirty ||
                lastStandDataDirty ||
                operationDataDirty ||
                consumableDataDirty ||
                currencyDataDirty ||
                cardDataDirty ||
                upgradeDataDirty ||
                advertismentDataDirty ||
                weaponDataDirty ||
                packDataDirty ||
                leaderboardDirty ||
                dailyDirty);
    }

    public void SaveData()
    {
        // don't save again for the next 30 sec
        maxSaveTimer = LOCAL_SAVE_MAX_INTERVAL;
        maxQueueTimer = LOCAL_SAVE_QUEUE_INTERVAL;

        if (!DataDirty())
        {
            Debug.Log("DataController: SaveData() Nothing new to save! " + DateTime.UtcNow.TimeOfDay.ToString());
            return;
        }

        // update local info
        UpdateLocalInfo(DateTime.UtcNow, localInfo.TotalPlayedTime.Add(new TimeSpan(0, 0, (int)Time.realtimeSinceStartup)));

        isSaving = true;
        Debug.Log("DataController: SaveData() " + savePriority.ToString() + " @ " + DateTime.UtcNow.TimeOfDay.ToString());

        // always do a local save
        if (achievementDataDirty) JsonUtils<List<AchievementController.Achievement>>.Save(GameUtils.ACHIEVEMENTS_JSON, AchievementController.Instance.Achievements);
        if (challengeDataDirty) JsonUtils<List<ChallengeController.ChallengeJSON>>.Save(GameUtils.CHALLENGES_JSON, ChallengeController.Instance.ChallengesJSON);
        if (lastStandDataDirty) JsonUtils<List<ChallengeController.LastStandJSON>>.Save(GameUtils.LAST_STANDS_JSON, ChallengeController.Instance.LastStandsJSON);
        if (operationDataDirty) JsonUtils<List<ChallengeController.OperationJSON>>.Save(GameUtils.OPERATIONS_JSON, ChallengeController.Instance.OperationsJSON);
        if (consumableDataDirty) JsonUtils<ConsumableController.ConsumableData>.Save(GameUtils.CONSUMABLES_JSON, ConsumableController.Instance.Consumables);
        if (currencyDataDirty) JsonUtils<CurrencyController.CurrencyData>.Save(GameUtils.CURRENCY_JSON, CurrencyController.Instance.Currency);
        if (cardDataDirty) JsonUtils<Dictionary<GameUtils.BaseID, Dictionary<GameUtils.EWeaponUpgrade, int>>>.Save(GameUtils.WEAPON_CARDS_JSON, CardsController.Instance.WeaponCards);
        if (upgradeDataDirty) JsonUtils<Dictionary<GameUtils.BaseID, Dictionary<GameUtils.EWeaponUpgrade, int>>>.Save(GameUtils.WEAPON_UPG_JSON, UpgradeController.Instance.WeaponUpgrades);
        if (advertismentDataDirty) JsonUtils<Dictionary<RewardData.ERewardType, DateTime>>.Save(GameUtils.WATCHED_ADS_JSON, AdvertismentController.Instance.WatchedAds);
        if (weaponDataDirty) JsonUtils<List<GameUtils.BaseID>>.Save(GameUtils.WEAPON_CTR_JSON, WeaponController.Instance.UnlockedWeapons);
        if (packDataDirty) JsonUtils<PackController.PackSaveData>.Save(GameUtils.PACK_DATA_JSON, PackController.Instance.PackSave);
        if (leaderboardDirty) JsonUtils<LeaderboardController.LeaderboardData>.Save(GameUtils.LEADERBOARD_DATA_JSON, LeaderboardController.Instance.Leaderboards);
        if (dailyDirty) JsonUtils<DailyController.DailyData>.Save(GameUtils.DAILY_JSON, DailyController.Instance.DynamicData);

        // reset dirty state
        achievementDataDirty = challengeDataDirty = lastStandDataDirty = operationDataDirty = consumableDataDirty = currencyDataDirty = cardDataDirty = upgradeDataDirty = advertismentDataDirty = weaponDataDirty = packDataDirty = leaderboardDirty = dailyDirty = false;

#if !UNITY_EDITOR && UNITY_ANDROID && ENABLE_CLOUD_SAVE
        if (PlayGamesPlatform.Instance.IsAuthenticated())
        {
            // do a remote save
            RemoteSaveGame();
        }
#else
        isSaving = false;
#endif
    }

    private void UpdateLocalInfo(DateTime time, TimeSpan playedTime)
    {
#if UNITY_EDITOR || RELEASE_BETA
        Debug.Log("Total played time was: " + localInfo.TotalPlayedTimeString());
#endif
        // change played time
        localInfo.TotalPlayedTime = playedTime;

#if UNITY_EDITOR || RELEASE_BETA
        Debug.Log("Total played time now: " + localInfo.TotalPlayedTimeString());
#endif
        // cange last saved time
        localInfo.LastSaved = time;

#if UNITY_ANDROID
        // set the local data to the user
        if (PlayGamesPlatform.Instance.IsAuthenticated())
        {
            localInfo.GpgsUserId = PlayGamesPlatform.Instance.GetUserId();
        }
#endif

        // save the local info
        JsonUtils<LocalInfo>.Save(GameUtils.LOCALINFO_JSON, localInfo);
    }

    private void OnGameSaved(bool success)
    {
        Debug.Log("DataController: OnGameSaved() " + success.ToString());
        isSaving = false;
    }

    #endregion // Save

    #region Load
    private void LoadLocalInfo()
    {
        System.Exception ex = JsonUtils<LocalInfo>.Load(GameUtils.LOCALINFO_JSON, out localInfo, GameUtils.ELoadType.DynamicData);
        if (ex != null && ex.GetType() == typeof(FileLoadException))
        {
            localInfo = new LocalInfo();
            localInfo.Setup();
        }
    }


    private void LoadAchievementData(List<AchievementController.Achievement> remoteData)
    {
        AchievementController.Instance.LoadAchievements(remoteData);
        Debug.Log("DataController: Achievments Loaded");
        achievementDataLoaded = true;
    }

    private void LoadChallengeData(List<ChallengeController.ChallengeJSON> remoteData)
    {
        ChallengeController.Instance.LoadChallenges(remoteData);
        Debug.Log("DataController: Challenge Loaded");
        challengeDataLoaded = true;
    }

    private void LoadLastStandData(List<ChallengeController.LastStandJSON> remoteData)
    {
        ChallengeController.Instance.LoadLastStands(remoteData);
        Debug.Log("DataController: Last Stand Loaded");
        lastStandDataLoaded = true;
    }

    private void LoadOperationData(List<ChallengeController.OperationJSON> remoteData)
    {
        ChallengeController.Instance.LoadOperations(remoteData);
        Debug.Log("DataController: Last Operation Loaded");
        operationDataLoaded = true;
    }

    private void LoadConsumableData(ConsumableController.ConsumableData remoteData)
    {
        ConsumableController.Instance.LoadConsumables(remoteData);
        Debug.Log("DataController: Consumable Loaded");
        consumableDataLoaded = true;
    }

    private void LoadCurrencyData(CurrencyController.CurrencyData remoteData)
    {
        CurrencyController.Instance.LoadCurrencies(remoteData);
        Debug.Log("DataController: Currency Loaded");
        currencyDataLoaded = true;
    }

    private void LoadCardData(Dictionary<GameUtils.BaseID, Dictionary<GameUtils.EWeaponUpgrade, int>> remoteData)
    {
        CardsController.Instance.LoadWeaponCards(remoteData);
        Debug.Log("DataController: Card Loaded");
        cardDataLoaded = true;
    }

    private void LoadUpgradeData(Dictionary<GameUtils.BaseID, Dictionary<GameUtils.EWeaponUpgrade, int>> remoteData)
    {
        UpgradeController.Instance.LoadWeaponUpgrades(remoteData);
        Debug.Log("DataController: Upgrade Loaded");
        upgradeDataLoaded = true;
    }

    private void LoadAdvertismentData(Dictionary<RewardData.ERewardType, System.DateTime> remoteData)
    {
        AdvertismentController.Instance.LoadAdvertismentsWatched(remoteData);
        Debug.Log("DataController: Advertisment Loaded");
        advertismentDataLoaded = true;
    }

    private void LoadWeaponData(List<GameUtils.BaseID> remoteData)
    {
        WeaponController.Instance.LoadWeaponController(remoteData);
        Debug.Log("DataController: Weapon Loaded");
        weaponDataLoaded = true;
    }

    private void LoadPackData(PackController.PackSaveData remoteData)
    {
        PackController.Instance.LoadPackData(remoteData);
        Debug.Log("DataController: Packs Loaded");
        packDataLoaded = true;
    }

    private void LoadLeaderboardData(LeaderboardController.LeaderboardData remoteData)
    {
        LeaderboardController.Instance.LoadLeaderboardData(remoteData);
        Debug.Log("DataController: Leaderboards Loaded");
        leaderboardLoaded = true;
    }

    private void LoadDailyData(DailyController.DailyData remoteData)
    {
        DailyController.Instance.LoadDailyData(remoteData);
        Debug.Log("DataController: Daily Loaded");
        dailyLoaded = true;
    }

    // has all data been loaded
    public bool AllLoaded
    {
        get
        {
            return achievementDataLoaded &&
                      challengeDataLoaded &&
                      lastStandDataLoaded &&
                      operationDataLoaded &&
                      consumableDataLoaded &&
                      currencyDataLoaded &&
                      cardDataLoaded &&
                      upgradeDataLoaded &&
                      advertismentDataLoaded &&
                      weaponDataLoaded &&
                      packDataLoaded &&
                      leaderboardLoaded &&
                      dailyLoaded;
        }
    }

    private void OnGameLoaded(SaveDataBundle saveData)
    {
        if (saveData == null)
        {
            // try loading local data
            // this is also called first time application is started
            RequestLocalLoadData();
            return;
        }
        TransferData(saveData);
    }

    // data is moved to controllers
    private void TransferData(SaveDataBundle saveData)
    {
        LoadAchievementData(saveData.Achievement);
        LoadChallengeData(saveData.Challenge);
        LoadLastStandData(saveData.LastStand);
        LoadOperationData(saveData.Operation);
        LoadConsumableData(saveData.Consumable);
        LoadCurrencyData(saveData.Currency);
        LoadCardData(saveData.Card);
        LoadUpgradeData(saveData.Upgrade);
        LoadAdvertismentData(saveData.Advertisment);
        LoadWeaponData(saveData.Weapon);
        LoadPackData(saveData.Pack);
        LoadLeaderboardData(saveData.Leaderboard);
        LoadDailyData(saveData.Daily);
    }

    // requests the load of all data
    public void RequestLoadData()
    {
        // don't load again if already all data loaded
        if (AllLoaded) return;
        if (localInfo == null)
        {
            LoadLocalInfo();
        }

        Debug.Log("DataController: RequestLoadData() " + System.DateTime.UtcNow.TimeOfDay.ToString());

#if UNITY_ANDROID && ENABLE_CLOUD_SAVE
        if (PlayGamesPlatform.Instance.IsAuthenticated())
        {
            RemoteLoadGame();
        }
        else
#endif
        {
            RequestLocalLoadData();
        }
    }

    // used when no google play services available
    private void RequestLocalLoadData()
    {
        Debug.Log("DataController: RequestLocalLoadData()");

        if (!achievementDataLoaded) LoadAchievementData(null);
        if (!challengeDataLoaded) LoadChallengeData(null);
        if (!lastStandDataLoaded) LoadLastStandData(null);
        if (!operationDataLoaded) LoadOperationData(null);
        if (!consumableDataLoaded) LoadConsumableData(null);
        if (!currencyDataLoaded) LoadCurrencyData(null);
        if (!cardDataLoaded) LoadCardData(null);
        if (!upgradeDataLoaded) LoadUpgradeData(null);
        if (!advertismentDataLoaded) LoadAdvertismentData(null);
        if (!weaponDataLoaded) LoadWeaponData(null);
        if (!packDataLoaded) LoadPackData(null);
        if (!leaderboardLoaded) LoadLeaderboardData(null);
        if (!dailyLoaded) LoadDailyData(null);
    }
#endregion // Load

    public override bool Update()
    {
        if (!base.Update()) return false;

        // don't try to save data unless it's already loaded
        if (!AllLoaded) return false;

        // use the timer to save again
        maxSaveTimer -= Time.deltaTime;
        if (savePriority == SAVEPRIORITY.QUEUE)
        {
            maxQueueTimer -= Time.deltaTime;
        }

        // save only if not already saving, high save priority or timer is up
        if (maxSaveTimer <= 0f || savePriority == SAVEPRIORITY.INSTANT || (savePriority == SAVEPRIORITY.QUEUE && isSaving == false && maxQueueTimer <= 0f))
        {
            SaveData();
            savePriority = SAVEPRIORITY.NORMAL;
        }

        return true;
    }

    void OnApplicationFocus(bool pauseStatus)
    {
        if (!pauseStatus)
        {
            //your app is now in the background
            if (AllLoaded)
            {
                SaveData();
            }
        }
        else
        {
            // don't save again until time is up
            maxSaveTimer = LOCAL_SAVE_MAX_INTERVAL;
        }
    }

    public void ResetAllData()
    {
#if !UNITY_EDITOR && UNITY_ANDROID && ENABLE_CLOUD_SAVE
        if (PlayGamesPlatform.Instance.IsAuthenticated())
        {
            // remove the remote game
            RemoteDeleteGame();
        }
#endif
        // reset local data
        AchievementController.Instance.ResetAchievements();
        ChallengeController.Instance.ResetChallenges();
        ChallengeController.Instance.ResetLastStands();
        ChallengeController.Instance.ResetOperations();
        ConsumableController.Instance.ResetConsumables();
        CurrencyController.Instance.ResetCurrencies();
        CardsController.Instance.ResetCards();
        UpgradeController.Instance.ResetUpgrades();
        AdvertismentController.Instance.ResetAdvertismentsWatched();
        WeaponController.Instance.ResetWeaponController();
        PackController.Instance.ResetPackData();
        LeaderboardController.Instance.ResetLeaderboardController();
        DailyController.Instance.ResetDailyData();

#if !UNITY_EDITOR && UNITY_ANDROID && ENABLE_CLOUD_SAVE
        if (PlayGamesPlatform.Instance.IsAuthenticated())
        {
            // remove the remote game
            RemoteSaveGame();
        }
#endif
    }

#if UNITY_ANDROID && ENABLE_CLOUD_SAVE
    #region Google Saved Game
    /// <summary>
    /// Creates the new save. Save returned in callback is closed!. Open it before use.
    /// </summary>
    /// <param name="save">Save.</param>
    /// <param name="callback">Invoked when save has been created.</param>
    private static void CreateNewSave(ISavedGameMetadata save, Action<ISavedGameMetadata> callback)
    {
        ISavedGameClient savedGameClient = PlayGamesPlatform.Instance.SavedGame;

        SavedGameMetadataUpdate.Builder builder = new SavedGameMetadataUpdate.Builder();
        builder = builder
            .WithUpdatedPlayedTime(save.TotalTimePlayed.Add(new TimeSpan(0, 0, (int)Time.realtimeSinceStartup)))
            .WithUpdatedDescription("Saved at " + DateTime.UtcNow);

        SavedGameMetadataUpdate updatedMetadata = builder.Build();

        SaveDataBundle newBundle = new SaveDataBundle();
        savedGameClient.CommitUpdate(save, updatedMetadata, SaveDataBundle.ToByteArray(newBundle), (SavedGameRequestStatus status, ISavedGameMetadata game) =>
        {
            if (status == SavedGameRequestStatus.Success)
            {
                if (callback != null) callback(game);
            }
            Debug.Log("Creating new save finished with status :" + status.ToString());
        });
    }

    /// <summary>
    /// Loads the saved game. This should be a starting point for loading data.
    /// </summary>
    private void RemoteLoadGame()
    {
        Debug.Log("DataController: RemoteLoadGame()");

        ISavedGameClient savedGameClient = PlayGamesPlatform.Instance.SavedGame;

        if (savedGameClient == null)
        {
            Debug.Log("Couldn't load from google saved games. Doing local load.");
            RequestLocalLoadData();
            return;
        }

        //savedGameClient.OpenWithAutomaticConflictResolution(GameUtils.CLOUD_SAVE_GAME_FILENAME, DataSource.ReadCacheOrNetwork, ConflictResolutionStrategy.UseLongestPlaytime, CompletedLoadCallback);
        savedGameClient.OpenWithManualConflictResolution(GameUtils.CLOUD_SAVE_GAME_FILENAME, DataSource.ReadCacheOrNetwork, true, ConflictResolverCallback, CompletedLoadCallback);
    }

    private void CompletedLoadCallback(SavedGameRequestStatus reqStatus, ISavedGameMetadata openedGame)
    {
        Debug.Log("DataController: CompletedLoadCallback() Status: " + reqStatus.ToString());
        ISavedGameClient savedGameClient = PlayGamesPlatform.Instance.SavedGame;

        if (reqStatus != SavedGameRequestStatus.Success)
        {
            Debug.Log("Couldn't load from google saved games. Doing local load. Status: " + reqStatus);
            RequestLocalLoadData();
            return;
        }

        // if there is no save game yet, create a new one
        if (string.IsNullOrEmpty(openedGame.Description))
        {
            Debug.Log("No remote saved game file found, creating a new one from local data.");
            // make sure the local data is loaded into memory or fresh data is created
            RequestLocalLoadData();
            // openedGame has not been saved on cloud before, create new file
            CreateNewSave(openedGame, (ISavedGameMetadata newlySavedGame) =>
            {
                RemoteLoadGame();
            });
            return;
        }

        // remember the metadata
        openSaveGameMetadata = openedGame;

        // if local data newer, discard save game
        if (PlayGamesPlatform.Instance.GetUserId() == localInfo.GpgsUserId || string.IsNullOrEmpty(localInfo.GpgsUserId))
        {
            if (openedGame.TotalTimePlayed < localInfo.TotalPlayedTime)
            {
                Debug.Log("Local data had higher total played time, replacing remote data with local data.");
                RequestLocalLoadData();
                RemoteSaveGame();
                return;
            }
        }

        // save should be opened now
        Debug.Log("Loading save from: " + openedGame.Filename + "\n" + openedGame.Description + "\nTotalTimePlayed = " + openedGame.TotalTimePlayed.ToString() + "\nOpened = " + openedGame.IsOpen.ToString());

        savedGameClient.ReadBinaryData(openedGame, (SavedGameRequestStatus newReqStatus, byte[] data) =>
        {
            Debug.Log("Reading save finished with status: " + newReqStatus.ToString());

            if (newReqStatus == SavedGameRequestStatus.Success)
            {
                SaveDataBundle bundle = SaveDataBundle.FromByteArray(data);
                OnGameLoaded(bundle);
            }
        });
    }

    /// <summary>
    /// Commits the save to cloud.
    /// </summary>
    private void RemoteSaveGame()
    {
        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            Debug.Log("DataController: RemoteSaveGame() no internet connectivity, not saving remotely.");
            return;
        }

        Debug.Log("DataController: RemoteSaveGame()");

        ISavedGameClient savedGameClient = PlayGamesPlatform.Instance.SavedGame;

        if (openSaveGameMetadata != null && openSaveGameMetadata.IsOpen)
        {
            Debug.Log("DataController: RemoteSaveGame() got static meta data, saving it.");
            CompletedSaveCallback(SavedGameRequestStatus.Success, openSaveGameMetadata);
            return;
        }

        //savedGameClient.OpenWithAutomaticConflictResolution(GameUtils.CLOUD_SAVE_GAME_FILENAME, DataSource.ReadCacheOrNetwork, ConflictResolutionStrategy.UseLongestPlaytime, CompletedSaveCallback);
        savedGameClient.OpenWithManualConflictResolution(GameUtils.CLOUD_SAVE_GAME_FILENAME, DataSource.ReadCacheOrNetwork, true, ConflictResolverCallback, CompletedSaveCallback);
    }

    private void ConflictResolverCallback(IConflictResolver resolver, ISavedGameMetadata original, byte[] originalData, ISavedGameMetadata unmerged, byte[] unmergedData)
    {
        Debug.Log("DataController: ConflictResolverCallback()");
        if (original.TotalTimePlayed > unmerged.TotalTimePlayed)
        {
            resolver.ChooseMetadata(original);
            Debug.Log("DataController: ConflictResolverCallback() using original: " + original.TotalTimePlayed.ToString() + " > " + unmerged.TotalTimePlayed.ToString());
        }
        else if (original.TotalTimePlayed == unmerged.TotalTimePlayed)
        {
            Debug.Log("DataController: ConflictResolverCallback() original TimeStamp: " + original.TotalTimePlayed.ToString() + " unmerged TimeStamp: " + unmerged.TotalTimePlayed.ToString());
            if (original.LastModifiedTimestamp > unmerged.LastModifiedTimestamp)
            {
                resolver.ChooseMetadata(original);
                Debug.Log("DataController: ConflictResolverCallback() using original(left) (timestamp): " + original.LastModifiedTimestamp.ToString() + " > " + unmerged.LastModifiedTimestamp.ToString());
            }
            else if (original.LastModifiedTimestamp == unmerged.LastModifiedTimestamp)
            {
                resolver.ChooseMetadata(original);
                Debug.Log("DataController: ConflictResolverCallback() using original because all is the same! " + original.LastModifiedTimestamp.ToString() + " == " + unmerged.LastModifiedTimestamp.ToString());
            }
            else
            {
                resolver.ChooseMetadata(unmerged);
                Debug.Log("DataController: ConflictResolverCallback() using unmerged(right) (timestamp): " + original.LastModifiedTimestamp.ToString() + " <= " + unmerged.LastModifiedTimestamp.ToString());
            }
        }
        else
        {
            resolver.ChooseMetadata(unmerged);
            Debug.Log("DataController: ConflictResolverCallback() using unmerged: " + original.TotalTimePlayed.ToString() + " < " + unmerged.TotalTimePlayed.ToString());
        }
    }

    private void CompletedSaveCallback(SavedGameRequestStatus reqStatus, ISavedGameMetadata openedGame)
    {
        Debug.Log("DataController: CompletedSaveCallback() Status: " + reqStatus.ToString());
        ISavedGameClient savedGameClient = PlayGamesPlatform.Instance.SavedGame;

        if (reqStatus == SavedGameRequestStatus.Success)
        {
            // adding real time since startup so we can determine longes playtime and resolve future conflicts easilly
            TimeSpan newPlayedTime = openedGame.TotalTimePlayed.Add(new TimeSpan(0, 0, (int)Time.realtimeSinceStartup));
            DateTime savedTime = DateTime.UtcNow;

            // make sure the save game has the highest time
            if (newPlayedTime < localInfo.TotalPlayedTime)
            {
                newPlayedTime = localInfo.TotalPlayedTime;
            }

            string newDescription = "Saved game at " + savedTime;
            SavedGameMetadataUpdate.Builder builder = new SavedGameMetadataUpdate.Builder()
                .WithUpdatedPlayedTime(newPlayedTime)
                .WithUpdatedDescription(newDescription);

            SavedGameMetadataUpdate updatedMetadata = builder.Build();

            // make sure local info is updated to this
            UpdateLocalInfo(savedTime, newPlayedTime);

            // save should be opened now
            Debug.Log("New save in: " + openedGame.Filename + "\n" + newDescription + "\nTotalTimePlayed = " + newPlayedTime.ToString() + "\nOpened = " + openedGame.IsOpen.ToString());

            SaveDataBundle newSave = new SaveDataBundle();
            savedGameClient.CommitUpdate(openedGame, updatedMetadata, SaveDataBundle.ToByteArray(newSave), (SavedGameRequestStatus status, ISavedGameMetadata newBundleMetadata) =>
            {
                if (status == SavedGameRequestStatus.Success)
                {
                    openedGame = newBundleMetadata;
                }
                OnGameSaved(status == SavedGameRequestStatus.Success);
            });
        }
    }

    private void RemoteDeleteGame()
    {
        Debug.Log("DataController: RemoteDeleteGame()");

        ISavedGameClient savedGameClient = PlayGamesPlatform.Instance.SavedGame;

        //savedGameClient.OpenWithAutomaticConflictResolution(GameUtils.CLOUD_SAVE_GAME_FILENAME, DataSource.ReadCacheOrNetwork, ConflictResolutionStrategy.UseLongestPlaytime, CompletedDeleteCallback);
        savedGameClient.OpenWithManualConflictResolution(GameUtils.CLOUD_SAVE_GAME_FILENAME, DataSource.ReadCacheOrNetwork, true, ConflictResolverCallback, CompletedDeleteCallback);
    }

    private void CompletedDeleteCallback(SavedGameRequestStatus reqStatus, ISavedGameMetadata openedGame)
    {
        Debug.Log("DataController: CompletedDeleteCallback() Status: " + reqStatus.ToString());
        ISavedGameClient savedGameClient = PlayGamesPlatform.Instance.SavedGame;

        if (reqStatus == SavedGameRequestStatus.Success)
        {
            // save should be opened now
            Debug.Log("Delete save in: " + openedGame.Filename + "\n" + openedGame.Description.ToString() + "\nTotalTimePlayed = " + openedGame.TotalTimePlayed.ToString() + "\nOpened = " + openedGame.IsOpen.ToString());

            savedGameClient.Delete(openedGame);
        }
    }
    #endregion // Google Saved Game
#endif
}



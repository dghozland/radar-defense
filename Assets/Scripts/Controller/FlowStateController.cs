using System;
using UnityEngine;
using System.Collections.Generic;
#if UNITY_ANDROID
using GooglePlayGames;
using GooglePlayGames.BasicApi;
#endif
using UnityEngine.SocialPlatforms;
using System.Timers;
using UnityEngine.UI;
#if UNITY_IOS
using UnityEngine.SocialPlatforms.GameCenter;
#endif

public class FlowStateController : UnitySingleton<FlowStateController>, IBaseStateController
{
    public const string INIT_SCENE_NAME = "Init";
    public const string MENU_SCENE_NAME = "MenuArt";
    public const string TUTORIAL_SCENE_NAME = "Tutorial";
    public const string OPEN_PRIZE_SCENE_NAME = "OpenPrize";
    public const string MISSION = "Mission_";
    public const string EVENT = "Event_";
    public const string LAST_STAND = "Laststand_";
    public const string OPERATION = "Operation_";
    private static bool isInitialized = false;
    private static GameObject loadingScreen;
    private static Text loadingScreenHint;
    private const int MAX_LOG = 500;
    private const int MAX_CALLSTACK = 500;
#if UNITY_ANDROID
    private Action backButtonPressed;
    private bool backButtonRegistered;
#endif

    public void RegisterBackButton(Action function)
    {
#if UNITY_ANDROID
        backButtonPressed += function;
        backButtonRegistered = true;
#endif
    }

    public void UnregisterBackButton(Action function)
    {
#if UNITY_ANDROID
        backButtonPressed -= function;
        if (backButtonPressed == null)
        {
            backButtonRegistered = false;
        }
#endif
    }

    private static EScene currentScene;
    public EScene CurrentScene
    {
        get { return currentScene; }
    }

    public enum EScene
    {
        Init,
        Menu,
        Battle
    }

/*#if RELEASE_BETA || UNITY_EDITOR
    public const string ENVIRONMENT_KEY = "32954502392601418958150062514677";
    public const string COLLECT_URL = "http://collect9315rdrcm.deltadna.net/collect/api";
    public const string ENGAGE_URL = "http://engage9315rdrcm.deltadna.net";
#endif

#if RELEASE_LIVE && !UNITY_EDITOR
    public const string ENVIRONMENT_KEY = "32954506767602025306811065014677";
    public const string COLLECT_URL = "http://collect9315rdrcm.deltadna.net/collect/api";
    public const string ENGAGE_URL = "http://engage9315rdrcm.deltadna.net";
#endif*/

    private Dictionary<string, int> errorLogs = new Dictionary<string, int>();

    // All States need to be named here
    public class FlowStates
    {
        public static readonly int Init = 0;
        public static readonly int Intro = 1;
        public static readonly int Menu = 2;
        public static readonly int Battle = 3;
    }

    public void LoadingScreen(bool active)
    {
        loadingScreen.SetActive(active);
        if (active)
        {
            loadingScreenHint.text = HintController.Instance.GetHint();
        }
    }

    BaseStateController baseState = new BaseStateController();    

    private FlowStateController()
    {
        // Register the states to the State Controller
        AddState(new InitState());
        AddState(new IntroState());
        AddState(new MenuState());
        AddState(new BattleState());
    }

    public override void Awake()
    {
        base.Awake();
        string sceneName = UnityEngine.SceneManagement.SceneManager.GetActiveScene().name;
        if (!isInitialized)
        {
#if UNITY_ANDROID
            backButtonRegistered = false;
#endif
            loadingScreen = (GameObject)GameObject.Instantiate(Resources.Load("prefab_loadingScreen"));
            loadingScreenHint = GameUtils.GetChildGameObject(loadingScreen, "HintText", true).GetComponent<Text>();
            loadingScreen.SetActive(false);
            DontDestroyOnLoad(loadingScreen);

            // Error Handling
            Application.logMessageReceived += HandleLog;

            SetupGoogleAndApple();

            // Always keep the flow state controller loaded through the scene changes
            DontDestroyOnLoad(gameObject);
            isInitialized = true;

            // Set Initial State
            switch (sceneName)
            {
                case MENU_SCENE_NAME:
                    {
                        InitState(FlowStates.Menu);
                        break;
                    }
                case INIT_SCENE_NAME:
                    {
                        InitState(FlowStates.Init);
                        break;
                    }
                default:
                    {
                        if (sceneName.StartsWith(MISSION)
                            || sceneName.StartsWith(EVENT)
                            || sceneName.StartsWith(LAST_STAND)
                            || sceneName.StartsWith(OPERATION)
                            || sceneName.StartsWith(TUTORIAL_SCENE_NAME))
                        {
                            InitState(FlowStates.Battle);
                            BattleState battleState = (BattleState)GetState(FlowStates.Battle);
                            battleState.SetBattleScene(sceneName);
                        }
                        else
                        {
                            Debug.LogWarning("The current scene is not taken into account in code. Battle scenes must start by either " + MISSION + " or " + LAST_STAND + " or " + OPERATION + " or " + EVENT + ". Initializing the flow state to Battle.");
                            InitState(FlowStates.Battle);
                        }
                        break;
                    }
            }
        }
        SetCurrentScene(sceneName);
    }

    public void SetupGoogleAndApple()
    {
#if UNITY_ANDROID
        // Google Play Services
        PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder()
            .EnableSavedGames()
            .Build();
        PlayGamesPlatform.InitializeInstance(config);
        PlayGamesPlatform.DebugLogEnabled = true;
        PlayGamesPlatform.Activate();
        Social.localUser.Authenticate(OnAuthenticationCallbackAndroid);
#elif UNITY_IOS
            // Apple Game Center
            GameCenterPlatform.ShowDefaultAchievementCompletionBanner(true);
            Social.localUser.Authenticate(OnAuthenticationCallbackiOS);
#else
            DataController.Instance.RequestLoadData();
#endif
    }

    private void SetCurrentScene(string sceneName)
    {
        switch (sceneName)
        {
            case MENU_SCENE_NAME: currentScene = EScene.Menu; break;
            case INIT_SCENE_NAME: currentScene = EScene.Init; break;
            default: currentScene = EScene.Battle; break;
        }
        Debug.Log("CurrentScene: " + currentScene.ToString());
    }

    public void OnDidFailToRegisterForRewardAdsHandler(string message)
    {
        Debug.Log("Register for Reward Ads failed: " + message);
    }

    public bool IsInMenuScene()
    {
        return CurrentScene == EScene.Menu;
    }

    public bool IsInInitScene()
    {
        return CurrentScene == EScene.Init;
    }

    public bool IsInBattleScene()
    {
        return CurrentScene != EScene.Menu && CurrentScene != EScene.Init;
    }

    public bool IsInTutorialScene()
    {
        return UnityEngine.SceneManagement.SceneManager.GetActiveScene().name == "Tutorial";
    }

    public void OnAuthenticationCallbackAndroid(bool success)
    {
        if (!success)
        {
            Debug.Log("GPGS: Login Failure.");
        }
        else
        {
            Debug.Log("GPGS: Login Successful.");
        }

        DataController.Instance.RequestLoadData();
    }

    public void OnAuthenticationCallbackiOS(bool success)
    {
        if (!success)
        {
            Debug.Log("GameCenter: Login Failure.");
        }
        else
        {
            Debug.Log("GameCenter: Login Successful.");
            Social.LoadAchievements(OnAchievementsLoaded);
        }

        DataController.Instance.RequestLoadData();
    }

    private void OnAchievementsLoaded(IAchievement[] achievements)
    {
        if (achievements.Length > 0)
        {
            Debug.Log("GameCenter: Successfuly loaded Achievements.");
        }
        else
        {
            Debug.Log("GameCenter: No Achievements found.");
        }
    }

    public void ShowQuitInfoPanel()
    {
        MenuUIController.Instance.ShowQuitInfoPopup(true);
    }

    public void Quit()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        UnityEngine.Application.Quit();
#endif
    }

    private void HandleLog(string logString, string stackTrace, LogType type)
    {

        if (type == LogType.Error || type == LogType.Exception)
        {
            // check if already happend
            if (errorLogs.ContainsKey(logString))
            {
                errorLogs[logString]++;
                return;
            }

            errorLogs.Add(logString, 1);

            // make sure log and callstack are not more than MAX_LOG/MAX_CALLSTACK characters
            if (stackTrace.Length > MAX_CALLSTACK) stackTrace = stackTrace.Substring(0, MAX_CALLSTACK);
            if (logString.Length > MAX_LOG) logString = logString.Substring(0, MAX_LOG);
        }
    }

    private void UpdateAndroidButtons()
    {
#if UNITY_ANDROID
        // Back Button Pressed
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (backButtonRegistered)
            {
                backButtonPressed.Invoke();
            }
        }
#endif
    }

#region Interface
    // Implementations from the Interface
    public void AddState(BaseState newState)
    {
        baseState.AddState(newState);
    }

    public void InitState(int newState)
    {
        baseState.InitState(newState);
    }

    public void SetState(int newState)
    {
        baseState.SetState(newState);
    }

    public BaseState GetState(int stateType)
    {
        return baseState.GetState(stateType);
    }

    public void Start()
    {
        baseState.Start();
    }

    public override bool Update()
    {
        if (!base.Update()) return false;
        UpdateAndroidButtons();
        baseState.Update();
        return false;
    }

    public bool IsInState(int stateType)
    {
        return baseState.IsInState(stateType);
    }
#endregion
}

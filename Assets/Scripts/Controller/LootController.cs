using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LootController : UnitySingleton<LootController>
{
    #region Enemy Bonus
    [System.Serializable]
    public class EnemyBonus
    {
        [Range(0f, 1f)]
        [SerializeField]
        private float creditsDropRate = 0.25f;
        [Range(0f, 1f)]
        [SerializeField]
        private float goldDropRate = 0.25f;
        [Range(0f, 1f)]
        [SerializeField]
        private float droneDropRate = 0.15f;
        [Range(0f, 1f)]
        [SerializeField]
        private float matchBoostDropRate = 0.35f;

        public LootObject.ELootType RollTheDice()
        {
            float randomEnemyBonus = Random.Range(0f, 1f);
            if (randomEnemyBonus <= creditsDropRate)
            {
                return LootObject.ELootType.Credits;
            }
            else if (randomEnemyBonus <= (creditsDropRate + goldDropRate))
            {
                return LootObject.ELootType.Gold;
            }
            else if (randomEnemyBonus <= (creditsDropRate + goldDropRate + droneDropRate))
            {
                return LootObject.ELootType.Drone;
            }
            else if (randomEnemyBonus <= (creditsDropRate + goldDropRate + droneDropRate + matchBoostDropRate))
            {
                return LootObject.ELootType.MatchBoost;
            }

            Debug.LogWarning("Enemy bonus drop rates are not set correctly.");
            return LootObject.ELootType.Credits;
        }
    }
    #endregion

    #region Loot Range Values
    [System.Serializable]
    public class LootRangeValues
    {
        [SerializeField]
        private int lootNormalMin = 0;

        [SerializeField]
        private int lootNormalMax = 0;

        [SerializeField]
        private int lootHardMin = 0;

        [SerializeField]
        private int lootHardMax = 0;

        [SerializeField]
        private int lootInsaneMin = 0;

        [SerializeField]
        private int lootInsaneMax = 0;

        public int RollLootValue(GameUtils.EGameDifficulty difficulty)
        {
            switch(difficulty)
            {
                // Note :: + 1 because max is exclusive
                case GameUtils.EGameDifficulty.Normal:
                    return Random.Range(lootNormalMin, lootNormalMax + 1);
                case GameUtils.EGameDifficulty.Hard:
                    return Random.Range(lootHardMin, lootHardMax + 1);
                case GameUtils.EGameDifficulty.Insane:
                    return Random.Range(lootInsaneMin, lootInsaneMax + 1);
                default:
                    return 0;
            }
        }
    }
    #endregion

    [Tooltip("Warning : Must equal 100%")]
    [SerializeField]
    public EnemyBonus enemyBonus;

    [SerializeField]
    public LootRangeValues creditLootValues;

    [SerializeField]
    public LootRangeValues goldLootValues;

    [SerializeField]
    private float lootLifeDuration = 7f;

    private GameObject lootContainer;
    private Queue<GameObject> inactiveLoot = new Queue<GameObject>();

    public override void Awake()
    {
        base.Awake();
        lootContainer = new GameObject("Loots");
    }
    
    public float LootLifeDuration
    {
        get { return lootLifeDuration; }
    }
    
    public void SpawnLoot(Vector3 pos)
    {
        GameObject loot = GetLootFromPool();
        loot.transform.position = pos;
        loot.GetComponent<LootObject>().Init(enemyBonus.RollTheDice(), lootLifeDuration, true);
    }

    public void PutLootInPool(GameObject loot)
    {
        loot.SetActive(false);
        inactiveLoot.Enqueue(loot);
    }

    public GameObject GetLootFromPool()
    {
        GameObject loot;
        if (inactiveLoot.Count > 0)
        {
            loot = inactiveLoot.Dequeue();
            loot.SetActive(true);
            return loot;
        }

        loot = (GameObject)Instantiate(Resources.Load("Loot"));
        loot.transform.parent = lootContainer.transform;

        return loot;
    }
}

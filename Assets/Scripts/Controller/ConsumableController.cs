using UnityEngine;
using System;
using System.IO;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.SceneManagement;

#pragma warning disable 0649

public class ConsumableController : UnitySingleton<ConsumableController>
{
    private static bool isInitialized = false;
    private static bool loaded = false;
    private static bool isSceneSetup = false;

    private readonly string REPAIR_TRIGGER_PARAM = "TriggerRepair";

    #region Serialized Fields
    [SerializeField]
    private AudioClip slowerConsumableSound;

    [SerializeField]
    private AudioClip ohShitConsumableSound;

    [SerializeField]
    private AudioClip pulseConsumableSound;

    [SerializeField]
    private AudioClip reloadConsumableSound;

    [SerializeField]
    private AudioClip repairConsumableSound;

    [SerializeField]
    private float pulseDuration = 5f;

    [Tooltip("The time for a pulse to cross the whole screen")]
    [SerializeField]
    private float pulseTime = 1f;

    [Tooltip("The time for a slower to cross the whole screen")]
    [SerializeField]
    private float slowerTime = 1f;

    [SerializeField]
    private float slowerMultiplier = 0.5f;

    [SerializeField]
    private float slowerDuration = 20f;

    [SerializeField]
    private float droneDuration = 10f;

    [SerializeField]
    private float droneSightRadius = 3f;

    [SerializeField]
    private float homingMissileSpeed = 500f;

    [SerializeField]
    private float homingMissileGroundPower = 50f;

    [SerializeField]
    private float homingMissileAirPower = 50f;

    [SerializeField]
    private float homingMissileRadius = 50f;

    [Tooltip("The time for cooldown is shared for all 5 consumables")]
    [SerializeField]
    private float cooldownTime = 1f;

    [Tooltip("The amount of health restored for weapons with the repair button")]
    [SerializeField]
    private float weaponRepair = 25f;

    [Tooltip("The amount of health restored for bases with the repair button")]
    [SerializeField]
    private float baseRepair = 25f;
    #endregion
    
    // Battle UI elements
    private Button[] consumableButtons;
    private Text[] consumableAmounts;
    private Image[] consumablePlusImages;
    private int[] battleConsumableAmounts;
    
    private float pulseTimer = 0f;
    private GameObject pulseRight;
    private GameObject pulseTop;
    private GameObject pulseLeft;
    private GameObject pulseBottom;
    private GameObject slowerRight;
    private GameObject slowerTop;
    private GameObject slowerLeft;
    private GameObject slowerBottom;
    

    private List<float> cooldowns;
    private bool isFirstWaveStarted;

    public enum EConsumableTrigger
    {
        ADVERTISMENT,
        GOLD,
        TUTORIAL,
        NORMAL
    }

    [Serializable]
    public class ConsumableData
    {
        public ConsumableData()
        {
            pulseAmount = 0;
            repairAmount = 0;
            ohShitAmount = 0;
            reloadAmount = 0;
            slowerAmount = 0;
            unlockedConsumables = new List<GameUtils.EConsumable>();
        }

        public ConsumableData(ConsumableData copy)
        {
            pulseAmount = copy.pulseAmount;
            repairAmount = copy.repairAmount;
            ohShitAmount = copy.ohShitAmount;
            reloadAmount = copy.reloadAmount;
            slowerAmount = copy.slowerAmount;
            unlockedConsumables = copy.unlockedConsumables;
        }

        public int pulseAmount;
        public int repairAmount;
        public int ohShitAmount;
        public int reloadAmount;
        public int slowerAmount;
        public List<GameUtils.EConsumable> unlockedConsumables;
    }

    [SerializeField]
    private static ConsumableData consumableData;

    [HideInInspector]
    public ConsumableData Consumables
    {
        get { return consumableData; }
        set { consumableData = value; }
    }

    public float PulseDuration
    {
        get { return pulseDuration; }
    }

    public float SlowerMultiplier
    {
        get { return slowerMultiplier; }
    }

    public float SlowerDuration
    {
        get { return slowerDuration; }
    }

    public bool IsInPulse()
    {
        return pulseTimer > 0f;
    }

    public Button RepairConsumableButton
    {
        get { return consumableButtons[(int)GameUtils.EConsumable.Repair]; }
    }

    public bool IsSceneSetup
    {
        get { return isSceneSetup; }
    }

    private void ResetCooldowns()
    {
        cooldowns = new List<float>();
        // add the 5 consumalbes
        for (int i = 0; i < (int)GameUtils.EConsumable.Count; ++i)
        {
            cooldowns.Add(0f);
        }

    }

    public override void Awake()
    {
        base.Awake();
        if (!isInitialized)
        {
            // Always keep the controller loaded through the scene changes
            DontDestroyOnLoad(gameObject);
            isInitialized = true;
        }

        isSceneSetup = false;
    }

    public override bool Update()
    {
        if (!base.Update()) return false;
        if (!CheckIfDataLoaded()) return false;
        if (!isSceneSetup) SetupScene();
        
        if (FlowStateController.Instance.IsInBattleScene())
        {
            UpdateInteractibility();
            UpdatePulse();
            UpdateSlower();
            UpdateCooldowns();
        }

        return true;
    }

    private bool CheckIfDataLoaded()
    {
        if (DataController.Instance.ConsumableDataLoaded && !loaded)
        {
            loaded = true;
        }

        return loaded;
    }

    private void SetupScene()
    {
        if (FlowStateController.Instance.IsInBattleScene())
        {
            isSceneSetup = true;

            Instance.SetupConsumableBattleUI();

            ResetCooldowns();
            isFirstWaveStarted = false;            
        }
    }
    
    #region Consumable Battle UI
    private void SetupConsumableBattleUI()
    {
        consumableButtons = new Button[(int)GameUtils.EConsumable.Count];
        consumableAmounts = new Text[(int)GameUtils.EConsumable.Count];
        consumablePlusImages = new Image[(int)GameUtils.EConsumable.Count];
        battleConsumableAmounts = new int[(int)GameUtils.EConsumable.Count];        

        for (int i = 0; i < (int)GameUtils.EConsumable.Count; ++i)
        {
            GameUtils.EConsumable consumableID = (GameUtils.EConsumable)i;
            string buttonName = consumableID.ToString() + "Button";
            consumableButtons[i] = GameObject.Find(buttonName).GetComponent<Button>();
            int i2 = i;
            consumableButtons[i].onClick.RemoveAllListeners();
            consumableButtons[i].onClick.AddListener(delegate { ConsumableButtonClicked(i2); });
            consumableButtons[i].interactable = false;
            consumableAmounts[i] = GameUtils.GetChildGameObject(consumableButtons[i].gameObject, "Amount").GetComponent<Text>();
            consumablePlusImages[i] = GameUtils.GetChildGameObject(consumableButtons[i].gameObject, "GetMoreIcon", true).GetComponent<Image>();
            consumablePlusImages[i].gameObject.SetActive(false);
            battleConsumableAmounts[i] = GetBattleConsumableAmount((GameUtils.EConsumable)i);

            // Update the amount text
            UpdateBattleConsumableAmountUI(i, true);

            // Set the lock
            if (!IsConsumableUnlocked(consumableID))
            {
                GameUtils.GetChildGameObject(consumableButtons[i].gameObject, "ImageLock", true).SetActive(true);
            }
        }
    }

    private void ConsumableButtonClicked(int consumableIndex)
    {
        ActivateConsumable(consumableIndex, false, EConsumableTrigger.NORMAL);
    }
    #endregion

    public void ActivateConsumable(int consumableIndex, bool overrideAmount, EConsumableTrigger trigger)
    {
        if (cooldowns[consumableIndex] > 0) return;
        if (!overrideAmount && ((battleConsumableAmounts[consumableIndex] <= 0) || (Instance.GetConsumableAmount((GameUtils.EConsumable)consumableIndex) <= 0)))
        {
            WaveController.Instance.CheckGetMoreConsumables(consumableIndex);
            return;
        }

        // If has completely run out of consumable and used the 'Get More' one too
        if (!WaveController.Instance.CanGetMoreConsumable(consumableIndex))
        {
            // Disable the consumable button
            consumableButtons[consumableIndex].interactable = false;
        }

        //Debug.Log("Consumable Activated: " + ((GameUtils.EConsumable)consumableIndex).ToString());
        cooldowns[consumableIndex] = cooldownTime;

        switch (consumableIndex)
        {
            case (int)GameUtils.EConsumable.Slower:
                ActivateSlowerConsumable();
                break;
            case (int)GameUtils.EConsumable.OhShit:
                ActivateOhShitConsumable();
                break;
            case (int)GameUtils.EConsumable.Pulse:
                ActivatePulseConsumable();
                break;
            case (int)GameUtils.EConsumable.Reload:
                ActivateReloadConsumable();
                break;
            case (int)GameUtils.EConsumable.Repair:
                ActivateRepairConsumable();
                break;
        }

        if (!overrideAmount)
        {
            // Reduce the amount
            Instance.ModifyConsumableAmount((GameUtils.EConsumable)consumableIndex, -1, DataController.SAVEPRIORITY.NORMAL);
        }
        
        // Play consumable sound
        PlayConsumableSound((GameUtils.EConsumable)consumableIndex);
    }

    public void ActivateDroneConsumable(Vector3 pos)
    {
        GameObject drone = (GameObject)Instantiate(Resources.Load("Drone"));
        drone.transform.position = pos;
        DroneObject droneObject = drone.GetComponent<DroneObject>();
        droneObject.SightRadius = droneSightRadius;
        SpawnDroneObject spawnObject = drone.GetComponent<SpawnDroneObject>();
        spawnObject.SpawnTime = droneDuration;
    }

    public void ActivateSlowerConsumable(bool isFree = false)
    {
        float thickness = 3;
        float width = PlayfieldController.Instance.TopRight.x - PlayfieldController.Instance.BottomLeft.x;
        float height = PlayfieldController.Instance.TopRight.y - PlayfieldController.Instance.BottomLeft.y;

        if (slowerBottom)   Destroy(slowerBottom);
        if (slowerLeft)     Destroy(slowerLeft);
        if (slowerRight)    Destroy(slowerRight);
        if (slowerTop)      Destroy(slowerTop);

        slowerRight = (GameObject)Instantiate(Resources.Load("SlowerOne"));
        Vector3 position = BaseController.Instance.Bases[GameUtils.BaseID.CITY].transform.position;
        position.y = PlayfieldController.Instance.BottomLeft.y + (height / 2f);
        slowerRight.transform.position = position;
        slowerRight.transform.localScale = new Vector3(thickness, height, 1);

        slowerLeft = (GameObject)Instantiate(Resources.Load("SlowerOne"));
        slowerLeft.transform.position = position;
        slowerLeft.transform.localScale = new Vector3(thickness, height, 1);

        slowerTop = (GameObject)Instantiate(Resources.Load("SlowerOne"));
        position = BaseController.Instance.Bases[GameUtils.BaseID.CITY].transform.position;
        position.x = PlayfieldController.Instance.BottomLeft.x + (width / 2f);
        slowerTop.transform.position = position;
        slowerTop.transform.localScale = new Vector3(width, thickness, 1);

        slowerBottom = (GameObject)Instantiate(Resources.Load("SlowerOne"));
        slowerBottom.transform.position = position;
        slowerBottom.transform.localScale = new Vector3(width, thickness, 1);

        if (!isFree)
        {
            AchievementController.Instance.UpdateAchievementProgression(AchievementData.EAchievementType.Slow, 1, AchievementData.EAchievementOverride.Progress);
        }        
    }

    private void UpdateSlower()
    {
        if (slowerRight != null)
        {
            Vector3 position = slowerRight.transform.position;
            position.x += PlayfieldController.Instance.PlayfieldWidth / slowerTime * Time.deltaTime;
            slowerRight.transform.position = position;
            if (position.x > PlayfieldController.Instance.TopRight.x)
            {
                Destroy(slowerRight);
            }
        }
        if (slowerLeft != null)
        {
            Vector3 position = slowerLeft.transform.position;
            position.x -= PlayfieldController.Instance.PlayfieldWidth / slowerTime * Time.deltaTime;
            slowerLeft.transform.position = position;
            if (position.x < PlayfieldController.Instance.BottomLeft.x)
            {
                Destroy(slowerLeft);
            }
        }
        if (slowerTop != null)
        {
            Vector3 position = slowerTop.transform.position;
            position.y += PlayfieldController.Instance.PlayfieldWidth / slowerTime * Time.deltaTime;
            slowerTop.transform.position = position;
            if (position.y > PlayfieldController.Instance.TopRight.y)
            {
                Destroy(slowerTop);
            }
        }
        if (slowerBottom != null)
        {
            Vector3 position = slowerBottom.transform.position;
            position.y -= PlayfieldController.Instance.PlayfieldWidth / slowerTime * Time.deltaTime;
            slowerBottom.transform.position = position;
            if (position.y < PlayfieldController.Instance.BottomLeft.y)
            {
                Destroy(slowerBottom);
            }
        }
    }

    private void UpdateCooldowns()
    {
        for (int i = 0; i < cooldowns.Count; ++i)
        {
            if(cooldowns[i] > 0f)
            {
                cooldowns[i] -= Time.deltaTime;
            }
        }
    }

    public void ActivateOhShitConsumable(bool isFree = false)
    {
        List<GameObject> aliveEnemies = new List<GameObject>(EnemyController.Instance.AliveEnemies);

        // QUESTION :: Is this still needed?!
        // Clean dead enemies
        // Note :: Enemies have a 2 second delay before getting
        // destroyed when their hp reaches 0.
        for (int i = aliveEnemies.Count - 1; i >= 0; --i)
        {
            if (aliveEnemies[i].GetComponent<MovingEnemyObject>().IsDead)
            {
                aliveEnemies.RemoveAt(i);
            }
        }

        Dictionary<GameUtils.BaseID, WeaponObject> weapons = WeaponController.Instance.ActiveWeapons;        
        for (int c = 0; c < GameUtils.HOMING_MISSILE_COUNT; ++c)
        {
            for (int i = 0; i < weapons.Count; ++i)
            {
                // each weapon will shot 3 homing missiles at its closest enemy
                WeaponObject weaponObject = weapons.ElementAt(i).Value;
                if (aliveEnemies.Count > 0)
                {
                    GameObject enemyObject = null;
                    float shortestDistanceSqr = float.PositiveInfinity;
                    for (int j = 0; j < aliveEnemies.Count; ++j)
                    {
                        float distanceSqr = Vector3.SqrMagnitude(aliveEnemies[j].transform.position - weaponObject.transform.position);
                        if (distanceSqr < shortestDistanceSqr)
                        {
//#if UNITY_EDITOR
                            Debug.Log("Shortest Distance Missile: " + c + " is GO: " + aliveEnemies[j].name + " @dist: " + distanceSqr + " @closest: " + shortestDistanceSqr);
//#endif
                            enemyObject = aliveEnemies[j];
                            shortestDistanceSqr = distanceSqr;
                        }
                    }
                    SetupHomingMissile(weaponObject, enemyObject);

                    // QUESTION :: What if the enemy doesn't get destroyed?!
                    aliveEnemies.Remove(enemyObject);
                }
                else
                {
                    break;
                }
            }
        }

        if (!isFree)
        {
            AchievementController.Instance.UpdateAchievementProgression(AchievementData.EAchievementType.OhShit, 1, AchievementData.EAchievementOverride.Progress);
        }        
    }

    private void SetupHomingMissile(WeaponObject weaponObject, GameObject targetObject)
    {
        GameObject homingMissileGO = WeaponController.Instance.GetProjectileFromPool(GameUtils.BaseID.MISSILE);
        MissileObject homingMissileProjectile = homingMissileGO.GetComponent<MissileObject>();
        homingMissileProjectile.WeaponID = GameUtils.BaseID.MISSILE;
        homingMissileProjectile.StartPosition = weaponObject.transform.position;
        homingMissileProjectile.TargetPosition = targetObject.transform.position;
        homingMissileProjectile.SetSpeed = homingMissileSpeed * PlayfieldController.Instance.WidthScale;
        homingMissileProjectile.GroundPower = homingMissileGroundPower;
        homingMissileProjectile.AirPower = homingMissileAirPower;
        homingMissileProjectile.Radius = homingMissileRadius * GameUtils.RADIUS_FACTOR;
        homingMissileProjectile.Init();
        homingMissileProjectile.TargetEnemy = targetObject;
        homingMissileProjectile.ExplodeOnTargetOnly = true;
    }

    public void ActivatePulseConsumable(bool isFree = false)
    {
        pulseTimer = pulseDuration;

        float thickness = 3;
        float width = PlayfieldController.Instance.TopRight.x - PlayfieldController.Instance.BottomLeft.x;
        float height = PlayfieldController.Instance.TopRight.y - PlayfieldController.Instance.BottomLeft.y;

        if (pulseBottom)    Destroy(pulseBottom);
        if (pulseLeft)      Destroy(pulseLeft);
        if (pulseRight)     Destroy(pulseRight);
        if (pulseTop)       Destroy(pulseTop);

        pulseRight = (GameObject)Instantiate(Resources.Load("PulseOne"));
        Vector3 position = BaseController.Instance.Bases[GameUtils.BaseID.CITY].transform.position;
        position.y = PlayfieldController.Instance.BottomLeft.y + (height / 2f);
        pulseRight.transform.position = position;
        pulseRight.transform.localScale = new Vector3(thickness, height, 1);

        pulseLeft = (GameObject)Instantiate(Resources.Load("PulseOne"));
        pulseLeft.transform.position = position;
        pulseLeft.transform.localScale = new Vector3(thickness, height, 1);

        pulseTop = (GameObject)Instantiate(Resources.Load("PulseOne"));
        position = BaseController.Instance.Bases[GameUtils.BaseID.CITY].transform.position;
        position.x = PlayfieldController.Instance.BottomLeft.x + (width / 2f);
        pulseTop.transform.position = position;
        pulseTop.transform.localScale = new Vector3(width, thickness, 1);

        pulseBottom = (GameObject)Instantiate(Resources.Load("PulseOne"));
        pulseBottom.transform.position = position;
        pulseBottom.transform.localScale = new Vector3(width, thickness, 1);

        if (!isFree)
        {
            AchievementController.Instance.UpdateAchievementProgression(AchievementData.EAchievementType.Pulse, 1, AchievementData.EAchievementOverride.Progress);
        }        
    }

    private void UpdateInteractibility()
    {
        if (!isFirstWaveStarted)
        {
            if (WaveController.Instance.InitWaveStarted)
            {
                isFirstWaveStarted = true;

                // For each consumable
                for (int i = 0; i < (int)GameUtils.EConsumable.Count; ++i)
                {
                    GameUtils.EConsumable consumableID = (GameUtils.EConsumable)i;
                    if (IsConsumableUnlocked(consumableID))
                    {
                        consumableButtons[i].interactable = true;
                    }
                }
            }
        }
    }

    // Note :: For tutorial
    public void EnableRepairConsumable()
    {
        consumableButtons[(int)GameUtils.EConsumable.Repair].interactable = true;
        GameUtils.GetChildGameObject(consumableButtons[(int)GameUtils.EConsumable.Repair].gameObject, "ImageLock", true).SetActive(false);
    }

    private void UpdatePulse()
    {
        if (pulseTimer > 0f)
        {
            pulseTimer -= Time.deltaTime;
        }

        if (pulseRight != null)
        {
            Vector3 position = pulseRight.transform.position;
            position.x += PlayfieldController.Instance.PlayfieldWidth / pulseTime *  Time.deltaTime;
            pulseRight.transform.position = position;
            if (position.x > PlayfieldController.Instance.TopRight.x)
            {
                Destroy(pulseRight);
            }
        }
        if (pulseLeft != null)
        {
            Vector3 position = pulseLeft.transform.position;
            position.x -= PlayfieldController.Instance.PlayfieldWidth / pulseTime * Time.deltaTime;
            pulseLeft.transform.position = position;
            if (position.x < PlayfieldController.Instance.BottomLeft.x)
            {
                Destroy(pulseLeft);
            }
        }
        if (pulseTop != null)
        {
            Vector3 position = pulseTop.transform.position;
            position.y += PlayfieldController.Instance.PlayfieldWidth / pulseTime * Time.deltaTime;
            pulseTop.transform.position = position;
            if (position.y > PlayfieldController.Instance.TopRight.y)
            {
                Destroy(pulseTop);
            }
        }
        if (pulseBottom != null)
        {
            Vector3 position = pulseBottom.transform.position;
            position.y -= PlayfieldController.Instance.PlayfieldWidth / pulseTime * Time.deltaTime;
            pulseBottom.transform.position = position;
            if (position.y < PlayfieldController.Instance.BottomLeft.y)
            {
                Destroy(pulseBottom);
            }
        }
    }

    public void ActivateReloadConsumable(bool isFree = false)
    {
        WeaponController.Instance.PerformQuickFullAmmoRegen();

        if (!isFree)
        {
            AchievementController.Instance.UpdateAchievementProgression(AchievementData.EAchievementType.Ammo, 1, AchievementData.EAchievementOverride.Progress);
        }        
    }

    public void ActivateRepairConsumable(bool isFree = false)
    {
        Dictionary<GameUtils.BaseID, GameObject> bases = BaseController.Instance.Bases;
        for (int i = 0; i < bases.Count; ++i)
        {
            // restore some health
            if (bases.ElementAt(i).Key < GameUtils.BaseID.CITY)
            {
                // weapons
                BaseObject baseObject = bases.ElementAt(i).Value.GetComponent<BaseObject>();
                baseObject.Health += weaponRepair;
            }
            else
            {
                // cities and bases
                BaseObject baseObject = bases.ElementAt(i).Value.GetComponent<BaseObject>();
                baseObject.Health += baseRepair;
            }

            // Trigger the repair anim for all bases
            GameUtils.GetChildGameObject(bases.ElementAt(i).Value, "RepairPanel").GetComponent<Animator>().SetTrigger(REPAIR_TRIGGER_PARAM);
        }
        
        if (!isFree)
        {
            AchievementController.Instance.UpdateAchievementProgression(AchievementData.EAchievementType.Repair, 1, AchievementData.EAchievementOverride.Progress);
        }        
    }

    private void PlayConsumableSound(GameUtils.EConsumable consumableType)
    {
        AudioClip consumableSound = null;
        switch (consumableType)
        {
            case GameUtils.EConsumable.Slower:
                consumableSound = slowerConsumableSound;
                break;
            case GameUtils.EConsumable.OhShit:
                consumableSound = ohShitConsumableSound;
                break;
            case GameUtils.EConsumable.Reload:
                consumableSound = reloadConsumableSound;
                break;
            case GameUtils.EConsumable.Repair:
                consumableSound = repairConsumableSound;
                break;
            case GameUtils.EConsumable.Pulse:
            default:
                consumableSound = pulseConsumableSound;
                break;
        }

        // Sound hook - Consumable sound
        AudioManager.Instance.PlaySound(consumableSound);
    }

    public int GetConsumableAmount(GameUtils.EConsumable consumableType)
    {
        switch (consumableType)
        {
            case GameUtils.EConsumable.Slower:
                return consumableData.slowerAmount;
            case GameUtils.EConsumable.OhShit:
                return consumableData.ohShitAmount;
            case GameUtils.EConsumable.Pulse:
                return consumableData.pulseAmount;
            case GameUtils.EConsumable.Reload:
                return consumableData.reloadAmount;
            case GameUtils.EConsumable.Repair:
                return consumableData.repairAmount;
            default:
                return 0;
        }
    }

    public int GetBattleConsumableAmount(GameUtils.EConsumable consumableType)
    {
        int battleConsumableAmountMax = 0;
        switch (consumableType)
        {
            case GameUtils.EConsumable.OhShit:
                battleConsumableAmountMax = GameUtils.OHSHIT_BATTLE_AMOUNT_MAX;
                break;
            case GameUtils.EConsumable.Pulse:
                battleConsumableAmountMax = GameUtils.PULSE_BATTLE_AMOUNT_MAX;
                break;
            case GameUtils.EConsumable.Reload:
                battleConsumableAmountMax = GameUtils.RELOAD_BATTLE_AMOUNT_MAX;
                break;
            case GameUtils.EConsumable.Repair:
                battleConsumableAmountMax = GameUtils.REPAIR_BATTLE_AMOUNT_MAX;
                break;
            case GameUtils.EConsumable.Slower:
                battleConsumableAmountMax = GameUtils.SLOWER_BATTLE_AMOUNT_MAX;
                break;
        }
        return Math.Min(battleConsumableAmountMax, GetConsumableAmount(consumableType));
    }

    public void ModifyAllConsumablesAmount(int value, DataController.SAVEPRIORITY priority)
    {
        ModifyConsumableAmount(GameUtils.EConsumable.OhShit, value, priority);
        ModifyConsumableAmount(GameUtils.EConsumable.Pulse, value, priority);
        ModifyConsumableAmount(GameUtils.EConsumable.Reload, value, priority);
        ModifyConsumableAmount(GameUtils.EConsumable.Repair, value, priority);
        ModifyConsumableAmount(GameUtils.EConsumable.Slower, value, priority);
    }

    public void ModifyConsumableAmount(GameUtils.EConsumable consumableType, int value, DataController.SAVEPRIORITY priority)
    {
        switch(consumableType)
        {
            case GameUtils.EConsumable.Slower:
                consumableData.slowerAmount += value;
                AchievementController.Instance.UpdateAchievementProgression(AchievementData.EAchievementType.StackSlow, consumableData.slowerAmount, AchievementData.EAchievementOverride.Larger);
                break;
            case GameUtils.EConsumable.OhShit:
                consumableData.ohShitAmount += value;
                AchievementController.Instance.UpdateAchievementProgression(AchievementData.EAchievementType.StackOhShit, consumableData.ohShitAmount, AchievementData.EAchievementOverride.Larger);
                break;
            case GameUtils.EConsumable.Pulse:
                consumableData.pulseAmount += value;
                AchievementController.Instance.UpdateAchievementProgression(AchievementData.EAchievementType.StackPulse, consumableData.pulseAmount, AchievementData.EAchievementOverride.Larger);
                break;
            case GameUtils.EConsumable.Reload:
                consumableData.reloadAmount += value;
                AchievementController.Instance.UpdateAchievementProgression(AchievementData.EAchievementType.StackAmmo, consumableData.reloadAmount, AchievementData.EAchievementOverride.Larger);
                break;
            case GameUtils.EConsumable.Repair:
                consumableData.repairAmount += value;
                AchievementController.Instance.UpdateAchievementProgression(AchievementData.EAchievementType.StackRepair, consumableData.repairAmount, AchievementData.EAchievementOverride.Larger);
                break;
        }
        
        if (FlowStateController.Instance.IsInBattleScene())
        {
            // Update the battle consumable amounts
            battleConsumableAmounts[(int)consumableType] += value;

            // Update the amount text
            UpdateBattleConsumableAmountUI((int)consumableType);
        }

        SaveConsumables(priority);
    }

    private void UpdateBattleConsumableAmountUI(int consumableIndex, bool isFirstUpdate = false)
    {
        if (battleConsumableAmounts[consumableIndex] == 0
            && (isFirstUpdate || WaveController.Instance.CanGetMoreConsumable(consumableIndex)))
        {
            consumableAmounts[consumableIndex].gameObject.SetActive(false);
            consumablePlusImages[consumableIndex].gameObject.SetActive(true);
        }
        else
        {
            consumableAmounts[consumableIndex].text = battleConsumableAmounts[consumableIndex].ToString();
        }
    }
    
    public void SetObjectToConsumableButtonPosition(int index, GameObject getMoreOverlay)
    {
        RectTransform rectOverlay = getMoreOverlay.GetComponent<RectTransform>();
        RectTransform rectButton = consumableButtons[index].GetComponent<RectTransform>();

        rectOverlay.anchorMin = rectButton.anchorMin;
        rectOverlay.anchorMax = rectButton.anchorMax; 

        Vector3 position = rectButton.anchoredPosition;
        position.x += (rectButton.rect.width / 1.6f);
        rectOverlay.anchoredPosition = position;
    }

    public bool IsConsumableUnlocked(GameUtils.EConsumable consumableID)
    {
        if (consumableData.unlockedConsumables.Contains(consumableID))
        {
            return true;
        }
        return false;
    }

    public void UnlockConsumable(GameUtils.EConsumable consumableID, bool showPopup = true)
    {
        if (IsConsumableUnlocked(consumableID))
        {
            return;
        }

        if (showPopup)
        {
            // Show unlock popup
            MenuUIController.Instance.ShowUnlockedConsumablePopup(consumableID, GetConsumableLvlUnlock(consumableID));
        }        

        consumableData.unlockedConsumables.Add(consumableID);

        // Give free consumables and save
        ModifyConsumableAmount(consumableID, GetFreeConsumableAmount(consumableID), DataController.SAVEPRIORITY.QUEUE);
    }

    public void UnlockConsumableByLevel(int level)
    {
        if (level >= GameUtils.REPAIR_UNLOCK_LVL)
        {
            // Make sure Repair is unlocked
            UnlockConsumable(GameUtils.EConsumable.Repair);
        }
        if (level >= GameUtils.RELOAD_UNLOCK_LVL)
        {
            // Make sure Ammo is unlocked
            UnlockConsumable(GameUtils.EConsumable.Reload);
        }
        if (level >= GameUtils.PULSE_UNLOCK_LVL)
        {
            // Make sure Pulse is unlocked
            UnlockConsumable(GameUtils.EConsumable.Pulse);
        }
        if (level >= GameUtils.SLOWER_UNLOCK_LVL)
        {
            // Make sure EMP is unlocked
            UnlockConsumable(GameUtils.EConsumable.Slower);
        }
        if (level >= GameUtils.OHSHIT_UNLOCK_LVL)
        {
            // Make sure Homing Missile is unlocked
            UnlockConsumable(GameUtils.EConsumable.OhShit);
        }
    }

    public int GetConsumableLvlUnlock(GameUtils.EConsumable consumableID)
    {
        switch (consumableID)
        {
            case GameUtils.EConsumable.OhShit:
                return GameUtils.OHSHIT_UNLOCK_LVL;
            case GameUtils.EConsumable.Pulse:
                return GameUtils.PULSE_UNLOCK_LVL;
            case GameUtils.EConsumable.Reload:
                return GameUtils.RELOAD_UNLOCK_LVL;
            case GameUtils.EConsumable.Repair:
                return GameUtils.REPAIR_UNLOCK_LVL;
            case GameUtils.EConsumable.Slower:
                return GameUtils.SLOWER_UNLOCK_LVL;
            default:
                return -1;
        }
    }

    public int GetFreeConsumableAmount(GameUtils.EConsumable consumableID)
    {
        switch (consumableID)
        {
            case GameUtils.EConsumable.OhShit:
                return GameUtils.OHSHIT_FREE_AMOUNT;
            case GameUtils.EConsumable.Pulse:
                return GameUtils.PULSE_FREE_AMOUNT;
            case GameUtils.EConsumable.Reload:
                return GameUtils.RELOAD_FREE_AMOUNT;
            case GameUtils.EConsumable.Repair:
                return GameUtils.REPAIR_FREE_AMOUNT;
            case GameUtils.EConsumable.Slower:
                return GameUtils.SLOWER_FREE_AMOUNT;
            default:
                return -1;
        }
    }

    #region Data Handling
    public void SaveConsumables(DataController.SAVEPRIORITY priority)
    {
        DataController.Instance.SaveConsumableData(priority);
    }

    public void LoadConsumables(ConsumableData remoteConsumable)
    {
        if (remoteConsumable == null)
        {
            System.Exception ex = JsonUtils<ConsumableData>.Load(GameUtils.CONSUMABLES_JSON, out consumableData, GameUtils.ELoadType.DynamicData);
            if (ex != null
                && ex.GetType() == typeof(FileLoadException))
            {
                ResetConsumables();
            }
        }
        else
        {
            consumableData = remoteConsumable;
        }
    }

    public void ResetConsumables()
    {
        // Note :: Needs to be initialized otherwise it crashes
        consumableData = new ConsumableData();

        SaveConsumables(DataController.SAVEPRIORITY.QUEUE);
    }
#endregion
}

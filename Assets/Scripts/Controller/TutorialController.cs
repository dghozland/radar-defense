using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
#if UNITY_ANDROID
using GooglePlayGames;
#endif

public class TutorialController : UnitySingleton<TutorialController>
{
    private static bool isInitialized = false;

    private readonly float TUTORIAL_STEP_FADE_OUT_DURATION = 0.15f;
    private readonly float ENEMY_DETECTED_DELAY = 0.9f;
    private readonly float CONSUMED_REPAIR_DELAY = 1.25f;
    private readonly float RADAR_QUICK_SCAN_DELAY = 1f;
    private readonly float INITIAL_WAVE_DELAY = 1.5f;
    private readonly float CITY_DAMAGED_VALUE = 100f;
    private readonly float ARTILLERY_DAMAGED_VALUE = 20f;
    private readonly float RADAR_SCAN_INFO_DURATION = WaveController.WAVE_INITIAL_DELAY - 2.5f;
    private bool isFirstUpdate = true;

    // Current Tutorial Flow 
    // 1) Welcome
    // 2) ConsumableRepairInfo
    // 3) CityInfo
    // 4) AmmoInfo
    // 5) RadarInfo
    // 6) InitialWaveWait
    // 7) EnemyDetected
    // 8) TutorialBattle
    // 9) MissionEnd
    // 10) UpgradeDamageInfo
    // 11) PlayInfo
    // ...
    // 12) Mission1
    // 13) FreeUpgradePack
    // 14) StoreOpenPrize
    // 15) UpgradingInfo
    public enum ETutorialStep
    {
        Welcome = 0,
        CityInfo = 1,
        WeaponSelection = 2,
        AmmoInfo = 3,
        RadarInfo = 4,
        ConsumableRepairInfo = 5,
        MissionStart = 6,
        InitialWaveWait = 7,
        EnemyDetected = 8,
        TutorialBattle = 9,
        MissionEnd = 10,
        UpgradePackInfo = 12,
        StoreOpenPrize = 13,
        UpgradeSpeedInfo = 14,
        UpgradeDamageInfo = 15,
        UpgradeHealthInfo = 16,
        PlayInfo = 17,
        Mission1 = 18,
        FreeUpgradePack = 19,
        UpgradingInfo = 20,
        Count
    }

    private float radarInfoTimer = 0f;
    private float consumableRepairInfoTimer = 0f;
    private bool consumedRepair = false;
    private bool dismissedRadarInfo = false;
    private float firstEnemyDetectedTimer = 0f;
    private float fadeOutTimer = 0f;
    private ETutorialStep nextTutorialStep = 0;
    //private bool isWeaponPlaced = false;
    private float playInfoDelayTimer = 0f;
    private float idleTimer = 0f;
    private bool haveBasesBeenDamaged = false;

    // UI elements
    private GameObject tutorialUICanvas;
    private GameObject menuUICanvas;
    private Button[] tutorialBattleButtons;
    private Button[] tutorialMenuButtons;
    private GameObject tapContinueGO;

    private ETutorialStep currentTutorialStep;

    public ETutorialStep CurrentTutorialStep
    {
        get { return currentTutorialStep; }
        set
        {
            ExitTutorialStep(currentTutorialStep);

            // If tutorial is over
            if (currentTutorialStep == ETutorialStep.PlayInfo)
            {
                EndTutorial(true);
                return;
            }
            else if (currentTutorialStep == ETutorialStep.UpgradingInfo)
            {
                EndTutorial(false);
                return;
            }

            currentTutorialStep = value;

            EnterTutorialStep(currentTutorialStep);

            //Debug.Log("currentTutorialStep = " + currentTutorialStep);
        }
    }

    public override void Awake()
    {
        // Note :: The Tutorial Controller is located 
        // in the Tutorial scene and in the Mission 1 scene.
        // It means it will only pass in the Awake function each time
        // the player enters the tutorial mission and the Mission 1.
        //
        // Once the tutorial battle is over,
        // the player will be bring to the Menu scene and once the tutorial is completely
        // done, it will destroy the persistent Tutorial Controller game object.
        //
        // For every time the player enters the Mission 1 scene, we will re-enable the
        // persistent Tutorial Controller game object for then to come back
        // to the Menu scene and do more steps.
        base.Awake();
        if (!isInitialized)
        {
            // Always keep the controller loaded through the scene changes
            DontDestroyOnLoad(gameObject);
            isInitialized = true;
            
            RegisterDependency(WaveController.Instance);
            RegisterDependency(WeaponController.Instance);
            RegisterDependency(ConsumableController.Instance);
            RegisterDependency(BaseController.Instance);

            // Note :: Only the tutorial scene has steps inside, not Mission 1.
            if (FlowStateController.Instance.IsInTutorialScene())
            {
                Instance.SetupTutorialBattleUI();
            }            
            
            SceneManager.sceneLoaded += OnSceneLoaded;
            SceneManager.sceneUnloaded += OnSceneUnloaded;
        }
    }

    // Note :: Needed when changing to the menu scene.
    // It will always happen when changing into the Tutorial scene.
    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        // Don't deal with the tutorial flow here if
        // it's not a level change into the Menu scene
        if (scene.name != FlowStateController.MENU_SCENE_NAME) return;

        // If the player finished the first part of the tutorial
        if (CurrentTutorialStep == ETutorialStep.MissionEnd)
        {
            Instance.SetupTutorialMenuUI();
            CurrentTutorialStep = ETutorialStep.UpgradeDamageInfo;
        }
        // If the player finished the Mission 1 for the first time
        else if (CurrentTutorialStep == ETutorialStep.Mission1)
        {
            Instance.SetupTutorialMenuUI();
            CurrentTutorialStep = ETutorialStep.FreeUpgradePack;
        }
    }

    // Note :: This is used to detect when the open pack anim is done.
    private void OnSceneUnloaded(Scene scene)
    {
        // If it's not the open prize scene being unloaded, return now
        if (scene.name != FlowStateController.OPEN_PRIZE_SCENE_NAME) return;
        
        // Needs to be on the open prize step from the store and not the battle
        if (CurrentTutorialStep == ETutorialStep.StoreOpenPrize)
        {
            GoToNextTutorialStep((int)currentTutorialStep, true);
        }
    }

    #region Tutorial Battle UI
    private void SetupTutorialBattleUI()
    {
        tutorialUICanvas = GameObject.Find("TutorialBattleUICanvas");

        // For each tutorial steps
        // Note :: There is gonna be empty slots in the array
        tutorialBattleButtons = new Button[(int)ETutorialStep.Count];
        for (int i = (int)ETutorialStep.Welcome; i < (int)ETutorialStep.Count; ++i)
        {
            // Skip button association if there's none for the battle part
            if (!HasBattleInfoAssociated((ETutorialStep)i)) continue;

            tutorialBattleButtons[i] = GameUtils.GetChildGameObject(tutorialUICanvas, "TutorialStep" + i + "Panel", true).GetComponent<Button>();

            int i2 = i;
            tutorialBattleButtons[i].onClick.AddListener(delegate { TutorialButtonClicked(i2); });

            // Safeguard
            tutorialBattleButtons[i].gameObject.SetActive(false);
        }

        tapContinueGO = GameUtils.GetChildGameObject(tutorialUICanvas, "prefab_TxtTapContinue", true);
        tapContinueGO.SetActive(false);

        // Hide the challenge start container for the tutorial
        GameObject battleUICanvas = GameObject.Find("BattleUICanvas");
        GameUtils.GetChildGameObject(battleUICanvas, "ChallengeStartContainer", true).SetActive(false);        
    }

    private void TutorialButtonClicked(int tutorialStepIndex)
    {
        // Don't go to the next step for theses steps
        // when clicking on the tutorial button
        if ((ETutorialStep)tutorialStepIndex == ETutorialStep.ConsumableRepairInfo)
        {
            // If player already used repair
            if (consumedRepair)
                return;

            //SetTutorialButtonActive(false, (ETutorialStep)tutorialStepIndex);

            HideTapToContinue();

            // Use a repair consumable
            ConsumableController.Instance.ActivateConsumable((int)GameUtils.EConsumable.Repair, false, ConsumableController.EConsumableTrigger.TUTORIAL);
            consumedRepair = true;

            // Let the update function handle the tutorial step transition to CityInfo
            consumableRepairInfoTimer = CONSUMED_REPAIR_DELAY;
            return;
        }

        // Exception when player click quickly on this step
        if ((ETutorialStep)tutorialStepIndex == ETutorialStep.RadarInfo)
        {
            // If radar is still passing
            if (!RadarController.Instance.IsRadarWaitTimePaused)
            {
                dismissedRadarInfo = true;

                //SetTutorialButtonActive(false, (ETutorialStep)tutorialStepIndex);

                HideTapToContinue();

                // Let the update function handle the tutorial step transition to InitialWaveWait
                return;
            }            
        }
        
        if ((ETutorialStep)tutorialStepIndex == ETutorialStep.UpgradeSpeedInfo)
        {
            // Force Upgrade the speed stat of the artillery
            MenuUIController.Instance.UpgradeArtilleryStat(GameUtils.EWeaponUpgrade.SPEED);
        }
        else if ((ETutorialStep)tutorialStepIndex == ETutorialStep.UpgradeDamageInfo)
        {
            // Force Upgrade the damage stat of the artillery
            MenuUIController.Instance.UpgradeArtilleryStat(GameUtils.EWeaponUpgrade.GROUND);
        }
        else if ((ETutorialStep)tutorialStepIndex == ETutorialStep.UpgradeHealthInfo)
        {
            // Force Upgrade the health stat of the artillery
            MenuUIController.Instance.UpgradeArtilleryStat(GameUtils.EWeaponUpgrade.HEALTH);
        }
        else if ((ETutorialStep)tutorialStepIndex == ETutorialStep.FreeUpgradePack)
        {
            // Force Buy Upgrade Pack for the artillery
            PackController.Instance.ObtainFreeArtilleryPack();

            // Set the Upgrade Pack price back to normal
            GameObject upgradePack = GameUtils.GetChildGameObject(menuUICanvas, "UpgradePack", true);

            GameUtils.GetChildGameObject(upgradePack, "TextCost", true).SetActive(true);
            GameUtils.GetChildGameObject(upgradePack, "TextFreeCost", true).SetActive(false);
        }

        GoToNextTutorialStep(tutorialStepIndex, true);
    }
    #endregion

    #region Tutorial Menu UI
    private void SetupTutorialMenuUI()
    {
        menuUICanvas = GameObject.Find("MenuUICanvas");
        tutorialUICanvas = GameObject.Find("TutorialMenuUICanvas");
        
        // For each tutorial steps
        // Note :: There is gonna be empty slots in the array
        tutorialMenuButtons = new Button[(int)ETutorialStep.Count];
        for (int i = (int)ETutorialStep.Welcome; i < (int)ETutorialStep.Count; ++i)
        {
            // Skip button association if there's none for the menu part
            if (!HasMenuInfoAssociated((ETutorialStep)i)) continue;

            tutorialMenuButtons[i] = GameUtils.GetChildGameObject(tutorialUICanvas, "TutorialStep" + i + "Panel", true).GetComponent<Button>();

            int i2 = i;
            tutorialMenuButtons[i].onClick.AddListener(delegate { TutorialButtonClicked(i2); });

            // Safeguard
            tutorialMenuButtons[i].gameObject.SetActive(false);
        }

        tapContinueGO = GameUtils.GetChildGameObject(tutorialUICanvas, "prefab_TxtTapContinue", true);
        tapContinueGO.SetActive(false);

        if (CurrentTutorialStep == ETutorialStep.Mission1)
        {
            // Set the Upgrade pack price to free
            GameObject upgradePack = GameUtils.GetChildGameObject(menuUICanvas, "UpgradePack", true);
            GameUtils.GetChildGameObject(upgradePack, "TextCost", true).SetActive(false);
            GameUtils.GetChildGameObject(upgradePack, "TextFreeCost", true).SetActive(true);
        }
    }
    #endregion

    private void GoToNextTutorialStep(int tutorialStepIndex, bool withFadeOut)
    {
        // Note :: This enable better maintenance when changing the tutorial steps.
        if ((ETutorialStep)tutorialStepIndex == ETutorialStep.Welcome)
        {
            nextTutorialStep = ETutorialStep.ConsumableRepairInfo; 
        }
        else if ((ETutorialStep)tutorialStepIndex == ETutorialStep.ConsumableRepairInfo)
        {
            nextTutorialStep = ETutorialStep.CityInfo;
        }
        else if ((ETutorialStep)tutorialStepIndex == ETutorialStep.CityInfo)
        {
            nextTutorialStep = ETutorialStep.AmmoInfo;
        }
        else if ((ETutorialStep)tutorialStepIndex == ETutorialStep.AmmoInfo)
        {
            nextTutorialStep = ETutorialStep.RadarInfo;
        }
        else if ((ETutorialStep)tutorialStepIndex == ETutorialStep.RadarInfo)
        {
            nextTutorialStep = ETutorialStep.InitialWaveWait;
        }
        else if ((ETutorialStep)tutorialStepIndex == ETutorialStep.UpgradeDamageInfo)
        {
            nextTutorialStep = ETutorialStep.PlayInfo;
        }
        else if ((ETutorialStep)tutorialStepIndex == ETutorialStep.PlayInfo)
        {
            // End & Destroy Tutorial Controller after this step
            nextTutorialStep = ETutorialStep.Count;
        }
        else if ((ETutorialStep)tutorialStepIndex == ETutorialStep.FreeUpgradePack)
        {
            nextTutorialStep = ETutorialStep.StoreOpenPrize;
        }
        else if ((ETutorialStep)tutorialStepIndex == ETutorialStep.StoreOpenPrize)
        {
            nextTutorialStep = ETutorialStep.UpgradingInfo;
        }
        else
        {
            nextTutorialStep = (ETutorialStep)(tutorialStepIndex + 1);
        }
        
        if (withFadeOut)
        {
            // Only do a fade out if there's none
            // Note :: Could happen if player spams clicks
            if (fadeOutTimer <= 0f)
            {
                HideTapToContinue();

                fadeOutTimer = TUTORIAL_STEP_FADE_OUT_DURATION;
            }            
        }
        else
        {
            HideTapToContinue();

            CurrentTutorialStep = nextTutorialStep;
        }
    }
    
    private void EnterTutorialStep(ETutorialStep newTutorialStep)
    {        
        SetTutorialButtonActive(true, newTutorialStep);
                
        if (newTutorialStep == ETutorialStep.Welcome)
        {
            // Need to click anywhere to continue
            tutorialUICanvas.GetComponent<CanvasGroup>().blocksRaycasts = true;

            // Don't update the radar wait time
            RadarController.Instance.IsRadarWaitTimePaused = true;

            // Unlock the artillery
            WeaponController.Instance.UnlockWeapon(GameUtils.BaseID.ARTILLERY, false);

            // Unlock the repair and enabled it
            ConsumableController.Instance.UnlockConsumable(GameUtils.EConsumable.Repair, false);
            ConsumableController.Instance.EnableRepairConsumable();

            // Disable the pause button
            WaveController.Instance.PauseButton.gameObject.SetActive(false);
            
            // The init wave timer won't decrease
            WaveController.Instance.InitWavePaused = true;

            idleTimer = 5f;
        }
        else if (newTutorialStep == ETutorialStep.CityInfo)
        {
            // Position objective info container on top of the city
            // Note :: Using a panel container to offset the text + target
            GameUtils.GetChildGameObject(GetTutorialButton(newTutorialStep).gameObject, "TutorialStep1Container").transform.position = BuildingController.Instance.ActiveBuildings[GameUtils.BaseID.CITY].transform.position;

            idleTimer = 3f;
        }
        //else if (newTutorialStep == ETutorialStep.WeaponSelection)
        //{
        //    // Need to click on the weapon selection panel
        //    tutorialUICanvas.GetComponent<CanvasGroup>().blocksRaycasts = false;
        //
        //    // Position weapon selection info container on top of the weapon selection panel
        //    // Note :: Using a panel container to offset the text + target
        //    GameUtils.GetChildGameObject(GetTutorialButton(newTutorialStep).gameObject, "TutorialStep2Container").transform.position = WeaponController.Instance.WeaponSelectionPanel.transform.position + new Vector3(0f, -100f * PlayfieldController.Instance.HeightScale, 0f);
        //
        //    idleTimer = 3f;
        //}
        else if (newTutorialStep == ETutorialStep.AmmoInfo)
        {
            // Need to click anywhere to continue
            tutorialUICanvas.GetComponent<CanvasGroup>().blocksRaycasts = true;

            // Don't update the radar wait time
            RadarController.Instance.IsRadarWaitTimePaused = true;

            // Position ammo info container on top of the weapon
            // Note :: Using a panel container to offset the text
            GameUtils.GetChildGameObject(GetTutorialButton(newTutorialStep).gameObject, "AmmoInfoContainer").transform.position = WeaponController.Instance.ActiveWeapons[WeaponController.Instance.SelectedWeaponID].transform.position;

            idleTimer = 3f;
        }
        else if (newTutorialStep == ETutorialStep.RadarInfo)
        {
            // Need to click anywhere to continue
            tutorialUICanvas.GetComponent<CanvasGroup>().blocksRaycasts = true;

            // Re-enable the update for the radar wait time to show it
            RadarController.Instance.IsRadarWaitTimePaused = false;

            // Timer used to disable the radar wait time update if 
            // the player waits the whole step
            radarInfoTimer = RADAR_SCAN_INFO_DURATION;

            // Position radar scan info text on center of the playfield
            GameUtils.GetChildGameObject(GetTutorialButton(newTutorialStep).gameObject, "prefab_windowTutorial").transform.position = PlayfieldController.Instance.RadarBorderTopGO.transform.position + new Vector3(0f, -120f, 0f);

            idleTimer = 3f;
        }
        else if (newTutorialStep == ETutorialStep.ConsumableRepairInfo)
        {
            // Need to click anywhere to continue
            tutorialUICanvas.GetComponent<CanvasGroup>().blocksRaycasts = true;
            
            // Don't update the radar wait time
            RadarController.Instance.IsRadarWaitTimePaused = true;
            
            // Position consumable repair info text on the right of the repair consumable
            GameUtils.GetChildGameObject(GetTutorialButton(newTutorialStep).gameObject, "RepairConsumableInfoContainer").transform.position = ConsumableController.Instance.RepairConsumableButton.transform.position;

            idleTimer = 3f;
        }
        //else if (newTutorialStep == ETutorialStep.MissionStart)
        //{
        //    idleTimer = 3f;
        //}
        else if (newTutorialStep == ETutorialStep.InitialWaveWait)
        {
            // Start the wave immediately
            WaveController.Instance.InitWavePaused = false;
            WaveController.Instance.StartInitWaveIn(INITIAL_WAVE_DELAY);

            // To scan immediately for the tutorial's first enemy
            RadarController.Instance.IsRadarWaitTimePaused = false;
            RadarController.Instance.PerformScanIn(RADAR_QUICK_SCAN_DELAY);
        }
        else if (newTutorialStep == ETutorialStep.EnemyDetected)
        {
            WaveController.Instance.Paused = true;

            // Need to click anywhere to continue
            tutorialUICanvas.GetComponent<CanvasGroup>().blocksRaycasts = true;

            // Position enemy detected info container on the enemy
            GameUtils.GetChildGameObject(GetTutorialButton(newTutorialStep).gameObject, "EnemyDetectedInfoContainer").transform.position = EnemyController.Instance.AliveEnemies[0].transform.position;

            idleTimer = 3f;
        }
        else if (newTutorialStep == ETutorialStep.TutorialBattle)
        {
            WaveController.Instance.Paused = false;
        }
        else if (newTutorialStep == ETutorialStep.MissionEnd)
        {
            // Need to click on continue button
            tutorialUICanvas.GetComponent<CanvasGroup>().blocksRaycasts = false;
        }
        //else if (newTutorialStep == ETutorialStep.UpgradePackInfo)
        //{
        //    MenuUIController.Instance.OpenUpgrade();
        //
        //    // Need to click anywhere to continue
        //    tutorialUICanvas.GetComponent<CanvasGroup>().blocksRaycasts = true;
        //
        //    // Position upgrade pack info container on the upgrade pack
        //    GameObject weaponFolder = GameUtils.GetChildGameObject(menuUICanvas, "WeaponFolder", true);
        //    GameObject upgradePack = GameUtils.GetChildGameObject(weaponFolder, "UpgradePack", true);
        //    GameUtils.GetChildGameObject(GetTutorialButton(newTutorialStep).gameObject, "UpgradePackInfoContainer").transform.position = upgradePack.transform.position;
        //
        //    idleTimer = 3f;
        //}
        //else if (newTutorialStep == ETutorialStep.UpgradeSpeedInfo)
        //{
        //    // Position upgrade speed info container on the speed upgrade
        //    GameObject speedUpgrade = GameUtils.GetChildGameObject(menuUICanvas, "prefab_upgradeInfo (0)", true);
        //    GameUtils.GetChildGameObject(GetTutorialButton(newTutorialStep).gameObject, "UpgradeSpeedInfoContainer").transform.position = speedUpgrade.transform.position;
        //
        //    idleTimer = 3f;
        //}
        else if (newTutorialStep == ETutorialStep.UpgradeDamageInfo)
        {
            MenuUIController.Instance.OpenUpgrade();

            // Need to click anywhere to continue
            tutorialUICanvas.GetComponent<CanvasGroup>().blocksRaycasts = true;

            // Position upgrade damage info container on the damage upgrade
            GameObject damageUpgrade = GameUtils.GetChildGameObject(menuUICanvas, "prefab_upgradeInfo (0)", true);
            GameUtils.GetChildGameObject(GetTutorialButton(newTutorialStep).gameObject, "UpgradeDamageInfoContainer").transform.position = damageUpgrade.transform.position;

            idleTimer = 3f;
        }
        //else if (newTutorialStep == ETutorialStep.UpgradeHealthInfo)
        //{
        //    // Position upgrade health info container on the health upgrade
        //    GameObject healthUpgrade = GameUtils.GetChildGameObject(menuUICanvas, "prefab_upgradeInfo (7)", true);
        //    GameUtils.GetChildGameObject(GetTutorialButton(newTutorialStep).gameObject, "UpgradeHealthInfoContainer").transform.position = healthUpgrade.transform.position;
        //
        //    idleTimer = 2f;
        //}
        else if (newTutorialStep == ETutorialStep.PlayInfo)
        {
            MenuUIController.Instance.OpenPlay();

            // Need to click anywhere to continue
            tutorialUICanvas.GetComponent<CanvasGroup>().blocksRaycasts = true;

            idleTimer = 3f;
        }
        else if (newTutorialStep == ETutorialStep.FreeUpgradePack)
        {
            MenuUIController.Instance.OpenUpgrade();

            // Need to click anywhere to continue
            tutorialUICanvas.GetComponent<CanvasGroup>().blocksRaycasts = true;

            // Position upgrade pack info container on the upgrade pack
            GameObject weaponFolder = GameUtils.GetChildGameObject(menuUICanvas, "WeaponFolder", true);
            GameObject upgradePackButtonGO = GameUtils.GetChildGameObject(weaponFolder, "BuyUpgradePackButton", true);
            GameUtils.GetChildGameObject(GetTutorialButton(newTutorialStep).gameObject, "FreeUpgradePackInfoContainer").transform.position = upgradePackButtonGO.transform.position;

            idleTimer = 3f;
        }
        else if (newTutorialStep == ETutorialStep.UpgradingInfo)
        {
            // Need to click anywhere to continue
            tutorialUICanvas.GetComponent<CanvasGroup>().blocksRaycasts = true;

            idleTimer = 3f;
        }
    }

    private void ExitTutorialStep(ETutorialStep oldTutorialStep)
    {
        SetTutorialButtonActive(false, oldTutorialStep);
    }

    private Button GetTutorialButton(ETutorialStep tutorialStep)
    {
        if (HasInfoAssociated(tutorialStep))
        {
            if (FlowStateController.Instance.IsInMenuScene())
            {
                return tutorialMenuButtons[(int)tutorialStep];
            }
            else
            {
                return tutorialBattleButtons[(int)tutorialStep];
            }
        }

        return null;
    }

    private void SetTutorialButtonActive(bool isActive, ETutorialStep tutorialStep)
    {
        if (HasInfoAssociated(tutorialStep))
        {
            if (FlowStateController.Instance.IsInMenuScene())
            {
                tutorialMenuButtons[(int)tutorialStep].gameObject.SetActive(isActive);
            }
            else
            {
                tutorialBattleButtons[(int)tutorialStep].gameObject.SetActive(isActive);
            }
        }
    }

    private bool HasInfoAssociated(ETutorialStep tutorialStep)
    {
        if (FlowStateController.Instance.IsInMenuScene())
        {
            return HasMenuInfoAssociated(tutorialStep);
        }
        else
        {
            return HasBattleInfoAssociated(tutorialStep);
        }
    }

    private bool HasBattleInfoAssociated(ETutorialStep tutorialStep)
    {
        // Note :: These steps have a button associated with
        return tutorialStep == ETutorialStep.Welcome
            || tutorialStep == ETutorialStep.CityInfo
            || tutorialStep == ETutorialStep.WeaponSelection
            || tutorialStep == ETutorialStep.AmmoInfo
            || tutorialStep == ETutorialStep.RadarInfo
            || tutorialStep == ETutorialStep.ConsumableRepairInfo
            || tutorialStep == ETutorialStep.MissionStart
            || tutorialStep == ETutorialStep.EnemyDetected
            || tutorialStep == ETutorialStep.MissionEnd;
    }

    private bool HasMenuInfoAssociated(ETutorialStep tutorialStep)
    {
        // Note :: These steps have a button associated with
        return tutorialStep == ETutorialStep.UpgradePackInfo
            || tutorialStep == ETutorialStep.UpgradeSpeedInfo
            || tutorialStep == ETutorialStep.UpgradeDamageInfo
            || tutorialStep == ETutorialStep.UpgradeHealthInfo
            || tutorialStep == ETutorialStep.PlayInfo
            || tutorialStep == ETutorialStep.FreeUpgradePack
            || tutorialStep == ETutorialStep.UpgradingInfo;
    }

    public override bool Update()
    {
        if (!base.Update()) return false;
        if (FlowStateController.Instance.IsInBattleScene() && !ConsumableController.Instance.IsSceneSetup) return false;
        if (isFirstUpdate) PerformFirstUpdate();

        UpdateIdleTimer();
        UpdateFadeOut();
        UpdateTutorialFlow();

        return true;
    }

    private void PerformFirstUpdate()
    {
        if (FlowStateController.Instance.IsInTutorialScene())
        {
            CurrentTutorialStep = ETutorialStep.Welcome;
        }
        else
        {
            // Note :: No need to exit/enter state,
            // setting the tutorial step directly
            currentTutorialStep = ETutorialStep.Mission1;
        }
        
        isFirstUpdate = false;
    }
    
    private void UpdateIdleTimer()
    {
        if (idleTimer > 0f)
        {
            idleTimer -= Time.deltaTime;

            // If idle for too long
            if (idleTimer <= 0f)
            {
                // Show tap to continue text
                if (tapContinueGO != null)
                {
                    tapContinueGO.SetActive(true);
                }
            }
        }
    }

    private void UpdateFadeOut()
    {
        if (fadeOutTimer > 0f)
        {
            fadeOutTimer -= Time.deltaTime;

            tutorialUICanvas.GetComponent<CanvasGroup>().alpha = (fadeOutTimer / TUTORIAL_STEP_FADE_OUT_DURATION);

            if (fadeOutTimer <= 0f)
            {
                CurrentTutorialStep = nextTutorialStep;
                tutorialUICanvas.GetComponent<CanvasGroup>().alpha = 1f;
            }
        }
    }

    private void UpdateTutorialFlow()
    {
        switch(currentTutorialStep)
        {
            case ETutorialStep.Welcome:
                UpdateWelcomeStep();
                break;
            //case ETutorialStep.WeaponSelection:
            //    UpdateWeaponSelectionStep();
            //    break;
            case ETutorialStep.RadarInfo:
                UpdateRadarInfoStep();
                break;
            case ETutorialStep.ConsumableRepairInfo:
                UpdateConsumableRepairInfoStep();
                break;
            case ETutorialStep.InitialWaveWait:
                UpdateInitialWaveWaitStep();
                break;
            case ETutorialStep.TutorialBattle:
                UpdateTutorialPlaythroughStep();
                break;
            case ETutorialStep.PlayInfo:
                UpdatePlayInfoStep();
                break;
        }
    }

    private void UpdateWelcomeStep()
    {
        if (!haveBasesBeenDamaged
            && WeaponController.Instance.SelectedWeaponID != GameUtils.BaseID.NONE)
        {
            // Put the bases HP not to full in order to use the repair consumable
            BaseController.Instance.Bases[GameUtils.BaseID.CITY].GetComponent<BaseObject>().ChangeHealthSilently(-CITY_DAMAGED_VALUE);
            BaseController.Instance.Bases[WeaponController.Instance.SelectedWeaponID].GetComponent<BaseObject>().ChangeHealthSilently(-ARTILLERY_DAMAGED_VALUE);

            haveBasesBeenDamaged = true;
        }        
    }

    //private void UpdateWeaponSelectionStep()
    //{
    //    if (WeaponController.Instance.WeaponsPlaced())
    //    {
    //        // Note :: This variable is needed not to always enter the
    //        // SetNextTutorialStep function, which would always restart the timer.
    //        if (!isWeaponPlaced)
    //        {
    //            // Note :: It's important to wait a few frames
    //            // to give the time to populate the weapon info
    //            GoToNextTutorialStep((int)currentTutorialStep, true);
    //        }            
    //
    //        isWeaponPlaced = true;
    //    }
    //}

    private void UpdateRadarInfoStep()
    {
        if (radarInfoTimer > 0f)
        {
            radarInfoTimer -= Time.deltaTime;

            // If the player waited the whole radar info step
            if (radarInfoTimer <= 0f)
            {
                RadarController.Instance.IsRadarWaitTimePaused = true;
            }
        }
        else if (dismissedRadarInfo)
        {
            GoToNextTutorialStep((int)currentTutorialStep, true);
        }
    }

    private void UpdateConsumableRepairInfoStep()
    {
        if (consumableRepairInfoTimer > 0f)
        {
            consumableRepairInfoTimer -= Time.deltaTime;

            // Once the repair anim has been shown entirely
            if (consumableRepairInfoTimer <= 0f)
            {
                GoToNextTutorialStep((int)currentTutorialStep, true);
            }
        }
    }

    private void UpdateInitialWaveWaitStep()
    {
        if (WaveController.Instance.InitWaveStarted)
        {
            firstEnemyDetectedTimer += Time.deltaTime;

            // Wait a delay before showing the enemy detected step
            if (firstEnemyDetectedTimer >= ENEMY_DETECTED_DELAY)
            {
                CurrentTutorialStep = ETutorialStep.EnemyDetected;
            }            
        }
    }

    private void UpdateTutorialPlaythroughStep()
    {
        if (WaveController.Instance.MatchStatus == WaveController.EMatchStatus.Cleared)
        {
            CurrentTutorialStep = ETutorialStep.MissionEnd;
        }
    }

    private void UpdatePlayInfoStep()
    {
        if (playInfoDelayTimer > 0f)
        {
            playInfoDelayTimer -= Time.deltaTime;

            // Once the delay is over
            if (playInfoDelayTimer <= 0f)
            {
                // Then open the play tab
                MenuUIController.Instance.OpenPlay();
            }
        }
    }

    private void EndTutorial(bool tutorialAchievementCompleted)
    {
        if (tutorialAchievementCompleted)
        {
#if UNITY_ANDROID
            if (PlayGamesPlatform.Instance.IsAuthenticated())
            {
                Social.ReportProgress(GPGSConstants.achievement_born_ready, 100.0f, (bool success) =>
                {
                    // handle success or failure
                    Debug.Log("Unlocked BornReady Achievement");
                });
            }
#elif UNITY_IOS
            if (Social.localUser.authenticated)
            {
                Social.ReportProgress(GCConstants.achievement_born_ready, 100.0f, (bool success) =>
                {
                    // handle success or failure
                    Debug.Log("Unlocked BornReady Achievement");
                });
            }
#endif
        }

        isInitialized = false;
        SceneManager.sceneLoaded -= OnSceneLoaded;
        SceneManager.sceneUnloaded -= OnSceneUnloaded;
        Destroy(gameObject);
    }

    private void HideTapToContinue()
    {
        // Always make sure tap to continue is hidden on click
        tapContinueGO.SetActive(false);
        idleTimer = 0f;
    }
}

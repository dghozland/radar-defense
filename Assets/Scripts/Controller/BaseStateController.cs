using System.Collections.Generic;

// Interface for being able to have the methods of BaseStateController
public interface IBaseStateController
{
    void InitState(int newState);
    void AddState(BaseState newState);
    void SetState(int newState);
    BaseState GetState(int stateType);
    void Start();
    bool Update();
    bool IsInState(int stateType);
}

// Base Class for the State Machines, never inherit from this, but only from the Interface!
public class BaseStateController
{
    private List<BaseState> listOfStates;
    private BaseState currentState;

    public BaseState CurrentState
    {
        get { return currentState; }
        set { currentState = value; }
    }

    public BaseStateController()
    {
        listOfStates = new List<BaseState>();
    }

    public void InitState(int initState)
    {
        currentState = GetState(initState);
        currentState.InEnterTransition = true;
    }

    public void AddState(BaseState newState)
    {
        if (!listOfStates.Contains(newState))
        {
            listOfStates.Add(newState);
        }
    }

    public void SetState(int newState)
    {
        BaseState gotoState = GetState(newState);
        // don't try to reenter a state we are already entering in
        if (gotoState == currentState && currentState.InEnterTransition) return;

        if (currentState != null)
        {
            currentState.ExitState();
        }
        currentState = gotoState;
        currentState.EnterState();
    }

    public BaseState GetState(int stateType)
    {
        foreach (BaseState state in listOfStates)
        {
            if (state.GetStateType() == stateType)
            {
                return state;
            }
        }
        return null;
    }

    public void Start()
    {

    }

    public void Update()
    {
        if (currentState.InEnterTransition)
        {
            currentState.EnterTransition();
        }
        else
        {
            currentState.UpdateState();
        }
    }

    public bool IsInState(int stateType)
    {
        return currentState.GetStateType() == stateType;
    }

}

using System;
using System.IO;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using Steamworks;

public class AchievementController : UnitySingleton<AchievementController>
{
    [Serializable]
    public class Achievement
    {
        public Achievement()
        {
            achievementID = string.Empty;
            achievementProgress = 0;
            hasClaimedReward = false;
        }

        public Achievement(Achievement copy)
        {
            achievementID = copy.achievementID;
            achievementProgress = copy.achievementProgress;
            hasClaimedReward = copy.hasClaimedReward;
        }

        public string achievementID;
        public float achievementProgress;
        public bool hasClaimedReward;
        
        public AchievementData.EAchievementType GetAchievementType()
        {
            AchievementData data = Instance.FindAchievementData(achievementID);
            if (data == null) return AchievementData.EAchievementType.None;
            return data.achievementType;
        }

        public bool isCompleted()
        {
            AchievementData data = Instance.FindAchievementData(achievementID);
            if (data == null) return false;
            return achievementProgress >= data.achievementGoal;
        }
        
        public bool isNew()
        {
            return isCompleted() && !hasClaimedReward;
        }
        
        public void ResetProgression()
        {
            achievementProgress = 0f;
            hasClaimedReward = false;
        }
        
        public void CompleteProgression()
        {
            AchievementData data = Instance.FindAchievementData(achievementID);
            if (data == null) return;
            achievementProgress = data.achievementGoal;
        }
    }
    
    private static bool isInitialized = false;
    private static bool loaded = false;
    private static bool isSceneSetup = false;

    // List of achievements data coming from the resources folder
    private AchievementData[] achievementsData;

    [Tooltip("List of achievements from JSON")]
    [SerializeField]
    private static List<Achievement> achievements = new List<Achievement>();

    [HideInInspector]
    public List<Achievement> Achievements
    {
        get { return achievements; }
        set { achievements = value; }
    }
    
    // UI elements
    private Button[] achievementButtons;
    private Button debugAchievementResetButton;
    private Button debugAchievementCompleteButton;
    private Text achievementProgression;

    public override void Awake()
    {
        base.Awake();
        if (!isInitialized)
        {
            // Always keep the controller loaded through the scene changes
            DontDestroyOnLoad(gameObject);
            isInitialized = true;        
        }
        isSceneSetup = false;
    }

    public override bool FixedUpdate()
    {
        if (!base.FixedUpdate()) return false;
        if (!CheckIfDataLoaded()) return false;
        if (!isSceneSetup) SetupScene();
        
        return true;
    }

    private bool CheckIfDataLoaded()
    {
        if (DataController.Instance.AchievementDataLoaded && !loaded)
        {
            loaded = true;
        }
        
        return loaded;
    }

    private void SetupScene()
    {
        // If in Menu scene
        if (FlowStateController.Instance.IsInMenuScene())
        {
            Instance.SetupAchievementMenuUI();
        }
        isSceneSetup = true;
    }

    #region Achievement UI
    public void SetupAchievementMenuUI()
    {
        // Note :: Needed since the achievement folder can be disabled on awake
        GameObject mainCanvas = GameObject.Find("MenuUICanvas");
        GameObject achievementsFolder = GameUtils.GetChildGameObject(mainCanvas, "AchievementsFolder", true);

        achievementButtons = new Button[achievementsData.Length];
        for (int i = 0; i < achievementsData.Length; ++i)
        {
            GameObject achievementObject = GameUtils.GetChildGameObject(achievementsFolder, "prefab_achievement (" + i + ")", true);
            if (achievementObject == null) continue;

            achievementButtons[i] = achievementObject.GetComponent<Button>();

            Achievement achievement = FindAchievements(achievementsData[i].name);
            if (achievement != null && achievement.isNew())
            {
                int i2 = i;
                achievementButtons[i].onClick.RemoveAllListeners();
                achievementButtons[i].onClick.AddListener(delegate { AchievementButtonClicked(i2); });
            }

            // Disable unused achievement buttons
            if ((i + 1) == achievementsData.Length)
            {
                while (true)
                {
                    i++;
                    GameObject unusedAchievementButton = GameUtils.GetChildGameObject(achievementsFolder, "prefab_achievement (" + i + ")", true);
                    if (unusedAchievementButton)
                    {
                        unusedAchievementButton.SetActive(false);
                    }
                    else
                    {
                        break;
                    }
                }
            }
        }

        achievementProgression = GameUtils.GetChildGameObject(achievementsFolder, "TextAchievementProgressionValue", true).GetComponent<Text>();

        debugAchievementResetButton = GameUtils.GetChildGameObject(achievementsFolder, "DebugAchievementResetButton", true).GetComponent<Button>();
        debugAchievementCompleteButton = GameUtils.GetChildGameObject(achievementsFolder, "DebugAchievementCompleteButton", true).GetComponent<Button>();

#if UNITY_EDITOR || DEBUG_MOBILE
        debugAchievementCompleteButton.onClick.AddListener(delegate { DebugAchievementCompleteButtonClicked(); });
        debugAchievementResetButton.onClick.AddListener(delegate { DebugAchievementResetButtonClicked(); });
#else
        debugAchievementCompleteButton.gameObject.SetActive(false);
        debugAchievementResetButton.gameObject.SetActive(false);
#endif
    }

    public void UpdateAchievementUI()
    {
        int achievementAmountDone = 0;
        for (int i = 0; i < achievementsData.Length; ++i)
        {
            if (achievementButtons[i] == null) continue;

            Achievement currentAchievement = FindAchievements(achievementsData[i].name);
            if (currentAchievement == null)
            {
                Debug.LogError("Didn't find achievment, this needs to be fixed! " + achievementsData[i].name);
                continue;
            }
            achievementButtons[i].interactable = currentAchievement.isNew();

            GameObject achievementWonContainer = GameUtils.GetChildGameObject(achievementButtons[i].gameObject, "achievementWon", true);
            GameUtils.GetChildGameObject(achievementWonContainer, "TextAchievementName", true).GetComponent<LocalizedText>().LocalizedTextID = achievementsData[i].achievementNameID;
            GameObject achievementClaimContainer = GameUtils.GetChildGameObject(achievementButtons[i].gameObject, "achievementClaim", true);
            GameUtils.GetChildGameObject(achievementClaimContainer, "TextAchievementName", true).GetComponent<LocalizedText>().LocalizedTextID = achievementsData[i].achievementDescID;
            GameObject achievementOffContainer = GameUtils.GetChildGameObject(achievementButtons[i].gameObject, "achievementOff", true);
            GameUtils.GetChildGameObject(achievementOffContainer, "TextAchievementName", true).GetComponent<LocalizedText>().LocalizedTextID = achievementsData[i].achievementDescID;

            if (currentAchievement.isNew())
            {
                int i2 = i;
                achievementButtons[i].onClick.RemoveAllListeners();
                achievementButtons[i].onClick.AddListener(delegate { AchievementButtonClicked(i2); });
            }

            Slider progressBar = GameUtils.GetChildGameObject(achievementButtons[i].gameObject, "Slider", true).GetComponent<Slider>();
            progressBar.maxValue = achievementsData[i].achievementGoal;
            progressBar.value = currentAchievement.achievementProgress;
            GameUtils.GetChildGameObject(achievementButtons[i].gameObject, "TextAmount", true).GetComponent<Text>().text = progressBar.value + "/" + progressBar.maxValue;

            achievementWonContainer.SetActive(false);
            achievementClaimContainer.SetActive(false);
            achievementOffContainer.SetActive(false);
            if (currentAchievement.isNew())
            {
                achievementClaimContainer.SetActive(true);
            }
            else if (currentAchievement.isCompleted())
            {
                achievementWonContainer.SetActive(true);
				//Steam
				SteamUserStats.SetAchievement(achievementsData[i].achievementNameID);
				SteamUserStats.StoreStats (); 
				Debug.Log("Achievement name : " + achievementsData[i].achievementNameID);
				//
            }
            else
            {
                achievementOffContainer.SetActive(true);
            }

            if (currentAchievement.isCompleted())
            {
                achievementAmountDone++;
            }
        }

        achievementProgression.text = (((float)achievementAmountDone / (float)achievementsData.Length) * 100f).ToString("0") + "%";
    }
    
    private void AchievementButtonClicked(int index)
    {
        PackController.Instance.CollectReward(achievementsData[index].achievementReward, GameUtils.ECurrencyType.Achievement, true, 0f, string.Empty);
        Achievement achievment = FindAchievements(achievementsData[index].name);
        achievment.hasClaimedReward = true;
        SaveAchievements(DataController.SAVEPRIORITY.QUEUE);
        UpdateAchievementUI();
    }

#if UNITY_EDITOR || DEBUG_MOBILE
    private void DebugAchievementResetButtonClicked()
    {
        ResetAchievementProgression();
        UpdateAchievementUI();
		//Steam reset
		SteamUserStats.ResetAllStats(true);
		SteamUserStats.RequestCurrentStats();
		SteamUserStats.StoreStats (); 
		Debug.Log("steam user stats should be reset : ");
		//
        SaveAchievements(DataController.SAVEPRIORITY.QUEUE);
    }

    private void DebugAchievementCompleteButtonClicked()
    {
        CompleteAchievementProgression();
        UpdateAchievementUI();
        SaveAchievements(DataController.SAVEPRIORITY.QUEUE);
    }
#endif
#endregion

    public void ResetAchievementProgression()
    {
        for (int i = 0; i < achievements.Count; ++i)
        {
            achievements[i].ResetProgression();
        }
    }

    public void CompleteAchievementProgression()
    {
        for (int i = 0; i < achievements.Count; ++i)
        {
            achievements[i].CompleteProgression();
        }
    }
    
    public void UpdateAchievementProgression(AchievementData.EAchievementType achievementType, float progressionValue, AchievementData.EAchievementOverride achievementOverride)
    {
        for (int i = 0; i < achievements.Count; ++i)
        {
            if (achievements[i].GetAchievementType() == achievementType)
            {
/*#if !UNITY_EDITOR
                float oldValue = achievements[i].achievementProgress;
#endif*/
                switch (achievementOverride)
                {
                    case AchievementData.EAchievementOverride.Replace:                  // always replace value
                        if (achievements[i].achievementProgress != progressionValue)
                        {
                            achievements[i].achievementProgress = progressionValue;
                        }
                        break;
                    case AchievementData.EAchievementOverride.Larger:                   // change when new value is larger
                        if (progressionValue > achievements[i].achievementProgress)
                        {
                            achievements[i].achievementProgress = progressionValue;
                        }
                        break;
                    case AchievementData.EAchievementOverride.LargerOrEqual:            // change when new value is larger or equal
                        if (progressionValue >= achievements[i].achievementProgress)
                        {
                            achievements[i].achievementProgress = progressionValue;
                        }
                        break;
                    case AchievementData.EAchievementOverride.Smaller:                  // change when new value is smaller
                        if (progressionValue < achievements[i].achievementProgress)
                        {
                            achievements[i].achievementProgress = progressionValue;
                        }
                        break;
                    case AchievementData.EAchievementOverride.SmallerOrEqual:           // change when new value is smaller or equal
                        if (progressionValue <= achievements[i].achievementProgress)
                        {
                            achievements[i].achievementProgress = progressionValue;
                        }
                        break;
                    case AchievementData.EAchievementOverride.Progress:                 // add to progression
                    default:
                        if (achievements[i].isCompleted()) continue;
                        achievements[i].achievementProgress += progressionValue;
                        break;
                }
            }
        }

        SaveAchievements(DataController.SAVEPRIORITY.NORMAL);
    }

    public int GetNewAchievementAmount()
    {
        int newAchievementCount = 0;
        for (int i = 0; i < achievements.Count; ++i)
        {
            if (achievements[i].isNew())
            {
                newAchievementCount++;
            }
        }
        return newAchievementCount;
    }
    
#region Data handling
    private void PopulateAchievementsData()
    {
        achievementsData = Resources.LoadAll<AchievementData>("Achievements").OrderBy(ob => ob.achievementOrder).ToArray();
    }

    private AchievementData FindAchievementData(string achievementID)
    {
        for (int i = 0; i < achievementsData.Length; ++i)
        {
            if (achievementsData[i].name == achievementID)
            {
                return achievementsData[i];
            }
        }

        Debug.LogWarning("Could not find achievement data with ID : " + achievementID);
        return null;
    }

    private Achievement FindAchievements(string achievementID)
    {
        for (int i = 0; i < achievements.Count; ++i)
        {
            if (achievements[i].achievementID == achievementID)
            {
                return achievements[i];
            }
        }

        Debug.LogWarning("Could not find achievement with ID : " + achievementID);
        return null;
    }

    public void SaveAchievements(DataController.SAVEPRIORITY priority)
    {
        DataController.Instance.SaveAchievementData(priority);
    }

    public void LoadAchievements(List<Achievement> remoteAchievements)
    {
        PopulateAchievementsData();

        if (remoteAchievements == null)
        {
            // If not, try to save the current data (if existing) from the Achievement controller.
            System.Exception ex = JsonUtils<List<Achievement>>.Load(GameUtils.ACHIEVEMENTS_JSON, out achievements, GameUtils.ELoadType.DynamicData);
            if (ex != null && ex.GetType() == typeof(FileLoadException))
            {
                ResetAchievements();
            }
        }
        else
        {
            achievements = remoteAchievements;
        }
        // make sure new and old achievments are cleaned up
        FixAchievements();
    }

    private void FixAchievements()
    {
        bool dirty = false;

        // remove achievments that don't exist anymore
        for (int i = achievements.Count - 1; i >= 0 ; i--)
        {
            if (FindAchievementData(achievements[i].achievementID) == null)
            {
                Debug.Log("Found old achievement: " + achievements[i].achievementID + " - cleaned");
                achievements.RemoveAt(i);
                dirty = true;
            }
        }

        // add achievments that don't exist yet
        for (int j = 0; j < achievementsData.Length; ++j)
        {
            if (FindAchievements(achievementsData[j].name) == null)
            {
                Debug.Log("Found missing achievement: " + achievementsData[j].name + " - added");
                Achievement newAchievement = new Achievement();
                newAchievement.achievementID = achievementsData[j].name;
                newAchievement.achievementProgress = 0f;
                newAchievement.hasClaimedReward = false;
                achievements.Add(newAchievement);
                dirty = true;
            }
        }

        if (dirty) SaveAchievements(DataController.SAVEPRIORITY.QUEUE);
    }

    public void ResetAchievements()
    {
        if (achievements == null)
        {
            achievements = new List<Achievement>();
        }
        achievements.Clear();

        for (int i = 0; i < achievementsData.Length; ++i)
        {
            Achievement newAchievement = new Achievement();
            newAchievement.achievementID = achievementsData[i].name;
            newAchievement.achievementProgress = 0f;
            newAchievement.hasClaimedReward = false;
            achievements.Add(newAchievement);
        }

        SaveAchievements(DataController.SAVEPRIORITY.QUEUE);
    }
#endregion
}


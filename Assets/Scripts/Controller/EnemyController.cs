using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System.Collections.Generic;

#pragma warning disable 0414
#pragma warning disable 0649

public class EnemyController : UnitySingleton<EnemyController>
{
    private readonly float ENEMY_ORIENTATION_DETECTION = 100f;

    [System.Serializable]
    public class EnemyRankInfo
    {
        public float veteranHealthMaxMultiplier = 1.5f;
        public float veteranSpeedMultiplier = 1.2f;
        public float veteranScoreMultiplier = 1.5f;
        public int veteranBaseSpawnPercentage = 6;
        public int veteranSpawnPercentagePerRound = 50;

        public float eliteHealthMaxMultiplier = 2f;
        public float eliteSpeedMultiplier = 1.4f;
        public float eliteScoreMultiplier = 2f;
        public int eliteBaseSpawnPercentage = 3;
        public int eliteSpawnPercentagePerRound = 25;

        public float heroHealthMaxMultiplier = 2.5f;
        public float heroSpeedMultiplier = 1.6f;
        public float heroScoreMultiplier = 2.5f;
        public int heroBaseSpawnPercentage = 1;
        public int heroSpawnPercentagePerRound = 10;

        public float bossHealthMaxMultiplier = 0.5f;
    }

    #region Exposed Enemy Values
    [SerializeField]
    private int numMaxEnemies = 15;

    [SerializeField]
    private EnemyRankInfo enemyRankInfo;

    [SerializeField]
    [Range(0f, 1f)]
    [Tooltip("Spawn chance of enemy bonus units per enemy spawn")]
    private float bonusUnitSpawnPercentage = 0.05f;

    [SerializeField]
    [Range(0f, 1f)]
    [Tooltip("Spawn chance of the Convoy (vs Transport Plane)")]
    private float bonusUnitConvoyPercentage = 0.80f;

    [SerializeField]
    [Tooltip("Health multiplier for normal enemies (debug purposes).")]
    private float normalEnemyHealthMultiplier = 1f;

    [SerializeField]
    [Tooltip("Health multiplier for hard enemies (debug purposes).")]
    private float hardEnemyHealthMultiplier = 1f;

    [SerializeField]
    [Tooltip("Health multiplier for insane enemies (debug purposes).")]
    private float insaneEnemyHealthMultiplier = 1f;

    [SerializeField]
    [Tooltip("Speed multiplier for all modes (debug purposes).")]
    private float normalEnemySpeedMultiplier = 1f;

    [SerializeField]
    [Tooltip("Speed multiplier for all modes (debug purposes).")]
    private float hardEnemySpeedMultiplier = 1f;

    [SerializeField]
    [Tooltip("Speed multiplier for all modes (debug purposes).")]
    private float insaneEnemySpeedMultiplier = 1f;
    #endregion

    private EnemyData[] enemiesData;
    private List<ChallengeData.EnemyInfo> enemiesInWave;
    private PathNode lastPathNode;
    
    private float nextSpawn;
    private int numAliveEnemiesFromWave;
    private int enemyAmountLeftToSpawn; // For Last Stands
    private List<GameObject> aliveEnemies = new List<GameObject>();
    private List<GameObject> aliveLoot = new List<GameObject>();
    private Text alertField;
    private float alertFieldTimer;
    private bool allEnemiesInWaveSpawned = true;

    private Dictionary<GameUtils.UnitType, Queue<GameObject>> inactiveEnemies = new Dictionary<GameUtils.UnitType, Queue<GameObject>>();
    private Queue<GameObject> inactiveSpawns = new Queue<GameObject>();
    private Queue<GameObject> inactiveJammedBlips = new Queue<GameObject>();
    private GameObject enemyContainer;
    private GameObject spawnContainer;
    private GameObject jammedBlipContainer;

    [Tooltip("Minimum distance to next enemy when spawning")]
    [SerializeField]
    private float minDistanceToOtherUnits = 150f;

    [Tooltip("Don't do too much, since this can reduce the framerate significantly!")]
    [SerializeField]
    private int triesToFindBetterPosition = 10;

    public class BossSpecial
    {
        public BossSpecial(GameUtils.EBossSpecial bossSpecialType)
        {
            this.bossSpecialType = bossSpecialType;
            active = 0;
            nextTimer = 0f;
            time = 0f;
            value = 0f;
        }

        public GameUtils.EBossSpecial bossSpecialType;
        public int active = 0;
        public float nextTimer;
        public float time;
        public float value;
    }

    private List<BossSpecial> bossSpecials = new List<BossSpecial>();

    public enum EEnemyOrientation
    {
        North,
        East,
        South,
        West
    }

    public enum EEnemyRank
    {
        Normal,
        Veteran,
        Elite,
        Hero
    }

    public void RegisterLoot(GameObject loot)
    {
        if (!aliveLoot.Contains(loot))
        {
            aliveLoot.Add(loot);
        }
    }    

    public void UnregisterLoot(GameObject loot)
    {
        if (aliveLoot.Contains(loot))
        {
            aliveLoot.Remove(loot);
        }
    }

    public List<GameObject> AliveEnemies
    {
        get { return aliveEnemies; }
    }
    
    public int NumAliveEnemiesFromWave
    {
        get { return numAliveEnemiesFromWave; }
        set { numAliveEnemiesFromWave = value; }
    }

    public void SetWaveEnemies(ChallengeData.WaveData2 waveData)
    {
        enemiesInWave = new List<ChallengeData.EnemyInfo>();
        for(int i = 0; i < waveData.enemyInfo.Length; ++i)
        {
            ChallengeData.EnemyInfo newEnemyWaveInfo = new ChallengeData.EnemyInfo();
            newEnemyWaveInfo.type = waveData.enemyInfo[i].type;
            newEnemyWaveInfo.amount = waveData.enemyInfo[i].amount;
            newEnemyWaveInfo.spawnPercentage = waveData.enemyInfo[i].spawnPercentage;
            enemiesInWave.Add(newEnemyWaveInfo);
        }
        enemyAmountLeftToSpawn = waveData.enemyAmount;
        allEnemiesInWaveSpawned = false;
    }
    
    public override void Awake()
    {
        base.Awake();

        enemyContainer = new GameObject("Enemies");
        spawnContainer = new GameObject("Spawns");
        jammedBlipContainer = new GameObject("JammedBlips");
        GameObject mask = GameObject.Find("RadarScanMask");
        enemyContainer.transform.parent = mask.transform;
        spawnContainer.transform.parent = mask.transform;
        jammedBlipContainer.transform.parent = mask.transform;

        alertField = GameObject.Find("AlertField").GetComponent<Text>();
        alertField.enabled = false;
        alertFieldTimer = 0f;
        
        nextSpawn = 0f;

        RegisterDependency(WaveController.Instance);
        RegisterDependency(BaseController.Instance);
        RegisterDependency(WeaponController.Instance);

        PopulateEnemiesData();
        PopulateBossSpecials();
    }

    private void PopulateEnemiesData()
    {
        // Note :: Not sure why its not able to cast
        Object[] loadedEnemiesData = Resources.LoadAll("Enemies", typeof(EnemyData));
        enemiesData = new EnemyData[loadedEnemiesData.Length];
        for (int i = 0; i < enemiesData.Length; ++i)
        {
            enemiesData[i] = loadedEnemiesData[i] as EnemyData;
        }
    }

    private void PopulateBossSpecials()
    {
        bossSpecials.Clear();
        bossSpecials.Add(new BossSpecial(GameUtils.EBossSpecial.Repair));
        bossSpecials.Add(new BossSpecial(GameUtils.EBossSpecial.JamRadar));
        bossSpecials.Add(new BossSpecial(GameUtils.EBossSpecial.Shield));
    }

    public override bool Update()
    {
        if (!base.Update()) return false;
        if (WaveController.Instance.Paused) return false;
        
        UpdateEnemySpawn();
        UpdateAlertField();
        UpdateSpecials();

        return true;
    }

    private void UpdateEnemySpawn()
    {
        if (allEnemiesInWaveSpawned) return;

        // If reached the maximum number of enemies coming from waves
        // Don't spawn the next one right now, queue it instead
        if (HasMaxEnemies()) return;

        nextSpawn -= Time.deltaTime;

        if (nextSpawn <= 0f)
        {
            SpawnEnemy();
        }
    }

    private void UpdateAlertField()
    {
        if (alertFieldTimer > 0f)
        {
            alertFieldTimer -= Time.deltaTime;
            alertField.enabled = true;
        }
        else
        {
            alertField.enabled = false;
        }
    }

    public bool HasMaxEnemies()
    {
        return numAliveEnemiesFromWave >= numMaxEnemies;
    }

    public void SpawnEnemy()
    {
        SpawnEnemy(GameUtils.UnitType.NONE, Vector3.zero, false, false);
        RollBonusUnit();
    }

    public void SpawnEnemy(GameUtils.UnitType unitTypeOverride, Vector3 overrideStartPosition, bool forceSpawn, bool disableBlip, EEnemyRank enemyRank = EEnemyRank.Normal)
    {
        // Whether the enemy is from the wave settings
        // (i.e. Minions and Bonus are not)
        bool isEnemyFromWave = false;

        // If no type override and is a normal spawn (not forced)
        if (unitTypeOverride == GameUtils.UnitType.NONE 
            && !forceSpawn)
        {
            isEnemyFromWave = true;
            numAliveEnemiesFromWave++;
            
            // Choose how to spawn the enemy unit based on the challenge type
            // If Mission
            if (WaveController.Instance.CurrentChallengeType == ChallengeData.EChallengeType.Mission)
            {
                // - Randomly pick one from the enemy remaining to spawn in the current wave -
                // Get all the index that still have enemies amount
                List<int> remainingEnemyInWaveIndexes = GetRemainingEnemyInWaveIndexes();

                // Roll between all remaining enemies indexes
                int randIndex = Random.Range(0, remainingEnemyInWaveIndexes.Count);

                // Get the enemy index
                int randEnemyIndex = remainingEnemyInWaveIndexes[randIndex];

                // Assign the unit type
                unitTypeOverride = enemiesInWave[randEnemyIndex].type;

                // Decrease the amount
                enemiesInWave[randEnemyIndex].amount--;

                // Is there more enemy to spawn
                allEnemiesInWaveSpawned = GetRemainingEnemyInWaveIndexes().Count == 0;
            }
            // If Last Stand
            else if (WaveController.Instance.CurrentChallengeType == ChallengeData.EChallengeType.LastStand)
            {
                // - Randomly pick one from the enemy spawn percentage in the current wave -
                // Assign the unit type
                unitTypeOverride = RollEnemyType();

                // Decrease the amount left to spawn
                enemyAmountLeftToSpawn--;

                // Manage the enemy rank
                enemyRank = RollEnemyRank();

                // Is there more enemy to spawn
                allEnemiesInWaveSpawned = enemyAmountLeftToSpawn == 0;
            }
            // If Operation
            else if (WaveController.Instance.CurrentChallengeType == ChallengeData.EChallengeType.Operation)
            {
                // - Randomly pick one from the enemy remaining to spawn in the current wave -
                // Get all the index that still have enemies amount
                List<int> remainingEnemyInWaveIndexes = GetRemainingEnemyInWaveIndexes();

                // Roll between all remaining enemies indexes
                int randIndex = Random.Range(0, remainingEnemyInWaveIndexes.Count);

                // Get the enemy index
                int randEnemyIndex = remainingEnemyInWaveIndexes[randIndex];

                // Assign the unit type
                unitTypeOverride = enemiesInWave[randEnemyIndex].type;

                // Decrease the amount
                enemiesInWave[randEnemyIndex].amount--;

                // Is there more enemy to spawn
                allEnemiesInWaveSpawned = GetRemainingEnemyInWaveIndexes().Count == 0;
            }

            // If there's enemy remaining in the current wave
            if (!allEnemiesInWaveSpawned)
            {
                // Set next spawn timer
                nextSpawn += GameUtils.GetTTK(ChallengeController.Instance.GetCurrentChallengeDifficulty(), ChallengeController.Instance.GetCurrentChallenge().GetChallengeType());
            }     
            
            //Debug.Log("Wave spawned " + unitTypeOverride.ToString() + "[" + enemyRank + "]");
        }

        // Load the prefab and set the values
        EnemyData enemyData = GetEnemyData(unitTypeOverride);
        GameObject enemyGO = GetEnemyFromPool(enemyData);

        MovingEnemyObject enemyObject = enemyGO.GetComponent<MovingEnemyObject>();
        SetupMovement(enemyObject, enemyData.movementType, enemyData.targetBaseType, overrideStartPosition);
        enemyObject.InitEnemy(enemyData, ChallengeController.Instance.GetCurrentChallengeDifficulty(), enemyRank, isEnemyFromWave);      
        
        // Store the enemy for reference
        aliveEnemies.Add(enemyGO);

        // Spawn new enemy crosshair
        if (!disableBlip)
        {
            GameObject spawn = GetSpawnFromPool();

            // Set the visual based on whether it's a special unit or not
            bool isSpecialUnit = GameUtils.IsSpecialUnit(enemyData.enemyType);
            GameUtils.GetChildGameObject(spawn, "NormalSpawn", true).SetActive(!isSpecialUnit);
            GameUtils.GetChildGameObject(spawn, "SpecialSpawn", true).SetActive(isSpecialUnit);

            spawn.transform.position = enemyObject.transform.position;
            spawn.GetComponent<SpawnEnemyObject>().Init();
        }

        // Play new enemy spawn sound
        AudioManager.Instance.PlaySound(AudioManager.ESoundType.NewEnemySpawn);

        // Show alert field if ballistic missile
        if (unitTypeOverride == GameUtils.UnitType.BALLISTICMISSILE)
        {
            ShowIncomingMissileAlert();
        }
    }

    private GameUtils.UnitType RollEnemyType()
    {
        bool foundRoll = false;

        GameUtils.UnitType enemyType = GameUtils.UnitType.INFANTRY;
        int randomRoll = Random.Range(1, 101); // between 1 and 100
        int totalRoll = 0;

        // For each type of enemy in the current wave
        for (int i = 0; i < enemiesInWave.Count; ++i)
        {
            totalRoll += enemiesInWave[i].spawnPercentage;
            if (randomRoll <= totalRoll)
            {
                enemyType = enemiesInWave[i].type;
                foundRoll = true;
                break;
            }
        }

        if (!foundRoll)
        {
            enemyType = enemiesInWave[0].type;

#if UNITY_EDITOR || DEBUG_MOBILE
            Debug.LogWarning("The roll enemy type did not work, spawning a random one instead. PLEASE CHECK WAVE SETTINGS!");
#endif
        }
        
        return enemyType;
    }

    private EEnemyRank RollEnemyRank()
    {
        int currentRound = WaveController.Instance.CurrentLastStandRoundIndex;

        // Try spawning an hero
        int randomRoll = Random.Range(1, 101); // between 1 and 100
        int heroSpawnPercentage = enemyRankInfo.heroBaseSpawnPercentage + (enemyRankInfo.heroSpawnPercentagePerRound * currentRound);
        if (randomRoll <= heroSpawnPercentage)
        {
            return EEnemyRank.Hero;
        }

        // Try spawning an elite
        randomRoll = Random.Range(1, 101); // between 1 and 100
        int eliteSpawnPercentage = enemyRankInfo.eliteBaseSpawnPercentage + (enemyRankInfo.eliteSpawnPercentagePerRound * currentRound);
        if (randomRoll <= eliteSpawnPercentage)
        {
            return EEnemyRank.Elite;
        }

        // Try spawning an veteran
        randomRoll = Random.Range(1, 101); // between 1 and 100
        int veteranSpawnPercentage = enemyRankInfo.veteranBaseSpawnPercentage + (enemyRankInfo.veteranSpawnPercentagePerRound * currentRound);
        if (randomRoll <= veteranSpawnPercentage)
        {
            return EEnemyRank.Veteran;
        }
                
        return EEnemyRank.Normal;
    }

    public void PutSpawnInPool(GameObject spawn)
    {
        spawn.SetActive(false);

        inactiveSpawns.Enqueue(spawn);
    }

    private GameObject GetSpawnFromPool()
    {
        GameObject spawn;
        if (inactiveSpawns.Count > 0)
        {
            spawn = inactiveSpawns.Dequeue();
            spawn.SetActive(true);
            return spawn;
        }

        spawn = (GameObject)Instantiate(Resources.Load("EnemySpawn"));
        spawn.transform.parent = spawnContainer.transform;

        return spawn;
    }

    public void PutJammedBlipInPool(GameObject jammedBlip)
    {
        jammedBlip.SetActive(false);

        inactiveJammedBlips.Enqueue(jammedBlip);
    }

    public GameObject GetJammedBlipFromPool()
    {
        GameObject jammedBlip;
        if (inactiveJammedBlips.Count > 0)
        {
            jammedBlip = inactiveJammedBlips.Dequeue();
            jammedBlip.SetActive(true);
            return jammedBlip;
        }

        jammedBlip = (GameObject)Instantiate(Resources.Load("JammedBlip"));
        jammedBlip.transform.parent = jammedBlipContainer.transform;

        return jammedBlip;
    }

    public void PutEnemyInPool(GameUtils.UnitType enemyType, GameObject enemy)
    {
        enemy.SetActive(false);

        if (!inactiveEnemies.ContainsKey(enemyType))
        {
            inactiveEnemies.Add(enemyType, new Queue<GameObject>());
        }

        inactiveEnemies[enemyType].Enqueue(enemy);
    }

    private GameObject GetEnemyFromPool(EnemyData enemyData)
    {
        GameObject enemy;
        if (inactiveEnemies.ContainsKey(enemyData.enemyType))
        {
            if (inactiveEnemies[enemyData.enemyType].Count > 0)
            {
                enemy = inactiveEnemies[enemyData.enemyType].Dequeue();
                enemy.SetActive(true);
                return enemy;
            }
        }
        // use the right prefab for air or ground units
        if (enemyData.isUnitAir)
            enemy = (GameObject)Instantiate(Resources.Load("EnemyPrefabAir"));

        // use the right prefab for infantry units
        else if (enemyData.enemyNameID == "ENEMY_INFANTRY_NAME" || enemyData.enemyNameID == "ENEMY_PARATROOPER_INFANTRY_NAME")
            enemy = (GameObject)Instantiate(Resources.Load("EnemyPrefabInfantry"));
       
        //
        else
            enemy = (GameObject)Instantiate(Resources.Load("EnemyPrefab"));

        enemy.name = enemyData.enemyType.ToString();
        enemy.transform.parent = enemyContainer.transform;

        return enemy;
    }

    private void RollBonusUnit()
    {
        // Don't spawn a bonus unit on the last enemy unit remaining
        if (allEnemiesInWaveSpawned) return;

        // Rolling the bonus unit spawn
        float randomRoll = Random.Range(0f, 1f);
        //Debug.Log("randomRoll = " + randomRoll + "/// bonusUnitSpawnPercentage = " + bonusUnitSpawnPercentage);
        if (randomRoll <= bonusUnitSpawnPercentage)
        {
            // Rolling the bonus unit type
            float randomBonusUnitTypeRoll = Random.Range(0f, 1f);
            GameUtils.UnitType bonusUnitType = randomBonusUnitTypeRoll < bonusUnitConvoyPercentage ? GameUtils.UnitType.CONVOY : GameUtils.UnitType.TRANSPORTPLANE;
            SpawnEnemy(bonusUnitType, Vector3.zero, true, false);
        }
    }

    private void ShowIncomingMissileAlert()
    {
        alertField.text = LocalizationManager.Instance.GetLocalizedString(LocalizationManager.INCOMING_MISSILE_TEXT);
        alertField.enabled = true;
        alertFieldTimer = GameUtils.ALERT_FIELD_TIMER;

        // Sound hook - Missile alert
        AudioManager.Instance.PlaySound(AudioManager.ESoundType.MissileAlert);
    }

    public void ShowSpecialAlert(GameUtils.EBossSpecial special, bool enable)
    {
        switch(special)
        {
            case GameUtils.EBossSpecial.Repair:
                alertField.text = LocalizationManager.Instance.GetLocalizedString(LocalizationManager.ALERT_REPAIR_TEXT);
                break;
            case GameUtils.EBossSpecial.JamRadar:
                alertField.text = LocalizationManager.Instance.GetLocalizedString(LocalizationManager.ALERT_ECM_TEXT);
                break;
            case GameUtils.EBossSpecial.Shield:
                alertField.text = LocalizationManager.Instance.GetLocalizedString(LocalizationManager.ALERT_SHIELD_TEXT);
                break;
        }
        
        alertField.enabled = enable;
        if (enable)
        {
            alertFieldTimer = GameUtils.ALERT_FIELD_TIMER;

            // Sound hook - Missile alert
            AudioManager.Instance.PlaySound(AudioManager.ESoundType.SpecialAlert);
        }
        else
        {
            alertFieldTimer = 0f;
        }
    }

    // QUESTION :: Why is CleanAliveEnemies needed?
    // Note :: I think it's for precaution, should not be needed normally.
    private void CleanAliveEnemies()
    {
        for (int i = 0; i < aliveEnemies.Count; ++i)
        {
            if (aliveEnemies[i] == null)
            {
                Debug.LogWarning("CleanAliveEnemies cleaned enemy at index " + i);
                aliveEnemies.RemoveAt(i);
                return;
            }
        }
    }

    public void SetupMovement(MovingEnemyObject enemyMovingObject, GameUtils.EMovementType movementType, GameUtils.ETargetBaseType targetBaseType, Vector3 overrideStartPosition)
    {
        switch (movementType)
        {
            case GameUtils.EMovementType.FollowPathToBase:
                {
                    //Debug.Log(enemyMovingObject.name + "'s target is End Node #" + endID);
                    //DebugUtils.DrawPositionCrossWithDuration(endNodes[endID].transform.position, Color.yellow, 30f, 100);

                    // Get the start node
                    PathNode startNode = null;
                    // If there's an override for the starting position (i.e. infantry)
                    if (overrideStartPosition != Vector3.zero)
                    {
                        // Note :: We are creating a new start node for the path finding system
                        // from where the current enemy is. This is happening when the enemy's target
                        // has been destroyed and it needs to find a new path.
                        GameObject newNode = (GameObject)Instantiate(Resources.Load("Node"));
                        newNode.name = "NewStartNode";
                        newNode.transform.position = overrideStartPosition;
                        startNode = newNode.GetComponent<PathNode>();
                        startNode.NodeType = PathNode.PathNodeType.NORMAL;
                        // add the connection of the nodes in both directions!
                        startNode.ConnectedNodes.Add(enemyMovingObject.GetNextPathNode());
                        enemyMovingObject.GetNextPathNode().ConnectedNodes.Add(startNode);
                    }
                    else
                    {
                        startNode = GetRandomValidStartNode(PathFindingController.Instance.StartNodes);
                        lastPathNode = startNode;
                    }

                    List<PathNode> endNodes = PathFindingController.Instance.EndNodes;
                    if (endNodes.Count == 0)
                    {
                        Debug.LogWarning("No End Node detected! Please have atleast 1 end node in the scene.");
                        return;
                    }
                    int endID = Random.Range(0, endNodes.Count);
                    enemyMovingObject.Path = PathFindingController.Instance.FindPath(startNode, endNodes[endID]);
                }
                break;
            case GameUtils.EMovementType.FollowPathToExit:
                {
                    List<PathNode> startNodes = PathFindingController.Instance.StartNodes;
                    if (startNodes.Count < 2)
                    {
                        Debug.LogError("Not enough Start Nodes detected! Please have atleast 2 start nodes in the scene.");
                        return;
                    }

                    PathNode startNode = GetRandomValidStartNode(startNodes);
                    lastPathNode = startNode;

                    int endID = Random.Range(0, startNodes.Count);
                    while (startNodes[endID] == startNode)
                    {
                        endID = Random.Range(0, startNodes.Count);
                    }
                    enemyMovingObject.Path = PathFindingController.Instance.FindPath(startNode, startNodes[endID]);
                }
                break;
            case GameUtils.EMovementType.StraightToBase:
                {
                    // these units will not follow a path
                    enemyMovingObject.Path = null;
                    if (overrideStartPosition != Vector3.zero)
                    {
                        enemyMovingObject.StartPosition = overrideStartPosition;
                    }
                    else
                    {
                        GetStartPosition(enemyMovingObject);
                    }
                    enemyMovingObject.TargetBase = GetTargetBase(targetBaseType);
                    enemyMovingObject.TargetPosition = enemyMovingObject.TargetBase.transform.position;
                }
                break;
            case GameUtils.EMovementType.Straight:
            default:
                {
                    // these units will not follow a path
                    enemyMovingObject.Path = null;
                    if (overrideStartPosition != Vector3.zero)
                    {
                        enemyMovingObject.StartPosition = overrideStartPosition;
                    }
                    else
                    {
                        GetStartPosition(enemyMovingObject);
                    }
                    enemyMovingObject.TargetBase = null;
                 }
                break;
        }
    }

    private PathNode GetRandomValidStartNode(List<PathNode> startNodes)
    {
        List<PathNode> validStartNodes = GetValidStartNodes(startNodes);

        PathNode startNode = null;
        int startID = Random.Range(0, validStartNodes.Count);
        // If there's 2 or more valid start nodes
        if (validStartNodes.Count > 1)
        {
            while (validStartNodes[startID] == lastPathNode)
            {
                startID = Random.Range(0, validStartNodes.Count);
            }
        }
        startNode = validStartNodes[startID];
        
        return startNode;         
    }
    
    private List<PathNode> GetValidStartNodes(List<PathNode> startNodes)
    {
        // Valid start nodes (valid if is the right orientation from the current wave)
        List<PathNode> validStartNodes = new List<PathNode>();

        // Get the current wave data for the wave orientations
        ChallengeData.WaveData2 currentWaveData = ChallengeController.Instance.GetCurrentWaveData();

        // For each start nodes
        for (int i = 0; i < startNodes.Count; ++i)
        {
            bool isValidNode = false;

            // WARNING :: If the start node is not close enough to the sides, it won't work.
            // ALTERNATIVE :: Refactor the path node to have a orientation variable assigned to it.
            if (currentWaveData.EnemyOrientationEast
                && Mathf.Abs(startNodes[i].transform.position.x - PlayfieldController.Instance.TopRight.x) < (ENEMY_ORIENTATION_DETECTION * PlayfieldController.Instance.WidthScale))
            {
                isValidNode = true;
            }
            else if (currentWaveData.EnemyOrientationNorth
                && Mathf.Abs(startNodes[i].transform.position.y - PlayfieldController.Instance.TopRight.y) < (ENEMY_ORIENTATION_DETECTION * PlayfieldController.Instance.HeightScale))
            {
                isValidNode = true;
            }
            else if (currentWaveData.EnemyOrientationSouth
                && Mathf.Abs(startNodes[i].transform.position.y - PlayfieldController.Instance.BottomLeft.y) < (ENEMY_ORIENTATION_DETECTION * PlayfieldController.Instance.HeightScale))
            {
                isValidNode = true;
            }
            else if (currentWaveData.EnemyOrientationWest
                && Mathf.Abs(startNodes[i].transform.position.x - PlayfieldController.Instance.BottomLeft.x) < (ENEMY_ORIENTATION_DETECTION * PlayfieldController.Instance.WidthScale))
            {
                isValidNode = true;
            }

            if (isValidNode)
            {
                validStartNodes.Add(startNodes[i]);
            }
        }

        // Safeguard
        if (validStartNodes.Count == 0)
        {
            Debug.LogWarning("No valid (with orientation) start nodes were found! Spawning enemy at random orientation.");
            validStartNodes = startNodes;
        }
        
        return validStartNodes;
    }

    public void RefreshEnemiesDestination(GameObject destroyedBase)
    {
        PathNode node = destroyedBase.GetComponent<BaseObject>().Node;
        CleanAliveEnemies();
        for (int i = 0; i < aliveEnemies.Count; ++i)
        {
            // BUG :: Sometimes, even with CleanAliveEnemies() before, aliveEnemies[i] is null..
            if (aliveEnemies[i] == null)
            {
                Debug.LogWarning("Could not clean alive enemies #" + i);
                continue;
            }
            MovingEnemyObject enemyMovingObject = aliveEnemies[i].GetComponent<MovingEnemyObject>();
            if ((enemyMovingObject.Path == null && enemyMovingObject.TargetBase == destroyedBase) || (enemyMovingObject.Path != null && enemyMovingObject.Path[enemyMovingObject.Path.Count - 1] == node))
            {
                //Debug.Log(aliveEnemies[i].name + " enemy's target position has been destroyed, switching target.");
                SetupMovement(enemyMovingObject, enemyMovingObject.EnemyData.movementType, enemyMovingObject.EnemyData.targetBaseType, aliveEnemies[i].transform.position);
                aliveEnemies[i].GetComponent<MovingEnemyObject>().InitPath();
            }
        }
    }

    private void GetStartPosition(MovingEnemyObject movingEnemyObject)
    {
        float localHeight = 1f + (PlayfieldController.Instance.HeightAdjust * PlayfieldController.Instance.HeightScale);
        float localWidth = 1f + (PlayfieldController.Instance.WidthAdjust * PlayfieldController.Instance.WidthScale);

        // Get the current wave data for the wave orientations
        ChallengeData.WaveData2 currentWaveData = ChallengeController.Instance.GetCurrentWaveData();
        List<EEnemyOrientation> enemyOrientations = new List<EEnemyOrientation>();
        if (currentWaveData.EnemyOrientationEast)
        {
            enemyOrientations.Add(EEnemyOrientation.East);
        }
        if (currentWaveData.EnemyOrientationNorth)
        {
            enemyOrientations.Add(EEnemyOrientation.North);
        }
        if (currentWaveData.EnemyOrientationSouth)
        {
            enemyOrientations.Add(EEnemyOrientation.South);
        }
        if (currentWaveData.EnemyOrientationWest)
        {
            enemyOrientations.Add(EEnemyOrientation.West);
        }

        // Safeguard
        if (enemyOrientations.Count == 0)
        {
            Debug.LogWarning("There were no enemy orientation set in the challenge data! Spawning enemy at random orientation.");
            enemyOrientations.Add(EEnemyOrientation.East);
            enemyOrientations.Add(EEnemyOrientation.North);
            enemyOrientations.Add(EEnemyOrientation.South);
            enemyOrientations.Add(EEnemyOrientation.West);
        }

        int randomEnemyOrientationIndex = Random.Range(0, enemyOrientations.Count);
        EEnemyOrientation randomEnemyOrientation = enemyOrientations[randomEnemyOrientationIndex];

        Vector3 startPosition = Vector3.zero;
        Vector3 targetPosition = Vector3.zero;
        bool goodPositions = false;
        float height = 0;
        float width = 0;
        int counter = 0;
        float adjustedDistance = minDistanceToOtherUnits * PlayfieldController.Instance.WidthScale;
        float minDistanceSqr = adjustedDistance * adjustedDistance;
        while (!goodPositions && counter < 10)
        {
            counter++;
            goodPositions = true;
            float enemyWidth = 50f * PlayfieldController.Instance.WidthScale;
            height = Random.Range(PlayfieldController.Instance.BottomLeft.y + localHeight + enemyWidth, PlayfieldController.Instance.TopRight.y - localHeight - enemyWidth);
            width = Random.Range(PlayfieldController.Instance.BottomLeft.x + localWidth + enemyWidth, PlayfieldController.Instance.TopRight.x - localWidth - enemyWidth);

            switch (randomEnemyOrientation)
            {
                case EEnemyOrientation.West: // Left side
                    {
                        startPosition = new Vector3(PlayfieldController.Instance.BottomLeft.x + localWidth, height);
                        targetPosition = new Vector3(PlayfieldController.Instance.TopRight.x - localWidth, height);
                    }
                    break;
                case EEnemyOrientation.North: // Top side
                    {
                        startPosition = new Vector3(width, PlayfieldController.Instance.TopRight.y - localHeight);
                        targetPosition = new Vector3(width, PlayfieldController.Instance.BottomLeft.y + localHeight);
                    }

                    break;
                case EEnemyOrientation.East: // Right side
                    {
                        startPosition = new Vector3(PlayfieldController.Instance.TopRight.x - localWidth, height);
                        targetPosition = new Vector3(PlayfieldController.Instance.BottomLeft.x + localWidth, height);
                    }
                    break;
                case EEnemyOrientation.South: // Bottom side
                    {
                        startPosition = new Vector3(width, PlayfieldController.Instance.BottomLeft.y + localHeight);
                        targetPosition = new Vector3(width, PlayfieldController.Instance.TopRight.y - localHeight);
                    }
                    break;
            }

            for (int i = 0; i < aliveEnemies.Count; ++i)
            {
                if (Vector2.SqrMagnitude(aliveEnemies[i].transform.position - startPosition) < minDistanceSqr)
                {
                    goodPositions = false;
                }
            }
        }

        movingEnemyObject.StartPosition = startPosition;
        movingEnemyObject.TargetPosition = targetPosition;
    }

    public GameObject GetTargetBase(GameUtils.ETargetBaseType targetBaseType)
    {
        // random target Bases
        List<GameObject> possibleBases = new List<GameObject>();
        for (int i = 0; i < BaseController.Instance.Bases.Count; ++i)
        {
            // all bases can be attacked
            if (targetBaseType == GameUtils.ETargetBaseType.All)
            {
                possibleBases.Add(BaseController.Instance.Bases.ElementAt(i).Value);
            }
            // only the city can be attacked
            else if (targetBaseType == GameUtils.ETargetBaseType.City && BaseController.Instance.Bases.ElementAt(i).Key == GameUtils.BaseID.CITY)
            {
                possibleBases.Add(BaseController.Instance.Bases.ElementAt(i).Value);
            }
        }
        if (possibleBases.Count == 0) return null;

        int baseIndex = Random.Range(0, possibleBases.Count);
        return possibleBases.ElementAt(baseIndex);
    }

    public EnemyData GetEnemyData(GameUtils.UnitType enemyType)
    {
        for (int i = 0; i < enemiesData.Length; ++i)
        {
            if (enemiesData[i].enemyType == enemyType)
            {
                return enemiesData[i];
            }
        }

        Debug.LogWarning("Could not find enemy data : " + enemyType.ToString());
        return null;
    }

    public bool HasClearedAllEnemies()
    {
        //Debug.Log("AliveEnemies.Count = " + AliveEnemies.Count + " /// GetRemainingEnemyIndexes().Count  = " + GetRemainingEnemyIndexes().Count);
        return AliveEnemies.Count == 0 && GetRemainingEnemyInWaveIndexes().Count == 0;
    }

    public bool HasClearedAllNonBossEnemies()
    {
        //Debug.Log("AliveEnemies.Count = " + AliveEnemies.Count + " /// GetRemainingEnemyIndexes().Count  = " + GetRemainingEnemyIndexes().Count);
        bool cleared = HasClearedAllEnemies();
        if (!cleared)
        {
            cleared = true;
            for (int i = 0; i < AliveEnemies.Count; ++i)
            {
                MovingEnemyObject movingEnemyObject = AliveEnemies[i].GetComponent<MovingEnemyObject>();
                if (!GameUtils.IsBossUnit(movingEnemyObject.EnemyData.enemyType))
                {
                    cleared = false;
                    break;
                }
            }
        }
        return cleared;
    }

    public bool HasClearedAllLoot()
    {
        return aliveLoot.Count == 0;
    }

    private List<int> GetRemainingEnemyInWaveIndexes()
    {
        // Get all the index that still have enemies amount
        List<int> remainingEnemyIndexes = new List<int>();
        for (int i = 0; i < enemiesInWave.Count; ++i)
        {
            if (enemiesInWave[i].amount > 0)
            {
                remainingEnemyIndexes.Add(i);
            }
        }
        
        return remainingEnemyIndexes;
    }

    public void CleanUp()
    {
        foreach (var enemy in aliveEnemies)
        {
            MovingEnemyObject enemyObject = enemy.GetComponent<MovingEnemyObject>();
            enemyObject.CleanUp();
            enemyObject.ClearObject();
        }
        aliveEnemies.Clear();
    }

    public bool IsEnemyAlive(GameObject enemy)
    {
        if (enemy)
        {
            MovingEnemyObject enemyObject = enemy.GetComponent<MovingEnemyObject>();
            if (!enemyObject.IsDead)
            {
                return true;
            }
        }
        return false;
    }

    #region Boss Specials
    private BossSpecial GetBossSpecial(GameUtils.EBossSpecial type)
    {
        for(int i = 0; i < bossSpecials.Count; ++i)
        {
            if (bossSpecials[i].bossSpecialType == type)
            {
                return bossSpecials[i];
            }
        }
        return null;
    }

    public void SetSpecialActive(GameUtils.EBossSpecial special, bool active)
    {
        BossSpecial bossSpecial = GetBossSpecial(special);
        int lastActivity = bossSpecial.active;
        bossSpecial.active += active ? 1 : -1;

        // only show special alert the first time
        if (lastActivity == 0 && active)
        {
            ShowSpecialAlert(special, true);
        }
        else if (bossSpecial.active == 0)
        {
            ShowSpecialAlert(special, false);
        }
    }

    public void SetSpecial(GameUtils.EBossSpecial special, float time, float value)
    {
        BossSpecial bossSpecial = GetBossSpecial(special);
        bossSpecial.time = time;
        if (bossSpecial.nextTimer <= 0)
        {
            bossSpecial.nextTimer = time;
        }
        bossSpecial.value = value;
    }

    private void RunSpecial(GameUtils.EBossSpecial special)
    {
        switch (special)
        {
            case GameUtils.EBossSpecial.Repair:
                {
                    for (int i = 0; i < aliveEnemies.Count; ++i)
                    {
                        // Don't repair dead enemies
                        if (IsEnemyAlive(aliveEnemies[i]))
                            continue;

                        aliveEnemies[i].GetComponent<MovingEnemyObject>().Heal(GetBossSpecial(special).value);
                    }
                }
                break;
            case GameUtils.EBossSpecial.Shield:
            case GameUtils.EBossSpecial.JamRadar:
                {
                    // Nothing do run
                }
                break;
        }
    }

    public bool IsSpecialActive(GameUtils.EBossSpecial special)
    {
        BossSpecial bossSpecial = GetBossSpecial(special);
        if (bossSpecial != null)
        {
            return GetBossSpecial(special).active > 0;
        }
        return false;
    }

    public float GetSpecialValue(GameUtils.EBossSpecial special)
    {
        BossSpecial bossSpecial = GetBossSpecial(special);
        if (bossSpecial != null)
        {
            return GetBossSpecial(special).value;
        }
        return 0f;
    }

    public void UpdateSpecials()
    {
        for (int i = 0; i < bossSpecials.Count; ++i)
        {
            // only update if it has a valid timer
            if (bossSpecials[i].active > 0 && bossSpecials[i].time > 0)
            {
                bossSpecials[i].nextTimer -= Time.deltaTime;
                if (bossSpecials[i].nextTimer < 0)
                {
                    bossSpecials[i].nextTimer = bossSpecials[i].time;
                    RunSpecial(bossSpecials[i].bossSpecialType);
                }
            }
        }
    }
    #endregion

    #region Enemy Rank Multipliers
    public float GetEnemyHealthMaxRankMultiplier(EEnemyRank enemyRank, bool isBoss)
    {
        float enemyHealthMaxMultiplier = 1f;
        switch (enemyRank)
        {
            case EEnemyRank.Hero:
                enemyHealthMaxMultiplier = enemyRankInfo.heroHealthMaxMultiplier;
                break;
            case EEnemyRank.Elite:
                enemyHealthMaxMultiplier = enemyRankInfo.eliteHealthMaxMultiplier;
                break;
            case EEnemyRank.Veteran:
                enemyHealthMaxMultiplier = enemyRankInfo.veteranHealthMaxMultiplier;
                break;
            case EEnemyRank.Normal:
            default:
                enemyHealthMaxMultiplier = 1f;
                break;
        }

        // Apply another health max multiplier for bosses that have a rank veteran and higher
        if (enemyRank > EEnemyRank.Normal
            && isBoss)
        {
            enemyHealthMaxMultiplier *= enemyRankInfo.bossHealthMaxMultiplier;
        }

        // For debug purposes
        switch (ChallengeController.Instance.GetCurrentChallengeDifficulty())
        {
            case GameUtils.EGameDifficulty.Insane:
                enemyHealthMaxMultiplier *= insaneEnemyHealthMultiplier;
                break;
            case GameUtils.EGameDifficulty.Hard:
                enemyHealthMaxMultiplier *= hardEnemyHealthMultiplier;
                break;
            case GameUtils.EGameDifficulty.Normal:
            default:
                enemyHealthMaxMultiplier *= normalEnemyHealthMultiplier;
                break;
        }

        return enemyHealthMaxMultiplier;
    }

    public float GetEnemySpeedRankMultiplier(EEnemyRank enemyRank)
    {
        float enemySpeedMultiplier = 1f;
        switch (enemyRank)
        {
            case EEnemyRank.Hero:
                enemySpeedMultiplier = enemyRankInfo.heroSpeedMultiplier;
                break;
            case EEnemyRank.Elite:
                enemySpeedMultiplier = enemyRankInfo.eliteSpeedMultiplier;
                break;
            case EEnemyRank.Veteran:
                enemySpeedMultiplier = enemyRankInfo.veteranSpeedMultiplier;
                break;
            case EEnemyRank.Normal:
            default:
                enemySpeedMultiplier = 1f;
                break;
        }

        // For debug purposes
        switch (ChallengeController.Instance.GetCurrentChallengeDifficulty())
        {
            case GameUtils.EGameDifficulty.Insane:
                enemySpeedMultiplier *= insaneEnemySpeedMultiplier;
                break;
            case GameUtils.EGameDifficulty.Hard:
                enemySpeedMultiplier *= hardEnemySpeedMultiplier;
                break;
            case GameUtils.EGameDifficulty.Normal:
            default:
                enemySpeedMultiplier *= normalEnemySpeedMultiplier;
                break;
        }

        return enemySpeedMultiplier;
    }

    public float GetEnemyScoreRankMultiplier(EEnemyRank enemyRank)
    {
        switch (enemyRank)
        {
            case EEnemyRank.Hero:
                return enemyRankInfo.heroScoreMultiplier;
            case EEnemyRank.Elite:
                return enemyRankInfo.eliteScoreMultiplier;
            case EEnemyRank.Veteran:
                return enemyRankInfo.veteranScoreMultiplier;
            case EEnemyRank.Normal:
            default:
                return 1f;
        }
    }
    #endregion
}

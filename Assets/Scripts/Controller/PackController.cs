using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Purchasing;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using UnityEngine.Purchasing.Security;
#if UNITY_ANDROID
using GooglePlayGames;
#endif
using System;

#pragma warning disable 0414
#pragma warning disable 0649


public class PackController : UnitySingleton<PackController>
{
    private bool loaded = false;
    [HideInInspector]
    public bool isSceneSetup = false;
    private bool isPricesInitialized = false;

    private readonly int PACK_WATCH_AD_PLACEHOLDER_AMOUNT = 3;
    private readonly int PACK_WEAPON_PLACEHOLDER_AMOUNT = 6;
    private readonly int PACK_STANDARD_PLACEHOLDER_AMOUNT = 4;    
    private readonly int PACK_GOLD_PLACEHOLDER_AMOUNT = 4;

    private readonly int PACK_CREDIT_PLACEHOLDER_AMOUNT = 8;
    private readonly int PACK_CASH_PLACEHOLDER_AMOUNT = 8;

    [SerializeField]
    private PackData upgradeSupportPack;

    [SerializeField]
    private PackData[] upgradeWeaponPacks;

    [SerializeField]
    private PackData freeUpgradeArtilleryPack;

    [SerializeField]
    private int watchAdPackCreditRewardBase = 5000;

    [SerializeField]
    private int watchAdPackCreditRewardPerCampaign = 2000;

    [SerializeField]
    private int watchAdPackGoldRewardBase = 25;

    [SerializeField]
    private int watchAdPackGoldRewardPerCampaign = 10;

    [SerializeField]
    private int watchAdPackUpgradeCardRewardBase = 2;

    [SerializeField]
    private int watchAdPackUpgradeCardRewardPerCampaign = 1;

    [SerializeField]
    private int watchAdPackSupportCardRewardBase = 3;

    private PackData[] packsStandardData;
    private PackData[] packsGoldData;
    private PackData[] packsCreditData;

    private float creditBaseRatio;
    private float goldBaseRatio;

    // UI elements
    private GameObject menuCanvas;
    private Button[] packStandardBuyButtons;
    private Button[] packGoldBuyButtons;
    private Button[] packCreditBuyButtons;

    [Serializable]
    public class PackSaveData
    {
        public int opendPacks;

        public PackSaveData()
        {
            opendPacks = 0;
        }

        public PackSaveData(PackSaveData copy)
        {
            opendPacks = copy.opendPacks;
        }
    }

    [SerializeField]
    private static PackSaveData packSaveData;

    public PackSaveData PackSave
    {
        get { return packSaveData; }
        set { packSaveData = value; }
    }

    public PackData UpgradeSupportPack
    {
        get { return upgradeSupportPack; }
    }

    public override void Awake()
    {
        base.Awake();

        RegisterDependency(CurrencyController.Instance);
        RegisterDependency(CardsController.Instance);
        RegisterDependency(LocalizationManager.Instance);
        RegisterDependency(WeaponController.Instance);
    }

    public override bool Update()
    {
        if (!base.Update()) return false;
        if (!CheckIfDataLoaded()) return false;
        if (!isSceneSetup) SetupScene();

        return true;
    }

    private bool CheckIfDataLoaded()
    {
        if (DataController.Instance.AllLoaded && !loaded)
        {
            loaded = true;
        }

        return loaded;
    }

    private void SetupScene()
    {
        if (FlowStateController.Instance.IsInMenuScene())
        {
            PopulatePacksData();
            SetupStoreMenuUI();
        }

        isSceneSetup = true;
    }

    #region Store UI
    private void SetupStoreMenuUI()
    {
        // Note :: Needed since the standard packs folder is disabled on awake
        menuCanvas = GameObject.Find("MenuUICanvas");

        SetupStandardPacksTab();
        SetupCreditPacksTab();
        SetupGoldPacksTab();
        SetupWeaponUpgradePackUI();
    }

    private void SetupStandardPacksTab()
    {
        GameObject standardPacksFolder = GameUtils.GetChildGameObject(menuCanvas, "StandardPacksFolder", true);
        bool move = !DailyController.Instance.HasAllURLItems();

        // Note :: They are already sorted by PackOrder
        packStandardBuyButtons = new Button[packsStandardData.Length];
        for (int i = 0; i < packsStandardData.Length; ++i)
        {

			packStandardBuyButtons[i] = GameUtils.GetChildGameObject(standardPacksFolder, GetPackPrefabName(packsStandardData[i].packType) + " (" + (i) + ")", true).GetComponent<Button>();

            if (move)
            {
                // disable the Like/Review/Follow Buttons
                if (i > 3 && i < 7)
                {
                    packStandardBuyButtons[i].gameObject.SetActive(false);
                    continue;
                }
            }
            else
            {
                // set the store pic
                if (packsStandardData[i].packRewardData.freeURLType == RewardData.EFreeURLType.Review)
                {
                    Image circleImage = GameUtils.GetChildGameObject(packStandardBuyButtons[i].gameObject, "Circle", true).GetComponent<Image>();
#if UNITY_ANDROID
                    circleImage.sprite = (Sprite)Resources.Load("Images/playStore", typeof(Sprite));
#elif UNITY_IOS
                    circleImage.sprite = (Sprite)Resources.Load("Images/appStore", typeof(Sprite));
#endif
                }
                // move all buttons lower
                if (i > 6)
                {
                    RectTransform packRect = packStandardBuyButtons[i].gameObject.GetComponent<RectTransform>();
                    MoveDownBy(packRect, 389f);
                }
            }

            int i2 = i;
            packStandardBuyButtons[i].onClick.AddListener(delegate { StandardPackBuyButtonClicked(i2); });

            SetupPackUI(packsStandardData[i], packStandardBuyButtons[i].gameObject);
        }
        if (!move)
        {
            for (int i = 2; i <= 4; ++i)
            {
                RectTransform iconRect = GameUtils.GetChildGameObject(standardPacksFolder, "PackSection" + i + "Icon", true).GetComponent<RectTransform>();
                RectTransform titleRect = GameUtils.GetChildGameObject(standardPacksFolder, "PackSection" + i + "Title", true).GetComponent<RectTransform>();
                MoveDownBy(iconRect, 389f);
                MoveDownBy(titleRect, 389f);
            }
            RectTransform contentRect = GameUtils.GetChildGameObject(standardPacksFolder, "Content", true).GetComponent<RectTransform>();
            Vector2 size = contentRect.sizeDelta;
            size.y += 389f;
            contentRect.sizeDelta = size;
        }
    }

    private void MoveDownBy(RectTransform rect, float amount)
    {
        Vector2 pos = rect.anchoredPosition;
        pos.y -= amount;
        rect.anchoredPosition = pos;
    }
    
    private void SetupCreditPacksTab()
    {
        GameObject creditPacksFolder = GameUtils.GetChildGameObject(menuCanvas, "CreditPacksFolder", true);

        // Note :: They are already sorted by PackOrder
        packCreditBuyButtons = new Button[packsCreditData.Length];
        for (int i = 0; i < packsCreditData.Length; ++i)
        {
            // If first credit pack
            if (i == 0)
            {
                // Store the basic credit ratio based on the first one
                // which does not have a discount
                creditBaseRatio = packsCreditData[i].packRewardData.currencyAmount / packsCreditData[i].packPrice;
            }

            packCreditBuyButtons[i] = GameUtils.GetChildGameObject(creditPacksFolder, GetPackPrefabName(packsCreditData[i].packType) + " (" + (i) + ")", true).GetComponent<Button>();
            int i2 = i;
            packCreditBuyButtons[i].onClick.AddListener(delegate { CreditPackBuyButtonClicked(i2); });

            SetupPackUI(packsCreditData[i], packCreditBuyButtons[i].gameObject, true);
        }
    }

    private void SetupGoldPacksTab()
    {
        GameObject goldPacksFolder = GameUtils.GetChildGameObject(menuCanvas, "GoldPacksFolder", true);

        // Note :: They are already sorted by PackOrder
        packGoldBuyButtons = new Button[packsGoldData.Length];
        for (int i = 0; i < packsGoldData.Length; ++i)
        {
            // If first gold pack
            if (i == 0)
            {
                // Store the basic gold ratio based on the first one
                // which does not have a discount
                goldBaseRatio = packsGoldData[i].packRewardData.currencyAmount / packsGoldData[i].packPrice;
            }

            packGoldBuyButtons[i] = GameUtils.GetChildGameObject(goldPacksFolder, GetPackPrefabName(packsGoldData[i].packType) + " (" + (i) + ")", true).GetComponent<Button>();
            int i2 = i;
            packGoldBuyButtons[i].onClick.AddListener(delegate { GoldPackBuyButtonClicked(i2); });
        
            SetupPackUI(packsGoldData[i], packGoldBuyButtons[i].gameObject, true);
        }
    }

    // Important :: This is for the tutorial step where
    // it goes straight to the upgrade tab.
    private void SetupWeaponUpgradePackUI()
    {
        // Upgrade pack
        GameObject upgradePack = GameUtils.GetChildGameObject(menuCanvas, "UpgradePack", true);
        
        GameUtils.GetChildGameObject(upgradePack, "WeaponPackImage").GetComponent<Image>().sprite = GetWeaponPackImage(GameUtils.BaseID.ARTILLERY);
        GameUtils.GetChildGameObject(upgradePack, "TextCost", true).GetComponent<Text>().text = GetWeaponPackPrice(GameUtils.BaseID.ARTILLERY).ToString("0");
    }

    private string GetPackPrefabName(PackData.EPackType packType)
    {
        switch (packType)
        {
            case PackData.EPackType.Boosters:
                return "prefab_StoreBoosters";
            case PackData.EPackType.Bundles:
                return "prefab_StoreBundles";
            case PackData.EPackType.Credit:
                return "prefab_StoreCredit";
            case PackData.EPackType.Gold:
                return "prefab_StoreGold";
            case PackData.EPackType.Support:
                return "prefab_StoreSupport";
            case PackData.EPackType.Weapon:
                return "prefab_StoreWeapon";
            default:
                return "null";
        }
    }

    private void SetupPackUI(PackData packData, GameObject packBuyGO, bool checkDiscount = false)
    {
        switch (packData.packType)
        {
            case PackData.EPackType.Boosters:
                SetupCratePackUI(packData, packBuyGO);
                break;
            case PackData.EPackType.Bundles:
                SetupCratePackUI(packData, packBuyGO);
                break;
            case PackData.EPackType.Credit:
                SetupCurrencyPackUI(packData, packBuyGO, checkDiscount);
                break;
            case PackData.EPackType.Gold:
                SetupCurrencyPackUI(packData, packBuyGO, checkDiscount);
                break;
            case PackData.EPackType.Support:
                SetupCratePackUI(packData, packBuyGO);
                break;
            case PackData.EPackType.Weapon:
                SetupCratePackUI(packData, packBuyGO);
                break;
        }
    }

    private void SetupCratePackUI(PackData packData, GameObject packBuyGO)
    {
        // Set pack name
        GameUtils.GetChildGameObject(packBuyGO, "TextPackName", true).GetComponent<LocalizedText>().LocalizedTextID = packData.packNameID;

        // Set pack desc
        if (packData.packType != PackData.EPackType.Boosters)
        {
            LocalizedText localizedPackContent = GameUtils.GetChildGameObject(packBuyGO, "TextPackContent", true).GetComponent<LocalizedText>();
            localizedPackContent.LocalizedFullText = "";
        
            localizedPackContent.LocalizedFullText += packData.packRewardData.upgradeCardAmount > 0 ? (packData.packRewardData.upgradeCardAmount * packData.packRewardAmount) + " " + LocalizationManager.UPGRADES_TEXT : "";
            if (localizedPackContent.LocalizedFullText != ""
                && packData.packRewardData.consumableAmount > 0)
            {
                localizedPackContent.LocalizedFullText += "\n";
            }
            localizedPackContent.LocalizedFullText += packData.packRewardData.consumableAmount > 0 ? (packData.packRewardData.consumableAmount * packData.packRewardAmount) + " " + LocalizationManager.CONSUMABLES_TEXT : "";
        }
        else
        {
            Text localizedDuration = GameUtils.GetChildGameObject(packBuyGO, "TextDuration", true).GetComponent<Text>();
            localizedDuration.text = "";
            localizedDuration.text = packData.packRewardData.boosterHours.ToString() + " H";
        }

        // Set image
        GameUtils.GetChildGameObject(packBuyGO, "Cards", true).GetComponent<Image>().sprite = packData.packImage;

        if (packData.packType != PackData.EPackType.Boosters)
        {
            // Set quantity
            GameUtils.GetChildGameObject(packBuyGO, "TextQty", true).GetComponent<Text>().text = "X" + packData.packRewardAmount;
        }

        // Set the price
        string priceContainerName = "";
        if (packData.currencyType == GameUtils.ECurrencyType.Credit)
        {
            priceContainerName = "PriceCredit";
        }
        else if (packData.currencyType == GameUtils.ECurrencyType.Gold)
        {
            priceContainerName = "PriceGold";
        }
        else if (packData.currencyType == GameUtils.ECurrencyType.WatchAd)
        {
            priceContainerName = "PriceWatchAd";
        }
        else if (packData.currencyType == GameUtils.ECurrencyType.DailyFree)
        {
            priceContainerName = "PriceDailyFree";
        }
        else if (packData.currencyType == GameUtils.ECurrencyType.FreeURL)
        {
            priceContainerName = "PriceDailyFree";
        }
        GameObject priceContainerGO = GameUtils.GetChildGameObject(packBuyGO, priceContainerName, true);
        priceContainerGO.SetActive(true);
        Text textPrice = GameUtils.GetChildGameObject(priceContainerGO, "TextPrice").GetComponent<Text>();
        if (packData.currencyType == GameUtils.ECurrencyType.WatchAd)
        {
            textPrice.GetComponent<LocalizedText>().LocalizedTextID = LocalizationManager.WATCH_AD_TEXT;
        }
        else if (packData.currencyType == GameUtils.ECurrencyType.DailyFree)
        {
            textPrice.GetComponent<LocalizedText>().LocalizedTextID = LocalizationManager.FREE_TEXT;
        }
        else if (packData.currencyType == GameUtils.ECurrencyType.FreeURL)
        {
            textPrice.GetComponent<LocalizedText>().LocalizedTextID = LocalizationManager.FREE_TEXT;
        }
        else
        {
            textPrice.text = packData.packPrice.ToString();
        }

        // Set whether is interactable or not based on weapon unlock
        ManageWeaponPackInteractibility(packData, packBuyGO.GetComponent<Button>());
    }

    private void SetCashButton(Button button, GameObject container, PackData packsGoldData)
    {
//#if UNITY_EDITOR
        GameUtils.GetChildGameObject(container, "TextPrice").GetComponent<Text>().text = packsGoldData.packPrice.ToString();
/*#else
            string price = PurchaseController.Instance.GetProductPrice(packsGoldData.productData.storeIdInternal);
            if (!string.IsNullOrEmpty(price))
            {
                button.interactable = true;
            }
            else
            {
                price = LocalizationManager.Instance.GetLocalizedString(LocalizationManager.NOT_AVAILABLE_WATCH_AD_TEXT);
                button.interactable = false;
            }
            GameUtils.GetChildGameObject(container, "TextPrice").GetComponent<Text>().text = price;
#endif*/
    }

    private void SetupCurrencyPackUI(PackData packData, GameObject packBuyGO, bool checkDiscount)
    {
        // Set pack name
        GameUtils.GetChildGameObject(packBuyGO, "TextPackName", true).GetComponent<LocalizedText>().LocalizedTextID = packData.packNameID;

        // Set the price
        string priceContainerName = "";
        // If the currency type is to watch an Ad
        if (packData.currencyType == GameUtils.ECurrencyType.WatchAd)
        {
            priceContainerName = "PriceWatchAd";
            GameObject priceContainerGO = GameUtils.GetChildGameObject(packBuyGO, priceContainerName, true);
            priceContainerGO.SetActive(true);
            GameUtils.GetChildGameObject(priceContainerGO, "TextPrice").GetComponent<LocalizedText>().LocalizedTextID = LocalizationManager.WATCH_AD_TEXT;
        }
        // If the currency type is free
        else if (packData.currencyType == GameUtils.ECurrencyType.FreeURL)
        {
            priceContainerName = "PriceDailyFree";
            GameObject priceContainerGO = GameUtils.GetChildGameObject(packBuyGO, priceContainerName, true);
            priceContainerGO.SetActive(true);
            GameUtils.GetChildGameObject(priceContainerGO, "TextPrice").GetComponent<LocalizedText>().LocalizedTextID = LocalizationManager.FREE_TEXT;
        }
        // If the currency type is free
        else if (packData.currencyType == GameUtils.ECurrencyType.DailyFree)
        {
            priceContainerName = "PriceDailyFree";
            GameObject priceContainerGO = GameUtils.GetChildGameObject(packBuyGO, priceContainerName, true);
            priceContainerGO.SetActive(true);
            GameUtils.GetChildGameObject(priceContainerGO, "TextPrice").GetComponent<LocalizedText>().LocalizedTextID = LocalizationManager.FREE_TEXT;
        }
        // If the currency type is gold
        else if (packData.currencyType == GameUtils.ECurrencyType.Gold)
        {
            priceContainerName = "PriceGold";
            GameObject priceContainerGO = GameUtils.GetChildGameObject(packBuyGO, priceContainerName, true);
            priceContainerGO.SetActive(true);
            GameUtils.GetChildGameObject(priceContainerGO, "TextPrice").GetComponent<Text>().text = packData.packPrice.ToString();
        }
        // If the currency type is real money
        else if (packData.currencyType == GameUtils.ECurrencyType.Cash)
        {
            priceContainerName = "PriceMoney";
            GameObject priceContainerGO = GameUtils.GetChildGameObject(packBuyGO, priceContainerName, true);
            priceContainerGO.SetActive(true);

            SetCashButton(packBuyGO.GetComponent<Button>(), priceContainerGO, packData);
        }
        
        // Set the currency reward image and amount
        if (packData.packRewardData.rewardType == RewardData.ERewardType.Credit)
        {
            GameUtils.GetChildGameObject(packBuyGO, "CreditIcon", true).SetActive(true);
        }
        else if (packData.packRewardData.rewardType == RewardData.ERewardType.Gold)
        {
            GameUtils.GetChildGameObject(packBuyGO, "GoldIcon", true).SetActive(true);
        }
        GameUtils.GetChildGameObject(packBuyGO, "TextQty", true).GetComponent<Text>().text = packData.packRewardData.currencyAmount.ToString() + "";

        // Set the discount
        if (checkDiscount)
        {
            if (packData.packRewardData.rewardType == RewardData.ERewardType.Credit)
            {
                float creditCurrentRatio = packData.packRewardData.currencyAmount / packData.packPrice;
                // If there's a better ratio
                if (creditCurrentRatio > creditBaseRatio)
                {
                    GameObject discountGO = GameUtils.GetChildGameObject(packBuyGO, "Discount", true);
                    discountGO.SetActive(true);

                    float discountPercentage = (1f - (creditCurrentRatio / creditBaseRatio)) * 100f;

                    // Round to nearest 5
                    discountPercentage = Mathf.Round(discountPercentage / 5f) * 5f;

                    GameUtils.GetChildGameObject(discountGO, "TextDiscount").GetComponent<Text>().text = discountPercentage.ToString("0") + "%";
                }
            }
            else if (packData.packRewardData.rewardType == RewardData.ERewardType.Gold)
            {
                float goldCurrentRatio = packData.packRewardData.currencyAmount / packData.packPrice;
                // If there's a better ratio
                if (goldCurrentRatio > goldBaseRatio)
                {
                    GameObject discountGO = GameUtils.GetChildGameObject(packBuyGO, "Discount", true);
                    discountGO.SetActive(true);

                    float discountPercentage = (1f - (goldCurrentRatio / goldBaseRatio)) * 100f;

                    // Round to nearest 5
                    discountPercentage = Mathf.Round(discountPercentage / 5f) * 5f;

                    GameUtils.GetChildGameObject(discountGO, "TextDiscount").GetComponent<Text>().text = discountPercentage.ToString("0") + "%";
                }
            }
        }        
    }

    public void RefreshWeaponPacksInteractibility()
    {
        // Note :: They are already sorted by PackOrder
        for (int i = 0; i < packsStandardData.Length; ++i)
        {
            ManageWeaponPackInteractibility(packsStandardData[i], packStandardBuyButtons[i]);
        }
    }

    private void ManageWeaponPackInteractibility(PackData packData, Button packBuyButton)
    {
        // Skip if it's not a weapon pack
        if (packData.packType != PackData.EPackType.Weapon)
            return;

        // Manage the interactibility whether the weapon is unlocked or not
        bool hasWeaponUnlocked = false;

        // Cycle through all the specific weapons in the pack reward data
        for (int i = 0; i < packData.packRewardData.specificWeapons.Length; ++i)
        {
            // If the unlocked weapons list contains the current weapon
            if (WeaponController.Instance.UnlockedWeapons.Contains(packData.packRewardData.specificWeapons[i]))
            {
                hasWeaponUnlocked = true;
                break;
            }
        }
        packBuyButton.interactable = hasWeaponUnlocked;        
    }

    public Sprite GetWeaponPackImage(GameUtils.BaseID weaponID)
    {
        for (int i = 0; i < upgradeWeaponPacks.Length; ++i)
        {
            // Note :: These packs will always have one specific weapon
            if (upgradeWeaponPacks[i].packRewardData.specificWeapons[0] == weaponID)
            {
                return upgradeWeaponPacks[i].packImage;
            }
        }

        return null;
    }

    public float GetWeaponPackPrice(GameUtils.BaseID weaponID)
    {
        for (int i = 0; i < upgradeWeaponPacks.Length; ++i)
        {
            // Note :: These packs will always have one specific weapon
            if (upgradeWeaponPacks[i].packRewardData.specificWeapons[0] == weaponID)
            {
                return upgradeWeaponPacks[i].packPrice;
            }
        }

        return 0;
    }
#endregion

    public override bool FixedUpdate()
    {
        if (!base.FixedUpdate()) return false;
        if (packsStandardData == null) return false;
        if (!DataController.Instance.AllLoaded) return false;

        UpdateFreeURLInteractibility();
        UpdateBoosterInteractibility();
        UpdateWatchAdPacksInteractibility();
        UpdateDailyFreePacksInteractibility();

        if (PurchaseController.Instance.IsInitialized() && !isPricesInitialized)
        {
            isPricesInitialized = true;
            UpdateCashPacks();
        }
        
        return true;
    }

    private void UpdateWatchAdPacksInteractibility()
    {
        for (int i = 0; i < packsStandardData.Length; ++i)
        {
            // If free pack
            if (packsStandardData[i].currencyType == GameUtils.ECurrencyType.WatchAd)
            {
                var rewardType = packsStandardData[i].packRewardData.rewardType;
                bool isAdReady = AdvertismentController.Instance.AdvertismentReady() && AdvertismentController.Instance.CanWatchAdForRewardType(rewardType);
                packStandardBuyButtons[i].interactable = isAdReady;

                GameObject priceContainerGO = GameUtils.GetChildGameObject(packStandardBuyButtons[i].gameObject, "PriceWatchAd", true);
                Text textPrice = GameUtils.GetChildGameObject(priceContainerGO, "TextPrice", true).GetComponent<Text>();
                if (!isAdReady)
                {
                    // If less than one minute
                    if (AdvertismentController.Instance.TimeUntilAdCanBeWatched(rewardType, GameUtils.ETimeType.Minutes) < 1)
                    {
                        if (AdvertismentController.Instance.TimeUntilAdCanBeWatched(rewardType, GameUtils.ETimeType.Seconds) > 0)
                        {
                            // Show the time left in seconds
                            textPrice.text = AdvertismentController.Instance.TimeUntilAdCanBeWatched(rewardType, GameUtils.ETimeType.Seconds).ToString() + " SEC";
                        }
                        else
                        {
                            textPrice.GetComponent<LocalizedText>().LocalizedTextID = LocalizationManager.NOT_AVAILABLE_WATCH_AD_TEXT;
                        }
                    }
                    else
                    {
                        // Show the time left in minutes
                        textPrice.text = AdvertismentController.Instance.TimeUntilAdCanBeWatched(rewardType, GameUtils.ETimeType.Minutes).ToString() + " MIN";
                    }
                }
                else
                {
                    textPrice.GetComponent<LocalizedText>().LocalizedTextID = LocalizationManager.WATCH_AD_TEXT;
                }
            }
        }
    }

    private void UpdateFreeURLInteractibility()
    {
        for (int i = 0; i < packsStandardData.Length; ++i)
        {
            // If free pack
            if (packsStandardData[i].packType == PackData.EPackType.Gold)
            {
                RewardData.EFreeURLType freeURLType = packsStandardData[i].packRewardData.freeURLType;
                if (freeURLType != RewardData.EFreeURLType.None && packStandardBuyButtons[i].IsActive())
                {
                    bool hasItem = DailyController.Instance.HasURLItem(freeURLType);
                    packStandardBuyButtons[i].interactable = !hasItem;
                    if (hasItem)
                    {
                        GameObject textContainer = GameUtils.GetChildGameObject(packStandardBuyButtons[i].gameObject, "PriceDailyFree", true);
                        Text textPrice = GameUtils.GetChildGameObject(textContainer, "TextPrice", true).GetComponent<Text>();
                        textPrice.GetComponent<LocalizedText>().LocalizedTextID = LocalizationManager.USED_TEXT;
                    }
                }
            }
        }
    }


    private void UpdateBoosterInteractibility()
    {
        for (int i = 0; i < packsStandardData.Length; ++i)
        {
            // If free pack
            if (packsStandardData[i].packType == PackData.EPackType.Boosters)
            {
                BoosterController.EBoosterPackType boosterType = packsStandardData[i].packRewardData.boosterType;
                bool isBoosterReady = DailyController.Instance.CanOpenBoosterForBoosterType(boosterType);
                packStandardBuyButtons[i].interactable = isBoosterReady;

                GameObject priceContainerGO = GameUtils.GetChildGameObject(packStandardBuyButtons[i].gameObject, "PriceGold", true);
                GameObject timeContainerGO = GameUtils.GetChildGameObject(packStandardBuyButtons[i].gameObject, "PriceDailyFree", true);
                Text textPrice = GameUtils.GetChildGameObject(timeContainerGO, "TextPrice", true).GetComponent<Text>();
                if (!isBoosterReady)
                {
                    priceContainerGO.SetActive(false);
                    timeContainerGO.SetActive(true);
                    // If less than one minute
                    if (DailyController.Instance.TimeUntilBoosterEnds(boosterType, GameUtils.ETimeType.Hours) < 1)
                    {
                        if (DailyController.Instance.TimeUntilBoosterEnds(boosterType, GameUtils.ETimeType.Minutes) < 1)
                        {
                            if (DailyController.Instance.TimeUntilBoosterEnds(boosterType, GameUtils.ETimeType.Seconds) > 0)
                            {
                                // Show the time left in seconds
                                textPrice.text = DailyController.Instance.TimeUntilBoosterEnds(boosterType, GameUtils.ETimeType.Seconds).ToString() + " SEC";
                            }
                        }
                        else
                        {
                            // Show the time left in minutes
                            textPrice.text = DailyController.Instance.TimeUntilBoosterEnds(boosterType, GameUtils.ETimeType.Minutes).ToString() + " MIN";
                        }
                    }
                    else
                    {
                        // Show the time left in hours
                        textPrice.text = DailyController.Instance.TimeUntilBoosterEnds(boosterType, GameUtils.ETimeType.Hours).ToString() + " H";
                    }
                }
                else
                {
                    priceContainerGO.SetActive(true);
                    timeContainerGO.SetActive(false);
                }
            }
        }
    }

    private void UpdateDailyFreePacksInteractibility()
    {
        for (int i = 0; i < packsStandardData.Length; ++i)
        {
            // If free pack
            if (packsStandardData[i].currencyType == GameUtils.ECurrencyType.DailyFree)
            {
                var rewardType = packsStandardData[i].packRewardData.rewardType;
                bool isDailyReady = DailyController.Instance.CanOpenDailyForRewardType(rewardType);
                packStandardBuyButtons[i].interactable = isDailyReady;

                GameObject priceContainerGO = GameUtils.GetChildGameObject(packStandardBuyButtons[i].gameObject, "PriceDailyFree", true);
                Text textPrice = GameUtils.GetChildGameObject(priceContainerGO, "TextPrice", true).GetComponent<Text>();
                if (!isDailyReady)
                {
                    // If less than one minute
                    if (DailyController.Instance.TimeUntilNextDaily(rewardType, GameUtils.ETimeType.Hours) < 1)
                    {
                        if (DailyController.Instance.TimeUntilNextDaily(rewardType, GameUtils.ETimeType.Minutes) < 1)
                        {
                            if (DailyController.Instance.TimeUntilNextDaily(rewardType, GameUtils.ETimeType.Seconds) > 0)
                            {
                                // Show the time left in seconds
                                textPrice.text = DailyController.Instance.TimeUntilNextDaily(rewardType, GameUtils.ETimeType.Seconds).ToString() + " SEC";
                            }
                            else
                            {
                                textPrice.GetComponent<LocalizedText>().LocalizedTextID = LocalizationManager.NOT_AVAILABLE_WATCH_AD_TEXT;
                            }
                        }
                        else
                        {
                            // Show the time left in minutes
                            textPrice.text = DailyController.Instance.TimeUntilNextDaily(rewardType, GameUtils.ETimeType.Minutes).ToString() + " MIN";
                        }
                    }
                    else
                    {
                        // Show the time left in hours
                        textPrice.text = DailyController.Instance.TimeUntilNextDaily(rewardType, GameUtils.ETimeType.Hours).ToString() + " H";
                    }
                }
                else
                {
                    textPrice.GetComponent<LocalizedText>().LocalizedTextID = LocalizationManager.FREE_TEXT;
                }
            }
        }
    }

    private void UpdateCashPacks()
    {
        for (int i = 0; i < packsGoldData.Length; ++i)
        {
            // Safeguard :: They should always be currency type 'cash'
            if (packsGoldData[i].currencyType == GameUtils.ECurrencyType.Cash)
            {
                string priceContainerName = "PriceMoney";
                GameObject priceContainerGO = GameUtils.GetChildGameObject(packGoldBuyButtons[i].gameObject, priceContainerName, true);
                if (!priceContainerGO.activeSelf)
                {
                    priceContainerGO.SetActive(true);
                }
                SetCashButton(packGoldBuyButtons[i], priceContainerGO, packsGoldData[i]);
            }
        }
    }

    private void AdvertismentWatched(bool finished, PackData rewardPack)
    {
        if (finished && rewardPack != null)
        {
            OpenPacks(rewardPack);
            AdvertismentController.Instance.WatchedAd(rewardPack.packRewardData.rewardType);
        }
        if (rewardPack == null)
        {
            Debug.LogError("Something went wrong, finished watching an add and no adverstimentPack set!");
        }
    }

    // Note :: For tutorial
    // TEMP :: Tutorial flow will change soon
    public void ObtainFreeArtilleryPack()
    {
        BuyPack(freeUpgradeArtilleryPack);
    }

    private void StandardPackBuyButtonClicked(int index)
    {
        BuyPack(packsStandardData[index]);       
    }

    private void CreditPackBuyButtonClicked(int index)
    {
        BuyPack(packsCreditData[index]);
    }

    private void GoldPackBuyButtonClicked(int index)
    {
        BuyPack(packsGoldData[index]);
    }

    public void WeaponPackBuyButtonClicked(GameUtils.BaseID weaponID)
    {
        for (int i = 0; i < upgradeWeaponPacks.Length; ++i)
        {
            // Note :: These packs will always have one specific weapon
            if (upgradeWeaponPacks[i].packRewardData.specificWeapons[0] == weaponID)
            {
                BuyPack(upgradeWeaponPacks[i]);
                break;
            }
        }       
    }

    public void SupportPackBuyButtonClicked()
    {
        BuyPack(upgradeSupportPack);
    }

    private void BuyPack(PackData packData)
    {
        // If the currency type is to watch an Ad
        if (packData.currencyType == GameUtils.ECurrencyType.WatchAd)
        {
            //Debug.Log("Start Watching Ad");
            AdvertismentController.Instance.RequestAdvertisment(AdvertismentWatched, "DailyPackAd", packData);
        }
        else if (packData.currencyType == GameUtils.ECurrencyType.DailyFree)
        {
            //Debug.Log("Open Daily Pack");
            DailyController.Instance.OpenDailyPack(packData.packRewardData.rewardType);
            OpenPacks(packData);
        }
        else if (packData.currencyType == GameUtils.ECurrencyType.FreeURL)
        {
            // Open the page
            DailyController.Instance.ViewPageAndReward(packData);
        }
        // If the currency type is real money
        else if (packData.currencyType == GameUtils.ECurrencyType.Cash)
        {
//#if UNITY_EDITOR
            OpenPacks(packData);
/*#else
            PurchaseController.Instance.BuyCashPack(packData.productData.storeIdInternal);
#endif*/
        }
        // If the currency type is credit
        else if (packData.currencyType == GameUtils.ECurrencyType.Credit)
        {
            if (CurrencyController.Instance.HasEnoughCurrency((int)packData.packPrice, GameUtils.ECurrencyType.Credit))
            {
                // Decrease currency
                CurrencyController.Instance.ModifyCreditsAmount((int)-packData.packPrice, GameUtils.CreditsSource.Credits2Pack, DataController.SAVEPRIORITY.QUEUE);

                // Open packs
                OpenPacks(packData);
            }
        }
        // If the currency type is gold
        else if (packData.currencyType == GameUtils.ECurrencyType.Gold)
        {
            if (CurrencyController.Instance.HasEnoughCurrency((int)packData.packPrice, GameUtils.ECurrencyType.Gold))
            {
                // Decrease currency
                CurrencyController.Instance.ModifyGoldAmount((int)-packData.packPrice, GameUtils.GoldSource.Gold2Pack, DataController.SAVEPRIORITY.QUEUE);

                if (packData.packType == PackData.EPackType.Boosters)
                {
                    BoosterController.Instance.AddBooster(packData.packRewardData.boosterType, packData.packRewardData.boosterHours);
                }
                // Open packs
                OpenPacks(packData);
            }
        }
    }
    
    // Reward the bought pack
    public void RewardPackFromStore(string packName, PurchaseEventArgs args, IPurchaseReceipt receipt)
    {
        for (int i = 0; i < packsGoldData.Length; ++i)
        {
            if (string.Compare(packsGoldData[i].productData.storeIdInternal, packName) == 0)
            {
                OpenPacks(packsGoldData[i], args, receipt);
                return;
            }
        }
    }

    public void RewardPackFromFreeURL(PackData packData)
    {
        OpenPacks(packData);
    }

    private void OpenPacks(PackData packData, PurchaseEventArgs args = null, IPurchaseReceipt receipt = null)
    {
        List<PrizeController.RewardRollInfo> listRewardRollInfo = new List<PrizeController.RewardRollInfo>();

        for (int i = 0; i < packData.packRewardAmount; ++i)
        {
            string storeID = string.Empty;
            if (packData.productData != null)
            {
                storeID = packData.productData.storeIdInternal;
            }
            listRewardRollInfo.Add(CollectReward(packData.packRewardData, packData.currencyType, false, packData.packPrice, storeID, args, receipt));
            
            // every pack opend counts
            packSaveData.opendPacks++;
        }

        // Open prizes once all the rewards have been collected
        PrizeController.Instance.InitOpenPrize(listRewardRollInfo);

        SavePackData(DataController.SAVEPRIORITY.NORMAL);
#if UNITY_ANDROID
        if (PlayGamesPlatform.Instance.IsAuthenticated())
        { 
            if (packSaveData.opendPacks >= GameUtils.GOOGLE_ACHIEVEMENTS_SUPPLIER)
            {
                Social.ReportProgress(GPGSConstants.achievement_supplier, 100.0f, (bool success) =>
                {
                    // handle success or failure
                    Debug.Log("Unlocked Supplier Achievement");
                });
            }
        }
#elif UNITY_IOS
            if (Social.localUser.authenticated)
            {
                if (packSaveData.opendPacks >= GameUtils.GOOGLE_ACHIEVEMENTS_SUPPLIER)
                {
                    Social.ReportProgress(GCConstants.achievement_supplier, 100.0f, (bool success) =>
                    {
                        // handle success or failure
                         Debug.Log("Unlocked Supplier Achievement");
                    });
                }
            }
#endif
    }

    public List<PrizeController.RewardRollInfo> CollectChallengeRewards(List<RewardData> rewardsData, GameUtils.ECurrencyType currencyType)
    {
        List<PrizeController.RewardRollInfo> listRewardRollInfo = new List<PrizeController.RewardRollInfo>();

        for (int i = 0; i < rewardsData.Count; ++i)
        {
            listRewardRollInfo.Add(CollectReward(rewardsData[i], currencyType, false, 0f, string.Empty));

            //listRewardRollInfo[i].rewardData.DebugConsoleReward();
        }

        return listRewardRollInfo;

        // Note :: The open prize anim is triggered
        // after the match end panel anim for
        // a smoother transition
    }
    
    public PrizeController.RewardRollInfo CollectReward(RewardData rewardData, GameUtils.ECurrencyType currencySpentType, bool showPrize, float price, string storeID, PurchaseEventArgs args = null, IPurchaseReceipt receipt = null)
    {
        DataController.SAVEPRIORITY priority = DataController.SAVEPRIORITY.QUEUE;
        if (currencySpentType == GameUtils.ECurrencyType.Cash && !string.IsNullOrEmpty(storeID))
        {
            priority = DataController.SAVEPRIORITY.INSTANT;
        }

        PrizeController.RewardRollInfo rewardRollInfo = new PrizeController.RewardRollInfo(rewardData);
        
        // If pack
        if (rewardData.rewardType == RewardData.ERewardType.Pack)
        {
            // For each weapon upgrade card
            for (int i = 0; i < rewardData.upgradeCardAmount; ++i)
            {
                // Roll a weapon upgrade card
                List<CardsController.WeaponCard> cardRolled = CardsController.Instance.RollWeaponCards(1, rewardData.specificWeapons, rewardData.specificWeaponUpgrade);
                if (cardRolled.Count > 0)
                {
                    // Track the upgrade card to show it afterward
                    rewardRollInfo.upgradeCardsRollInfo.Add(cardRolled[0]);
                }
            }

            // For each consumable            
            for (int i = 0; i < rewardData.consumableAmount; ++i)
            {
                // Roll a consumable
                List<GameUtils.EConsumable> consumableRolled = CardsController.Instance.RollConsumable();

                if (consumableRolled.Count > 0)
                {
                    // Track the consumable card to show it afterward
                    rewardRollInfo.consumablesRollInfo.Add(consumableRolled[0]);
                }
            }
        }
        // If credits        
        else if (rewardData.rewardType == RewardData.ERewardType.Credit)
        {
            GameUtils.CreditsSource creditSource = GameUtils.CreditsSource.Pack2Credits;
            if (currencySpentType == GameUtils.ECurrencyType.Achievement)
            {
                creditSource = GameUtils.CreditsSource.AchievementReward;
            }
            else if (currencySpentType == GameUtils.ECurrencyType.StarReward)
            {
                creditSource = GameUtils.CreditsSource.ThreeStarReward;
            }
            CurrencyController.Instance.ModifyCreditsAmount(rewardData.currencyAmount, creditSource, priority);
/*#if !UNITY_EDITOR
            products.AddVirtualCurrency("Credit", "PREMIUM", rewardData.currencyAmount);
#endif*/
        }
        // If gold
        else if (rewardData.rewardType == RewardData.ERewardType.Gold)
        {
            GameUtils.GoldSource goldSource = GameUtils.GoldSource.Pack2Gold;
            if (currencySpentType == GameUtils.ECurrencyType.Achievement)
            {
                goldSource = GameUtils.GoldSource.AchievementReward;
            }
            else if (currencySpentType == GameUtils.ECurrencyType.StarReward)
            {
                goldSource = GameUtils.GoldSource.OneStarReward;
            }
            CurrencyController.Instance.ModifyGoldAmount(rewardData.currencyAmount, goldSource, priority);
/*#if !UNITY_EDITOR
            products.AddVirtualCurrency("Gold", "PREMIUM", rewardData.currencyAmount);
#endif*/
        }

        // If we want to show the prize now
        // Note :: Multiple packs will be handled differently
        if (showPrize)
        {
            // Open prize animation
            PrizeController.Instance.InitOpenPrize(new List<PrizeController.RewardRollInfo> { rewardRollInfo });
        }
			
        return rewardRollInfo;
    }

#region Data Handling
    private void PopulatePacksData()
    {
        packsStandardData = Resources.LoadAll<PackData>("Packs/Standard").OrderBy(ob => ob.packOrder).ToArray();
        packsCreditData = Resources.LoadAll<PackData>("Packs/Credit").OrderBy(ob => ob.packOrder).ToArray();
        packsGoldData = Resources.LoadAll<PackData>("Packs/Gold").OrderBy(ob => ob.packOrder).ToArray();

        AdjustWatchAdPacks();
    }

    private void AdjustWatchAdPacks()
    {
        // Modify the watch ad packs based on the player's progression
        // Get the player's progression
        int challengeProgressionIndex = ChallengeController.Instance.GetChallengeProgressionIndex();
        int campaignProgressionIndex = Mathf.FloorToInt(challengeProgressionIndex / 12f);

        for (int i = 0; i < packsStandardData.Length; ++i)
        {
            if (packsStandardData[i].currencyType == GameUtils.ECurrencyType.WatchAd)
            {
                // Create a new reward data in order to have a modified 
                // reward that is based on the player's progression
                RewardData watchAdReward = ScriptableObject.CreateInstance<RewardData>();
                switch (packsStandardData[i].packType)
                {
                    case PackData.EPackType.Credit:
                        watchAdReward.rewardType = RewardData.ERewardType.Credit;
                        watchAdReward.currencyAmount = watchAdPackCreditRewardBase + (campaignProgressionIndex * watchAdPackCreditRewardPerCampaign);
                        break;
                    case PackData.EPackType.Gold:
                        watchAdReward.rewardType = RewardData.ERewardType.Gold;
                        watchAdReward.currencyAmount = watchAdPackGoldRewardBase + (campaignProgressionIndex * watchAdPackGoldRewardPerCampaign);
                        break;
                    case PackData.EPackType.Bundles:
                        watchAdReward.rewardType = RewardData.ERewardType.Pack;
                        watchAdReward.upgradeCardAmount = watchAdPackUpgradeCardRewardBase + (campaignProgressionIndex * watchAdPackUpgradeCardRewardPerCampaign);
                        watchAdReward.consumableAmount = watchAdPackSupportCardRewardBase;
                        break;
                }

                // Affect the new reward data to the watch ad pack
                packsStandardData[i].packRewardData = watchAdReward;
            }
        }
    }
    
    public void SavePackData(DataController.SAVEPRIORITY priority)
    {
        DataController.Instance.SavePackData(priority);
    }

    public void LoadPackData(PackSaveData remotePackData)
    {
        if (remotePackData == null)
        {
            System.Exception ex = JsonUtils<PackSaveData>.Load(GameUtils.PACK_DATA_JSON, out packSaveData, GameUtils.ELoadType.DynamicData);
            if (ex != null
                && ex.GetType() == typeof(FileLoadException))
            {
                ResetPackData();
            }
        }
        else
        {
            packSaveData = remotePackData;
        }
    }

    public void ResetPackData()
    {
        // Note :: Needs to be initialized otherwise it crashes
        packSaveData = new PackSaveData();
        packSaveData.opendPacks = 0;

        SavePackData(DataController.SAVEPRIORITY.QUEUE);
    }

#endregion
}

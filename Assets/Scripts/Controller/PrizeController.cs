using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections.Generic;

public class PrizeController : UnitySingleton<PrizeController>
{
    public class RewardRollInfo
    {
        public RewardData rewardData;

        // Note :: Must have a maximum of 5 cards total in order to show the pack
        public List<CardsController.WeaponCard> upgradeCardsRollInfo;
        public List<GameUtils.EConsumable> consumablesRollInfo;

        public RewardRollInfo(RewardData rewardData)
        {
            this.rewardData = rewardData;
            upgradeCardsRollInfo = new List<CardsController.WeaponCard>();
            consumablesRollInfo = new List<GameUtils.EConsumable>();
        }
    }

    private static bool isInitialized = false;
    private static bool loaded = false;
    private static bool isSceneSetup = false;

    private readonly int MAX_CARDS_PER_PACK = 5;
    private readonly float PACK_INTRO_START_DELAY = 0.75f;
    private readonly float COLLECT_PRIZE_DELAY = 0.75f;
    private readonly float COLLECT_PACK_DELAY = 1.25f;
    private readonly int OPEN_PRIZE_SCENE_INDEX = 3;
    private readonly float PACK_COUNTER_DECREMENT_INTERVAL = 0.07f;
    private readonly float PACK_COUNTER_DECREMENT_DELAY = 0.10f;

    private readonly string BACKGROUND_OUT_TRIGGER_PARAM = "TriggerBackgroundOut";
    private readonly string ONE_PRIZE_IN_TRIGGER_PARAM = "TriggerOnePrizeIn";
    private readonly string TWO_PRIZE_IN_TRIGGER_PARAM = "TriggerTwoPrizeIn";
    private readonly string THREE_PRIZE_IN_TRIGGER_PARAM = "TriggerThreePrizeIn";
    private readonly string COLLECT_PRIZE_TRIGGER_PARAM = "TriggerCollectPrize";
    private readonly string COLLECT_PACK_TRIGGER_PARAM = "TriggerCollectPack";
    private readonly string NEXT_PACK_TRIGGER_PARAM = "TriggerNextPack";
    private readonly string CLOSE_PACK_COUNTER_TRIGGER_PARAM = "TriggerClosePackCounter";


    public enum EPrizeState
    {
        None,
        Start,
        PrizeIdle,
        PackIntro,
        PackCardsIn,
        PackIdle,
        PackCardsOut,
        PackOutro,
        End
    }
    
    private List<RewardRollInfo> listIndividualRewardsRollInfo;
    private List<RewardRollInfo> listCombinedRewardsRollInfo; // Used to show the right amount of prizes
    private List<CardsController.WeaponCard> mergedWeaponUpgradesRollInfo; // Used to open packs
    private List<GameUtils.EConsumable> mergedConsumablesRollInfo; // Used to open packs
    private int currentCardIndex;    

    // UI elements
    private GameObject darkBackground;
    private GameObject onePrizeContainer;
    private GameObject twoPrizeContainer;
    private GameObject threePrizeContainer;
    private GameObject[] eachPrizeContainer;
    private GameObject[] eachTargetPrizeContainer;
    private GameObject openPackContainer;
    private GameObject packCounterContainer;
    private Text packCounterText;
    private List<GameObject> listCardContainers;
    private List<GameObject> listCardTargetContainers;
    private Button inputButton;
    private Button skipButton;
    private GameObject tapContinueGO;
    
    private int packCounter;
    private EPrizeState currentPrizeState;
    private bool isBackgroundIn;
    private float collectPrizeDelayTimer;
    private float packIntroDelayTimer;
    private float collectPackDelayTimer;
    private float packCounterDecrementTimer;
    private int packCounterDecrementTracker;
    private int numberOfCardsShowing;
    private float idleTimer;

    public EPrizeState CurrentPrizeState
    {
        get { return currentPrizeState; }
        set
        {
            ExitPrizeState(currentPrizeState);
            currentPrizeState = value;
            EnterPrizeState(currentPrizeState);

            //Debug.Log("CurrentPrizeState = " + CurrentPrizeState);
        }
    }

    public override void Awake()
    {
        base.Awake();
        if (!isInitialized)
        {
            // Always keep the controller loaded through the scene changes
            DontDestroyOnLoad(gameObject);
            isInitialized = true;
        }
        isSceneSetup = false;
    }

    private void SetupScene()
    {
        // Important to set the camera of the loaded prize canvas
        GameObject prizeCanvas = GameObject.Find("PrizeUICanvas");
        prizeCanvas.GetComponent<Canvas>().worldCamera = Camera.main;

        darkBackground = GameUtils.GetChildGameObject(prizeCanvas, "DarkBG", true);
        onePrizeContainer = GameUtils.GetChildGameObject(prizeCanvas, "OnePrizeContainer", true);
        twoPrizeContainer = GameUtils.GetChildGameObject(prizeCanvas, "TwoPrizeContainer", true);
        threePrizeContainer = GameUtils.GetChildGameObject(prizeCanvas, "ThreePrizeContainer", true);
        openPackContainer = GameUtils.GetChildGameObject(prizeCanvas, "OpenPackContainer", true);
        openPackContainer.SetActive(false);

        listCardContainers = new List<GameObject>();
        for (int i = 0; i < MAX_CARDS_PER_PACK; ++i)
        {
            listCardContainers.Add(GameUtils.GetChildGameObject(openPackContainer, "Card0" + i, true));

            // Note :: This is to avoid seeing a visual glitch
            // where you see the target prefab for one frame
            listCardContainers[i].SetActive(false);
        }

        listCardTargetContainers = new List<GameObject>();
        for (int i = 0; i < MAX_CARDS_PER_PACK; ++i)
        {
            listCardTargetContainers.Add(GameUtils.GetChildGameObject(openPackContainer, "TargetCard0" + i, true));

            // Note :: This is to avoid seeing a visual glitch
            // where you see the target prefab for one frame
            listCardTargetContainers[i].SetActive(false);
        }

        packCounterContainer = GameUtils.GetChildGameObject(prizeCanvas, "PackCounterContainer", true);
        packCounterContainer.SetActive(false);
        packCounterText = GameUtils.GetChildGameObject(packCounterContainer, "TextQty").GetComponent<Text>();

        inputButton = GameUtils.GetChildGameObject(prizeCanvas, "InputButton", true).GetComponent<Button>();
        inputButton.onClick.AddListener(delegate { InputButtonClicked(); });
        inputButton.interactable = false;

        skipButton = GameUtils.GetChildGameObject(prizeCanvas, "SkipButton", true).GetComponent<Button>();
        skipButton.onClick.AddListener(delegate { SkipButtonClicked(); });
        skipButton.gameObject.SetActive(false);

        tapContinueGO = GameUtils.GetChildGameObject(prizeCanvas, "prefab_TxtTapContinue", true);
        tapContinueGO.SetActive(false);
        
        currentCardIndex = 0;
        isBackgroundIn = false;
        collectPrizeDelayTimer = 0f;
        packIntroDelayTimer = 0f;
        collectPackDelayTimer = 0f;
        packCounter = 0;
        packCounterDecrementTimer = 0f;
        packCounterDecrementTracker = 0;
        numberOfCardsShowing = 0;
        idleTimer = 0f;

        isSceneSetup = true;
    }

    public override bool FixedUpdate()
    {
        if (!base.FixedUpdate()) return false;
        if (!CheckIfDataLoaded()) return false;

        // If the Open Prize scene is loaded and ready
        if (isSceneSetup)
        {
            UpdatePrizeFlow();
        }        
        
        
        return true;
    }
    
    private bool CheckIfDataLoaded()
    {
        if (DataController.Instance.AllLoaded && !loaded)
        {
            loaded = true;
        }

        return loaded;
    }

    private void EnterPrizeState(EPrizeState newPrizeState)
    {
        switch(newPrizeState)
        {
            case EPrizeState.Start:
                {
                    // Disable the main canvas not to click through it
                    EnableMainCanvas(false);

                    // Start a coroutine to load the open prize scene
                    // Note :: Coroutine is needed since we want to
                    // wait for the next frame to do the scene setup.
                    StartCoroutine(LoadOpenPrizeScene());
                    
                }                
                break;
            case EPrizeState.PrizeIdle:
                {
                    idleTimer = GameUtils.TAP_CONTINUE_DELAY;
                }                
                break;
            case EPrizeState.PackIntro:
                {
                    // We want to have a delay before starting
                    // the whole open pack sequence not to overlap
                    packIntroDelayTimer = PACK_INTRO_START_DELAY;
                }                
                break;
            case EPrizeState.PackCardsIn:
                {
                    // Apply delay on the input button interactability
                    collectPackDelayTimer = COLLECT_PACK_DELAY;

                    // Populate the pack sequence with data
                    StartOpenPack();
                }
                break;
            case EPrizeState.PackIdle:
                {
                    idleTimer = GameUtils.TAP_CONTINUE_DELAY;
                }
                break;
            case EPrizeState.PackCardsOut:
                {
                }
                break;
            case EPrizeState.PackOutro:
                break;
            case EPrizeState.End:
                {
                    // Trigger the background out anim
                    darkBackground.GetComponent<Animator>().SetTrigger(BACKGROUND_OUT_TRIGGER_PARAM);

                    // Disable the skip button
                    skipButton.gameObject.SetActive(false);
                }                
                break;
        }
    } 

    private void ExitPrizeState(EPrizeState oldPrizeState)
    {
        switch (oldPrizeState)
        {
            case EPrizeState.Start:
                break;
            case EPrizeState.PrizeIdle:
                {
                    // Hide tap to continue text
                    if (tapContinueGO)
                    {
                        tapContinueGO.SetActive(false);
                    }

                    // Trigger the out anim for each prize
                    for (int i = 0; i < eachPrizeContainer.Length; ++i)
                    {
                        if (eachPrizeContainer[i])
                        {
                            eachPrizeContainer[i].GetComponent<Animator>().SetTrigger(COLLECT_PRIZE_TRIGGER_PARAM);
                        }
                    }
                }
                break;
            case EPrizeState.PackIntro:
                {
                    // This will automatically play 'Cards In' anim
                    // and wait for the player to collect the pack
                    if (openPackContainer)
                    {
                        openPackContainer.SetActive(true);
                    }

                    // Add a delay before decrementing the pack counter
                    packCounterDecrementTimer = PACK_COUNTER_DECREMENT_DELAY;
                    packCounterDecrementTracker = 0;
                }
                break;
            case EPrizeState.PackCardsIn:
                break;
            case EPrizeState.PackIdle:
                {
                    // Hide tap to continue text
                    if (tapContinueGO)
                    {
                        tapContinueGO.SetActive(false);
                    }

                    // Trigger the cards out anim
                    if (openPackContainer && openPackContainer.activeInHierarchy)
                    {
                        openPackContainer.GetComponent<Animator>().SetTrigger(COLLECT_PACK_TRIGGER_PARAM);
                    }                    
                }
                break;
            case EPrizeState.PackCardsOut:
                {
                    if (HasCardsLeftToOpen())
                    {
                        // Re-enter the OpenPackCardsIn anim
                        if (openPackContainer && openPackContainer.activeInHierarchy)
                        {
                            openPackContainer.GetComponent<Animator>().SetTrigger(NEXT_PACK_TRIGGER_PARAM);
                        }                        

                        // Add a delay before decrementing the pack counter
                        packCounterDecrementTimer = PACK_COUNTER_DECREMENT_DELAY;
                        packCounterDecrementTracker = 0;
                    }
                    else
                    {
                        // Close the pack counter
                        if (packCounterContainer && packCounterContainer.activeInHierarchy)
                        {
                            packCounterContainer.GetComponent<Animator>().SetTrigger(CLOSE_PACK_COUNTER_TRIGGER_PARAM);
                        }                        
                    }
                }                               
                break;
            case EPrizeState.PackOutro:
                break;
            case EPrizeState.End:
                break;
        }
    }

    private void UpdatePrizeFlow()
    {
        switch (currentPrizeState)
        {
            case EPrizeState.Start:
                UpdateStartState();
                break;
            case EPrizeState.PrizeIdle:
                UpdatePrizeIdleState();
                break;
            case EPrizeState.PackIntro:
                UpdatePackIntroState();
                break;
            case EPrizeState.PackCardsIn:
                UpdatePackCardsInState();
                break;
            case EPrizeState.PackIdle:
                UpdatePackIdleState();
                break;
            case EPrizeState.PackCardsOut:
                UpdatePackCardsOutState();
                break;
            case EPrizeState.PackOutro:
                break;
            case EPrizeState.End:
                UpdateClosePrizeState();
                break;
        }
    }

    private void UpdateStartState()
    {
        // If the background in anim is over
        if (!isBackgroundIn
            && darkBackground.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("BackgroundIn"))
        {
            isBackgroundIn = true;

            // Put a small delay before being able to collect the prize
            collectPrizeDelayTimer = COLLECT_PRIZE_DELAY;

            // Sound hook - Open Prize
            AudioManager.Instance.PlaySound(AudioManager.ESoundType.OpenPrize);
        }

        // Once the background in anim is in
        if (isBackgroundIn)
        {
            // Activate a delay to collect the prize
            collectPrizeDelayTimer -= Time.deltaTime;
            
            if (collectPrizeDelayTimer <= 0f)
            {
                // Activate the input button
                inputButton.interactable = true;

                // Move to the next state
                CurrentPrizeState = EPrizeState.PrizeIdle;
            }
        }
    }

    private void UpdatePrizeIdleState()
    {
        UpdateIdleTimer();
    }

    private void UpdatePackIntroState()
    {
        // Until this delay is not over,
        // don't start anything
        if (packIntroDelayTimer > 0f)
        {
            packIntroDelayTimer -= Time.deltaTime;

            if (packIntroDelayTimer <= 0f)
            {
                // This will automatically play the 'Pack Counter Intro'
                // followed by 'Pack Counter Intro2'
                packCounterContainer.SetActive(true);

                // Sound hook - Open Pack
                AudioManager.Instance.PlaySound(AudioManager.ESoundType.OpenPack);

                // Enable the skip button
                skipButton.gameObject.SetActive(true);
            }
        }

        // If the pack counter container is active
        // and the intro anim is over
        if (packCounterContainer.activeInHierarchy
            && packCounterContainer.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("PackCounterIntro2"))
        {
            CurrentPrizeState = EPrizeState.PackCardsIn;
        }
    }

    private void UpdatePackCardsInState()
    {
        if (openPackContainer.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("OpenPackCardsIn"))
        {
            // Manage the decrementing of the pack counter
            if (packCounterDecrementTimer > 0f)
            {
                packCounterDecrementTimer -= Time.deltaTime;

                if (packCounterDecrementTimer <= 0f)
                {
                    // Decrement by 1 and repeat
                    SetPackCounter(-1);
                    packCounterDecrementTracker++;

                    // Sound hook - Open Pack Card In
                    AudioManager.Instance.PlaySound(AudioManager.ESoundType.OpenPackCardIn);

                    // Repeat until all active cards are decremented
                    if (packCounterDecrementTracker < numberOfCardsShowing)
                    {
                        packCounterDecrementTimer += PACK_COUNTER_DECREMENT_INTERVAL;
                    }                    
                }
            }

            // Activate a delay to collect the pack
            if (collectPackDelayTimer > 0f)
            {
                collectPackDelayTimer -= Time.deltaTime;

                if (collectPackDelayTimer <= 0f)
                {
                    // Activate the input button
                    inputButton.interactable = true;

                    // Move to the next state
                    CurrentPrizeState = EPrizeState.PackIdle;
                }
            }
        }
    }

    private void UpdatePackIdleState()
    {
        UpdateIdleTimer();
    }

    private void UpdatePackCardsOutState()
    {
        if (openPackContainer.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("OpenPackCardsOut")
            && openPackContainer.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).normalizedTime > 0.8f)
        {
            if (HasCardsLeftToOpen())
            {
                CurrentPrizeState = EPrizeState.PackCardsIn;
            }
            else
            {
                CurrentPrizeState = EPrizeState.End;
            }            
        }
    }
    
    private void UpdateClosePrizeState()
    {
        // If the background out anim is over
        if (darkBackground.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("BackgroundOut"))
        {
            // Disable completely the background
            darkBackground.SetActive(false);

            // Re-enable the main canvas
            EnableMainCanvas(true);

            // Unload the scene
            // Note :: This might be too heavy on the performance,
            // consider the alternative to keep the scene loaded
            SceneManager.UnloadScene(OPEN_PRIZE_SCENE_INDEX);
            
            // Consider the scene not setup anymore since we unloaded it
            isSceneSetup = false;
        }
    }

    private void UpdateIdleTimer()
    {
        if (idleTimer > 0f)
        {
            idleTimer -= Time.deltaTime;

            // If idle for too long
            if (idleTimer <= 0f)
            {
                // Show tap to continue text
                tapContinueGO.SetActive(true);
            }
        }
    }
                
    private void InputButtonClicked()
    {
        if (currentPrizeState == EPrizeState.PrizeIdle)
        {
            // When collecting the prizes, if there's a pack to open then
            // go into PackIntro state, otherwise go into ClosePrize state.
            if (HasCardsLeftToOpen())
            {
                CurrentPrizeState = EPrizeState.PackIntro;
            }
            else
            {
                CurrentPrizeState = EPrizeState.End;
            }
        }        
        else if (currentPrizeState == EPrizeState.PackIdle)
        {            
            CurrentPrizeState = EPrizeState.PackCardsOut;
        }

        // Note :: Make the button not interactable to avoid calling this function again.
        // The prize states take care of enabling the button when needed.
        inputButton.interactable = false;
    }

    private void SkipButtonClicked()
    {
        // Hide the open pack UI
        packCounterContainer.SetActive(false);
        openPackContainer.SetActive(false);

        CurrentPrizeState = EPrizeState.End;
    }
    
    private void EnableMainCanvas(bool isEnable)
    {
        // If in the Menu scene
        if (FlowStateController.Instance.IsInMenuScene())
        {
            MenuUIController.Instance.EnableMenuUI(isEnable);
        }
        // If in the Battle scene
        else if (FlowStateController.Instance.IsInBattleScene())
        {
            WaveController.Instance.EnableBattleUI(isEnable);
        }
    }

    public void InitOpenPrize(List<RewardRollInfo> newPrizes)
    {
        // Set the current prize data to show it
        listIndividualRewardsRollInfo = newPrizes;

        CombineSameRewards();
        MergeCardRewards();
        
        CurrentPrizeState = EPrizeState.Start;        
    }

    private void MergeCardRewards()
    {
        mergedWeaponUpgradesRollInfo = new List<CardsController.WeaponCard>();
        mergedConsumablesRollInfo = new List<GameUtils.EConsumable>();

        // For each individual reward roll info
        for (int i = 0; i < listIndividualRewardsRollInfo.Count; ++i)
        {
            // If it contains cards
            if (listIndividualRewardsRollInfo[i].rewardData.rewardType == RewardData.ERewardType.Pack)
            {
                // Combine the cards roll together
                // For each upgrade card
                for (int j = 0; j < listIndividualRewardsRollInfo[i].upgradeCardsRollInfo.Count; ++j)
                {
                    mergedWeaponUpgradesRollInfo.Add(listIndividualRewardsRollInfo[i].upgradeCardsRollInfo[j]);
                }
                // For each consumable
                for (int j = 0; j < listIndividualRewardsRollInfo[i].consumablesRollInfo.Count; ++j)
                {
                    mergedConsumablesRollInfo.Add(listIndividualRewardsRollInfo[i].consumablesRollInfo[j]);
                }
            }
        }
    }
    
    // Note :: Used to combine all the pack rewards together
    // in order to show the total amount of cards won.
    private void CombineSameRewards()
    {
        listCombinedRewardsRollInfo = new List<RewardRollInfo>();

        // For each individual reward roll info
        for (int i = 0; i < listIndividualRewardsRollInfo.Count; ++i)
        {
            // If it contains cards
            if (listIndividualRewardsRollInfo[i].rewardData.rewardType == RewardData.ERewardType.Pack)
            {
                // Get the reference to the combined pack reward roll info
                RewardRollInfo combinedPackRewardRollInfo = GetCombinedPackRewardsRollInfo(listIndividualRewardsRollInfo[i].rewardData);

                // Combine the cards roll together
                // For each upgrade card
                for (int j = 0; j < listIndividualRewardsRollInfo[i].upgradeCardsRollInfo.Count; ++j)
                {
                    combinedPackRewardRollInfo.upgradeCardsRollInfo.Add(listIndividualRewardsRollInfo[i].upgradeCardsRollInfo[j]);
                }
                // For each consumable
                for (int j = 0; j < listIndividualRewardsRollInfo[i].consumablesRollInfo.Count; ++j)
                {
                    combinedPackRewardRollInfo.consumablesRollInfo.Add(listIndividualRewardsRollInfo[i].consumablesRollInfo[j]);
                }
            }
            // Otherwise just add the credits / gold rewards
            else
            {
                bool found = false;
                for (int j = 0; j < listCombinedRewardsRollInfo.Count; ++j)
                {
                    if (listCombinedRewardsRollInfo[j].rewardData.rewardType == listIndividualRewardsRollInfo[i].rewardData.rewardType)
                    {
                        listCombinedRewardsRollInfo[j].rewardData.currencyAmount += listIndividualRewardsRollInfo[i].rewardData.currencyAmount;
                        found = true;
                        break;
                    }
                }
                if (!found)
                {
                    // Note :: Here we can add directly the individual reward
                    // since we're not modifying it, only for the pack rewards.
                    listCombinedRewardsRollInfo.Add(listIndividualRewardsRollInfo[i]);
                }
            }
        }
    }

    private RewardRollInfo GetCombinedPackRewardsRollInfo(RewardData rewardData)
    {
        RewardRollInfo packRewardRollInfo = null;
        // Try to find the combined pack reward
        for (int i = 0; i < listCombinedRewardsRollInfo.Count; ++i)
        {
            if (listCombinedRewardsRollInfo[i].rewardData.rewardType == RewardData.ERewardType.Pack)
            {
                packRewardRollInfo = listCombinedRewardsRollInfo[i];
                break;
            }
        }

        // If it's the first try to combine, create one
        if (packRewardRollInfo == null)
        {            
            packRewardRollInfo = new RewardRollInfo(rewardData);
            listCombinedRewardsRollInfo.Add(packRewardRollInfo);
        }

        return packRewardRollInfo;       
    }

    private IEnumerator LoadOpenPrizeScene()
    {
        // Load the open prize scene additively
        SceneManager.LoadScene(OPEN_PRIZE_SCENE_INDEX, LoadSceneMode.Additive);

        // IMPORTANT :: Wait for the next frame since
        // LoadScene is guaranteed to complete in the next frame
        yield return StartCoroutine(WaitFor.Frames(1));

        // Init the scene UI elements
        SetupScene();

        // Start the open prize anim
        StartOpenPrize();
    }

    private void StartOpenPrize()
    {
        GameObject activePrizeContainer = null;
        
        // Activate the right prize container based on the prize count
        switch(listCombinedRewardsRollInfo.Count)
        {
            case 1:
                onePrizeContainer.SetActive(true);
                onePrizeContainer.GetComponent<Animator>().SetTrigger(ONE_PRIZE_IN_TRIGGER_PARAM);
                activePrizeContainer = onePrizeContainer;                
                break;
            case 2:
                twoPrizeContainer.SetActive(true);
                twoPrizeContainer.GetComponent<Animator>().SetTrigger(TWO_PRIZE_IN_TRIGGER_PARAM);
                activePrizeContainer = twoPrizeContainer;
                break;
            case 3:
                threePrizeContainer.SetActive(true);
                threePrizeContainer.GetComponent<Animator>().SetTrigger(THREE_PRIZE_IN_TRIGGER_PARAM);
                activePrizeContainer = threePrizeContainer;
                break;
            default:
                Debug.LogWarning("Something went wrong with the Open Prize sequence. Invalid prize amount.");
                break;
        }

        // For each prize
        eachPrizeContainer = new GameObject[listCombinedRewardsRollInfo.Count];
        eachTargetPrizeContainer = new GameObject[listCombinedRewardsRollInfo.Count];
        for (int i = 0; i < listCombinedRewardsRollInfo.Count; ++i)
        {
            // Setup the prize UI
            eachPrizeContainer[i] = GameUtils.GetChildGameObject(activePrizeContainer, "Prize0" + i, true);
            SetPrizeUI(listCombinedRewardsRollInfo[i], eachPrizeContainer[i]);

            eachTargetPrizeContainer[i] = GameUtils.GetChildGameObject(activePrizeContainer, "target0" + i, true);
        }
    }

    private void SetPrizeUI(RewardRollInfo rewardRollInfo, GameObject eachPrizeContainer)
    {
        // Set each prize container type
        GameObject creditPrize = GameUtils.GetChildGameObject(eachPrizeContainer, "prefab_prizeCredit", true);
        creditPrize.SetActive(false);
        GameObject goldPrize = GameUtils.GetChildGameObject(eachPrizeContainer, "prefab_prizeGold", true);
        goldPrize.SetActive(false);
        GameObject packPrize = GameUtils.GetChildGameObject(eachPrizeContainer, "prefab_prizePack", true);
        packPrize.SetActive(false);

        // Depending on the reward type, show the right prize
        if (rewardRollInfo.rewardData.rewardType == RewardData.ERewardType.Credit)
        {
            creditPrize.SetActive(true);
            GameUtils.GetChildGameObject(creditPrize, "textQty").GetComponent<Text>().text = "+ " + rewardRollInfo.rewardData.currencyAmount;
        }
        else if (rewardRollInfo.rewardData.rewardType == RewardData.ERewardType.Gold)
        {
            goldPrize.SetActive(true);
            GameUtils.GetChildGameObject(goldPrize, "textQty").GetComponent<Text>().text = "+ " + rewardRollInfo.rewardData.currencyAmount;
        }
        else if (rewardRollInfo.rewardData.rewardType == RewardData.ERewardType.Pack)
        {
            int numberOfCards = rewardRollInfo.upgradeCardsRollInfo.Count + rewardRollInfo.consumablesRollInfo.Count;
            SetPackCounter(numberOfCards);    

            packPrize.SetActive(true);
            GameUtils.GetChildGameObject(packPrize, "textQty").GetComponent<Text>().text = "+ " + (numberOfCards);
        }
        else if (rewardRollInfo.rewardData.rewardType == RewardData.ERewardType.Booster)
        {
            creditPrize.SetActive(true);
            GameUtils.GetChildGameObject(creditPrize, "textQty").GetComponent<Text>().text = "+ " + rewardRollInfo.rewardData.boosterHours + " H";
            Sprite icon;
            switch (rewardRollInfo.rewardData.boosterType)
            {
                case BoosterController.EBoosterPackType.Offensive:
                    icon = (Sprite)Resources.Load("Images/cards_offensiveIcon", typeof(Sprite));
                    break;
                case BoosterController.EBoosterPackType.Defensive:
                    icon = (Sprite)Resources.Load("Images/cards_defensiveIcon", typeof(Sprite));
                    break;
                case BoosterController.EBoosterPackType.Credits:
                default:
                    icon = (Sprite)Resources.Load("Images/cards_warbondIcon", typeof(Sprite));
                    break;
            }
            GameUtils.GetChildGameObject(creditPrize, "icon", true).GetComponent<Image>().sprite = icon;
        }
    }

    private void StartOpenPack()
    {
        // Get the next pack reward roll info
        //RewardRollInfo currentPackRewardRollInfo = GetNextPackRewardRollInfo();
        RewardRollInfo currentPackRewardRollInfo = CreateNextPackRewardRollInfo();

        // Show the right cards / targets based on the number of cards to open
        numberOfCardsShowing = currentPackRewardRollInfo.upgradeCardsRollInfo.Count + currentPackRewardRollInfo.consumablesRollInfo.Count;
        List<GameObject> cardContainersToShow;
        List<GameObject> targetCardContainersToShow;        
        PopulateContainersToShow(numberOfCardsShowing, out cardContainersToShow, out targetCardContainersToShow);

        // For each upgrade card rolled
        for (int i = 0; i < currentPackRewardRollInfo.upgradeCardsRollInfo.Count; ++i)
        {
            // Activate the right card and populate it
            CardsController.WeaponCard upgradeCard = currentPackRewardRollInfo.upgradeCardsRollInfo[i];
                        
            GameObject cardPrefab = GameUtils.GetChildGameObject(cardContainersToShow[i], "prefab_cardUpgrade", true);
            GameUtils.GetChildGameObject(cardPrefab, "TextCategory", true).GetComponent<Text>().text = GameUtils.GetWeaponUpgradeLocalizedName(upgradeCard.weaponID, upgradeCard.weaponUpgrade);
            GameUtils.GetChildGameObject(cardPrefab, "Icon", true).GetComponent<Image>().sprite = GameUtils.GetWeaponIcon(upgradeCard.weaponID);
            GameUtils.GetChildGameObject(cardPrefab, "TextName", true).GetComponent<Text>().text = GameUtils.GetWeaponLocalizedName(upgradeCard.weaponID);
            cardPrefab.SetActive(true);
            
            // Activate the right target
            GameObject targetCardPrefab = GameUtils.GetChildGameObject(targetCardContainersToShow[i], "prefab_targetUpgrade", true);
            targetCardPrefab.SetActive(true);
        }

        // For each consumable rolled
        for (int i = 0; i < currentPackRewardRollInfo.consumablesRollInfo.Count; ++i)
        {
            // Activate the right card and populate it
            GameUtils.EConsumable consumable = currentPackRewardRollInfo.consumablesRollInfo[i];
            
            GameObject cardPrefab = GameUtils.GetChildGameObject(cardContainersToShow[i + currentPackRewardRollInfo.upgradeCardsRollInfo.Count], "prefab_cardConsummable", true);
            GameUtils.GetChildGameObject(cardPrefab, "TextCategory", true).GetComponent<Text>().text = "x1";
            GameUtils.GetChildGameObject(cardPrefab, "Icon", true).GetComponent<Image>().sprite = GameUtils.GetConsumableIcon(consumable, false);
            GameUtils.GetChildGameObject(cardPrefab, "TextName", true).GetComponent<Text>().text = GameUtils.GetConsumableLocalizedName(consumable);
            cardPrefab.SetActive(true);            

            // Activate the right target
            GameObject targetCardPrefab = GameUtils.GetChildGameObject(targetCardContainersToShow[i + currentPackRewardRollInfo.upgradeCardsRollInfo.Count], "prefab_targetConsummable", true);
            targetCardPrefab.SetActive(true);
        }
    }
    
    private RewardRollInfo CreateNextPackRewardRollInfo()
    {
        RewardRollInfo currentPackRewardRollInfo = null;

        RewardData dummyPackReward = ScriptableObject.CreateInstance<RewardData>();
        dummyPackReward.rewardType = RewardData.ERewardType.Pack;

        currentPackRewardRollInfo = new RewardRollInfo(dummyPackReward);
        // Populate the next pack reward roll info with 5 cards
        for (int i = 0; i < MAX_CARDS_PER_PACK; ++i)
        {
            // If there's still weapon upgrade card to show
            if (currentCardIndex < mergedWeaponUpgradesRollInfo.Count)
            {
                currentPackRewardRollInfo.upgradeCardsRollInfo.Add(mergedWeaponUpgradesRollInfo[currentCardIndex]);
            }
            // If there's still consumable card to show
            else if (currentCardIndex < (mergedWeaponUpgradesRollInfo.Count + mergedConsumablesRollInfo.Count))
            {
                currentPackRewardRollInfo.consumablesRollInfo.Add(mergedConsumablesRollInfo[currentCardIndex - mergedWeaponUpgradesRollInfo.Count]);
            }
            else
            {
                // No more cards to show
                break;
            }

            currentCardIndex++;
        }

        // Safeguard
        if (currentPackRewardRollInfo == null)
        {
            Debug.LogError("Something went wrong with the open pack sequence! Could not find another pack reward!");
            return null;
        }
        
        return currentPackRewardRollInfo;
    }
    
    private bool HasCardsLeftToOpen()
    {
        return currentCardIndex < (mergedWeaponUpgradesRollInfo.Count + mergedConsumablesRollInfo.Count);
    }

    private void PopulateContainersToShow(int numberOfCards, out List<GameObject> cardContainersToShow, out List<GameObject> targetCardContainersToShow)
    {
        // Before opening a pack, hide all by default
        for (int i = 0; i < listCardContainers.Count; ++i)
        {
            GameUtils.GetChildGameObject(listCardContainers[i], "prefab_cardUpgrade", true).SetActive(false);            
            GameUtils.GetChildGameObject(listCardContainers[i], "prefab_cardConsummable", true).SetActive(false);
            GameUtils.GetChildGameObject(listCardTargetContainers[i], "prefab_targetUpgrade", true).SetActive(false);
            GameUtils.GetChildGameObject(listCardTargetContainers[i], "prefab_targetConsummable", true).SetActive(false);
        }

        // Show the right cards / targets based on the number of cards to open
        cardContainersToShow = new List<GameObject>();
        targetCardContainersToShow = new List<GameObject>();

        // Note :: 
        // One card = Card02
        // Two cards = Card01, Card03
        // Three cards = Card01, Card02, Card03 
        // Four cards = Card00, Card01, Card03, Card04
        
        switch (numberOfCards)
        {
            case 1:
                cardContainersToShow.Add(listCardContainers[2]);
                targetCardContainersToShow.Add(listCardTargetContainers[2]);
                break;
            case 2:
                cardContainersToShow.Add(listCardContainers[1]);
                cardContainersToShow.Add(listCardContainers[3]);
                targetCardContainersToShow.Add(listCardTargetContainers[1]);
                targetCardContainersToShow.Add(listCardTargetContainers[3]);
                break;
            case 3:
                cardContainersToShow.Add(listCardContainers[1]);
                cardContainersToShow.Add(listCardContainers[2]);
                cardContainersToShow.Add(listCardContainers[3]);
                targetCardContainersToShow.Add(listCardTargetContainers[1]);
                targetCardContainersToShow.Add(listCardTargetContainers[2]);
                targetCardContainersToShow.Add(listCardTargetContainers[3]);
                break;
            case 4:
                cardContainersToShow.Add(listCardContainers[0]);
                cardContainersToShow.Add(listCardContainers[1]);
                cardContainersToShow.Add(listCardContainers[3]);
                cardContainersToShow.Add(listCardContainers[4]);
                targetCardContainersToShow.Add(listCardTargetContainers[0]);
                targetCardContainersToShow.Add(listCardTargetContainers[1]);
                targetCardContainersToShow.Add(listCardTargetContainers[3]);
                targetCardContainersToShow.Add(listCardTargetContainers[4]);
                break;
            case 5:
                cardContainersToShow.Add(listCardContainers[0]);
                cardContainersToShow.Add(listCardContainers[1]);
                cardContainersToShow.Add(listCardContainers[2]);
                cardContainersToShow.Add(listCardContainers[3]);
                cardContainersToShow.Add(listCardContainers[4]);
                targetCardContainersToShow.Add(listCardTargetContainers[0]);
                targetCardContainersToShow.Add(listCardTargetContainers[1]);
                targetCardContainersToShow.Add(listCardTargetContainers[2]);
                targetCardContainersToShow.Add(listCardTargetContainers[3]);
                targetCardContainersToShow.Add(listCardTargetContainers[4]);
                break;
        }
    }

    private void SetPackCounter(int valueToAdd)
    {
        packCounter += valueToAdd;
        packCounterText.text = "x" + packCounter;
    }

}

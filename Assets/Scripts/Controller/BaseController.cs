using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class BaseController : UnitySingleton<BaseController>
{
    private bool started = false;
    
    [SerializeField]
    private Dictionary<GameUtils.BaseID, GameObject> bases = new Dictionary<GameUtils.BaseID, GameObject>();
    
    public Dictionary<GameUtils.BaseID, GameObject> Bases
    {
        get { return bases; }
    }

    public void RegisterBase(GameUtils.BaseID baseID, GameObject baseObject)
    {
        bases.Add(baseID, baseObject);
        started = true;
    }

    public void UnregisterBase(GameUtils.BaseID baseID)
    {
        bases.Remove(baseID);
    }
    
    public override bool Update()
    {
        if (!base.Update()) return false;
        if (WaveController.Instance.Paused) return false;
        if (!started) return false;
        if (!WaveController.Instance.InitWaveStarted) return false;

        CheckFailedWaves();
        return true;
    }

    private bool IsObjectiveFailed()
    {
        int countWeapons = 0;
        bool cityFound = false;

        // For each bases
        for (int i = 0; i < bases.Count; ++i)
        {
            GameObject baseGameObject = bases.ElementAt(i).Value;
            if (baseGameObject)
            {
                BaseObject baseObject = baseGameObject.GetComponent<BaseObject>();
                if (baseObject.BaseID == GameUtils.BaseID.CITY)
                {
                    cityFound = true;
                }
                if (baseObject.BaseID < GameUtils.BaseID.CITY)
                {
                    countWeapons++;
                }
            }
        }

        // Challenge fails if city is destroyed or all weapons are
        return !cityFound || countWeapons == 0;
    }

    public void CheckFailedWaves()
    {
        if (IsObjectiveFailed())
        {
            WaveController.Instance.FailedWaves();
        }
    }
}

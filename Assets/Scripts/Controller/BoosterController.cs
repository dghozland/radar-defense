﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BoosterController : UnitySingleton<BoosterController>
{
    private readonly string IS_BOOST_ACTIVE_BOOL_PARAM = "IsBoostActive";

    [SerializeField]
    private float boostDuration = 30f;
    [SerializeField]
    private float boostMultiplier = 1.25f;

    [SerializeField]
    public float killCredits = 1.5f;
    [SerializeField]
    public float lootCredits = 1.5f;
    [SerializeField]
    public float matchRewardCredits = 1.5f;
    [SerializeField]
    public float multiKillCredits = 1.5f;
    [SerializeField]
    public float waveTimer = 1.5f;

    [SerializeField]
    public float weaponSpeed = 1.5f;
    [SerializeField]
    public float groundDamage = 1.5f;
    [SerializeField]
    public float airDamage = 1.5f;
    [SerializeField]
    public float explosionRadius = 1.5f;

    [SerializeField]
    public float health = 1.5f;
    [SerializeField]
    public float regeneration = 1.5f;
    [SerializeField]
    public float startingAmmo = 1.5f;
    [SerializeField]
    public float sightRadius = 1.5f;

    // UI elements
    private static Animator boostPanelAnim;
    private static Text boostNameField;
    private static Text boostTimerField;
    private static bool isInitialized = false;

    private static EBoostType currentBoost;
    private static float boostTimer;

    private GameObject boosterField;
    private GameObject boosterBack;
    private GameObject boosterCreditsIcon;
    private GameObject boosterOffensiveIcon;
    private GameObject boosterDefensiveIcon;

    public enum EBoostType
    {
        None,
        Damage,
        RegenSpeed,
        ProjectileSpeed,
        RadarFrequency,
        Count
    }

    public enum EBoosterPackType
    {
        None,
        Credits,
        Offensive,
        Defensive
    }

    public override void Awake()
    {
        base.Awake();
        if (!isInitialized)
        {
            isInitialized = true;
            DontDestroyOnLoad(gameObject);
        }
        if (FlowStateController.Instance.IsInBattleScene())
        {
            RegisterDependency(WaveController.Instance);
            GameObject battleUI = GameObject.Find("BattleUICanvas");
            boostPanelAnim = GameUtils.GetChildGameObject(battleUI, "BoostPanel").GetComponent<Animator>();
            boostNameField = GameUtils.GetChildGameObject(battleUI, "BoostNameField").GetComponent<Text>();
            boostTimerField = GameUtils.GetChildGameObject(battleUI, "BoostTimerField").GetComponent<Text>();

            UpdateBoosterField(battleUI);

            boostNameField.text = "";
            boostTimerField.text = "";
            currentBoost = EBoostType.None;
        }
    }

    public override bool Update()
    {
        if (!base.Update()) return false;
        if (FlowStateController.Instance.IsInBattleScene())
        {
            if (WaveController.Instance.Paused) return false;
            UpdateBattleBoost();
        }

        return true;
    }

    #region BoosterPacks
    public float IsStatUpgraded(GameUtils.EWeaponUpgrade upgrade)
    {
        switch(upgrade)
        {
            case GameUtils.EWeaponUpgrade.GROUND:            if (BoosterTypeActive(EBoosterPackType.Offensive)) return groundDamage; break;
            case GameUtils.EWeaponUpgrade.AIR:               if (BoosterTypeActive(EBoosterPackType.Offensive)) return airDamage; break;
            case GameUtils.EWeaponUpgrade.EXPLOSION_RADIUS:  if (BoosterTypeActive(EBoosterPackType.Offensive)) return explosionRadius; break;
            case GameUtils.EWeaponUpgrade.SPEED:             if (BoosterTypeActive(EBoosterPackType.Offensive)) return weaponSpeed; break;
            case GameUtils.EWeaponUpgrade.VISIBILITY_RADIUS: if (BoosterTypeActive(EBoosterPackType.Defensive)) return sightRadius; break;
            case GameUtils.EWeaponUpgrade.REGEN:             if (BoosterTypeActive(EBoosterPackType.Defensive)) return regeneration; break;
            case GameUtils.EWeaponUpgrade.RELOAD:            if (BoosterTypeActive(EBoosterPackType.Defensive)) return startingAmmo; break;
            case GameUtils.EWeaponUpgrade.HEALTH:            if (BoosterTypeActive(EBoosterPackType.Defensive)) return health; break;
        }
        return 1f;
    }

    public bool BoosterTypeActive(EBoosterPackType type)
    {
        return !DailyController.Instance.CanOpenBoosterForBoosterType(type);
    }

    public void AddBooster(EBoosterPackType boosterType, int boosterHours)
    {
        DailyController.Instance.AddBooster(boosterType, boosterHours);
    }

    public void UpdateBoosterField(GameObject battleUI)
    {
        boosterField = GameUtils.GetChildGameObject(battleUI, "BoosterField", true);
        boosterBack = GameUtils.GetChildGameObject(boosterField, "boosterBack", true);
        boosterCreditsIcon = GameUtils.GetChildGameObject(boosterField, "boosterCredits", true);
        boosterOffensiveIcon = GameUtils.GetChildGameObject(boosterField, "boosterOffensive", true);
        boosterDefensiveIcon = GameUtils.GetChildGameObject(boosterField, "boosterDefensive", true);
        boosterCreditsIcon.SetActive(false);
        boosterOffensiveIcon.SetActive(false);
        boosterDefensiveIcon.SetActive(false);

        int activeBoosters = 0;
        float iconWidth = 0f;
        var boosters = DailyController.Instance.DynamicData.ActiveBoosterPacks;
        if (boosters.Count > 0)
        {
            EBoosterPackType[] boostersEnum = GameUtils.GetEnumValues<EBoosterPackType>();
            for(int i = boostersEnum.Length - 1; i > 0; --i)
            {
                if (BoosterTypeActive(boostersEnum[i]))
                {
                    GameObject boosterIcon;
                    activeBoosters++;
                    // enable the booster icon
                    switch (boostersEnum[i])
                    {

                        case EBoosterPackType.Offensive:
                            boosterOffensiveIcon.SetActive(true);
                            boosterIcon = boosterOffensiveIcon;
                            break;
                        case EBoosterPackType.Defensive:
                            boosterDefensiveIcon.SetActive(true);
                            boosterIcon = boosterDefensiveIcon;
                            break;
                        case EBoosterPackType.Credits:
                        default:
                            boosterCreditsIcon.SetActive(true);
                            boosterIcon = boosterCreditsIcon;
                            break;
                    }
                    // position the booster icon
                    RectTransform rectTransform = boosterIcon.GetComponent<RectTransform>();
                    iconWidth = rectTransform.rect.width;
                    rectTransform.anchoredPosition = new Vector2((-activeBoosters * iconWidth) + (iconWidth / 2f) - 5f, rectTransform.anchoredPosition.y);
                }
            }
        }
        if (activeBoosters == 0)
        {
            boosterBack.SetActive(false);
            boosterField.SetActive(false);
        }
        else
        {
            RectTransform rectTransform = boosterBack.GetComponent<RectTransform>();
            Vector2 size = rectTransform.sizeDelta;
            size.x = (activeBoosters * iconWidth) + 15f;
            rectTransform.sizeDelta = size;
        }
    }
    #endregion // BoosterPacks

    #region BattleBoost
    public EBoostType CurrentBattleBoost
    {
        get { return currentBoost; }
        set
        {
            currentBoost = value;
            if (currentBoost != EBoostType.None)
            {
                boostTimer = boostDuration;
                boostPanelAnim.SetBool(IS_BOOST_ACTIVE_BOOL_PARAM, true);
            }
            else
            {
                boostPanelAnim.SetBool(IS_BOOST_ACTIVE_BOOL_PARAM, false);
            }

            // Set the boost name
            boostNameField.text = GetBattleBoostLocalizedName(currentBoost);
        }
    }

    public float BattleBoostMultiplier
    {
        get { return boostMultiplier; }
    }

    private void UpdateBattleBoost()
    {
        if (boostTimer > 0f)
        {
            boostTimer -= Time.deltaTime;

            boostTimerField.text = boostTimer.ToString("F1") + "sec";

            // If boost ended
            if (boostTimer <= 0f)
            {
                // Reset boost
                CurrentBattleBoost = EBoostType.None;
                boostTimerField.text = 0.ToString("F1");
            }
        }
    }

    public void AwardBattleBoost(Vector3 pos)
    {
        CurrentBattleBoost = RollBattleBoost();
        WaveController.Instance.SpawnFlyingText(GetBattleBoostLocalizedName(currentBoost), 
                                                pos, 
                                                FlyingTextObject.EFlyingTextType.Damage, 
                                                Color.cyan, 
                                                FontStyle.Normal, 
                                                FlyingTextObject.EFlyingTextBehaviour.Normal, 
                                                FlyingTextObject.EFlyingTextDirection.Up);
    }

    private EBoostType RollBattleBoost()
    {
        return (EBoostType)UnityEngine.Random.Range((int)EBoostType.Damage, (int)EBoostType.Count);
    }

    private string GetBattleBoostLocalizedName(EBoostType boostType)
    {
        switch (boostType)
        {
            case EBoostType.None:
                return "";
            case EBoostType.Damage:
                return LocalizationManager.Instance.GetLocalizedString(LocalizationManager.BOOST_DAMAGE_SPEED_TEXT);
            case EBoostType.RegenSpeed:
                return LocalizationManager.Instance.GetLocalizedString(LocalizationManager.BOOST_REGEN_SPEED_TEXT);
            case EBoostType.ProjectileSpeed:
                return LocalizationManager.Instance.GetLocalizedString(LocalizationManager.BOOST_PROJECTILE_SPEED_TEXT);
            case EBoostType.RadarFrequency:
                return LocalizationManager.Instance.GetLocalizedString(LocalizationManager.BOOST_RADAR_FREQUENCY_TEXT);
            default:
                return "Should not happen";
        }
    }
    #endregion // BattleBoost
}

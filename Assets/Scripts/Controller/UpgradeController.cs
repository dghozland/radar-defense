using UnityEngine;
using System.Collections.Generic;
using System.IO;

#pragma warning disable 0649

public class UpgradeController : UnitySingleton<UpgradeController>
{
    [SerializeField]
    private int[] cardCostPerLvl;

    [SerializeField]
    private int[] upgradeCostPerLvl; 

    // weapon upgrades (level 0 - 10)
    private static Dictionary<GameUtils.BaseID, Dictionary<GameUtils.EWeaponUpgrade, int>> weaponUpgrades;

    public Dictionary<GameUtils.BaseID, Dictionary<GameUtils.EWeaponUpgrade, int>> WeaponUpgrades
    {
        get { return weaponUpgrades; }
        set { weaponUpgrades = value; }
    }
        
        
    public override void Awake()
    {
        base.Awake();
    }

    #region Weapon Upgrades
    public void SaveWeaponUpgrades(DataController.SAVEPRIORITY priority)
    {
        DataController.Instance.SaveUpgradeData(priority);
    }

    public void LoadWeaponUpgrades(Dictionary<GameUtils.BaseID, Dictionary<GameUtils.EWeaponUpgrade, int>> remoteUpgrades)
    {
        if (remoteUpgrades == null)
        {
            System.Exception ex = JsonUtils<Dictionary<GameUtils.BaseID, Dictionary<GameUtils.EWeaponUpgrade, int>>>.Load(GameUtils.WEAPON_UPG_JSON, out weaponUpgrades, GameUtils.ELoadType.DynamicData);
            if (ex != null
                && ex.GetType() == typeof(FileLoadException))
            {
                ResetWeaponUpgrades();
            }
        }
        else
        {
            weaponUpgrades = remoteUpgrades;
        }
    }

    public void ResetWeaponUpgrades()
    {
        weaponUpgrades = new Dictionary<GameUtils.BaseID, Dictionary<GameUtils.EWeaponUpgrade, int>>();
        for (int i = (int)GameUtils.BaseID.MISSILE; i < (int)GameUtils.BaseID.CITY; ++i)
        {
            GameUtils.BaseID weaponID = (GameUtils.BaseID)i;
            weaponUpgrades[weaponID] = new Dictionary<GameUtils.EWeaponUpgrade, int>()
            {
                { GameUtils.EWeaponUpgrade.SPEED, 0 },
                { GameUtils.EWeaponUpgrade.RELOAD, 0 },
                { GameUtils.EWeaponUpgrade.REGEN, 0 },
                { GameUtils.EWeaponUpgrade.AIR, 0 },
                { GameUtils.EWeaponUpgrade.GROUND, 0 },
                { GameUtils.EWeaponUpgrade.EXPLOSION_RADIUS, 0 },
                { GameUtils.EWeaponUpgrade.VISIBILITY_RADIUS, 0 },
                { GameUtils.EWeaponUpgrade.HEALTH, 0 },
            };
        }
        SaveWeaponUpgrades(DataController.SAVEPRIORITY.QUEUE);
    }

#if UNITY_EDITOR || DEBUG_MOBILE
    public void MaxWeaponUpgrades()
    {
        // For each weapon
        for (int i = (int)GameUtils.BaseID.MISSILE; i < (int)GameUtils.BaseID.CITY; ++i)
        {
            SetWeaponUpgradesLevel((GameUtils.BaseID)i, 10);
        }
    }

    public void SetWeaponUpgradesLevel(GameUtils.BaseID weaponID, int upgradeLevel)
    {
        // For each weapon upgrades
        for (int i = 0; i < (int)GameUtils.EWeaponUpgrade.COUNT; ++i)
        {
            weaponUpgrades[weaponID][(GameUtils.EWeaponUpgrade)i] = upgradeLevel; 
        }

        SaveWeaponUpgrades(DataController.SAVEPRIORITY.QUEUE);
    }
#endif

    public void UpgradeWeapon(GameUtils.BaseID weaponID, GameUtils.EWeaponUpgrade weaponUpgrade, bool isNoCost)
    {
        // Cannot level higher than max
        if (weaponUpgrades[weaponID][weaponUpgrade] >= GameUtils.MAX_UPGRADE_LEVEL) return;

        int cardsNeeded = GetAmountOfCardsPerLvl(weaponUpgrades[weaponID][weaponUpgrade]);
        int creditsNeeded = GetCreditCostPerUpgradeLvl(weaponUpgrades[weaponID][weaponUpgrade]);

        // Check requirements for upgrading (cards + currencies)
        if (isNoCost
            || (CardsController.Instance.HasAmountOfWeaponCards(weaponID, weaponUpgrade, cardsNeeded)
            && CurrencyController.Instance.HasEnoughCurrency(creditsNeeded, GameUtils.ECurrencyType.Credit)))
        {
            if (!isNoCost)
            {
                // Remove cards and credits
                CardsController.Instance.RemoveWeaponCard(weaponID, weaponUpgrade, cardsNeeded);
                CurrencyController.Instance.ModifyCreditsAmount(-creditsNeeded, GameUtils.CreditsSource.WeaponUpgraded, DataController.SAVEPRIORITY.QUEUE);
            }

            weaponUpgrades[weaponID][weaponUpgrade]++;
            SaveWeaponUpgrades(DataController.SAVEPRIORITY.QUEUE);
        }
    }

    public int GetWeaponUpgradeLevel(GameUtils.BaseID weaponID, GameUtils.EWeaponUpgrade weaponUpgrade)
    {
        return weaponUpgrades[weaponID][weaponUpgrade];
    }

    public int GetWeaponRankLevel(GameUtils.BaseID weaponID, int weaponLvlLimit = -1)
    {
        int totalUpgradeLvl = 0;
        // For each weapon upgrades
        for (int i = 0; i < (int)GameUtils.EWeaponUpgrade.COUNT; ++i)
        {
            totalUpgradeLvl += weaponUpgrades[weaponID][(GameUtils.EWeaponUpgrade)i];
        }

        int weaponRank = Mathf.FloorToInt(totalUpgradeLvl / (int)GameUtils.EWeaponUpgrade.COUNT);

        // If there's a limit to apply
        if (weaponLvlLimit >= 0)
        {
            // Take the minimum between the limit and the current
            weaponRank = Mathf.Min(weaponRank, weaponLvlLimit);
        }      

        return weaponRank;
    }

    public int GetWeaponRankProgression(GameUtils.BaseID weaponID)
    {
        int totalUpgradeLvl = 0;
        // For each weapon upgrades
        for (int i = 0; i < (int)GameUtils.EWeaponUpgrade.COUNT; ++i)
        {
            totalUpgradeLvl += weaponUpgrades[weaponID][(GameUtils.EWeaponUpgrade)i];
        }

        int weaponRankProgression = Mathf.FloorToInt(totalUpgradeLvl % (int)GameUtils.EWeaponUpgrade.COUNT);
        return weaponRankProgression;
    }

    public Sprite GetWeaponRankIcon(GameUtils.BaseID weaponID, int weaponLvlLimit)
    {
        int weaponRankLevel = GetWeaponRankLevel(weaponID, weaponLvlLimit);
        
        Sprite weaponRankIcon = null;
        switch (weaponRankLevel)
        {
            case 0:
                // TODO :: Lv 0
                weaponRankIcon = (Sprite)Resources.Load("Images/gs_weaponLvl_1", typeof(Sprite));
                break;
            case 1:
                weaponRankIcon = (Sprite)Resources.Load("Images/gs_weaponLvl_1", typeof(Sprite));
                break;
            case 2:
                weaponRankIcon = (Sprite)Resources.Load("Images/gs_weaponLvl_2", typeof(Sprite));
                break;
            case 3:
                weaponRankIcon = (Sprite)Resources.Load("Images/gs_weaponLvl_3", typeof(Sprite));
                break;
            case 4:
                weaponRankIcon = (Sprite)Resources.Load("Images/gs_weaponLvl_4", typeof(Sprite));
                break;
            case 5:
                weaponRankIcon = (Sprite)Resources.Load("Images/gs_weaponLvl_5", typeof(Sprite));
                break;
            case 6:
                weaponRankIcon = (Sprite)Resources.Load("Images/gs_weaponLvl_6", typeof(Sprite));
                break;
            case 7:
                weaponRankIcon = (Sprite)Resources.Load("Images/gs_weaponLvl_7", typeof(Sprite));
                break;
            case 8:
                weaponRankIcon = (Sprite)Resources.Load("Images/gs_weaponLvl_8", typeof(Sprite));
                break;
            case 9:
                weaponRankIcon = (Sprite)Resources.Load("Images/gs_weaponLvl_9", typeof(Sprite));
                break;
            case 10:
                weaponRankIcon = (Sprite)Resources.Load("Images/gs_weaponLvl_10", typeof(Sprite));
                break;
        }

        return weaponRankIcon;
    }
    #endregion // Weapons

    public int GetAmountOfCardsPerLvl(int currentUpgradeLvl)
    {
        if (currentUpgradeLvl < GameUtils.MAX_UPGRADE_LEVEL)
        {
            return cardCostPerLvl[currentUpgradeLvl];
        }
        return 0;
    }

    public int GetCreditCostPerUpgradeLvl(int currentUpgradeLvl)
    {
        if (currentUpgradeLvl < GameUtils.MAX_UPGRADE_LEVEL)
        {
            return upgradeCostPerLvl[currentUpgradeLvl];
        }
        return 0;
    }

    public void ResetUpgrades()
    {
        ResetWeaponUpgrades();
    }
}

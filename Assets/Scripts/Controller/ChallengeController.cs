using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using System;
using System.IO;
using Newtonsoft.Json;
using UnityEngine.SceneManagement;
using System.Linq;
#if UNITY_ANDROID
using GooglePlayGames;
#endif

#pragma warning disable 0649

public class ChallengeController : UnitySingleton<ChallengeController>
{
    // IMPORTANT :: Should have been only the challengeID
    // and challengeDifficulty and it should be the parent class
    // for MissionJSON (campaign) and LastStandJSON, where
    // MissionJSON would have the number of stars.
    // This cannot be changed anymore since we're live!!!
    [Serializable]
    public class ChallengeJSON
    {
        public ChallengeJSON()
        {
            challengeID = string.Empty;
            challengeDifficulty = GameUtils.EGameDifficulty.Normal;
            stars = 0;
            animatedStars = 0;
        }

        public ChallengeJSON(ChallengeJSON copy)
        {
            challengeID = copy.challengeID;
            challengeDifficulty = copy.challengeDifficulty;
            stars = copy.stars;
            animatedStars = copy.animatedStars;
        }

        public string challengeID;
        public GameUtils.EGameDifficulty challengeDifficulty;
        public int stars = 0;
        public int animatedStars = 0;

        public bool ChallengeDataExists()
        {
            return Instance.FindChallengeData(challengeID) != null;
        }

        public string GetChallengeNameID()
        {
            return Instance.FindChallengeData(challengeID).challengeNameID;
        }

        public string GetChallengeNumber()
        {
            return Instance.FindChallengeData(challengeID).GetChallengeNumber(challengeDifficulty);
        }

        public int GetWeaponLvlLimit()
        {
            return Instance.FindChallengeData(challengeID).weaponLvlLimit;
        }

        public string GetSceneName()
        {
            return Instance.FindChallengeData(challengeID).sceneName;
        }

        public ChallengeData.EChallengeType GetChallengeType()
        {
            return Instance.FindChallengeData(challengeID).challengeType;
        }
    }

    [Serializable]
    public class LastStandJSON : ChallengeJSON
    {
        public LastStandJSON()
        {
            challengeID = string.Empty;
            challengeDifficulty = GameUtils.EGameDifficulty.Normal;
            stars = 0;
            animatedStars = 0;
            highestScore = 0;
            highestWave = 0;
        }

        public LastStandJSON(LastStandJSON copy)
        {
            challengeID = copy.challengeID;
            challengeDifficulty = copy.challengeDifficulty;
            stars = copy.stars;
            animatedStars = copy.animatedStars;
            highestScore = copy.highestScore;
            highestWave = copy.highestWave;
        }

        public int highestScore;
        public int highestWave;
    }

    [Serializable]
    public class OperationJSON : ChallengeJSON
    {
        public OperationJSON()
        {
            challengeID = string.Empty;
            challengeDifficulty = GameUtils.EGameDifficulty.Insane;
            stars = 0;
            animatedStars = 0;
        }
    }

    private static bool isInitialized = false;
    private static bool loaded = false;
    private static bool isSceneSetup = false;

    // List of challenges data coming from the resources folder
    private ChallengeData[] challengesData;

    // List of challenges data coming from the resources folder
    private ChallengeData[] lastStandsData;

    // List of challenges data coming from the resources folder
    private ChallengeData[] operationData;

    #region Exposed Values
    [Tooltip("Base gold reward for the one-star reward. (i.e. Lvl 1 = 4 gold)")]
    [SerializeField]
    private int oneStarBaseGoldReward = 4;

    [Tooltip("Lvl modulo where the gold reward will increase by 1 per this value. (i.e. Lvl 10 => 4 + (10 (Lvl) % 5 (this value) = 2) = 6 gold)")]
    [SerializeField]
    private int oneStarLvlModulo = 5;

    [Tooltip("Base card reward for the two-star reward (i.e. Lvl 1 = 1 card)")]
    [SerializeField]
    private int twoStarBaseCardReward = 1;

    [Tooltip("Lvl modulo where the card reward will increase by 1 per this value. (i.e. Lvl 25 => 1 + (25 (Lvl) % 15 (this value) = 1) = 2 cards)")]
    [SerializeField]
    private int twoStarLvlModulo = 15;

    [Tooltip("Base credit reward for the three-star reward (i.e. Normal = 10000 credits")]
    [SerializeField]
    private int threeStarBaseCreditReward = 10000;

    [Tooltip("Increment amount per difficulty for the three-star reward (i.e. Hard = 10000 + 5000 (this value) = 15000 credits")]
    [SerializeField]
    private int threeStarCreditIncrementPerDifficulty = 5000;

    [Tooltip("Gold reward for the one-star reward for the Normal Last Stand.")]
    [SerializeField]
    private int oneStarGoldRewardNormalLastStand = 100;

    [Tooltip("Gold reward for the one-star reward for the Hard Last Stand.")]
    [SerializeField]
    private int oneStarGoldRewardHardLastStand = 200;

    [Tooltip("Gold reward for the one-star reward for the Insane Last Stand.")]
    [SerializeField]
    private int oneStarGoldRewardInsaneLastStand = 300;

    [Tooltip("Card reward for the two-star reward for the Normal Last Stand.")]
    [SerializeField]
    private int twoStarCardRewardNormalLastStand = 20;

    [Tooltip("Card reward for the two-star reward for the Hard Last Stand.")]
    [SerializeField]
    private int twoStarCardRewardHardLastStand = 40;

    [Tooltip("Card reward for the two-star reward for the Insane Last Stand.")]
    [SerializeField]
    private int twoStarCardRewardInsaneLastStand = 60;

    [Tooltip("Credit reward for the three-star reward for the Normal Last Stand.")]
    [SerializeField]
    private int threeStarCreditRewardNormalLastStand = 100000;

    [Tooltip("Credit reward for the three-star reward for the Hard Last Stand.")]
    [SerializeField]
    private int threeStarCreditRewardHardLastStand = 200000;

    [Tooltip("Credit reward for the three-star reward for the Insane Last Stand.")]
    [SerializeField]
    private int threeStarCreditRewardInsaneLastStand = 300000;

    [Tooltip("Credit reward for skipping a wave in campaign at normal difficulty.")]
    [SerializeField]
    private int normalWaveCreditReward = 500;

    [Tooltip("Credit reward for skipping a wave in campaign at hard difficulty.")]
    [SerializeField]
    private int hardWaveCreditReward = 1000;

    [Tooltip("Credit reward for skipping a wave in campaign at insane difficulty.")]
    [SerializeField]
    private int insaneWaveCreditReward = 1500;

    [Tooltip("Credit reward for skipping a wave in last stand at normal difficulty.")]
    [SerializeField]
    private int normalWaveCreditRewardLastStand = 250;

    [Tooltip("Credit reward for skipping a wave in last stand at hard difficulty.")]
    [SerializeField]
    private int hardWaveCreditRewardLastStand = 500;

    [Tooltip("Credit reward for skipping a wave at in last stand insane difficulty.")]
    [SerializeField]
    private int insaneWaveCreditRewardLastStand = 750;

    [SerializeField]
    private int creditTutorialReward = 25000;

    [SerializeField]
    private int goldTutorialReward = 100;

    [SerializeField]
    private RewardData[] cardTutorialRewards;

    [SerializeField]
    private int normalGrindCardAmountReward = 1;

    [SerializeField]
    private int hardGrindCardAmountReward = 2;

    [SerializeField]
    private int insaneGrindCardAmountReward = 3;

    [Tooltip("Credit reward for each round completed in normal Last Stand.")]
    [SerializeField]
    private int normalLastStandBonusAmountReward = 10000;

    [Tooltip("Credit reward for each round completed in hard Last Stand.")]
    [SerializeField]
    private int hardLastStandBonusAmountReward = 15000;

    [Tooltip("Credit reward for each round completed in insane Last Stand.")]
    [SerializeField]
    private int insaneLastStandBonusAmountReward = 20000;

    [SerializeField]
    private int normalLastStandUnlockLvl = 15;

    [SerializeField]
    private int hardLastStandUnlockLvl = 37;

    [SerializeField]
    private int insaneLastStandUnlockLvl = 61;
    #endregion

    [Tooltip("List of campaign missions from JSON")]
    [SerializeField]
    private static List<ChallengeJSON> challengesJSON = new List<ChallengeJSON>();

    [Tooltip("List of last stands from JSON")]
    [SerializeField]
    private static List<LastStandJSON> lastStandsJSON = new List<LastStandJSON>();

    [Tooltip("List of operations from JSON")]
    [SerializeField]
    private static List<OperationJSON> operationJSON = new List<OperationJSON>();

    [HideInInspector]
    public List<ChallengeJSON> ChallengesJSON
    {
        get { return challengesJSON; }
        set { challengesJSON = value; }
    }

    [HideInInspector]
    public List<LastStandJSON> LastStandsJSON
    {
        get { return lastStandsJSON; }
        set { lastStandsJSON = value; }
    }

    [HideInInspector]
    public List<OperationJSON> OperationsJSON
    {
        get { return operationJSON; }
        set { operationJSON = value; }
    }

    private int currentLevel;
    private ChallengeJSON currentChallengeJSON;

    // UI elements
    private Button[] challengeButtons;
    private Button[] lastStandButtons;
    private ChallengeData.EChallengeType currentChallengeTab = ChallengeData.EChallengeType.Mission;
    private Button debugRemoveMissionLocksButton;
    private Button debugRedoTutorialButton;

    private Queue<GameObject> starAnimations;
    [SerializeField]
    private float starInitialDelay = 0.5f;
    [SerializeField]
    private float starAnimationTime = 0.5f;
    private float starAnimationDelay = 0f;

    private int grindCardAmountRolled = 0;

    public int CurrentLevel
    {
        get { return currentLevel; }
    }

    public void SetCurrentLevel(int levelIndex, ChallengeData.EChallengeType challengeType)
    {
        currentLevel = levelIndex;

        switch (challengeType)
        {
            case ChallengeData.EChallengeType.Mission:
                currentChallengeJSON = challengesJSON[currentLevel];
                break;
            case ChallengeData.EChallengeType.LastStand:
                currentChallengeJSON = lastStandsJSON[currentLevel];
                break;
            case ChallengeData.EChallengeType.Operation:
                currentChallengeJSON = operationJSON[currentLevel];
                break;
        }
    }

    public bool IsSceneSetup
    {
        get { return isSceneSetup; }
    }

    public ChallengeData.EChallengeType CurrentChallengeTab
    {
        get { return currentChallengeTab; }
        set { currentChallengeTab = value; }
    }

    public int GrindCardAmountRolled
    {
        get { return grindCardAmountRolled; }
    }

    public override void Awake()
    {
        base.Awake();
        if (!isInitialized)
        {
            // Always keep the controller loaded through the scene changes
            DontDestroyOnLoad(gameObject);
            isInitialized = true;

            RegisterDependency(LeaderboardController.Instance);
        }
        isSceneSetup = false;
        starAnimations = new Queue<GameObject>();
    }

    public override bool Update()
    {
        if (!base.Update()) return false;
        if (!CheckIfDataLoaded()) return false;
        if (!isSceneSetup) DoSceneSetup();

        StarAnimationUpdate();
        return true;
    }

    private void StarAnimationUpdate()
    {
        // speed up anims for inital stars
        while (starAnimations.Count > 3)
        {
            GameObject star = starAnimations.Dequeue();
            if (star != null)
            {
                Toggle toggle = star.GetComponent<Toggle>();
                if (toggle != null)
                {
                    toggle.isOn = true;
                }
            }
        }
        // normal progress
        if (starAnimations.Count > 0)
        {
            starAnimationDelay -= Time.deltaTime;
            if (starAnimationDelay <= 0)
            {
                starAnimationDelay = starAnimationTime;
                GameObject star = starAnimations.Dequeue();
                if (star != null)
                {
                    star.GetComponent<Toggle>().isOn = true;
                    Animator anim = star.GetComponentInChildren<Animator>();
                    if (anim != null)
                    {
                        anim.Play("New", 0, 0);
                    }
                }
            }
        }
    }

    private bool CheckIfDataLoaded()
    {
        if (DataController.Instance.ChallengeDataLoaded && !loaded)
        {
            loaded = true;
        }

        return loaded;
    }

    private void DoSceneSetup()
    {
        if (FlowStateController.Instance.IsInBattleScene())
        {
            if (currentChallengeJSON == null)
            {
                GetChallengeIDfromScene(SceneManager.GetActiveScene().name);
            }

            SetupMapBackground();
        }

        if (FlowStateController.Instance.IsInMenuScene())
        {
            Instance.SetupChallengeMenuUI();
        }

        isSceneSetup = true;
    }

    private void SetupMapBackground()
    {
        GameObject battleUI = GameObject.Find("BattleUICanvas");
        Image mapBackground = GameUtils.GetChildGameObject(battleUI, "MapBackground").GetComponent<Image>();
        mapBackground.sprite = GetCurrentChallengeData().challengeMap;
        mapBackground.color = Color.white;
    }

    private void ReplaceBossButton(ref Button missionButton, int index)
    {
        GameObject bossButton = (GameObject)Instantiate(Resources.Load("prefab_missionOnBoss"));
        // parent it
        bossButton.transform.SetParent(missionButton.transform.parent);
        // set position
        RectTransform bossRect = bossButton.GetComponent<RectTransform>();
        RectTransform missionRect = missionButton.GetComponent<RectTransform>();
        bossRect.offsetMax = missionRect.offsetMax;
        bossRect.offsetMin = missionRect.offsetMin;
        bossRect.anchoredPosition = missionRect.anchoredPosition;
        bossButton.transform.position = missionButton.transform.position;
        // Set the scale to one in order to ignore the parent's scale
        bossButton.transform.localScale = Vector3.one;
        // destroy mission Button
        missionButton.gameObject.SetActive(false);
        // set the new button
        missionButton = bossButton.GetComponent<Button>();
        // name it
        missionButton.name = "prefab_missionOnBoss(" + index + ")";
    }

    private void ShowStarAnimation(Button challengeButton, int animatedStars, int newStars)
    {
        GameObject star1 = GameUtils.GetChildGameObject(challengeButton.gameObject, "Star1", true);
        GameObject star2 = GameUtils.GetChildGameObject(challengeButton.gameObject, "Star2", true);
        GameObject star3 = GameUtils.GetChildGameObject(challengeButton.gameObject, "Star3", true);

        if (animatedStars < 1 && newStars > 0)
        {
            starAnimations.Enqueue(star1);
            //star1.Play("Normal", 0, 0);
        }
        if (animatedStars < 2 && newStars > 1)
        {
            starAnimations.Enqueue(star2);
            //2.Play("Normal", 0, 0);
        }
        if (animatedStars < 3 && newStars > 2)
        {
            starAnimations.Enqueue(star3);
            //star3.Play("Normal", 0, 0);
        }
    }

    public void SetupChallengeMenuUI()
    {
        // Note :: Needed since the challenge folder can be disabled on awake
        GameObject mainCanvas = GameObject.Find("MenuUICanvas");
        GameObject campaignFolder = GameUtils.GetChildGameObject(mainCanvas, "CampaignFolder", true);
        RectTransform contentRectTransform = GameUtils.GetChildGameObject(campaignFolder, "Content", true).GetComponent<RectTransform>();
        RectTransform viewportRectTransform = GameUtils.GetChildGameObject(campaignFolder, "Viewport", true).GetComponent<RectTransform>();

        bool lockNextChallenges = false;

        starAnimationDelay = starInitialDelay;

        challengeButtons = new Button[challengesJSON.Count];
        for (int i = 0; i < challengesJSON.Count; ++i)
        {
            if (i == 0) continue; // Skip tutorial

            bool isBoss = IsLevelBoss(challengesJSON[i].challengeID, challengesJSON[i].challengeDifficulty);

            challengeButtons[i] = GameUtils.GetChildGameObject(campaignFolder, "prefab_missionOn (" + i + ")", true).GetComponent<Button>();
            if (isBoss)
            {
                ReplaceBossButton(ref challengeButtons[i], i);
            }

            challengeButtons[i].GetComponentInChildren<LocalizedText>().LocalizedTextID = challengesJSON[i].GetChallengeNameID();
            GameUtils.GetChildGameObject(challengeButtons[i].gameObject, "TextLevelNum").GetComponent<Text>().text = challengesJSON[i].GetChallengeNumber();
            GameUtils.GetChildGameObject(challengeButtons[i].gameObject, "Star1").GetComponent<Toggle>().isOn = challengesJSON[i].stars > 0 && challengesJSON[i].animatedStars > 0;
            GameUtils.GetChildGameObject(challengeButtons[i].gameObject, "Star2").GetComponent<Toggle>().isOn = challengesJSON[i].stars > 1 && challengesJSON[i].animatedStars > 1;
            GameUtils.GetChildGameObject(challengeButtons[i].gameObject, "Star3").GetComponent<Toggle>().isOn = challengesJSON[i].stars > 2 && challengesJSON[i].animatedStars > 2;

            SetChallengeButtonActive(challengeButtons[i], !lockNextChallenges);
            bool isDaily = DailyController.Instance.IsDailyMission(i);
            if (!lockNextChallenges)
            {
                GameUtils.GetChildGameObject(challengeButtons[i].gameObject, "DailyMission", true).SetActive(isDaily);
            }

            if (challengesJSON[i].stars > 0)
            {
                // Try to unlock weapons & consumables
                // Note :: Always try to unlock weapons & consumables
                // each time we load the play tab menu in order to debug faster.
                WeaponController.Instance.UnlockWeaponByLevel(i);
                ConsumableController.Instance.UnlockConsumableByLevel(i);

                // If just completed some new star(s) 
                if (challengesJSON[i].animatedStars < challengesJSON[i].stars)
                {
                    // If at Sector 001 and never got any stars on it before
                    if (i == 1
                        && challengesJSON[i].animatedStars == 0)
                    {

                    }

                    // If at Sector 003 and never got any stars on it before
                    if (i == 3
                        && challengesJSON[i].animatedStars == 0)
                    {
                        // Show a one-time boss popup
                        MenuUIController.Instance.ShowBossInfoPopup(true);
                    }

                    ShowStarAnimation(challengeButtons[i], challengesJSON[i].animatedStars, challengesJSON[i].stars);
                    challengesJSON[i].animatedStars = challengesJSON[i].stars;
                    SaveChallenges(DataController.SAVEPRIORITY.NORMAL);
                }
            }

            // If the current challenge has no star, lock all the next challenges
            // except if the current level is a boss level, then unlock the next no matter what
            bool scrollToChallenge = false;
            if ((!lockNextChallenges && challengesJSON[i].stars == 0 && !isBoss))
            {
                lockNextChallenges = true;
                scrollToChallenge = true;
            }

            if ((!DailyController.Instance.IsDailyComplete() && isDaily) || (DailyController.Instance.IsDailyComplete() && scrollToChallenge))
            {
                ScrollToButtonPosition(challengeButtons[i], contentRectTransform, viewportRectTransform);
            }                                               

            int i2 = i;
            challengeButtons[i].onClick.AddListener(delegate { ChallengeButtonPressed(i2); });

            // TEMP :: Disable unused challenge buttons
            if ((i + 1) == challengesJSON.Count)
            {
                while (true)
                {
                    i++;
                    GameObject unusedChallengeButton = GameUtils.GetChildGameObject(campaignFolder, "prefab_missionOn (" + i + ")", true);
                    if (unusedChallengeButton)
                    {
                        unusedChallengeButton.SetActive(false);
                    }
                    else
                    {
                        break;
                    }
                }
            }
        }

        debugRemoveMissionLocksButton = GameUtils.GetChildGameObject(campaignFolder, "DebugRemoveMissionLocksButton", true).GetComponent<Button>();
        debugRedoTutorialButton = GameUtils.GetChildGameObject(campaignFolder, "DebugRedoTutorialButton", true).GetComponent<Button>();
#if UNITY_EDITOR || DEBUG_MOBILE
        debugRemoveMissionLocksButton.onClick.AddListener(delegate { DebugRemoveMissionLocksButtonClicked(); });
        debugRedoTutorialButton.onClick.AddListener(delegate { DebugRedoTutorialButtonClicked(); });
#else
        debugRemoveMissionLocksButton.gameObject.SetActive(false);
        debugRedoTutorialButton.gameObject.SetActive(false);
#endif

        GameObject lastStandFolder = GameUtils.GetChildGameObject(mainCanvas, "LastStandFolder", true);
        GameObject normalLastStandContainer = GameUtils.GetChildGameObject(lastStandFolder, "LastStandNormalContainer", true);
        GameObject hardLastStandContainer = GameUtils.GetChildGameObject(lastStandFolder, "LastStandHardContainer", true);
        GameObject insaneLastStandContainer = GameUtils.GetChildGameObject(lastStandFolder, "LastStandInsaneContainer", true);
        lastStandButtons = new Button[lastStandsJSON.Count];
        for (int i = 0; i < lastStandsJSON.Count; ++i)
        {
            GameObject currentLastStandContainer = null;
            int currentLastStandUnlockLvl = 0;

            // Get the right last stand container and parameters
            // based on the last stand difficulty
            switch (lastStandsJSON[i].challengeDifficulty)
            {
                case GameUtils.EGameDifficulty.Insane:
                    currentLastStandContainer = insaneLastStandContainer;
                    currentLastStandUnlockLvl = insaneLastStandUnlockLvl;
                    break;
                case GameUtils.EGameDifficulty.Hard:
                    currentLastStandContainer = hardLastStandContainer;
                    currentLastStandUnlockLvl = hardLastStandUnlockLvl;
                    break;
                case GameUtils.EGameDifficulty.Normal:
                default:
                    currentLastStandContainer = normalLastStandContainer;
                    currentLastStandUnlockLvl = normalLastStandUnlockLvl;
                    break;
            }

            // Populate the last stand button
            lastStandButtons[i] = GameUtils.GetChildGameObject(currentLastStandContainer, "prefab_missionOn", true).GetComponent<Button>();
            lastStandButtons[i].GetComponentInChildren<LocalizedText>().LocalizedTextID = lastStandsJSON[i].GetChallengeNameID();
            GameUtils.GetChildGameObject(lastStandButtons[i].gameObject, "TextLevelNum").GetComponent<Text>().text = lastStandsJSON[i].GetChallengeNumber();
            GameUtils.GetChildGameObject(lastStandButtons[i].gameObject, "Star1").GetComponent<Toggle>().isOn = lastStandsJSON[i].stars > 0 && lastStandsJSON[i].animatedStars > 0;
            GameUtils.GetChildGameObject(lastStandButtons[i].gameObject, "Star2").GetComponent<Toggle>().isOn = lastStandsJSON[i].stars > 1 && lastStandsJSON[i].animatedStars > 1;
            GameUtils.GetChildGameObject(lastStandButtons[i].gameObject, "Star3").GetComponent<Toggle>().isOn = lastStandsJSON[i].stars > 2 && lastStandsJSON[i].animatedStars > 2;

            // Manage lock based on campaign progression
            int campaignProgressionIndex = GetChallengeProgressionIndex();
            bool hasUnlockedCurrentLastStand = campaignProgressionIndex > currentLastStandUnlockLvl;
            SetChallengeButtonActive(lastStandButtons[i], hasUnlockedCurrentLastStand);
            GameObject unlockLvlTextGO = GameUtils.GetChildGameObject(currentLastStandContainer, "UnlockLvlText", true);
            unlockLvlTextGO.SetActive(!hasUnlockedCurrentLastStand);
            unlockLvlTextGO.GetComponent<LocalizedText>().LocalizedFullText = LocalizationManager.UNLOCK_LVL_TEXT + " " + ConvertSectorNumberToChallengeNumber(currentLastStandUnlockLvl);

            if (lastStandsJSON[i].stars > 0)
            {
                if (lastStandsJSON[i].animatedStars < lastStandsJSON[i].stars)
                {
                    ShowStarAnimation(lastStandButtons[i], lastStandsJSON[i].animatedStars, lastStandsJSON[i].stars);
                    lastStandsJSON[i].animatedStars = lastStandsJSON[i].stars;
                    SaveLastStands(DataController.SAVEPRIORITY.NORMAL);
                }
            }

            int i2 = i;
#if UNITY_IOS
            if (!Social.localUser.authenticated)
            {
                lastStandButtons[i].onClick.AddListener(delegate { MenuUIController.Instance.ShowLastStandInfoPopup(true, i2); });
            }
            else
            {
                lastStandButtons[i].onClick.AddListener(delegate { LastStandButtonClicked(i2); });
            }
#elif UNITY_ANDROID
            if (!PlayGamesPlatform.Instance.IsAuthenticated())
            {
                lastStandButtons[i].onClick.AddListener(delegate { MenuUIController.Instance.ShowLastStandInfoPopup(true, i2); });
            }
            else
            {
                lastStandButtons[i].onClick.AddListener(delegate { LastStandButtonClicked(i2); });
            }
#else
            lastStandButtons[i].onClick.AddListener(delegate { LastStandButtonClicked(i2); });
#endif

            // Leaderboard button
            Button leaderboard = GameUtils.GetChildGameObject(currentLastStandContainer, "LeaderboardButton", true).GetComponent<Button>();
            GameObject lockGO = GameUtils.GetChildGameObject(leaderboard.gameObject, "Lock", true);
#if ENABLE_LEADERBOARDS
            leaderboard.onClick.AddListener(delegate { LeaderboardButtonClicked(i2); });
            lockGO.SetActive(false);
#else
            leaderboard.interactable = false;
            lockGO.SetActive(true);
#endif

            // Highest score
            string leaderboardNameID = LeaderboardController.Instance.GetLeaderboardNameID(lastStandsJSON[i].challengeDifficulty);
            Text points = GameUtils.GetChildGameObject(currentLastStandContainer, "bestScoreNum", true).GetComponent<Text>();
            points.text = LeaderboardController.Instance.GetBestScore(leaderboardNameID).ToString();

            // Weapon Max Upgrade Lvl
            GameObject weaponMaxLvlGO = GameUtils.GetChildGameObject(currentLastStandContainer, "WeaponMaxLvlText", true);
            weaponMaxLvlGO.SetActive(hasUnlockedCurrentLastStand);
            weaponMaxLvlGO.GetComponent<LocalizedText>().LocalizedFullText = LocalizationManager.WEAPON_MAX_LVL_TEXT + " " + lastStandsJSON[i].GetWeaponLvlLimit();
        }
    }

    private void ScrollToButtonPosition(Button challengeButton, RectTransform contentRectTransform, RectTransform viewportRectTransform)
    {
        // scroll to this position
        float height = contentRectTransform.rect.height;
        Vector2 pos = contentRectTransform.offsetMax;
        RectTransform buttonRect = challengeButton.GetComponent<RectTransform>();
        pos.y = -buttonRect.offsetMax.y - (viewportRectTransform.rect.height / 2) + (buttonRect.rect.height / 2);
        contentRectTransform.offsetMax = pos;

        pos.y -= height - viewportRectTransform.rect.height;
        contentRectTransform.offsetMin = pos;
    }

    private void GetChallengeIDfromScene(string name)
    {
        string challengeID = String.Empty;

        // try normal challenges
        foreach (var challenge in challengesData)
        {
            if (string.Equals(challenge.sceneName, name))
            {
                challengeID = challenge.name;
                break;
            }
        }
        if (!string.IsNullOrEmpty(challengeID))
        {
            foreach (var json in challengesJSON)
            {
                if (string.Equals(json.challengeID, challengeID) && json.challengeDifficulty == GameUtils.EGameDifficulty.Normal)
                {
                    SetCurrentLevel(challengesJSON.IndexOf(json), ChallengeData.EChallengeType.Mission);
                    return;
                }
            }
        }
        // maybe a last stand
        foreach (var challenge in lastStandsData)
        {
            if (string.Equals(challenge.sceneName, name))
            {
                challengeID = challenge.name;
                break;
            }
        }
        if (!string.IsNullOrEmpty(challengeID))
        {
            foreach (var json in lastStandsJSON)
            {
                if (string.Equals(json.challengeID, challengeID))
                {
                    SetCurrentLevel(lastStandsJSON.IndexOf(json), ChallengeData.EChallengeType.LastStand);
                    return;
                }
            }
        }
        // maybe a operation
        foreach (var challenge in operationData)
        {
            if (string.Equals(challenge.sceneName, name))
            {
                challengeID = challenge.name;
                break;
            }
        }
        if (!string.IsNullOrEmpty(challengeID))
        {
            foreach (var json in operationJSON)
            {
                if (string.Equals(json.challengeID, challengeID))
                {
                    SetCurrentLevel(operationJSON.IndexOf(json), ChallengeData.EChallengeType.Operation);
                    return;
                }
            }
        }
    }

    private void SetChallengeButtonActive(Button challengeButton, bool isActive)
    {
        challengeButton.interactable = isActive;
        GameUtils.GetChildGameObject(challengeButton.gameObject, "MissionOffPanel", true).SetActive(!isActive);
        GameUtils.GetChildGameObject(challengeButton.gameObject, "MissionOnPanel", true).SetActive(isActive);
    }

    private void ChallengeButtonPressed(int challengeIndex)
    {
        SetCurrentLevel(challengeIndex, ChallengeData.EChallengeType.Mission);
        BattleState battleState = (BattleState)FlowStateController.Instance.GetState(FlowStateController.FlowStates.Battle);
        battleState.SetBattleScene(challengesJSON[challengeIndex].GetSceneName());
        FlowStateController.Instance.SetState(FlowStateController.FlowStates.Battle);
    }

    public void LastStandButtonClicked(int lastStandIndex)
    {
        SetCurrentLevel(lastStandIndex, ChallengeData.EChallengeType.LastStand);
        BattleState battleState = (BattleState)FlowStateController.Instance.GetState(FlowStateController.FlowStates.Battle);
        battleState.SetBattleScene(lastStandsJSON[lastStandIndex].GetSceneName());
        FlowStateController.Instance.SetState(FlowStateController.FlowStates.Battle);
    }

    private void OperationButtonClicked(int operationIndex)
    {
        SetCurrentLevel(operationIndex, ChallengeData.EChallengeType.Operation);
        BattleState battleState = (BattleState)FlowStateController.Instance.GetState(FlowStateController.FlowStates.Battle);
        battleState.SetBattleScene(operationJSON[operationIndex].GetSceneName());
        FlowStateController.Instance.SetState(FlowStateController.FlowStates.Battle);
    }

    private void LeaderboardButtonClicked(int leaderboardIndex)
    {
        //only enabled for real live games
#if UNITY_ANDROID && ENABLE_LEADERBOARDS
        switch (leaderboardIndex)
        {
            case 1: LeaderboardController.Instance.ShowLeaderboard(GPGSConstants.leaderboard_raid_board); break;
            case 2: LeaderboardController.Instance.ShowLeaderboard(GPGSConstants.leaderboard_onslaught_board); break;
            case 0:
            default: LeaderboardController.Instance.ShowLeaderboard(GPGSConstants.leaderboard_skirmish_board); break;
        }
#elif UNITY_IOS && ENABLE_LEADERBOARDS
        switch(leaderboardIndex)
        {
            case 1: LeaderboardController.Instance.ShowLeaderboard(GCConstants.leaderboard_raid_board); break;
            case 2: LeaderboardController.Instance.ShowLeaderboard(GCConstants.leaderboard_onslaught_board); break;
            case 0:
            default: LeaderboardController.Instance.ShowLeaderboard(GCConstants.leaderboard_skirmish_board); break;
        }
#endif
    }

    public void EnterTutorial()
    {
        ChallengeButtonPressed(0);
    }

#if UNITY_EDITOR || DEBUG_MOBILE
    private void DebugRemoveMissionLocksButtonClicked()
    {
        for (int i = 1; i < challengeButtons.Length; ++i)
        {
            SetChallengeButtonActive(challengeButtons[i], true);
        }
    }

    private void DebugRedoTutorialButtonClicked()
    {
        EnterTutorial();
    }
#endif

    private void UpdateMissionAchievements()
    {
        string achievementString = string.Empty;
        // Note :: Safeguard for the tutorial (aka 'currentLevel > 0')
        // since there's no achievement for the tutorial
        if (currentChallengeJSON.GetChallengeType() == ChallengeData.EChallengeType.Mission
            && currentLevel > 0
            && currentLevel <= GameUtils.NORMAL_LAST_MISSION_NUMBER)
        {
            achievementString = "Mission_" + currentLevel;
        }
        else if (currentChallengeJSON.GetChallengeType() == ChallengeData.EChallengeType.LastStand)
        {
            // Note :: Adding '+ 1' to the currentLevel because the naming start at 1 and not 0
            achievementString = "LastStand_" + (currentLevel + 1);
        }
        else if (currentChallengeJSON.GetChallengeType() == ChallengeData.EChallengeType.Operation)
        {
            // Note :: Adding '+ 1' to the currentLevel because the naming start at 1 and not 0
            achievementString = "Operation_" + (currentLevel + 1);
        }
        if (!string.IsNullOrEmpty(achievementString))
        {
            AchievementData.EAchievementType achievmentType = (AchievementData.EAchievementType)Enum.Parse(typeof(AchievementData.EAchievementType), achievementString);
            AchievementController.Instance.UpdateAchievementProgression(achievmentType, 1, AchievementData.EAchievementOverride.Replace);
        }
    }

    public void ClearedChallenge(int stars, out List<PrizeController.RewardRollInfo> challengeRewardsRollInfo)
    {
        // Note :: This list will be populate for the Open Prize anim.
        challengeRewardsRollInfo = new List<PrizeController.RewardRollInfo>();

        if (currentChallengeJSON.GetChallengeType() == ChallengeData.EChallengeType.Mission)
        {
            if (DailyController.Instance.CompleteDaily(CurrentLevel))
            {
                int dailyReward = DailyController.Instance.GetDailyMissionReward();
                WaveController.Instance.RecordCredits(dailyReward, GameUtils.CreditsSource.DailyMission);
                CurrencyController.Instance.ModifyCreditsAmount(dailyReward, GameUtils.CreditsSource.DailyMission, DataController.SAVEPRIORITY.QUEUE);
            }
        }

        // If it's the Mission 1 and did not have any stars completed before
        if (currentChallengeJSON.GetChallengeNameID() == "CHALLENGE_MISSION1_NAME"
            && currentChallengeJSON.challengeDifficulty == GameUtils.EGameDifficulty.Normal
            && currentChallengeJSON.stars == 0)
        {
            // Activate the persistent Tutorial Controller
            Instantiate(Resources.Load("PersistentTutorialController"));
        }

        if (stars > 2)
        {
            UpdateMissionAchievements();
        }

        // If got a new star highscore
        if (stars > currentChallengeJSON.stars)
        {
            List<RewardData> starRewards = new List<RewardData>();

            if (stars >= 1
                && currentChallengeJSON.stars < 1)
            {
                // Give 1-star reward (which is a gold reward)
                starRewards.Add(CreateOneStarGoldReward());
            }
            if (stars >= 2
                && currentChallengeJSON.stars < 2)
            {
                // Give 2-star reward (which is a card reward)
                starRewards.AddRange(CreateTwoStarCardReward());
            }
            if (stars >= 3
                && currentChallengeJSON.stars < 3)
            {
                // Give 3-star reward (which is a credits reward)
                starRewards.Add(CreateThreeStarCreditReward());
            }

            // Collect the rewards and store the roll info
            challengeRewardsRollInfo.AddRange(PackController.Instance.CollectChallengeRewards(starRewards, GameUtils.ECurrencyType.StarReward));

            // Update the stars in the challenge JSON
            currentChallengeJSON.stars = stars;

            SaveChallenges(DataController.SAVEPRIORITY.NORMAL);
        }

#if UNITY_ANDROID
        if (PlayGamesPlatform.Instance.IsAuthenticated())
        {
            if (currentLevel >= GameUtils.NORMAL_LAST_MISSION_NUMBER)
            {
                Social.ReportProgress(GPGSConstants.achievement_captain, 100.0f, (bool success) =>
                {
                    // handle success or failure
                    Debug.Log("Unlocked Captain Achievement");
                });
            }
        }
#elif UNITY_IOS
        if (Social.localUser.authenticated)
        {
            if (currentLevel >= GameUtils.NORMAL_LAST_MISSION_NUMBER)
            {
                Social.ReportProgress(GCConstants.achievement_captain, 100.0f, (bool success) =>
                {
                    // handle success or failure
                    Debug.Log("Unlocked Captain Achievement");
                });
            }
        }
#endif

        // Give match reward
        int machtCreditReward = GetMatchCreditReward();
        if (BoosterController.Instance.BoosterTypeActive(BoosterController.EBoosterPackType.Credits))
        {
            int bonus = Mathf.CeilToInt(machtCreditReward * BoosterController.Instance.matchRewardCredits) - machtCreditReward;
            WaveController.Instance.RecordCredits(bonus, GameUtils.CreditsSource.BoosterMatchReward);
            machtCreditReward += bonus;
        }
        CurrencyController.Instance.ModifyCreditsAmount(machtCreditReward, GameUtils.CreditsSource.MatchReward, DataController.SAVEPRIORITY.NORMAL);

        // Roll grind cards    
        List<RewardData> grindCardRewards = new List<RewardData>();
        grindCardRewards.AddRange(CreateGrindCardsReward());
        grindCardAmountRolled = grindCardRewards.Count;

        // Collect the rewards and store the roll info
        challengeRewardsRollInfo.AddRange(PackController.Instance.CollectChallengeRewards(grindCardRewards, GameUtils.ECurrencyType.GrindCardReward));

        // If Last Stand
        if (currentChallengeJSON.GetChallengeType() == ChallengeData.EChallengeType.LastStand)
        {
            // Give Last Stand bonus rewards
            CurrencyController.Instance.ModifyCreditsAmount(GetLastStandBonusRewardAmount(), GameUtils.CreditsSource.LastStandBonusReward, DataController.SAVEPRIORITY.NORMAL);

            // Post score in the leaderboard
            LeaderboardController.Instance.PostScoreToLeaderboard(GetCurrentChallengeDifficulty(), WaveController.Instance.CurrentScore);
        }
    }

    public int GetMatchCreditReward()
    {
        int creditPerWave = GetCreditPerWave(GetCurrentChallengeDifficulty());

        // Calculate the number of waves for each mode
        int numberOfWaves = 0;
        // If Last Stand
        if (GetCurrentChallenge().GetChallengeType() == ChallengeData.EChallengeType.LastStand)
        {
            // (Last Stand Round * Num Wave Total) + Current Wave
            numberOfWaves = (WaveController.Instance.CurrentLastStandRoundIndex * GetCurrentChallengeWavesData().Length) + WaveController.Instance.CurrentWaveIndex;
        }
        // If Mission
        else
        {
            // Num Wave Total
            numberOfWaves = GetCurrentChallengeWavesData().Length;
        }
        int matchReward = creditPerWave * numberOfWaves;

        return matchReward;
    }

    private RewardData CreateOneStarGoldReward()
    {
        // Give 1-star reward (which is a gold reward)
        RewardData oneStarReward = ScriptableObject.CreateInstance<RewardData>();
        oneStarReward.rewardType = RewardData.ERewardType.Gold;
        oneStarReward.currencyAmount = GetOneStarGoldRewardAmount();
        return oneStarReward;
    }

    public int GetOneStarGoldRewardAmount()
    {
        if (IsTutorial())
        {
            return goldTutorialReward;
        }

        if (currentChallengeJSON.GetChallengeType() == ChallengeData.EChallengeType.LastStand)
        {
            return GetOneStarRewardLastStand(GetCurrentChallengeDifficulty());
        }
        else
        {
            // i.e. Lv 71 = 71 / 5 = 14.2 = 14 + 4 = 18 gold
            int oneStarGoldRewardAmount = Mathf.FloorToInt(currentLevel / oneStarLvlModulo) + oneStarBaseGoldReward;
            if (IsCurrentLevelBoss())
            {
                oneStarGoldRewardAmount *= GameUtils.BOSS_STAR_REWARDS_MULTIPLIER;
            }
            return oneStarGoldRewardAmount;
        }
    }

    private RewardData[] CreateTwoStarCardReward()
    {
        if (IsTutorial())
        {
            return cardTutorialRewards;
        }

        // Give 2-star reward (which is a card reward)
        RewardData twoStarPackReward = ScriptableObject.CreateInstance<RewardData>();
        twoStarPackReward.rewardType = RewardData.ERewardType.Pack;
        twoStarPackReward.upgradeCardAmount = GetTwoStarCardRewardAmount();

        return new RewardData[1] { twoStarPackReward };
    }

    public int GetTwoStarCardRewardAmount()
    {
        if (IsTutorial())
        {
            // Give specific cards instead
            return 3;
        }

        if (currentChallengeJSON.GetChallengeType() == ChallengeData.EChallengeType.LastStand)
        {
            return GetTwoStarRewardLastStand(GetCurrentChallengeDifficulty());
        }
        else
        {
            // i.e. Lvl 71 = 71 / 15 = 4.7333 = 4 + 1 = 5 cards
            int cardRewardAmount = Mathf.FloorToInt(currentLevel / twoStarLvlModulo) + twoStarBaseCardReward;
            if (IsCurrentLevelBoss())
            {
                cardRewardAmount *= GameUtils.BOSS_STAR_REWARDS_MULTIPLIER;
            }
            return cardRewardAmount;
        }
    }

    private RewardData CreateThreeStarCreditReward()
    {
        // Give 3-star reward (which is a credits reward)
        RewardData threeStarReward = ScriptableObject.CreateInstance<RewardData>();
        threeStarReward.rewardType = RewardData.ERewardType.Credit;
        threeStarReward.currencyAmount = GetThreeStarCreditRewardAmount();
        return threeStarReward;
    }

    public int GetThreeStarCreditRewardAmount()
    {
        if (IsTutorial())
        {
            return creditTutorialReward;
        }

        if (currentChallengeJSON.GetChallengeType() == ChallengeData.EChallengeType.LastStand)
        {
            return GetThreeStarRewardLastStand(GetCurrentChallengeDifficulty());
        }
        else
        {
            int threeStarCreditRewardAmount = threeStarBaseCreditReward + (int)GetCurrentChallengeDifficulty() * threeStarCreditIncrementPerDifficulty;
            if (IsCurrentLevelBoss())
            {
                threeStarCreditRewardAmount *= GameUtils.BOSS_STAR_REWARDS_MULTIPLIER;
            }
            return threeStarCreditRewardAmount;
        }
    }

    private List<RewardData> CreateGrindCardsReward()
    {
        List<RewardData> grindCardRewards = new List<RewardData>();

        // For each grind card to roll
        int grindCardAmount = RollGrindCardRewardAmount();
        for (int i = 0; i < grindCardAmount; ++i)
        {
            ChallengeData.GrindCardRarity grindCardRolled = RollGrindCardRarity();

            RewardData grindCardReward = ScriptableObject.CreateInstance<RewardData>();
            grindCardReward.rewardType = RewardData.ERewardType.Pack;
            grindCardReward.upgradeCardAmount = 1;
            grindCardReward.specificWeapons = new GameUtils.BaseID[] { grindCardRolled.weaponID };
            grindCardReward.specificWeaponUpgrade = grindCardRolled.weaponUpgrade;

            grindCardRewards.Add(grindCardReward);
        }

        return grindCardRewards;
    }

    private ChallengeData.GrindCardRarity RollGrindCardRarity()
    {
        ChallengeData.GrindCardRarity[] grindCardsRarities = GetCurrentChallengeGrindCardRarities();
        ChallengeData.GrindCardRarity grindCardRarity = null;

        float randomRoll = UnityEngine.Random.Range(0f, 1f);
        float totalRarity = 0;
        for (int i = 0; i < grindCardsRarities.Length; ++i)
        {
            totalRarity += (grindCardsRarities[i].rarity / 100f);
            if (randomRoll < totalRarity)
            {
                grindCardRarity = grindCardsRarities[i];
                break;
            }
        }
        return grindCardRarity;
    }

    public bool IsTutorial()
    {
        return currentChallengeJSON.challengeID == "Challenge000_Tutorial";
    }

    public int RollGrindCardRewardAmount()
    {
        ChallengeData.GrindCardRarity[] grindCardRarities = GetCurrentChallengeGrindCardRarities();
        // If there are no grind cards available
        if (grindCardRarities == null
            || grindCardRarities.Length == 0)
            return 0;

        return UnityEngine.Random.Range(1, GetGrindCardMaxAmount());
    }

    public int GetGrindCardMaxAmount()
    {
        ChallengeData.GrindCardRarity[] grindCardRarities = GetCurrentChallengeGrindCardRarities();
        // If there are no grind cards available
        if (grindCardRarities == null
            || grindCardRarities.Length == 0)
            return 0;

        switch (GetCurrentChallengeDifficulty())
        {
            case GameUtils.EGameDifficulty.Insane:
                return insaneGrindCardAmountReward;
            case GameUtils.EGameDifficulty.Hard:
                return hardGrindCardAmountReward;
            case GameUtils.EGameDifficulty.Normal:
            default:
                return normalGrindCardAmountReward;
        }
    }

    public int GetLastStandBonusRewardAmount()
    {
        return WaveController.Instance.CurrentLastStandRoundIndex * GetLastStandBonusPerDifficulty();
    }

    private int GetLastStandBonusPerDifficulty()
    {
        switch (GetCurrentChallengeDifficulty())
        {
            case GameUtils.EGameDifficulty.Insane:
                return insaneLastStandBonusAmountReward;
            case GameUtils.EGameDifficulty.Hard:
                return hardLastStandBonusAmountReward;
            case GameUtils.EGameDifficulty.Normal:
            default:
                return normalLastStandBonusAmountReward;
        }
    }

    private int GetCreditPerWave(GameUtils.EGameDifficulty difficulty)
    {
        if (currentChallengeJSON.GetChallengeType() == ChallengeData.EChallengeType.LastStand)
        {
            switch (difficulty)
            {
                case GameUtils.EGameDifficulty.Insane:
                    return insaneWaveCreditRewardLastStand;
                case GameUtils.EGameDifficulty.Hard:
                    return hardWaveCreditRewardLastStand;
                case GameUtils.EGameDifficulty.Normal:
                case GameUtils.EGameDifficulty.Custom:
                default:
                    return normalWaveCreditRewardLastStand;
            }
        }
        else
        {
            switch (difficulty)
            {
                case GameUtils.EGameDifficulty.Insane:
                    return insaneWaveCreditReward;
                case GameUtils.EGameDifficulty.Hard:
                    return hardWaveCreditReward;
                case GameUtils.EGameDifficulty.Normal:
                case GameUtils.EGameDifficulty.Custom:
                default:
                    return normalWaveCreditReward;
            }
        }
    }

    private int GetOneStarRewardLastStand(GameUtils.EGameDifficulty difficulty)
    {
        switch (difficulty)
        {
            case GameUtils.EGameDifficulty.Normal:
                return oneStarGoldRewardNormalLastStand;
            case GameUtils.EGameDifficulty.Hard:
                return oneStarGoldRewardHardLastStand;
            case GameUtils.EGameDifficulty.Insane:
                return oneStarGoldRewardInsaneLastStand;
            case GameUtils.EGameDifficulty.Custom:
                break;
        }

        return oneStarGoldRewardNormalLastStand;
    }

    private int GetTwoStarRewardLastStand(GameUtils.EGameDifficulty difficulty)
    {
        switch (difficulty)
        {
            case GameUtils.EGameDifficulty.Normal:
                return twoStarCardRewardNormalLastStand;
            case GameUtils.EGameDifficulty.Hard:
                return twoStarCardRewardHardLastStand;
            case GameUtils.EGameDifficulty.Insane:
                return twoStarCardRewardInsaneLastStand;
            case GameUtils.EGameDifficulty.Custom:
                break;
        }

        return twoStarCardRewardNormalLastStand;
    }

    private int GetThreeStarRewardLastStand(GameUtils.EGameDifficulty difficulty)
    {
        switch (difficulty)
        {
            case GameUtils.EGameDifficulty.Normal:
                return threeStarCreditRewardNormalLastStand;
            case GameUtils.EGameDifficulty.Hard:
                return threeStarCreditRewardHardLastStand;
            case GameUtils.EGameDifficulty.Insane:
                return threeStarCreditRewardInsaneLastStand;
            case GameUtils.EGameDifficulty.Custom:
                break;
        }

        return threeStarCreditRewardNormalLastStand;
    }

    public string ConvertSectorNumberToChallengeNumber(int sectorNumber)
    {
        int bossLvlAmountBefore = Mathf.FloorToInt(sectorNumber / GameUtils.BOSS_LVL_AT_EACH);
        int challengeNumber = sectorNumber - bossLvlAmountBefore + 1; // + 1 because you unlock after clearing this level
        return challengeNumber.ToString("000");
    }

#region Data Handling
    private void PopulateChallengesData()
    {
        challengesData = Resources.LoadAll<ChallengeData>("Challenges/Campaign");
    }

    private void PopulateLastStandsData()
    {
        lastStandsData = Resources.LoadAll<ChallengeData>("Challenges/LastStand");
    }

    private void PopulateOperationsData()
    {
        operationData = Resources.LoadAll<ChallengeData>("Challenges/Operation");
    }

    public ChallengeJSON GetChallengeAt(int index)
    {
        return challengesJSON[index];
    }

    public ChallengeJSON GetCurrentChallenge()
    {
        return currentChallengeJSON;
    }

    public ChallengeData GetCurrentChallengeData()
    {
        return FindChallengeData(currentChallengeJSON.challengeID);
    }

    public GameUtils.EGameDifficulty GetCurrentChallengeDifficulty()
    {
        return currentChallengeJSON.challengeDifficulty;
    }

    public float GetCurrentChallengeTimeBetweenWaves()
    {
        return FindChallengeData(currentChallengeJSON.challengeID).timeBetweenWaves;
    }

    public ChallengeData.WaveData2[] GetCurrentChallengeWavesData()
    {
        return FindChallengeData(currentChallengeJSON.challengeID).GetWavesData(GetCurrentChallengeDifficulty());
    }

    public ChallengeData.WaveData2 GetCurrentWaveData()
    {
        return GetCurrentChallengeWavesData()[WaveController.Instance.CurrentWaveIndex];
    }

    public ChallengeData.GrindCardRarity[] GetCurrentChallengeGrindCardRarities()
    {
        return FindChallengeData(currentChallengeJSON.challengeID).GetGrindCardRarities(GetCurrentChallengeDifficulty());
    }

    private ChallengeData FindChallengeData(string challengeID)
    {
        // Search through the campaign missions
        for (int i = 0; i < challengesData.Length; ++i)
        {
            // Note :: ChallengeID is the name of the challenge data file
            if (challengesData[i].name == challengeID)
            {
                return challengesData[i];
            }
        }

        // Search through the last stands
        for (int i = 0; i < lastStandsData.Length; ++i)
        {
            // Note :: ChallengeID is the name of the challenge data file
            if (lastStandsData[i].name == challengeID)
            {
                return lastStandsData[i];
            }
        }

        // Search through the operations
        for (int i = 0; i < operationData.Length; ++i)
        {
            // Note :: ChallengeID is the name of the challenge data file
            if (operationData[i].name == challengeID)
            {
                return operationData[i];
            }
        }

        Debug.LogWarning("Could not find challenge data with ID : " + challengeID);
        return null;
    }

    public int GetChallengeProgressionIndex()
    {
        int lastChallengeClearedIndex;
        for (lastChallengeClearedIndex = challengesJSON.Count - 1; lastChallengeClearedIndex >= 0; --lastChallengeClearedIndex)
        {
            if (challengesJSON[lastChallengeClearedIndex].stars > 0)
            {
                break;
            }
        }

        return lastChallengeClearedIndex + 1;
    }

    private bool IsLevelBoss(string challengeID, GameUtils.EGameDifficulty challengeDifficulty)
    {
        ChallengeData data = FindChallengeData(challengeID);
        ChallengeData.WaveData2[] waveData = data.GetWavesData(challengeDifficulty);
        for (int i = 0; i < waveData.Length; ++i)
        {
            if (IsWaveBoss(waveData[i]))
            {
                return true;
            }
        }
        return false;
    }

    public bool IsCurrentLevelBoss()
    {
        ChallengeData.WaveData2[] waveData = GetCurrentChallengeWavesData();
        for (int i = 0; i < waveData.Length; ++i)
        {
            if (IsWaveBoss(waveData[i]))
            {
                return true;
            }
        }
        return false;
    }

    public bool IsWaveBoss(ChallengeData.WaveData2 waveData)
    {
        ChallengeData.EnemyInfo[] enemyInfo = waveData.enemyInfo;
        for (int j = 0; j < enemyInfo.Length; ++j)
        {
            if (GameUtils.IsBossUnit(enemyInfo[j].type))
            {
                return true;
            }
        }
        return false;
    }

    private bool FindChallengeJSON(string challengeID)
    {
        for (int i = 0; i < challengesJSON.Count; ++i)
        {
            // Note :: ChallengeID is the name of the challenge data file
            if (challengesJSON[i].challengeID == challengeID)
            {
                return true;
            }
        }
        return false;
    }

    private bool FindLastStandJSON(string challengeID)
    {
        for (int i = 0; i < lastStandsJSON.Count; ++i)
        {
            // Note :: ChallengeID is the name of the challenge data file
            if (lastStandsJSON[i].challengeID == challengeID)
            {
                return true;
            }
        }
        return false;
    }

    private bool FindOperationJSON(string challengeID)
    {
        for (int i = 0; i < operationJSON.Count; ++i)
        {
            // Note :: ChallengeID is the name of the challenge data file
            if (operationJSON[i].challengeID == challengeID)
            {
                return true;
            }
        }
        return false;
    }

    public void SaveChallenges(DataController.SAVEPRIORITY priority)
    {
        DataController.Instance.SaveChallengeData(priority);
    }

    public void SaveLastStands(DataController.SAVEPRIORITY priority)
    {
        DataController.Instance.SaveLastStandData(priority);
    }

    public void SaveOperations(DataController.SAVEPRIORITY priority)
    {
        DataController.Instance.SaveOperationData(priority);
    }

    public void LoadChallenges(List<ChallengeJSON> remoteLoad)
    {
        PopulateChallengesData();

        if (remoteLoad == null)
        {
            // Note :: Using a temp list of challenges to see if there's JSON data.
            // If not, try to save the current data (if existing) from the Challenge controller.
            List<ChallengeJSON> jsonChallenges = new List<ChallengeJSON>();
            System.Exception ex = JsonUtils<List<ChallengeJSON>>.Load(GameUtils.CHALLENGES_JSON, out jsonChallenges, GameUtils.ELoadType.DynamicData);
            if (ex != null
                && ex.GetType() == typeof(FileLoadException))
            {
                ResetChallenges();
            }
            else
            {
                // WARNING :: Not supporting mismatch!
                // Load the challenge JSON data to the Challenge controller
                challengesJSON = jsonChallenges;
            }
        }
        else
        {
            challengesJSON = remoteLoad;
        }

        CleanChallengeJSON();
    }

    public void LoadLastStands(List<LastStandJSON> remoteLoad)
    {
        PopulateLastStandsData();

        if (remoteLoad == null)
        {
            // Note :: Using a temp list of challenges to see if there's JSON data.
            // If not, try to save the current data (if existing) from the Challenge controller.
            List<LastStandJSON> jsonLastStands = new List<LastStandJSON>();
            System.Exception ex = JsonUtils<List<LastStandJSON>>.Load(GameUtils.LAST_STANDS_JSON, out jsonLastStands, GameUtils.ELoadType.DynamicData);
            if ((ex != null && ex.GetType() == typeof(FileLoadException)))
            {
                ResetLastStands();
            }
            else
            {
                // WARNING :: Not supporting mismatch!
                // Load the last stand JSON data to the Challenge controller
                lastStandsJSON = jsonLastStands;
            }
        }
        else
        {
            lastStandsJSON = remoteLoad;
        }

        CleanLastStandJSON();
    }

    public void LoadOperations(List<OperationJSON> remoteLoad)
    {
        PopulateOperationsData();

        if (remoteLoad == null)
        {
            // Note :: Using a temp list of challenges to see if there's JSON data.
            // If not, try to save the current data (if existing) from the Challenge controller.
            List<OperationJSON> operations = new List<OperationJSON>();
            System.Exception ex = JsonUtils<List<OperationJSON>>.Load(GameUtils.OPERATIONS_JSON, out operations, GameUtils.ELoadType.DynamicData);
            if ((ex != null && ex.GetType() == typeof(FileLoadException)))
            {
                ResetOperations();
            }
            else
            {
                // WARNING :: Not supporting mismatch!
                // Load the last stand JSON data to the Challenge controller
                operationJSON = operations;
            }
        }
        else
        {
            operationJSON = remoteLoad;
        }

        CleanOperationJSON();
    }

    private void CleanChallengeJSON()
    {
        bool dirty = false;
        // remove JSON that don't exist
        for (int i = challengesJSON.Count - 1; i >= 0; --i)
        {
            if (!challengesJSON[i].ChallengeDataExists())
            {
                Debug.Log("Removing challenge: " + challengesJSON[i].challengeID);
                challengesJSON.RemoveAt(i);
                dirty = true;
            }
        }
        // add JSON that are missing
        for (int i = 0; i < challengesData.Length; ++i)
        {
            if (!FindChallengeJSON(challengesData[i].name))
            {
                challengesJSON.Add(NewChallengeJSON(challengesData[i], GameUtils.EGameDifficulty.Normal));
                challengesJSON.Add(NewChallengeJSON(challengesData[i], GameUtils.EGameDifficulty.Hard));
                challengesJSON.Add(NewChallengeJSON(challengesData[i], GameUtils.EGameDifficulty.Insane));
                dirty = true;
            }
        }

        if (dirty)
        {
            DataController.Instance.SaveChallengeData(DataController.SAVEPRIORITY.NORMAL);
        }
    }

    private void CleanLastStandJSON()
    {
        bool dirty = false;
        // remove JSON that don't exist
        for (int i = lastStandsJSON.Count - 1; i >= 0; --i)
        {
            if (!lastStandsJSON[i].ChallengeDataExists())
            {
                Debug.Log("Removing last stand: " + lastStandsJSON[i].challengeID);
                lastStandsJSON.RemoveAt(i);
                dirty = true;
            }
        }
        // add JSON that are missing
        for (int i = 0; i < lastStandsData.Length; ++i)
        {
            if (!FindLastStandJSON(lastStandsData[i].name))
            {
                lastStandsJSON.Add(NewLastStandJSON(lastStandsData[i]));
                dirty = true;
            }
        }

        if (dirty)
        {
            DataController.Instance.SaveLastStandData(DataController.SAVEPRIORITY.NORMAL);
        }
    }

    private void CleanOperationJSON()
    {
        bool dirty = false;
        // remove JSON that don't exist
        for (int i = operationJSON.Count - 1; i >= 0; --i)
        {
            if (!operationJSON[i].ChallengeDataExists())
            {
                Debug.Log("Removing operation: " + operationJSON[i].challengeID);
                operationJSON.RemoveAt(i);
                dirty = true;
            }
        }
        // add JSON that are missing
        for (int i = 0; i < operationData.Length; ++i)
        {
            if (!FindOperationJSON(operationData[i].name))
            {
                operationJSON.Add(NewOperationJSON(operationData[i]));
                dirty = true;
            }
        }

        if (dirty)
        {
            DataController.Instance.SaveOperationData(DataController.SAVEPRIORITY.NORMAL);
        }
    }

    private ChallengeJSON NewChallengeJSON(ChallengeData challengeData, GameUtils.EGameDifficulty difficulty)
    {
        ChallengeJSON newChallenge = new ChallengeJSON();
        newChallenge.challengeID = challengeData.name;
        newChallenge.challengeDifficulty = difficulty;
        newChallenge.stars = 0;
        return newChallenge;
    }

    public void ResetChallenges()
    {
        if (challengesJSON == null)
        {
            challengesJSON = new List<ChallengeJSON>();
        }
        challengesJSON.Clear();

        // Create a challenge per game difficulty
        for (int j = 0; j < (int)GameUtils.EGameDifficulty.Custom; ++j)
        {
            // For each challenge
            for (int i = 0; i < challengesData.Length; ++i)
            {
                // Only create one tutorial challenge on normal
                if (challengesData[i].name == "Challenge000_Tutorial" && j > 0) continue;

                challengesJSON.Add(NewChallengeJSON(challengesData[i], (GameUtils.EGameDifficulty)j ));
            }
        }

        SaveChallenges(DataController.SAVEPRIORITY.QUEUE);
    }

    private LastStandJSON NewLastStandJSON(ChallengeData lastStandData)
    {
        LastStandJSON newLastStand = new LastStandJSON();
        newLastStand.challengeID = lastStandData.name;

        // Choose the difficulty based on the number of waves
        // Note :: Each last stand data only has one difficulty
        if (lastStandData.normalWaves.Length > 0)
        {
            newLastStand.challengeDifficulty = GameUtils.EGameDifficulty.Normal;
        }
        else if (lastStandData.hardWaves.Length > 0)
        {
            newLastStand.challengeDifficulty = GameUtils.EGameDifficulty.Hard;
        }
        else if (lastStandData.insaneWaves.Length > 0)
        {
            newLastStand.challengeDifficulty = GameUtils.EGameDifficulty.Insane;
        }
        newLastStand.highestScore = 0;
        newLastStand.highestWave = 0;
        return newLastStand;
    }

    public void ResetLastStands()
    {
        if (lastStandsJSON == null)
        {
            lastStandsJSON = new List<LastStandJSON>();
        }
        lastStandsJSON.Clear();

        // For each last stand
        for (int i = 0; i < lastStandsData.Length; ++i)
        {
            lastStandsJSON.Add(NewLastStandJSON(lastStandsData[i]));
        }

        SaveLastStands(DataController.SAVEPRIORITY.QUEUE);
    }

    private OperationJSON NewOperationJSON(ChallengeData operationData)
    {
        OperationJSON newOperation = new OperationJSON();
        newOperation.challengeID = operationData.name;
        // operations are always difficulty Insane
        newOperation.challengeDifficulty = GameUtils.EGameDifficulty.Insane;
        return newOperation;
    }

    public void ResetOperations()
    {
        if (operationJSON == null)
        {
            operationJSON = new List<OperationJSON>();
        }
        operationJSON.Clear();
        // For each operation
        for (int i = 0; i < operationData.Length; ++i)
        {     
            operationJSON.Add(NewOperationJSON(operationData[i]));
        }

        SaveOperations(DataController.SAVEPRIORITY.QUEUE);
    }
    #endregion
}

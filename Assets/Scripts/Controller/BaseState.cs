public class BaseState
{
    protected bool inEnterTransition;

    public BaseState()
    {
        inEnterTransition = true;
    }

    public bool InEnterTransition
    {
        get { return inEnterTransition; }
        set { inEnterTransition = value; }
    }

    public virtual void EnterTransition()
    {
        inEnterTransition = false;
    }

    public virtual void EnterState()
    {
        inEnterTransition = true;
    }

    public virtual void UpdateState()
    {

    }

    public virtual void ExitState()
    {

    }

    public virtual int GetStateType()
    {
        return -1;
    }

}
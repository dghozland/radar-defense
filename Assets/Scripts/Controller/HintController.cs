﻿using UnityEngine;
using System.Collections.Generic;

public class HintController : UnitySingleton<HintController>
{
    private static List<string> hints = new List<string>();
    private static bool isInitialized = false;

    public override void Awake()
    {
        base.Awake();
        if (!isInitialized)
        {
            isInitialized = true;
            DontDestroyOnLoad(gameObject);
        }
    }

    public void RegisterHint(string hint)
    {
        if (!hints.Contains(hint))
        {
            hints.Add(hint);
        }
    }

    public string GetHint()
    {
        int hintId = Random.Range(0, hints.Count);
        return hints[hintId];
    }
}

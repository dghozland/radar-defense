using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FPSController : UnitySingleton<FPSController>
{
    private static bool isInitialized = false;

#if !RELEASE_LIVE
    private readonly string FPS_TEXT = "FPS : ";
    private int currentCount;
    private int newCount;
    private float lastTime;
    private int interval;
    private float updateHighLow;
    private int low;
    private int high;
#endif
    // UI elements
    private Text textField;

    public override void Awake()
    {
        base.Awake();
        if (!isInitialized)
        {
            // Always keep the controller loaded through the scene changes
            DontDestroyOnLoad(gameObject);
            isInitialized = true;
            InitFPSCounter();
#if RELEASE_LIVE
            textField.enabled = false;
            gameObject.SetActive(false);
#endif
        }
        ResetValues();
    }

    private void ResetValues()
    {
#if !RELEASE_LIVE
        currentCount = 0;
        newCount = 0;
        interval = 5;
        lastTime = Time.time;
        low = 100;
        high = 0;
        updateHighLow = 5f;
#endif
    }

    private void InitFPSCounter()
    {
        Canvas cnvFPSUI = Instantiate((Canvas)Resources.Load("cnvFPSUI", typeof(Canvas)));
        textField = cnvFPSUI.gameObject.GetComponentInChildren<Text>();
        DontDestroyOnLoad(cnvFPSUI);
    }

#if !RELEASE_LIVE
    public override bool Update()
    {
        updateHighLow -= Time.deltaTime;
        if (lastTime + (1f / interval) < Time.time)
        {
            lastTime = Time.time;
            currentCount = ((currentCount + (newCount * interval)) / 2);
            newCount = 0;
            if (currentCount > high)
            {
                high = currentCount;
            }
            if (currentCount < low)
            {
                low = currentCount;
            }
        }
        if (updateHighLow < 0)
        {
            updateHighLow = 5;
            high -= 5;
            low += 5;
        }
        newCount++;
        return true;
    }

    public override bool FixedUpdate()
    {
        textField.text = FPS_TEXT + currentCount.ToString() + "|H:" + high.ToString() + "|L:" + low + " " + Screen.currentResolution.width + ":" + Screen.currentResolution.height;
        return true;
    }
#endif
}

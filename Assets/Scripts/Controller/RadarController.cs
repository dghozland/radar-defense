using UnityEngine;
using System.Collections;
using UnityEngine.UI;

#pragma warning disable 0649

public class RadarController : UnitySingleton<RadarController>
{
    private GameObject radarScan;
    private GameObject radarScanMask;
    private Image radarScanImage;

    private float startX;
    private float endX;
    private float speed;
    private float radarWaitTimer = 0f;
    private float initialRadarWaitTimer = 0f;
    private bool init = false;
    private bool jammed = false;
    private Text radarScanTimer;
    private RectTransform radarScanRect;

    [SerializeField]
    private float scanWaitNormal = 2f;

    [SerializeField]
    private float scanWaitHard = 3f;

    [SerializeField]
    private float scanWaitInsane = 5f;

    [SerializeField]
    private float scanWaitCustom = 1f;

    [SerializeField]
    private float revealTimerNormal = 5f;

    [SerializeField]
    private float revealTimerHard = 5f;

    [SerializeField]
    private float revealTimerInsane = 5f;

    [SerializeField]
    private float revealTimerCustom = 5f;

    [SerializeField]
    private float timeToScan = 7f;

    [SerializeField]
    private float initialScanWait = 0.5f;

    [SerializeField]
    private float RadarBoostMultiplier = 10f;

    [SerializeField]
    private float RadarDetectedSoundDelay = 0.5f;

    [SerializeField]
    private Sprite normalScanImage;

    [SerializeField]
    private Sprite jammedScanImage;

    private float timeBetweenScans;
    private bool isRadarWaitTimePaused = false;
    private float detectSoundWaitTimer;

    public bool IsRadarWaitTimePaused
    {
        get { return isRadarWaitTimePaused; }
        set { isRadarWaitTimePaused = value; }
    }

    public override void Awake()
    {
        base.Awake();
        radarScan = GameObject.Find("RadarScan");
        radarScanMask = GameObject.Find("RadarScanMask");
        radarScanImage = GameUtils.GetChildGameObject(radarScan, "in").GetComponent<Image>();
        Image[] images = radarScan.GetComponentsInChildren<Image>();
        GameObject radarScanTimerGO = GameObject.Find("RadarScanTimer");
        radarScanTimer = radarScanTimerGO.GetComponentInChildren<Text>();
        radarScanTimer.text = 0.ToString("F1");
        foreach (var image in images)
        {
            image.enabled = false;
        }
        radarScan.SetActive(false);
        RegisterDependency(WaveController.Instance);
        RegisterDependency(ConsumableController.Instance);
        RegisterDependency(ChallengeController.Instance);
        RegisterDependency(WeaponController.Instance);
    }

    public override bool Update()
    {
        if (!base.Update()) return false;
        if (WaveController.Instance.Paused) return false;
        if (!WeaponController.Instance.WeaponsPlaced()) return false;
        if (!init)
        {
            Init();
        }
        else
        {
            UpdateSpeed();
            UpdateSwipe();
            UpdateDetectSound();
            UpdateScanImage();
        }
        return true;
    }

    private void Init()
    {
        if (PlayfieldController.Instance.Initialized)
        {
            radarScanRect = radarScan.GetComponent<RectTransform>();
            RectTransform radarScanMaskRect = radarScanMask.GetComponent<RectTransform>();

            float scale = PlayfieldController.Instance.WidthScale;

            startX = radarScanRect.position.x - (radarScanMaskRect.rect.width * scale / 2) - (radarScanRect.rect.width * scale / 2);
            endX = radarScanRect.position.x + (radarScanMaskRect.rect.width * scale / 2) + (radarScanRect.rect.width * scale / 2);

            radarScanRect.position = new Vector3(startX, radarScanRect.position.y);

            timeBetweenScans = GetScanWaitTime();

            init = true;

            radarWaitTimer = 0f;
            detectSoundWaitTimer = 0;
            initialRadarWaitTimer = initialScanWait;
            radarScan.SetActive(true);
            Image[] images = radarScan.GetComponentsInChildren<Image>();
            foreach (var image in images)
            {
                image.enabled = true;
            }
        }
    }
    
    private void UpdateSpeed()
    {
        speed = (endX - startX) / timeToScan;
    }

    private void UpdateSwipe()
    {
        Vector3 position = radarScanRect.position;

        // initial wait
        if (initialRadarWaitTimer > 0f)
        {
            // Don't update the radar wait time
            if (isRadarWaitTimePaused)
                return;

            initialRadarWaitTimer -= Time.deltaTime;
            radarScanTimer.text = 0.ToString("F1");
            return;
        }

        // Radar is in wait state
        if (radarWaitTimer > 0f)
        {
            // Don't update the radar wait time
            if (isRadarWaitTimePaused) 
                return;

            float deltaTime = Time.deltaTime;
            // Boost - Radar Frequency
            bool isRadarFrequencyBoosted = BoosterController.Instance.CurrentBattleBoost == BoosterController.EBoostType.RadarFrequency;
            deltaTime *= isRadarFrequencyBoosted ? RadarBoostMultiplier : 1f;

            radarWaitTimer -= deltaTime;

            radarScanTimer.text = radarWaitTimer.ToString("F1");
        }
        else
        {
            radarScanTimer.text = 0.ToString("F1");

            // Radar will start swiping
            if ((position.x - startX) < 0.1f)
            {
                // Wait for the pulse effect before swiping
                if (ConsumableController.Instance.IsInPulse()) return;
            }

            // Radar is swiping
            if (position.x <= endX)
            {
                position.x += speed * Time.deltaTime;
                radarScanRect.position = position;
            }
            // Radar swipe ended
            else
            {
                radarWaitTimer = timeBetweenScans;
                position.x = startX;
                radarScanRect.position = position;
            }
        }
    }

    private void UpdateDetectSound()
    {
        if (detectSoundWaitTimer > 0)
        {
            detectSoundWaitTimer -= Time.deltaTime;
        }
    }

    private void UpdateScanImage()
    {
        if (EnemyController.Instance.IsSpecialActive(GameUtils.EBossSpecial.JamRadar))
        {
            if (!jammed)
            {
                radarScanImage.sprite = jammedScanImage;
                jammed = true;
            }
        }
        else
        {
            if (jammed)
            {
                radarScanImage.sprite = normalScanImage;
                jammed = false;
            }
        }
    }

    public void PerformScanIn(float scanDelay)
    {
        radarWaitTimer = scanDelay;
    }

    private float GetScanWaitTime()
    {
        switch (ChallengeController.Instance.GetCurrentChallengeDifficulty())
        {
            case GameUtils.EGameDifficulty.Hard:
                return scanWaitHard;
            case GameUtils.EGameDifficulty.Insane:
                return scanWaitInsane;
            case GameUtils.EGameDifficulty.Custom:
                return scanWaitCustom;
            case GameUtils.EGameDifficulty.Normal:
            default:
                return scanWaitNormal;
        }
    }

    public float GetRevealTimer()
    {
        switch (ChallengeController.Instance.GetCurrentChallengeDifficulty())
        {
            case GameUtils.EGameDifficulty.Hard:
                return revealTimerHard;
            case GameUtils.EGameDifficulty.Insane:
                return revealTimerInsane;
            case GameUtils.EGameDifficulty.Custom:
                return revealTimerCustom;
            case GameUtils.EGameDifficulty.Normal:
            default:
                return revealTimerNormal;
        }
    }

    public void PlayDetectedSound()
    {
        if (detectSoundWaitTimer <= 0)
        {
            detectSoundWaitTimer = RadarDetectedSoundDelay;
            // Sound hook - Radar scan sound
            AudioManager.Instance.PlaySound(AudioManager.ESoundType.RadarScan);
        }
    }

    public void PlayJammedDetectedSound()
    {
        if (detectSoundWaitTimer <= 0)
        {
            detectSoundWaitTimer = RadarDetectedSoundDelay;
            // Sound hook - Radar scan sound
            AudioManager.Instance.PlaySound(AudioManager.ESoundType.RadarScanJammed);
        }
    }
}

using UnityEngine;
using System.Collections.Generic;
using System;
using UnityEngine.SceneManagement;

#pragma warning disable 0649

public class PathFindingController : UnitySingleton<PathFindingController>
{
    [SerializeField]
    private GameUtils.LineParameters lineParameters;
    private List<PathNode> pathNodes;
    private int pathNodeCount;
    private string mapName;
    private bool init;

    private PathFindingController()
    {
        pathNodes = new List<PathNode>();
        init = false;
    }

    public override void Awake()
    {
        base.Awake();

        if (lineParameters.lineMaterial == null)
        {
            lineParameters.lineMaterial = new Material(Shader.Find("Sprites/Default"));
        }

        mapName = SceneManager.GetActiveScene().name;
        pathNodeCount = GameObject.FindObjectsOfType<PathNode>().Length;
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    public void RegisterNode(PathNode node)
    {
        if (!pathNodes.Contains(node))
        {
            pathNodes.Add(node);
        }
    }

    public override bool Update()
    {
        if (!base.Update()) return false;

        if (!init)
        {
            // wait until all nodes registered
            if (pathNodes.Count == pathNodeCount)
            {
                // wait until their position has been scaled
                foreach (PathNode node in pathNodes)
                {
                    if (!node.IsScaled) return false;
                }
                // all nodes are scaled
                // draw roads!
                CreateConnectionLines(false);
                CreateConnectionLines(true);
                init = true;
            }
        }
        return true;
    }

    public void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if (!string.Equals(SceneManager.GetActiveScene().name, mapName))
        {
            mapName = SceneManager.GetActiveScene().name;
            pathNodeCount = GameObject.FindObjectsOfType<PathNode>().Length;
            pathNodes.Clear();
            init = false;
        }
    }

    private void CreateConnectionLines(bool isLarge)
    {
        // Make a container for all the lines
        GameObject connectionLineContainer = new GameObject("ConnectionLineContainer");
        connectionLineContainer.transform.parent = GameObject.Find("PathSystem").transform;
        
        // Only create one line per connection
        List<PathNode> openList = new List<PathNode>();
        List<PathNode> closedList = new List<PathNode>();
        openList.AddRange(pathNodes);
        
        while (openList.Count > 0)
        {
            PathNode currentPathNode = openList[openList.Count - 1];

            // If current path node was already visited
            if (closedList.Contains(currentPathNode))
                continue;

            // For each connected nodes
            for (int j = 0; j < currentPathNode.ConnectedNodes.Count; ++j)
            {
                PathNode currentConnectedPathNode = currentPathNode.ConnectedNodes[j];

                // Do not create a line if the connected node has already been visited
                if (closedList.Contains(currentConnectedPathNode))
                    continue;
                
                CreateRoad(connectionLineContainer, currentPathNode.transform.position, currentConnectedPathNode.transform.position, isLarge);                
            }

            // Close the current path node
            closedList.Add(currentPathNode);
            openList.Remove(currentPathNode);
        }

        for (int i = 0; i < pathNodes.Count; ++i)
        {
            if (pathNodes[i].NodeType == PathNode.PathNodeType.NORMAL)
            {
                CreateConnection(connectionLineContainer, pathNodes[i].transform.position, isLarge);
            }
        }
    }

    private void CreateRoad(GameObject container, Vector3 startPos, Vector3 endPos, bool isLarge)
    {
        if (isLarge)
        {
            GameObject connectionLineLargeGO = new GameObject("ConnectionLineLarge");
            connectionLineLargeGO.transform.parent = container.transform;
            connectionLineLargeGO.transform.position = startPos;
            connectionLineLargeGO.AddComponent<LineRenderer>();
            LineRenderer connectionLineLarge = connectionLineLargeGO.GetComponent<LineRenderer>();
            connectionLineLarge.material = lineParameters.lineMaterial;
            connectionLineLarge.SetColors(lineParameters.lineLargeColor, lineParameters.lineLargeColor);
            connectionLineLarge.SetWidth(lineParameters.lineLargeWidth * PlayfieldController.Instance.WidthScale, lineParameters.lineLargeWidth * PlayfieldController.Instance.WidthScale);
            connectionLineLarge.SetPosition(0, startPos);
            connectionLineLarge.SetPosition(1, endPos);
        }
        else
        {
            GameObject connectionLineSmallGO = new GameObject("ConnectionLineSmall");
            connectionLineSmallGO.transform.parent = container.transform;
            connectionLineSmallGO.transform.position = startPos;
            connectionLineSmallGO.AddComponent<LineRenderer>();
            LineRenderer connectionLineSmall = connectionLineSmallGO.GetComponent<LineRenderer>();
            connectionLineSmall.material = lineParameters.lineMaterial;
            connectionLineSmall.SetColors(lineParameters.lineSmallColor, lineParameters.lineSmallColor);
            connectionLineSmall.SetWidth(lineParameters.lineSmallWidth * PlayfieldController.Instance.WidthScale, lineParameters.lineSmallWidth * PlayfieldController.Instance.WidthScale);
            startPos.z = -0.01f;
            endPos.z = -0.01f;
            connectionLineSmall.SetPosition(0, startPos);
            connectionLineSmall.SetPosition(1, endPos);
        }
    }

    private void CreateConnection(GameObject container, Vector3 pos, bool isLarge)
    {
        GameObject roadConnection = (GameObject)Instantiate(Resources.Load("RoadConnection"));
        roadConnection.transform.parent = container.transform;
        roadConnection.transform.position = pos;

        if (isLarge)
        {
            roadConnection.GetComponent<SpriteRenderer>().color = lineParameters.lineLargeColor;
            roadConnection.GetComponent<SpriteRenderer>().sortingOrder = 0;
            roadConnection.transform.localScale = new Vector3(lineParameters.lineLargeWidth * 1.001f * PlayfieldController.Instance.WidthScale, lineParameters.lineLargeWidth * 1.001f * PlayfieldController.Instance.WidthScale, 1f);
        }
        else
        {
            roadConnection.GetComponent<SpriteRenderer>().color = lineParameters.lineSmallColor;
            roadConnection.transform.localScale = new Vector3(lineParameters.lineSmallWidth * 1.001f * PlayfieldController.Instance.WidthScale, lineParameters.lineSmallWidth * 1.001f * PlayfieldController.Instance.WidthScale, 1f);
        }
    }

    public List<PathNode> StartNodes
    {
        get
        {
            List<PathNode> startNodes = new List<PathNode>();
            for (int i = 0; i < pathNodes.Count; ++i)
            {
                if (pathNodes[i].NodeType == PathNode.PathNodeType.START)
                {
                    startNodes.Add(pathNodes[i]);
                }
            }
            return startNodes;
        }
    }

    public List<PathNode> EndNodes
    {
        get
        {
            List<PathNode> endNodes = new List<PathNode>();
            for (int i = 0; i < pathNodes.Count; ++i)
            {
                if (pathNodes[i].NodeType == PathNode.PathNodeType.END)
                {
                    endNodes.Add(pathNodes[i]);
                }
            }
            return endNodes;
        }
    }

    public List<PathNode> PathNodes
    {
        get { return pathNodes; }
    }

    public void RemoveAtNode(GameObject destroyedBase)
    {
        if (destroyedBase != null)
        {
            PathNode node = destroyedBase.GetComponent<BaseObject>().Node;
            if (node != null)
            {
                node.NodeType = PathNode.PathNodeType.NORMAL;
            }
            else
            {
                Debug.Log("Base " + destroyedBase.name + " in Scene " + SceneManager.GetActiveScene().name + " has no PathNode assigned! ");
            }
            // Update the enemies destination if needed
            EnemyController.Instance.RefreshEnemiesDestination(destroyedBase);
        }
    }

    // returns a list of Nodes to follow to get from startNode to endNode
    public List<PathNode> FindPath(PathNode startNode, PathNode endNode)
    {
        List<PathNode> openList = new List<PathNode>();
        List<PathNode> closedList = new List<PathNode>();

        openList.Add(startNode);
        startNode.f = 0;
        while (openList.Count > 0)
        {
            //find the node with the least f on the open list, call it "q"
            PathNode q = openList[0];
            for (int i = 1; i < openList.Count; ++i)
            {
                if (openList[i].f < q.f)
                {
                    q = openList[i];
                }
            }

            //pop q off the open list
            openList.Remove(q);

            //generate q's successors 
            List<PathNode> successors = q.ConnectedNodes;

            //for each successor
            for (int i = 0; i < successors.Count; ++i)
            {
                //if successor is the goal, stop the search
                if (successors[i] == endNode)
                {
                    successors[i].parent = q;
                    return GeneratePathList(endNode);
                }
                
                //successor.g = q.g + distance between successor and q
                float g = Vector3.SqrMagnitude(successors[i].transform.position - q.transform.position);
                
                //successor.h = distance from goal to successor
                float h = Vector3.SqrMagnitude(successors[i].transform.position - endNode.transform.position);
                
                //successor.f = successor.g + successor.h
                successors[i].f = g + h;

                //if a node with the same position as successor is in the OPEN list \
                //    which has a lower f than successor, skip this successor
                if (openList.Contains(successors[i])) continue;
                
                //if a node with the same position as successor is in the CLOSED list \ 
                //    which has a lower f than successor, skip this successor
                if (closedList.Contains(successors[i])) continue;
                
                //otherwise, add the node to the open list
                openList.Add(successors[i]);
                
                //and set their parents to q
                successors[i].parent = q;
            }
            //push q on the closed list
            closedList.Add(q);
        }
        Debug.LogError("Couldn't Find Path, probably missing a link somewhere, FIX IT! " + startNode.name + " -> " + endNode.name + " in map " + SceneManager.GetActiveScene().name);
        return null;
    }

    private List<PathNode> GeneratePathList(PathNode endNode)
    {
        List<PathNode> result = new List<PathNode>();

        PathNode currentNode = endNode;
        while(currentNode.parent != null)
        {
            result.Insert(0, currentNode);
            currentNode = currentNode.parent;
        }
        result.Insert(0, currentNode);

        ClearParentsAndF();

        return result;
    }

    private void ClearParentsAndF()
    {
        for(int i = 0; i < pathNodes.Count; ++i)
        {
            pathNodes[i].f = 0;
            pathNodes[i].parent = null;
        }
    }

    public override void OnDestroy()
    {
        base.OnDestroy();

        SceneManager.sceneLoaded -= OnSceneLoaded;
    }
}
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

#pragma warning disable 0649
#pragma warning disable 0169

public class PlayfieldController : UnitySingleton<PlayfieldController>
{
    [Serializable]
    public class Shake
    {
        [Range(0, 100)]
        public float amplitude = 20f;
        
        [Range(0.00001f, 0.99999f)]
        public float frequency = 0.7f;
        
        [Range(1, 4)]
        public int octaves = 3;
        
        [Range(0.00001f, 5)]
        public float persistance = 2f;
        
        [Range(0.00001f, 100)]
        public float lacunarity = 50f;
        
        [Range(0.00001f, 0.99999f)]
        public float burstFrequency = 0.7f;
        
        [Range(0, 5)]
        public int burstContrast = 2;
    }

    private float heightScale;
    private float widthScale;
    private float currentHeight;
    private float currentWidth;
    private float playfieldWidth;
    private float playfieldHeight;
    private float yOffset;
    private float xOffset;
    private float yOffsetOriginal;

    private SpriteRenderer leftSideBorder;
    private SpriteRenderer topSideBorder;
    private SpriteRenderer rightSideBorder;
    private SpriteRenderer bottomSideBorder;

    private Vector2 bottomLeft;
    private Vector2 topRight;
    
    private Camera mainCamera;

    #region Serialized Fields
    [SerializeField]
    private Sprite greenDot;

    [SerializeField]
    [Range(1, 5)]
    private float disturbanceDuration = 1.5f;

    [SerializeField]
    [Range(0.1f, 20)]
    private float flashSpeed = 4f;

    [SerializeField]
    private Shake camShake;

    [SerializeField]
    private float heightAdjust = 10f;

    [SerializeField]
    private float widthAdjust = 10f;
    #endregion

    private GameObject radarBorderGO;
    private GameObject radarBorderTopGO;
    private Image radarGlow;
    private float disturbanceTimer = 0f;
    private float fadeDir = -1f;

    public float PlayfieldWidth
    {
        get { return TopRight.x - BottomLeft.x;  }
    }

    public float HeightAdjust
    {
        get { return heightAdjust; }
    }

    public float WidthAdjust
    {
        get { return widthAdjust; }
    }

    public Vector2 BottomLeft
    {
        get { return bottomLeft; }
    }
    
    public Vector2 TopRight
    {
        get { return topRight; }
    }

    public float HeightScale
    {
        get { return heightScale; }
    }

    public float WidthScale
    {
        get { return widthScale; }
    }
    
    public GameObject RadarBorderTopGO
    {
        get { return radarBorderTopGO; }
    }

    public float GetPlayfieldLeftOffset()
    {
        return BottomLeft.x;
    }

    public float GetPlayfieldTopOffset()
    {
        return currentHeight - TopRight.y;
    }

    public float GetPlayfieldRightOffset()
    {
        return currentWidth - TopRight.x;
    }

    public float GetPlayfieldBottomOffset()
    {
        return BottomLeft.y;
    }

    public override void Awake()
    {
        base.Awake();
        mainCamera = Camera.main;
        Init();
    }

    public void Init()
    {
        currentHeight = Screen.height;
        currentWidth = Screen.width;

        widthScale = currentWidth / GameUtils.STANDARD_SCREEN_WIDTH;
        heightScale = widthScale;

        GameObject battleUI = GameObject.Find("BattleUICanvas");

        GameObject radarPanelGO = GameUtils.GetChildGameObject(battleUI, "RadarPanel", true);
        radarBorderGO = GameUtils.GetChildGameObject(radarPanelGO, "RadarBorder", true);
        radarBorderTopGO = GameUtils.GetChildGameObject(radarPanelGO, "RadarBorderTop", true);
        radarGlow = GameUtils.GetChildGameObject(radarPanelGO, "RadarGlow", true).GetComponent<Image>();

        RectTransform rectRadarPanel = radarPanelGO.GetComponent<RectTransform>();

        playfieldWidth = widthScale * GameUtils.STANDARD_WIDTH;
        playfieldHeight = heightScale * GameUtils.STANDARD_HEIGHT;

        yOffsetOriginal = rectRadarPanel.anchoredPosition.y;
        xOffset = widthScale * rectRadarPanel.anchoredPosition.x;
        yOffset = heightScale * rectRadarPanel.anchoredPosition.y;

        SetupScreen();
        SetupPlayfield();
    }

    private void SetupScreen()
    {
        //SetupArea("Screen", transform, 0, 0, currentWidth, currentHeight, greenDot);

        mainCamera.transform.position = new Vector3(currentWidth / 2, currentHeight / 2, -GameUtils.LINE_THICKNESS);
        mainCamera.orthographicSize = currentHeight / 2;
    }

    private void SetupPlayfield()
    {
        bottomLeft = CalculateBottomLeft();
        topRight = CalculateTopRight();

        //SetupArea("Playfield", transform, bottomLeft.x, bottomLeft.y, topRight.x, topRight.y, greenDot);
    }

    private void SetupArea(string name, Transform parent, float bottomLeftX, float bottomLeftY, float topRightX, float topRightY, Sprite sprite)
    {
        GameObject screenGO = new GameObject(name);
        screenGO.transform.parent = parent;

        GameObject LeftSide = new GameObject("LeftSide");
        LeftSide.transform.parent = screenGO.transform;
        leftSideBorder = LeftSide.AddComponent<SpriteRenderer>();
        leftSideBorder.sortingOrder = 100;

        GameObject TopSide = new GameObject("TopSide");
        TopSide.transform.parent = screenGO.transform;
        topSideBorder = TopSide.AddComponent<SpriteRenderer>();
        topSideBorder.sortingOrder = 100;

        GameObject RightSide = new GameObject("RightSide");
        RightSide.transform.parent = screenGO.transform;
        rightSideBorder = RightSide.AddComponent<SpriteRenderer>();
        rightSideBorder.sortingOrder = 100;

        GameObject BottomSide = new GameObject("BottomSide");
        BottomSide.transform.parent = screenGO.transform;
        bottomSideBorder = BottomSide.AddComponent<SpriteRenderer>();
        bottomSideBorder.sortingOrder = 100;

        leftSideBorder.transform.position = new Vector3(bottomLeftX, (topRightY - bottomLeftY) * 0.5f + bottomLeftY);
        leftSideBorder.transform.localScale = new Vector3(GameUtils.LINE_THICKNESS, topRightY - bottomLeftY);
        leftSideBorder.sprite = sprite;

        topSideBorder.transform.position = new Vector3(bottomLeftX + (topRightX - bottomLeftX) * 0.5f, topRightY);
        topSideBorder.transform.localScale = new Vector3(topRightX - bottomLeftX, GameUtils.LINE_THICKNESS);
        topSideBorder.sprite = sprite;

        rightSideBorder.transform.position = new Vector3(topRightX, (topRightY - bottomLeftY) * 0.5f + bottomLeftY);
        rightSideBorder.transform.localScale = new Vector3(GameUtils.LINE_THICKNESS, topRightY - bottomLeftY);
        rightSideBorder.sprite = sprite;

        bottomSideBorder.transform.position = new Vector3(bottomLeftX + (topRightX - bottomLeftX) * 0.5f, bottomLeftY);
        bottomSideBorder.transform.localScale = new Vector3(topRightX - bottomLeftX, GameUtils.LINE_THICKNESS);
        bottomSideBorder.sprite = sprite;
    }

    public override bool Update()
    {
        if (!base.Update()) return false;
        if (WaveController.Instance.Paused) return false;

        UpdateDisturbance();

        return true;
    }

    private void UpdateDisturbance()
    {
        if (disturbanceTimer > 0f)
        {
            // First tick
            if (disturbanceTimer == disturbanceDuration)
            {
                // Show the red glow
                radarGlow.enabled = true;
            }

            disturbanceTimer -= Time.deltaTime;

            // Perform a flashing effect
            float fadeValue = Mathf.Clamp01(flashSpeed * Time.deltaTime);
            Color flashingColorRadar = radarGlow.color;
            flashingColorRadar.a = Mathf.Clamp01(flashingColorRadar.a + (fadeDir * fadeValue));
            radarGlow.color = flashingColorRadar;
            if (radarGlow.color.a <= 0.5f)
            {
                // Change fade direction to fade in
                fadeDir = 1f;
            }
            else if (radarGlow.color.a >= 1f)
            {
                // Change fade direction to fade out
                fadeDir = -1f;
            }
            if (disturbanceTimer <= 0f)
            {
                // Hide the red glow
                radarGlow.enabled = false;
            }

            // Perform a cam shake
            Vector3 newPos = Vector3.zero;
            Vector2 shakeValue = GameUtils.Shake2D(camShake);
            newPos += new Vector3(shakeValue.x, shakeValue.y, 0f);
            radarBorderGO.transform.localPosition = newPos;

            if (disturbanceTimer <= 0f)
            {
                // Reset the radar border position
                radarBorderGO.transform.localPosition = Vector3.zero;                
            }
        }
    }

    public void OnDrawGizmos()
    {
        widthScale = heightScale = currentWidth / GameUtils.STANDARD_SCREEN_WIDTH;
        bottomLeft = CalculateBottomLeft();
        topRight = CalculateTopRight();

        SetupAreaEditor(bottomLeft.x, bottomLeft.y, topRight.x, topRight.y);
    }

    private void SetupAreaEditor(float x1, float y1, float x2, float y2)
    {
        Debug.DrawLine(new Vector3(x1, y1), new Vector3(x1, y2), Color.magenta);
        Debug.DrawLine(new Vector3(x1, y1), new Vector3(x2, y1), Color.magenta);
        Debug.DrawLine(new Vector3(x1, y2), new Vector3(x2, y2), Color.magenta);
        Debug.DrawLine(new Vector3(x2, y1), new Vector3(x2, y2), Color.magenta);

        float localHeight = heightAdjust * HeightScale;
        float localWidth = widthAdjust * WidthScale;

        Debug.DrawLine(new Vector3(x1 + localWidth, y1 + localHeight), new Vector3(x1 + localWidth, y2 - localHeight), Color.green);
        Debug.DrawLine(new Vector3(x1 + localWidth, y1 + localHeight), new Vector3(x2 - localWidth, y1 + localHeight), Color.green);
        Debug.DrawLine(new Vector3(x1 + localWidth, y2 - localHeight), new Vector3(x2 - localWidth, y2 - localHeight), Color.green);
        Debug.DrawLine(new Vector3(x2 - localWidth, y1 + localHeight), new Vector3(x2 - localWidth, y2 - localHeight), Color.green);
    }

    private Vector2 CalculateBottomLeft()
    {
        float bottomPos = (currentHeight / 2f) + yOffset  - (playfieldHeight / 2f);
        float leftPos = currentWidth  + xOffset - (playfieldWidth / 2f);
        return new Vector2(leftPos, bottomPos);
    }

    private Vector2 CalculateTopRight()
    {
        float topPos = (currentHeight / 2f) + yOffset + (playfieldHeight / 2f);
        float rightPos = currentWidth + xOffset + (playfieldWidth / 2f);
        return new Vector2(rightPos, topPos);
    }

    public bool IsInsidePlayArea(Vector3 position)
    {
        // the actual playable area is a bit smaller to counter dynamic expanding objects
        return (position.x >= (BottomLeft.x + (widthAdjust * WidthScale)) && 
                position.x <= (TopRight.x - (widthAdjust * WidthScale)) &&
                position.y >= (BottomLeft.y + (heightAdjust * HeightScale)) && 
                position.y <= (TopRight.y - (heightAdjust * HeightScale)));
    }

    public Vector3 AdjustScalePosition(Vector3 pos)
    {
        pos.x *= WidthScale;
        pos.y *= HeightScale;
        float originalPosDiff = ((GameUtils.STANDARD_SCREEN_HEIGHT / 2f) + yOffsetOriginal - (GameUtils.STANDARD_HEIGHT / 2f)) * HeightScale;
        float yPosDiff = BottomLeft.y - originalPosDiff;
        pos.y += yPosDiff;
        pos.z = 0f;

        return pos;
    }

    public Vector3 AdjustScaleSize(Vector3 localScale)
    {
        localScale.x *= WidthScale;
        localScale.y *= HeightScale;

        return localScale;
    }

    public Vector3 AdjustScaleSizeInverse(Vector3 localScale)
    {
        localScale.x /= WidthScale;
        localScale.y /= HeightScale;

        return localScale;
    }

    public void DisturbPlayField()
    {
        disturbanceTimer = disturbanceDuration;
    }
}
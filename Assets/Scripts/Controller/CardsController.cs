using UnityEngine;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using System.Linq;

public class CardsController : UnitySingleton<CardsController>
{
    private RarityData rarityData;

    #region Rarities
    public GameUtils.BaseID RollWeaponRarity()
    {
        GameUtils.BaseID weaponType = GameUtils.BaseID.ARTILLERY;
        float randomRoll = UnityEngine.Random.Range(0f, 1f);
        float totalRarity = 0;
        for (int j = (int)GameUtils.BaseID.MISSILE; j < (int)GameUtils.BaseID.CITY; ++j)
        {
            totalRarity += (rarityData.weaponCardsRarities[j] / 100f);
            if (randomRoll < totalRarity)
            {
                weaponType = (GameUtils.BaseID)j;
                break;
            }
        }
        //Debug.Log("Rolled " + weaponUpgradeType);

        return weaponType;
    }

    public GameUtils.EWeaponUpgrade RollWeaponUpgradeRarity(GameUtils.BaseID weaponID)
    {
        // get rarity adjustments based on levels remaining
        int total = 0;
        int lowestLevel = GameUtils.MAX_UPGRADE_LEVEL;
        List<int> levelRemaining = new List<int>();
        for (int j = (int)GameUtils.EWeaponUpgrade.SPEED; j < (int)GameUtils.EWeaponUpgrade.COUNT; ++j)
        {
            GameUtils.EWeaponUpgrade weaponUpgrade = (GameUtils.EWeaponUpgrade)j;
            // get the amount of levels left until max
            int amount = GameUtils.MAX_UPGRADE_LEVEL - UpgradeController.Instance.GetWeaponUpgradeLevel(weaponID, weaponUpgrade);
            // remove levels that could be used with cards
            // this is needed to prevent exploiting the system
            // by not leveling the upgrade to gain more cards
            amount -= CardsToLevel(weaponID, weaponUpgrade);
            // now insert in the list
            levelRemaining.Insert(j, amount);
            total += amount;
            if (amount < lowestLevel)
            {
                lowestLevel = amount;
            }
        }

        // adjust rarities from the design settings to give higher chances to low level upgrades
        List<float> adjustedRarities = new List<float>();
        for (int j = (int)GameUtils.EWeaponUpgrade.SPEED; j < (int)GameUtils.EWeaponUpgrade.COUNT; ++j)
        {
            float countedLevel = (float)(levelRemaining[j] - lowestLevel);
            float countedTotal = total - (lowestLevel * levelRemaining.Count);
            float adjustment = countedTotal > 0 ? countedLevel / countedTotal : (rarityData.weaponUpgradeCardsRarities[j] / 100f);
            adjustedRarities.Insert(j, ((rarityData.weaponUpgradeCardsRarities[j]/100f) + adjustment) / 2f);
        }

        // rool the dice
        GameUtils.EWeaponUpgrade weaponUpgradeType = GameUtils.EWeaponUpgrade.HEALTH;
        float randomRoll = UnityEngine.Random.Range(0f, 1f);

        // check which upgrade made it
        float totalRarity = 0;
        for (int j = (int)GameUtils.EWeaponUpgrade.SPEED; j < (int)GameUtils.EWeaponUpgrade.COUNT; ++j)
        {
            totalRarity += adjustedRarities[j];
            if (randomRoll < totalRarity)
            {
                weaponUpgradeType = (GameUtils.EWeaponUpgrade)j;
                break;
            }
        }
        //Debug.Log("Rolled " + weaponUpgradeType);

        return weaponUpgradeType;
    }

    public GameUtils.EConsumable RollConsumableRarity()
    {
        GameUtils.EConsumable consumableType = GameUtils.EConsumable.Reload;
        float randomRoll = UnityEngine.Random.Range(0f, 1f);
        float totalRarity = 0;
        for (int j = (int)GameUtils.EConsumable.Slower; j < (int)GameUtils.EConsumable.Count; ++j)
        {
            totalRarity += (rarityData.consumableCardsRarities[j] / 100f);
            if (randomRoll < totalRarity)
            {
                consumableType = (GameUtils.EConsumable)j;
                break;
            }
        }

        return consumableType;
    }
    #endregion // Rarities

    private int CardsToLevel(GameUtils.BaseID weaponID, GameUtils.EWeaponUpgrade upgrade)
    {
        int cards2level = 0;
        int cardsUsed = 0;
        if (weaponCards[weaponID][upgrade] > 0)
        {
            int currentLevel = UpgradeController.Instance.GetWeaponUpgradeLevel(weaponID, upgrade);
            for (int i = currentLevel; i < GameUtils.MAX_UPGRADE_LEVEL; ++i)
            {
                if ((weaponCards[weaponID][upgrade] - cardsUsed) >= (i + 1))
                {
                    // got enough cards, count the level
                    cardsUsed += i + 1;
                    cards2level++;
                }
                else
                {
                    // not enough cards for this level
                    break;
                }
            }
        }
        return cards2level;
    }

    public struct WeaponCard
    {
        public GameUtils.BaseID weaponID;
        public GameUtils.EWeaponUpgrade weaponUpgrade;
    }

    private static Dictionary<GameUtils.BaseID, Dictionary<GameUtils.EWeaponUpgrade, int>> weaponCards;

    public Dictionary<GameUtils.BaseID, Dictionary<GameUtils.EWeaponUpgrade, int>> WeaponCards
    {
        get { return weaponCards; }
        set { weaponCards = value; }
    }

    public override void Awake()
    {
        base.Awake();
        PopulateRarity();
    }

    #region Weapons
    public void SaveWeaponCards(DataController.SAVEPRIORITY priority)
    {
        DataController.Instance.SaveCardData(priority);
    }

    private void PopulateRarity()
    {
        rarityData = Resources.Load<RarityData>("Rarity/RarityData");
    }

    public void LoadWeaponCards(Dictionary<GameUtils.BaseID, Dictionary<GameUtils.EWeaponUpgrade, int>> remoteCards)
    {
        PopulateRarity();
        if (remoteCards == null)
        {
            System.Exception ex = JsonUtils<Dictionary<GameUtils.BaseID, Dictionary<GameUtils.EWeaponUpgrade, int>>>.Load(GameUtils.WEAPON_CARDS_JSON, out weaponCards, GameUtils.ELoadType.DynamicData);
            if (ex != null
                && ex.GetType() == typeof(FileLoadException))
            {
                ResetWeaponCards();
            }
        }
        else
        {
            weaponCards = remoteCards;
        }
    }

    public void ResetWeaponCards()
    {
        PopulateRarity();
        weaponCards = new Dictionary<GameUtils.BaseID, Dictionary<GameUtils.EWeaponUpgrade, int>>();
        for (int i = (int)GameUtils.BaseID.MISSILE; i < (int)GameUtils.BaseID.CITY; ++i)
        {
            GameUtils.BaseID weaponID = (GameUtils.BaseID)i;
            weaponCards[weaponID] = new Dictionary<GameUtils.EWeaponUpgrade, int>()
            {
                { GameUtils.EWeaponUpgrade.SPEED, 0 },
                { GameUtils.EWeaponUpgrade.RELOAD, 0 },
                { GameUtils.EWeaponUpgrade.REGEN, 0 },
                { GameUtils.EWeaponUpgrade.AIR, 0 },
                { GameUtils.EWeaponUpgrade.GROUND, 0 },
                { GameUtils.EWeaponUpgrade.EXPLOSION_RADIUS, 0 },
                { GameUtils.EWeaponUpgrade.VISIBILITY_RADIUS, 0 },
                { GameUtils.EWeaponUpgrade.HEALTH, 0 },
            };
        }
        SaveWeaponCards(DataController.SAVEPRIORITY.QUEUE);
    }

    public bool HasAmountOfWeaponCards(GameUtils.BaseID weaponID, GameUtils.EWeaponUpgrade weaponUpgrade, int amount)
    {
        return weaponCards[weaponID][weaponUpgrade] >= amount;
    }

    public int GetWeaponCards(GameUtils.BaseID weaponID, GameUtils.EWeaponUpgrade weaponUpgrade)
    {
        return weaponCards[weaponID][weaponUpgrade];
    }

    public void AddWeaponCard(GameUtils.BaseID weaponID, GameUtils.EWeaponUpgrade weaponUpgrade, int amount)
    {
        weaponCards[weaponID][weaponUpgrade] += amount;
        SaveWeaponCards(DataController.SAVEPRIORITY.QUEUE);
    }

    public void RemoveWeaponCard(GameUtils.BaseID weaponID, GameUtils.EWeaponUpgrade weaponUpgrade, int amount)
    {
        if (weaponCards[weaponID][weaponUpgrade] >= amount)
        {
            weaponCards[weaponID][weaponUpgrade] -= amount;
            SaveWeaponCards(DataController.SAVEPRIORITY.QUEUE);
        }
    }

    public List<WeaponCard> RollWeaponCards(int amount, GameUtils.BaseID[] specificWeapons, GameUtils.EWeaponUpgrade specificWeaponUpgrade = GameUtils.EWeaponUpgrade.COUNT)
    {
        List<WeaponCard> result = new List<WeaponCard>();

        // For each amount
        for (int i = 0; i < amount; ++i)
        {
            // If no more cards to roll
            if (GetAmountOfCardsRemaining() == 0) return result;

            GameUtils.BaseID weaponSelected;
            GameUtils.EWeaponUpgrade weaponUpgradeSelected = specificWeaponUpgrade;

            // Keep rolling until we get an upgrade that isn't maxed out
            int ctr = 0;
            do
            {
                // If there's no weapon specified
                if (specificWeapons == null || specificWeapons.Length == 0)
                {
                    // Keep rolling a random weapon
                    weaponSelected = RollWeaponRarity();
                }
                else
                {
                    // Roll a random weapon through the specific weapons list
                    int randomWeaponIndex = Random.Range(0, specificWeapons.Length);
                    weaponSelected = specificWeapons[randomWeaponIndex];
                }

                // If there's no weapon upgrade specified
                if (specificWeaponUpgrade == GameUtils.EWeaponUpgrade.COUNT)
                {
                    // Keep rolling a random upgrade
                    weaponUpgradeSelected = RollWeaponUpgradeRarity(weaponSelected);
                }                
                
                // Found a valid card
                if (GetAmountOfCardsToMaxOut(weaponSelected, weaponUpgradeSelected) > 0)
                {
                    break;
                }

                ctr++;
                if (ctr == 1000)
                {
                    Debug.LogWarning("Something went wrong with rolling the weapon upgrades. There's a high possibility that the player tried to roll a weapon upgrade card when having full upgrades.");
                }
            }
            while (ctr < 1000);

            // Create the upgrade card
            WeaponCard newCard = new WeaponCard();
            newCard.weaponID = weaponSelected;
            newCard.weaponUpgrade = weaponUpgradeSelected;

            AddWeaponCard(newCard.weaponID, newCard.weaponUpgrade, 1);
            result.Add(newCard);
        }

        return result;
    }

#if UNITY_EDITOR || DEBUG_MOBILE
    public void MaxWeaponCards(GameUtils.BaseID weaponID)
    {
        // For each weapon upgrade
        for (int i = (int)GameUtils.EWeaponUpgrade.SPEED; i < (int)GameUtils.EWeaponUpgrade.COUNT; ++i)
        {
            // Give 100 cards
            AddWeaponCard(weaponID, (GameUtils.EWeaponUpgrade)i, 100);
        }            
    }
#endif
    #endregion // Weapons

    #region Consumables
    public List<GameUtils.EConsumable> RollConsumable()
    {
        List<GameUtils.EConsumable> result = new List<GameUtils.EConsumable>();

        GameUtils.EConsumable consumableRolled = RollConsumableRarity();
        result.Add(consumableRolled);

        // Modify the amount
        ConsumableController.Instance.ModifyConsumableAmount(consumableRolled, 1, DataController.SAVEPRIORITY.QUEUE);

        return result;
    }
    #endregion

    private int GetAmountOfCardsRemaining()
    {
        int cardsRemaining = 0;
        
        // For each weapon
        for (int i = (int)GameUtils.BaseID.MISSILE; i < (int)GameUtils.BaseID.CITY; ++i)
        {
            GameUtils.BaseID weaponID = (GameUtils.BaseID)i;

            // For each weapon upgrade
            for (int j = (int)GameUtils.EWeaponUpgrade.SPEED; j < (int)GameUtils.EWeaponUpgrade.COUNT; ++j)
            {
                GameUtils.EWeaponUpgrade weaponUpgrade = (GameUtils.EWeaponUpgrade)j;
                
                cardsRemaining += GetAmountOfCardsToMaxOut(weaponID, weaponUpgrade);
            }
        }

        return cardsRemaining;
    }

    private int GetAmountOfCardsToMaxOut(GameUtils.BaseID weaponID, GameUtils.EWeaponUpgrade weaponUpgradeID)
    {
        int amountOfCardsToMaxOut = 0;

        int currentWeaponUpgradeLvl = UpgradeController.Instance.GetWeaponUpgradeLevel(weaponID, weaponUpgradeID);

        // For each upgrade lvl remaining
        for (int i = currentWeaponUpgradeLvl; i < GameUtils.MAX_UPGRADE_LEVEL; ++i)
        {
            amountOfCardsToMaxOut += UpgradeController.Instance.GetAmountOfCardsPerLvl(i);
        }

        amountOfCardsToMaxOut -= GetWeaponCards(weaponID, weaponUpgradeID);
        //Debug.Log("amountOfCardsToMaxOut = " + amountOfCardsToMaxOut);

        // Note :: Safeguard, normally it should be always 0 or higher
        return Mathf.Max(amountOfCardsToMaxOut, 0);
    }
    
    public void ResetCards()
    {
        ResetWeaponCards();
        SaveWeaponCards(DataController.SAVEPRIORITY.QUEUE);
    }
}

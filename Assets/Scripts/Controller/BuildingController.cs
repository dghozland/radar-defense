using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BuildingController : UnitySingleton<BuildingController>
{
    private bool isInitialized;
    // List of buildings data coming from the resources folder
    private BuildingData[] buildingsData;

    private List<BuildingPositionObject> buildingPositions;
    private Dictionary<GameUtils.BaseID, GameObject> activeBuildings;
    private Dictionary<GameUtils.BaseID, GameObject> inactiveBuildings;

    public Dictionary<GameUtils.BaseID, GameObject> ActiveBuildings
    {
        get { return activeBuildings; }
    }

    public override void Awake()
    {
        base.Awake();
        isInitialized = false;
        PopulateBuildingsData();

        activeBuildings = new Dictionary<GameUtils.BaseID, GameObject>();
        inactiveBuildings = new Dictionary<GameUtils.BaseID, GameObject>();
        buildingPositions = new List<BuildingPositionObject>();
        buildingPositions.AddRange(FindObjectsOfType<BuildingPositionObject>());
    }

    public override bool Update()
    {
        if (!base.Update()) return false;
        if (!isInitialized && ConsumableController.Instance.IsSceneSetup && AllBuildingPositionsScaled())
        {
            // Note :: Not called in the Awake function
            // to let the other controllers awake first.
            // There was a problem where when loading a battle scene
            // directly, the base name was not localized.
            InitBuildings();
            isInitialized = true;
        }
        return true;
    }

    private bool AllBuildingPositionsScaled()
    {
        for (int i = 0; i < buildingPositions.Count; ++i)
        {
            if (!buildingPositions[i].GetComponent<ScaleObject>().IsScaled) return false;
        }
        return true;
    }

    private void InitBuildings()
    {
        GameObject bases = GameObject.Find("Bases");

        while (buildingPositions.Count > 0)
        {
            GameUtils.BaseID buildingType = buildingPositions[0].BuildingType;

            // Instantiate the building, name it and parent it
            GameObject buildingPrefab = (GameObject)Instantiate(Resources.Load("BuildingPrefab"), buildingPositions[0].transform.position, Quaternion.identity);
            BuildingObject buildingObject = buildingPrefab.GetComponent<BuildingObject>();
            buildingObject.Node = buildingPositions[0].Node;

            buildingPrefab.name = buildingType.ToString().ToUpper();
            buildingPrefab.transform.parent = bases.transform;

            RegisterBuilding(buildingType, buildingPrefab);

            // Disable and remove the building position object
            buildingPositions[0].gameObject.SetActive(false);
            buildingPositions.RemoveAt(0);
        }
    }

    public void RegisterBuilding(GameUtils.BaseID baseID, GameObject building)
    {
        // Init the building from the building data
        building.GetComponent<BuildingObject>().InitBuildingData(FindBuildingData(baseID));

        // add to active list
        activeBuildings.Add(baseID, building);
    }

    public void UnregisterBuilding(GameUtils.BaseID baseID)
    {
        // Remove from active list and add to inactive list
        inactiveBuildings.Add(baseID, activeBuildings[baseID]);
        activeBuildings.Remove(baseID);
    }

    public void ConsumableConsumed(LootObject.ELootType lootType)
    {
        GameUtils.EConsumable consumableType = ConvertLootTypeToConsumable(lootType);

        // For each active buildings
        foreach (var value in activeBuildings.Values)
        {
            BuildingObject buildingObject = value.GetComponent<BuildingObject>();
            if (buildingObject.ConsumableProduction == consumableType)
            {
                // Re-enable the consumable production icon (if all consumables have been clicked)
                buildingObject.ManageConsumableProductionUIVisibility(-1);
                break;
            }
        }
    }

    private GameUtils.EConsumable ConvertLootTypeToConsumable(LootObject.ELootType lootType)
    {
        switch (lootType)
        {
            case LootObject.ELootType.OhShit:
                return GameUtils.EConsumable.OhShit;
            case LootObject.ELootType.Pulse:
                return GameUtils.EConsumable.Pulse;
            case LootObject.ELootType.Reload:
                return GameUtils.EConsumable.Reload;    
            case LootObject.ELootType.Slower:
                return GameUtils.EConsumable.Slower;
            case LootObject.ELootType.Repair:
            default:
                return GameUtils.EConsumable.Repair;
        }
    }

    #region Data Handling
    private void PopulateBuildingsData()
    {
        buildingsData = Resources.LoadAll<BuildingData>("Buildings");
    }

    private BuildingData FindBuildingData(GameUtils.BaseID baseID)
    {
        for (int i = 0; i < buildingsData.Length; ++i)
        {
            if (baseID == buildingsData[i].baseID)
            {
                return buildingsData[i];
            }
        }

        return null;
    }
    #endregion
}

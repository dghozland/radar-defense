using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Linq;
using UnityEngine.SceneManagement;
using System;

public class WaveController : UnitySingleton<WaveController>
{
    private const float WAVE_NOTIFICATION_DURATION = 3f;
    public const float WAVE_INITIAL_DELAY = 5f;
    private const float OPEN_PRIZE_ANIM_TRANSITION_NORMALIZED_TIME = 0.95f;
    private const string SLIDE_OUT_TRIGGER = "SlideOut";
    private const float CHALLENGE_START_PANEL_SLIDE_OUT_DELAY = 1.5f;

    private bool isSceneInitialized = false;

    public enum EMatchStatus
    {
        OnGoing,
        Cleared,
        Failed,
        Aborted
    }

    private ChallengeData.WaveData2[] wavesData;

    private ChallengeData.EChallengeType currentChallengeType;
    private int amountOfBases = 0;
    private float allBasesHealth = 0;
    private float initialWaveDelayTimer = 0f;
    private int currentScore = 0;
    private int screenScore = 0;
    private int scoreIncreaseRate = 0;
    private int killScore = 0;
    private int lootCreditScore = 0;
    private int lootGoldScore = 0;
    private int waveBonusScore = 0;
    private int multiKillScore = 0;
    private int boosterKillScore = 0;
    private int boosterLootCreditScore = 0;
    private int boosterWaveBonusScore = 0;
    private int boosterMultiKillScore = 0;
    private int boosterMatchRewardScore = 0;
    private int dailyMissionScore = 0;
    private float scoreIncreaseTimer = 0f;
    private EMatchStatus matchStatus;
    private bool starRewardPrizesShown;
    private int originalStars = 0;
    
    // UI elements
    private GameObject battleCanvas;
    private Text waveTextField;
    private Slider waveProgression;
    private Text waveNextField;
    private Button waveStartButton;
    private Text notificationField;
    private Text scoreTextField;
    private GameObject matchEndPanel;
    private Text matchEndInfoTitle;
    private Button matchEndMenuButton;
    private Button matchEndRetryButton;
    private GameObject getMoreConsumablePanel;
    private Button dismissGetMoreButton;    
    private GameObject getMoreOverlay;
    private Button pauseOverlayButton;
    private Button closeGetMoreButton;
    private Button getMoreWatchButton;
    private Button getMoreBuyButton;
    private Text getMoreBuyCostText;
    private Text getMoreGoldAvailableText;
    private GameObject challengeStartPanel;
    private Text challengeNameText;
    private GameObject grindCardsPanel;
    private GameObject lootTableContainer;
    private Text grindCardRewardAmountText;
    private Button startChallengeButton;
    private Button pauseButton;
    private GameObject pauseOverlay;
    private Text challengeNamePauseText;
    private Slider musicSlider;
    private Slider soundSlider;
    private Button pauseContinueButton;
    private Button pauseAbortButton;
    private Button fullResolutionButton;
    private Button twoThirdsResolutionButton;
    private Button halfResolutionButton;
    private Button debugCheatButton1;
    private Button debugCheatButton2;
    private Button debugWatchAdButton1;
    private Button debugWatchAdReset1;

    private Animator challengeStartAnimator;
    
    private int currentWaveIndex;
    private int currentWaveNumber;
    private int currentLastStandRoundIndex;
    private bool waitingForNextLastStandRound;

    private float challengeStartPanelTimer;
    private float waveDurationTimer;
    private float waveDuration;
    private float timeBetweenWavesTimer;
    private float notificationTimer;
    private string waveLocalizedName;

    private bool paused;
    private bool initWavePaused;
    private bool initWaveStarted;
    private int[] getMoreConsumableCounters;
    private int getMoreConsumableIndex;
    private float timePlayed;
    private float lastStandWaveIdleTimer = 0f;

    #region Exposed Values
    [SerializeField]
    private int waveBonusCreditPerSecondNormal = 50;

    [SerializeField]
    private int waveBonusCreditPerSecondHard = 100;

    [SerializeField]
    private int waveBonusCreditPerSecondInsane = 150;

    [SerializeField]
    private float lastStandIdleTimerForWaveStart = 10f;
    #endregion

    private List<PrizeController.RewardRollInfo> challengeRewardsToOpen;
    private Queue<GameObject> inactiveFlyingText = new Queue<GameObject>();
    private GameObject flyingTextContainer;

    public void FiredWeapon()
    {
        // not idleing
        lastStandWaveIdleTimer = lastStandIdleTimerForWaveStart;
    }

    public int CurrentWaveIndex
    {
        get { return currentWaveIndex; }
    }

    public int CurrentLastStandRoundIndex
    {
        get { return currentLastStandRoundIndex; }
    }

    public bool Paused
    {
        get { return paused; }
        set { paused = value; }
    }

    public bool InitWavePaused
    {
        get { return initWavePaused; }
        set { initWavePaused = value; }
    }

    public bool InitWaveStarted
    {
        get { return initWaveStarted; }
    }
    
    public EMatchStatus MatchStatus
    {
        get { return matchStatus; }
    }
    
    public Button MenuButton
    {
        get { return matchEndMenuButton; }
    }

    public Button PauseButton
    {
        get { return pauseButton; }
    }

    public int CurrentScore
    {
        get { return currentScore; }
    }

    public ChallengeData.EChallengeType CurrentChallengeType
    {
        get { return currentChallengeType; }
    }

    public override void Awake()
    {
        base.Awake();

        flyingTextContainer = new GameObject("FlyingText");
        lastStandWaveIdleTimer = lastStandIdleTimerForWaveStart;

        RegisterDependency(ChallengeController.Instance);
        RegisterDependency(LocalizationManager.Instance);

        FlowStateController.Instance.RegisterBackButton(PauseOn);

        // Setup UI elements
        battleCanvas = GameObject.Find("BattleUICanvas");
        waveTextField = GameUtils.GetChildGameObject(battleCanvas, "WaveField").GetComponent<Text>();
        waveProgression = GameUtils.GetChildGameObject(battleCanvas, "WaveProgression").GetComponent<Slider>();
        waveNextField = GameUtils.GetChildGameObject(battleCanvas, "WaveNextField").GetComponent<Text>();
        waveStartButton = GameUtils.GetChildGameObject(battleCanvas, "WaveStartButton").GetComponent<Button>();
        notificationField = GameUtils.GetChildGameObject(battleCanvas, "NotificationField").GetComponent<Text>();
        scoreTextField = GameUtils.GetChildGameObject(battleCanvas, "ScoreField").GetComponent<Text>();       
        matchEndPanel = GameUtils.GetChildGameObject(battleCanvas, "prefab_matchEndPanel", true);
        matchEndInfoTitle = GameUtils.GetChildGameObject(matchEndPanel, "MatchEndInfoTitle", true).GetComponent<Text>();
        matchEndMenuButton = GameUtils.GetChildGameObject(matchEndPanel, "MenuButton", true).GetComponent<Button>();
        matchEndRetryButton = GameUtils.GetChildGameObject(matchEndPanel, "RetryButton", true).GetComponent<Button>();
        matchEndMenuButton.gameObject.SetActive(false);
        matchEndRetryButton.gameObject.SetActive(false);
        matchEndPanel.SetActive(false);
        pauseButton = GameUtils.GetChildGameObject(battleCanvas, "PauseButton").GetComponent<Button>();
        pauseOverlay = GameUtils.GetChildGameObject(battleCanvas, "PauseOverlay", true);
        challengeNamePauseText = GameUtils.GetChildGameObject(pauseOverlay, "ChallengeNamePauseField", true).GetComponent<Text>();
        getMoreConsumablePanel = GameUtils.GetChildGameObject(battleCanvas, "GetMoreConsumablePanel", true);
        dismissGetMoreButton = GameUtils.GetChildGameObject(getMoreConsumablePanel, "DismissGetMoreConsumableButton", true).GetComponent<Button>();
        getMoreOverlay = GameUtils.GetChildGameObject(getMoreConsumablePanel, "GetMoreOverlay", true);
        musicSlider = GameUtils.GetChildGameObject(pauseOverlay, "MusicSlider", true).GetComponentInChildren<Slider>();
        soundSlider = GameUtils.GetChildGameObject(pauseOverlay, "SoundSlider", true).GetComponentInChildren<Slider>();
        pauseOverlayButton = GameUtils.GetChildGameObject(pauseOverlay, "PauseOverlayButton", true).GetComponent<Button>();
        pauseContinueButton = GameUtils.GetChildGameObject(pauseOverlay, "ContinueButton", true).GetComponent<Button>();
        pauseAbortButton = GameUtils.GetChildGameObject(pauseOverlay, "AbortButton", true).GetComponent<Button>();
        closeGetMoreButton = GameUtils.GetChildGameObject(getMoreOverlay, "CloseGetMoreConsumableButton", true).GetComponent<Button>();
        getMoreWatchButton = GameUtils.GetChildGameObject(getMoreOverlay, "Watch", true).GetComponent<Button>();
        getMoreBuyButton = GameUtils.GetChildGameObject(getMoreOverlay, "Buy", true).GetComponent<Button>();
        getMoreBuyCostText = GameUtils.GetChildGameObject(getMoreBuyButton.gameObject, "Text", true).GetComponent<Text>();
        getMoreGoldAvailableText = GameUtils.GetChildGameObject(getMoreOverlay, "GoldAvailable", true).GetComponent<Text>();
        challengeStartPanel = GameUtils.GetChildGameObject(battleCanvas, "ChallengeStartPanel", true);
        challengeNameText = GameUtils.GetChildGameObject(challengeStartPanel, "ChallengeNameField", true).GetComponent<Text>();
        grindCardsPanel = GameUtils.GetChildGameObject(challengeStartPanel, "RewardWindow (1)", true);
        lootTableContainer = GameUtils.GetChildGameObject(grindCardsPanel, "LootTableContainer", true);
        grindCardRewardAmountText = GameUtils.GetChildGameObject(challengeStartPanel, "qtyText", true).GetComponent<Text>();
        startChallengeButton = GameUtils.GetChildGameObject(challengeStartPanel, "StartBtn", true).GetComponent<Button>();
        startChallengeButton.gameObject.SetActive(false);
        pauseOverlay.SetActive(false);
        getMoreConsumablePanel.SetActive(false);
        grindCardsPanel.SetActive(false);
        challengeStartPanel.SetActive(true);

        startChallengeButton.onClick.AddListener(delegate { StartChallengeButtonClicked(); });
        matchEndMenuButton.onClick.AddListener(delegate { MenuButtonPressed(); });
        matchEndRetryButton.onClick.AddListener(delegate { RetryButtonPressed(); });
        pauseButton.onClick.AddListener(delegate { PauseButtonPressed(true); });
        pauseOverlayButton.onClick.AddListener(delegate { PauseButtonPressed(false); });
        musicSlider.onValueChanged.AddListener(delegate { MusicSliderValueChanged(); });
        soundSlider.onValueChanged.AddListener(delegate { SoundSliderValueChanged(); });
        pauseContinueButton.onClick.AddListener(delegate { PauseButtonPressed(false); });
        pauseAbortButton.onClick.AddListener(delegate { AbortButtonPressed(); });
        waveStartButton.onClick.AddListener(delegate { WaveStartButtonClicked(); });
        dismissGetMoreButton.onClick.AddListener(delegate { HideGetMoreConsumablePanel(); });
        getMoreWatchButton.onClick.AddListener(delegate { WatchAdButtonClicked(); });
        closeGetMoreButton.onClick.AddListener(delegate { HideGetMoreConsumablePanel(); });
        getMoreBuyButton.onClick.AddListener(delegate { GetMoreBuyButtonClicked(); });

        fullResolutionButton = GameUtils.GetChildGameObject(pauseOverlay, "FullResolutionButton", true).GetComponent<Button>();
        twoThirdsResolutionButton = GameUtils.GetChildGameObject(pauseOverlay, "TwoThirdsResolutionButton", true).GetComponent<Button>();
        halfResolutionButton = GameUtils.GetChildGameObject(pauseOverlay, "HalfResolutionButton", true).GetComponent<Button>();
        debugCheatButton1 = GameUtils.GetChildGameObject(pauseOverlay, "DebugCheatButton1", true).GetComponent<Button>();
        debugCheatButton2 = GameUtils.GetChildGameObject(pauseOverlay, "DebugCheatButton2", true).GetComponent<Button>();
        debugWatchAdButton1 = GameUtils.GetChildGameObject(pauseOverlay, "DebugWatchAdButton1", true).GetComponent<Button>();
        debugWatchAdReset1 = GameUtils.GetChildGameObject(pauseOverlay, "DebugWatchAdReset1", true).GetComponent<Button>();

        GameUtils.GetChildGameObject(pauseOverlay, "TextVersion", true).GetComponent<Text>().text = "Version: " + Application.version;

#if UNITY_EDITOR || DEBUG_MOBILE
        debugCheatButton1.onClick.AddListener(delegate { DebugCheatButton1Clicked(); });
        debugCheatButton2.onClick.AddListener(delegate { DebugCheatButton2Clicked(); });
        debugWatchAdButton1.onClick.AddListener(delegate { DebugWatchAdButtonClicked(); });
        debugWatchAdReset1.onClick.AddListener(delegate { DebugWatchAdResetButtonClicked(); });
        fullResolutionButton.onClick.AddListener(delegate { ResolutionButtonPressed(SettingsController.EResolutionType.Full); });
        twoThirdsResolutionButton.onClick.AddListener(delegate { ResolutionButtonPressed(SettingsController.EResolutionType.TwoThirds); });
        halfResolutionButton.onClick.AddListener(delegate { ResolutionButtonPressed(SettingsController.EResolutionType.Half); });
#else
        debugCheatButton1.gameObject.SetActive(false);
        debugCheatButton2.gameObject.SetActive(false);
        debugWatchAdButton1.gameObject.SetActive(false);
        debugWatchAdReset1.gameObject.SetActive(false);
        fullResolutionButton.gameObject.SetActive(false);
        twoThirdsResolutionButton.gameObject.SetActive(false);
        halfResolutionButton.gameObject.SetActive(false);
        GameUtils.GetChildGameObject(pauseOverlay, "DebugLabelResolution", true).SetActive(false);
#endif

        challengeStartAnimator = GameUtils.GetChildGameObject(battleCanvas, "ChallengeStartContainer", true).GetComponent<Animator>();


        challengeStartPanelTimer = 0f;
        paused = false;
        matchStatus = EMatchStatus.OnGoing;
        initWavePaused = false;
        initWaveStarted = false;
        getMoreConsumableCounters = new int[(int)GameUtils.EConsumable.Count] { 0, 0, 0, 0, 0 };
        waveTextField.text = "";
        waveProgression.value = 0f;
        waveProgression.gameObject.SetActive(false);
        waveNextField.gameObject.SetActive(false);
        waveStartButton.gameObject.SetActive(false);
        notificationField.text = "";
        scoreTextField.text = "";
        starRewardPrizesShown = false;
        timePlayed = 0f;
        dailyMissionScore = 0;
    }

    private void Start()
    {
        initialWaveDelayTimer = WAVE_INITIAL_DELAY;
    }

    private void OnRewardAdClosedHandler(bool finished, PackData pack)
    {
        if (finished)
        {
            getMoreConsumableCounters[getMoreConsumableIndex]++;
            HideGetMoreConsumablePanel();
            ConsumableController.Instance.ActivateConsumable(getMoreConsumableIndex, true, ConsumableController.EConsumableTrigger.ADVERTISMENT);
        }
    }

    public override bool FixedUpdate()
    {
        if (!base.FixedUpdate()) return false;
        if (!getMoreOverlay.activeSelf && !pauseOverlay.activeSelf) return false;

        bool enable = AdvertismentController.Instance.AdvertismentReady();
        if (!enable || !CanGetMoreConsumable(getMoreConsumableIndex))
        {
#if UNITY_EDITOR || DEBUG_MOBILE
            debugWatchAdButton1.interactable = false;
#endif
            getMoreWatchButton.interactable = false;
        }
        else
        {
#if UNITY_EDITOR || DEBUG_MOBILE
            debugWatchAdButton1.interactable = true;
#endif
            getMoreWatchButton.interactable = true;
        }
        
        getMoreBuyButton.interactable = CanGetMoreConsumable(getMoreConsumableIndex) && CurrencyController.Instance.Currency.gold >= GameUtils.CONSUMABLE_DEPLETED_GOLD_COST;

        return true;
    }

    private void PopulateWavesData()
    {
        wavesData = ChallengeController.Instance.GetCurrentChallengeWavesData();
    }

    public override bool Update()
    {
        if (!base.Update()) return false;
        if (!ChallengeController.Instance.IsSceneSetup) return false;
        if (!isSceneInitialized) InitScene();

        // Tick thoses even if paused
        UpdateChallengeStartPanel();
        UpdateMatchEndPanel();        

        if (paused) return false;
        if (!WeaponController.Instance.WeaponsPlaced()) return false;
        if (!UpdateInitialWaveDelay()) return false;

        UpdateWaveInfo();
        UpdateTimeBetweenWaves();
        UpdateNotification();
        UpdateScreenScore();

        timePlayed += Time.deltaTime;

        return true;
    }

    private void InitScene()
    {
        currentChallengeType = ChallengeController.Instance.GetCurrentChallenge().GetChallengeType();

        // Map name
        string mapName = "";
        string challengeNumber = ChallengeController.Instance.GetCurrentChallenge().GetChallengeNumber();
        string challengeLocalizedName = LocalizationManager.Instance.GetLocalizedString(ChallengeController.Instance.GetCurrentChallengeData().challengeNameID);
        if (currentChallengeType == ChallengeData.EChallengeType.LastStand)
        {            
            switch (ChallengeController.Instance.GetCurrentChallengeDifficulty())
            {
                case GameUtils.EGameDifficulty.Normal:
                    mapName = LocalizationManager.Instance.GetLocalizedString(LocalizationManager.NORMAL_LAST_STAND_TITLE);
                    break;
                case GameUtils.EGameDifficulty.Hard:
                    mapName = LocalizationManager.Instance.GetLocalizedString(LocalizationManager.HARD_LAST_STAND_TITLE);
                    break;
                case GameUtils.EGameDifficulty.Insane:
                    mapName = LocalizationManager.Instance.GetLocalizedString(LocalizationManager.INSANE_LAST_STAND_TITLE);
                    break;
            }
        }
        else
        {
            mapName = LocalizationManager.Instance.GetLocalizedString(LocalizationManager.LEVEL_TEXT);
        }
        challengeNameText.text = mapName + " " + challengeNumber + " - " + challengeLocalizedName;
        challengeNamePauseText.text = challengeNameText.text;

        // Grind cards        
        ChallengeData.GrindCardRarity[] grindCardRarities = ChallengeController.Instance.GetCurrentChallengeGrindCardRarities();
        if (grindCardRarities != null && grindCardRarities.Length > 0)
        {
            startChallengeButton.gameObject.SetActive(true);
            grindCardsPanel.SetActive(true);

            // Set the amount of grind cards reward
            int grindCardMaxAmount = ChallengeController.Instance.GetGrindCardMaxAmount();
            if (grindCardMaxAmount > 1)
            {
                grindCardRewardAmountText.text = "1 - " + grindCardMaxAmount + " " + LocalizationManager.Instance.GetLocalizedString(LocalizationManager.CARDS_TEXT);
            }
            else
            {
                grindCardRewardAmountText.text = grindCardMaxAmount + " " + LocalizationManager.Instance.GetLocalizedString(LocalizationManager.CARDS_TEXT);
            }

            // Sort them by rarity
            grindCardRarities = grindCardRarities.OrderBy(ob => ob.rarity).ToArray();

            // For each grind card
            int maxCards = Math.Min(grindCardRarities.Length, GameUtils.MAX_GRIND_CARDS_AMOUNT);

            RectTransform lootContainerRect = lootTableContainer.GetComponent<RectTransform>();
            float lootWidth = lootContainerRect.rect.width;
            float cardsDistance = 180f;
            float cardsYOffset = -53f;
            float restWidth = (lootWidth - (maxCards * cardsDistance)) / (maxCards + 1);
            float usedCardDistance = cardsDistance + restWidth;
            Vector2 pos = lootContainerRect.anchoredPosition;
      
            for (int i = 0; i < maxCards; ++i)
            {
                //GameObject grindCardDesc = GameUtils.GetChildGameObject(grindCardsPanel, "rewardDescription (" + i + ")", true);
                GameObject grindCardDesc = (GameObject)Instantiate(Resources.Load("prefab_rewardDescription"));
                grindCardDesc.transform.SetParent(lootTableContainer.transform, false);
               
                pos.x = i * usedCardDistance - ((maxCards - 1) / 2f * usedCardDistance);
                pos.y = cardsYOffset;
                grindCardDesc.GetComponent<RectTransform>().anchoredPosition = pos;
                GameUtils.GetChildGameObject(grindCardDesc, "cardIcon", true).GetComponent<Image>().sprite = GameUtils.GetGrindCardWeaponIcon(grindCardRarities[i].weaponID);
                GameUtils.GetChildGameObject(grindCardDesc, "NameText", true).GetComponent<Text>().text = GameUtils.GetWeaponUpgradeLocalizedName(grindCardRarities[i].weaponID, grindCardRarities[i].weaponUpgrade);

                if (grindCardRarities[i].rarity <= GameUtils.RARITY_LEVEL_RARE_MAX_VALUE)
                {
                    GameUtils.GetChildGameObject(grindCardDesc, "RarityRareText", true).SetActive(true);
                }
                else if (grindCardRarities[i].rarity <= GameUtils.RARITY_LEVEL_UNCOMMON_MAX_VALUE)
                {
                    GameUtils.GetChildGameObject(grindCardDesc, "RarityUncommonText", true).SetActive(true);
                }
                else
                {
                    GameUtils.GetChildGameObject(grindCardDesc, "RarityCommonText", true).SetActive(true);
                }                
            }
        }
        else
        {
            startChallengeButton.gameObject.SetActive(false);
            grindCardsPanel.SetActive(false);

            if (!ChallengeController.Instance.IsTutorial())
            {
                // TEMP :: Until we have enemy units info on all maps
                challengeStartPanelTimer = CHALLENGE_START_PANEL_SLIDE_OUT_DELAY;
            }            
        }        
        
        waveLocalizedName = LocalizationManager.Instance.GetLocalizedString(LocalizationManager.WAVE_TEXT);
        
        isSceneInitialized = true;
    }

    private bool UpdateInitialWaveDelay()
    {
        if (initWavePaused) return false;

        if (initialWaveDelayTimer > 0f)
        {
            initialWaveDelayTimer -= Time.deltaTime;

            if (initialWaveDelayTimer <= 0f)
            {
                InitWave();
            }
            else
            {
                return false;
            }
        }

        return true;
    }

    public void StartInitWaveIn(float initWaveDelay)
    {
        initialWaveDelayTimer = initWaveDelay;
    }

    private void InitWave()
    {
        currentWaveIndex = 0;
        currentWaveNumber = 1;
        currentLastStandRoundIndex = 0;

        initWaveStarted = true;

        PopulateWavesData();

        amountOfBases = BaseController.Instance.Bases.Count;
        allBasesHealth = CountAllBasesHealth();

        scoreTextField.text = screenScore.ToString("D6");

        RunWave();
    }

    private void UpdateWaveInfo()
    {
        // Update wave duration
        if (waveDurationTimer > 0f)
        {
            // If not at maximum enemy
            if (!EnemyController.Instance.HasMaxEnemies())
            {
                waveDurationTimer -= Time.deltaTime;
                waveProgression.value = 1f - (waveDurationTimer / waveDuration);

                if (waveDurationTimer <= 0f)
                { 
                    if (currentChallengeType == ChallengeData.EChallengeType.LastStand)
                    {
                        // Wait until all enemies are dead to start the next round
                        waitingForNextLastStandRound = true;
                    }
                    else
                    {
                        WaveFinished();
                    }                    
                }
            }            
        }

        // updating the idle timer
        if (waitingForNextLastStandRound)
        {
            lastStandWaveIdleTimer -= Time.deltaTime;
        }

        // If waiting for next round, check if all enemies are dead
        if (waitingForNextLastStandRound && (EnemyController.Instance.HasClearedAllNonBossEnemies() || lastStandWaveIdleTimer < 0))
        {
            // Start the next round
            WaveFinished();
            // reset the idle timer
            lastStandWaveIdleTimer = lastStandIdleTimerForWaveStart;

            waitingForNextLastStandRound = false;
        }

        // Update wave numbers
        if (currentChallengeType == ChallengeData.EChallengeType.Mission)
        {
            waveTextField.text = waveLocalizedName + " " + (currentWaveNumber).ToString("D2") + "/" + wavesData.Length.ToString("D2");
        }
        else if (currentChallengeType == ChallengeData.EChallengeType.LastStand)
        {
            // For Last Stands, hide the total number of waves
            waveTextField.text = waveLocalizedName + " " + (currentWaveNumber).ToString("D2");
        }
        else if (currentChallengeType == ChallengeData.EChallengeType.Operation)
        {
            waveTextField.text = waveLocalizedName + " " + (currentWaveNumber).ToString("D2") + "/" + wavesData.Length.ToString("D2");
        }
    }

    private void UpdateTimeBetweenWaves()
    {
        if (timeBetweenWavesTimer > 0f)
        {
            timeBetweenWavesTimer -= Time.deltaTime;

            waveNextField.text = LocalizationManager.Instance.GetLocalizedString(LocalizationManager.NEXT_WAVE_IN_TEXT) + " " + timeBetweenWavesTimer.ToString("0") + " " + "sec";

            if (timeBetweenWavesTimer <= 0f)
            {
                StartNextWave();
            }
        }
    }

    private void UpdateNotification()
    {
        if (notificationTimer > 0f)
        {
            notificationTimer -= Time.deltaTime;

            if (notificationTimer <= (WAVE_NOTIFICATION_DURATION / 3f))
            {
                Color notificationColor = notificationField.color;
                notificationColor.a -= (WAVE_NOTIFICATION_DURATION / 3f) * Time.deltaTime;
                notificationField.color = notificationColor;
            }

            if (notificationTimer <= 0f)
            {
                notificationField.text = "";
            }
        }
    }

    private void UpdateScreenScore()
    {
        if (screenScore < currentScore)
        {
            // If the timer isn't started (aka just got a new value to add to the score)
            // or another new value arrived close to the same time as another value
            if (scoreIncreaseTimer <= 0f
                || scoreIncreaseRate != (currentScore - screenScore))
            {
                scoreIncreaseTimer = 1f;
                scoreIncreaseRate = currentScore - screenScore;
            }
            
            scoreIncreaseTimer -= Time.deltaTime;

            if (scoreIncreaseTimer <= 0f)
            {
                screenScore = currentScore;
            }
            else
            {
                // Note :: If < 1, set it to 1 to see an increase
                int scoreIncreaseClamped = (int)(scoreIncreaseRate * Time.deltaTime);
                scoreIncreaseClamped = Mathf.Clamp(scoreIncreaseClamped, 1, int.MaxValue);
                screenScore += scoreIncreaseClamped;
            }

            // Note :: D6 = Show 6 numbers
            // i.e. -1234 ("D6") -> -001234
            scoreTextField.text = screenScore.ToString("D6");
        }        
    }

    private void UpdateChallengeStartPanel()
    {
        if (challengeStartPanelTimer > 0f)
        {
            challengeStartPanelTimer -= Time.deltaTime;

            if (challengeStartPanelTimer <= 0f)
            {
                challengeStartAnimator.SetTrigger(SLIDE_OUT_TRIGGER);
            }
        }
    }

    private void UpdateMatchEndPanel()
    {
        // If the star reward prizes have not been shown
        // and the challenge has been cleared
        if (!starRewardPrizesShown
            && challengeRewardsToOpen != null)
        {
            // If there are prizes to open
            if (challengeRewardsToOpen.Count > 0)
            {
                Animator matchEndAnimator = matchEndPanel.GetComponent<Animator>();

                // Show the prizes only after the match end anim
                if (matchEndAnimator.GetCurrentAnimatorStateInfo(0).IsName("anim_matchEndPanel")
                    && matchEndAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime > OPEN_PRIZE_ANIM_TRANSITION_NORMALIZED_TIME)
                {
                    // Show star rewards
                    PrizeController.Instance.InitOpenPrize(challengeRewardsToOpen);

                    // Don't show the star reward again
                    starRewardPrizesShown = true;

                    // Enable the continue button
                    matchEndMenuButton.interactable = true;
                }
            }
            else
            {
                // Enable the continue button
                matchEndMenuButton.interactable = true;
            }         
        }
    }

    private void RunWave()
    {
        EnemyController.Instance.SetWaveEnemies(wavesData[currentWaveIndex]);
        waveDuration = wavesData[currentWaveIndex].GetWaveDuration(ChallengeController.Instance.GetCurrentChallengeDifficulty(), currentChallengeType);
        waveDurationTimer = waveDuration;

        ShowNotification(LocalizationManager.Instance.GetLocalizedString(LocalizationManager.WAVE_TEXT) + " " + currentWaveNumber);
        waveProgression.gameObject.SetActive(true);
        waveNextField.gameObject.SetActive(false);
        waveStartButton.gameObject.SetActive(false);

        // Sound hook - New wave sound
        AudioManager.Instance.PlaySound(AudioManager.ESoundType.NewWave);

        //Debug.Log("Starting Wave " + (currentWaveIndex + 1));
    }

    private void ShowNotification(string msg)
    {
        notificationField.text = msg;
        Color notificationColor = notificationField.color;
        notificationColor.a = 1f;
        notificationField.color = notificationColor;
        notificationTimer = WAVE_NOTIFICATION_DURATION;
    }

    public void WaveFinished()
    {
        // If is Last Stand 
        if (currentChallengeType == ChallengeData.EChallengeType.LastStand)
        {
            // If is at the last wave
            if (IsCurrentWaveLast())
            {
                // Increment the last stand round index
                currentLastStandRoundIndex++;

                // Reset the wave index
                currentWaveIndex = -1;
            }

            // Set time between waves which will trigger the start of the next wave
            timeBetweenWavesTimer = ChallengeController.Instance.GetCurrentChallengeTimeBetweenWaves();

            // Update UI for the time between waves
            waveProgression.gameObject.SetActive(false);
            waveNextField.gameObject.SetActive(true);
            waveStartButton.gameObject.SetActive(false);
        }
        else if (currentChallengeType == ChallengeData.EChallengeType.Mission)
        {
            // If not at the last wave
            if (!IsCurrentWaveLast())
            {
                // Set time between waves which will trigger the start of the next wave
                timeBetweenWavesTimer = ChallengeController.Instance.GetCurrentChallengeTimeBetweenWaves();

                // Update UI for the time between waves
                waveProgression.gameObject.SetActive(false);
                waveNextField.gameObject.SetActive(true);
                waveStartButton.gameObject.SetActive(true);
            }
        }
        else if (currentChallengeType == ChallengeData.EChallengeType.Operation)
        {
            // If not at the last wave
            if (!IsCurrentWaveLast())
            {
                // Set time between waves which will trigger the start of the next wave
                timeBetweenWavesTimer = ChallengeController.Instance.GetCurrentChallengeTimeBetweenWaves();

                // Update UI for the time between waves
                waveProgression.gameObject.SetActive(false);
                waveNextField.gameObject.SetActive(true);
                waveStartButton.gameObject.SetActive(true);
            }
        }

        // Achievement hook - Wave type
        AchievementController.Instance.UpdateAchievementProgression(AchievementData.EAchievementType.Wave, 1, AchievementData.EAchievementOverride.Progress);
    }
    
    private void StartNextWave()
    {
        currentWaveIndex++;
        currentWaveNumber++;
        RunWave();
    }

    private void ClearedWaves()
    {
        matchStatus = EMatchStatus.Cleared;

        // Get the number of stars the player had before the match ended
        originalStars = ChallengeController.Instance.GetCurrentChallenge().stars;

        // Challenge cleared
        ChallengeController.Instance.ClearedChallenge(CountStars(), out challengeRewardsToOpen);

        MatchEnded();        
    }

    public void FailedWaves()
    {
        // If last stand
        if (currentChallengeType == ChallengeData.EChallengeType.LastStand)
        {
            // Never consider the match has being failed
            ClearedWaves();
            return;
        }

        matchStatus = EMatchStatus.Failed;

        MatchEnded();
    }

    private void AbortMatch()
    {
        // If last stand
        if (currentChallengeType == ChallengeData.EChallengeType.LastStand)
        {
            // Never consider the match has being aborted
            ClearedWaves();
            return;
        }

        matchStatus = EMatchStatus.Aborted;

        MatchEnded();
    }

    private void MatchEnded()
    {
        FlowStateController.Instance.UnregisterBackButton(PauseOn);
        FlowStateController.Instance.RegisterBackButton(MenuButtonPressed);
        AudioManager.Instance.FadeOutBattleMusic();
        paused = true;

        bool isLastStand = currentChallengeType == ChallengeData.EChallengeType.LastStand;

        if (matchEndPanel)
        {
            matchEndPanel.SetActive(true);
        }

        string matchEndTitleFullTextID = "";
        Color matchEndTitleColor = Color.white;
        switch (matchStatus)
        {
            case EMatchStatus.Cleared:
                if (isLastStand)
                {
                    matchEndTitleFullTextID = LocalizationManager.WAVE_TEXT + " " + currentWaveNumber;
                }
                else
                {
                    matchEndTitleFullTextID = LocalizationManager.LEVEL_CLEARED_TEXT;
                }
                matchEndTitleColor = Color.green;
                break;
            case EMatchStatus.Failed:
                matchEndTitleFullTextID = LocalizationManager.LEVEL_FAILED_TEXT;
                matchEndTitleColor = Color.red;
                break;
            case EMatchStatus.Aborted:
                matchEndTitleFullTextID = LocalizationManager.LEVEL_RETREAT_TEXT;
                matchEndTitleColor = Color.yellow;
                break;
        }
        if (matchEndInfoTitle)
        {
            matchEndInfoTitle.GetComponent<LocalizedText>().LocalizedFullText = matchEndTitleFullTextID;
            matchEndInfoTitle.color = matchEndTitleColor;
            matchEndInfoTitle.enabled = true;
        }
        if (matchEndMenuButton)
        {
            matchEndMenuButton.gameObject.SetActive(true);
            // Note :: If cleared, we will enable the menu button after the open prize
            matchEndMenuButton.interactable = matchStatus != EMatchStatus.Cleared;
        }
        bool showRetry = false;
        if (currentChallengeType == ChallengeData.EChallengeType.LastStand)
        {
            showRetry = true;
        }
        else
        {
            showRetry = matchStatus != EMatchStatus.Cleared;
        }
        if (matchEndRetryButton)
        {
            matchEndRetryButton.gameObject.SetActive(showRetry);
        }
        
        // Populate match end stats
        GameObject statsPanel = GameUtils.GetChildGameObject(matchEndPanel, "StatsPanel", true);
        GameUtils.GetChildGameObject(statsPanel, "SectorText", true).GetComponent<LocalizedText>().LocalizedTextID = ChallengeController.Instance.GetCurrentChallengeData().challengeNameID;
        int challengeCredit = matchStatus == EMatchStatus.Cleared ? ChallengeController.Instance.GetMatchCreditReward() : 0;
        GameUtils.GetChildGameObject(statsPanel, "SectorScore", true).GetComponent<Text>().text = "+" + challengeCredit;
        GameUtils.GetChildGameObject(statsPanel, "KillScore", true).GetComponent<Text>().text = "+" + killScore;
        GameUtils.GetChildGameObject(statsPanel, "LootCredit", true).GetComponent<Text>().text = "+" + lootCreditScore;
        GameUtils.GetChildGameObject(statsPanel, "LootGold", true).GetComponent<Text>().text = "+" + lootGoldScore;
        GameObject waveSkipContainer = GameUtils.GetChildGameObject(statsPanel, "wave", true);
        waveSkipContainer.SetActive(!isLastStand);
        GameUtils.GetChildGameObject(waveSkipContainer, "WaveBonusScore", true).GetComponent<Text>().text = "+" + waveBonusScore;
        GameUtils.GetChildGameObject(statsPanel, "MultiKillScore", true).GetComponent<Text>().text = "+" + multiKillScore;
        int boosterCredits = (boosterKillScore + boosterLootCreditScore + boosterMatchRewardScore + boosterMultiKillScore + boosterWaveBonusScore);
        int matchCredtis = (challengeCredit + killScore + lootCreditScore + waveBonusScore + multiKillScore);
        int totalMatchCredits = matchCredtis + boosterCredits + dailyMissionScore;
        float percentage = ((float)boosterCredits / (float)matchCredtis * 100f);
        GameUtils.GetChildGameObject(statsPanel, "BoosterText", true).GetComponent<LocalizedText>().LocalizedFullText = LocalizationManager.BONUS_BOOSTER_TEXT + " " + percentage.ToString("F0") + "%";
        GameUtils.GetChildGameObject(statsPanel, "BoosterScore", true).GetComponent<Text>().text = "+" + boosterCredits;
        int matchGold = lootGoldScore;
        bool isCurrentLevelBoss = ChallengeController.Instance.IsCurrentLevelBoss();
        GameObject grindCardContainer = GameUtils.GetChildGameObject(statsPanel, "special", true);
        grindCardContainer.SetActive(isCurrentLevelBoss && !isLastStand);
        int grindCardReward = matchStatus == EMatchStatus.Cleared ? ChallengeController.Instance.GrindCardAmountRolled : 0;
        GameUtils.GetChildGameObject(grindCardContainer, "SpecialAmount", true).GetComponent<Text>().text = "+" + grindCardReward;
        GameObject lastStandBonusContainer = GameUtils.GetChildGameObject(statsPanel, "lastStandBonus", true);
        lastStandBonusContainer.SetActive(isLastStand);
        int lastStandBonusReward = matchStatus == EMatchStatus.Cleared ? ChallengeController.Instance.GetLastStandBonusRewardAmount() : 0;
        GameUtils.GetChildGameObject(lastStandBonusContainer, "LastStandBonusAmount", true).GetComponent<Text>().text = "+" + lastStandBonusReward;

        // daily
        bool isDaily = dailyMissionScore > 0;
        GameObject dailyContainer = GameUtils.GetChildGameObject(statsPanel, "daily", true);
        dailyContainer.SetActive(isDaily);
        if (isDaily)
        {
            GameUtils.GetChildGameObject(dailyContainer, "DailyScore", true).GetComponent<Text>().text = "+" + dailyMissionScore;
        }

        // Populate the star rewards panel
        int matchStars = CountStars();
        GameObject oneStarRewardPanel = GameUtils.GetChildGameObject(matchEndPanel, "OneStarRewardPanel", true);
        bool hasClearedOneStar = matchStars > 0;
        bool hasClearedOneStarFirstTime = originalStars < 1 && hasClearedOneStar;
        GameUtils.GetChildGameObject(oneStarRewardPanel, "StarOff", true).SetActive(!hasClearedOneStar);
        GameUtils.GetChildGameObject(oneStarRewardPanel, "StarOn", true).SetActive(hasClearedOneStar);
        GameUtils.GetChildGameObject(oneStarRewardPanel, "StarAnimated", true).SetActive(hasClearedOneStar);
        GameUtils.GetChildGameObject(oneStarRewardPanel, "PrizeVisible", true).SetActive(hasClearedOneStarFirstTime);
        int oneStarGoldReward = hasClearedOneStarFirstTime ? ChallengeController.Instance.GetOneStarGoldRewardAmount() : 0;
        GameUtils.GetChildGameObject(oneStarRewardPanel, "PrizeText", true).GetComponent<Text>().text = "+ " + oneStarGoldReward;
        GameUtils.GetChildGameObject(oneStarRewardPanel, "PrizeWon", true).SetActive(originalStars >= 1 && hasClearedOneStar);
        GameObject firstObjectiveOnPanel = GameUtils.GetChildGameObject(oneStarRewardPanel, "ObjectiveOn", true);
        firstObjectiveOnPanel.SetActive(hasClearedOneStar);
        GameObject firstObjectiveOffPanel = GameUtils.GetChildGameObject(oneStarRewardPanel, "ObjectiveOff", true);
        firstObjectiveOffPanel.SetActive(!hasClearedOneStar);
        GameObject activeFirstObjectivePanel = hasClearedOneStar ? firstObjectiveOnPanel : firstObjectiveOffPanel;
        string firstObjectiveTextID = isLastStand ? LocalizationManager.LAST_STAND_FIRST_OBJECTIVE_DESC_TEXT : LocalizationManager.MISSION_FIRST_OBJECTIVE_DESC_TEXT;
        GameUtils.GetChildGameObject(activeFirstObjectivePanel, "ObjectiveDesc", true).GetComponent<LocalizedText>().LocalizedTextID = firstObjectiveTextID;

        GameObject twoStarRewardPanel = GameUtils.GetChildGameObject(matchEndPanel, "TwoStarRewardPanel", true);
        bool hasClearedTwoStar = matchStars > 1;
        bool hasClearedTwoStarFirstTime = originalStars < 2 && hasClearedTwoStar;
        GameUtils.GetChildGameObject(twoStarRewardPanel, "StarOff", true).SetActive(!hasClearedTwoStar);
        GameUtils.GetChildGameObject(twoStarRewardPanel, "StarOn", true).SetActive(hasClearedTwoStar);
        GameUtils.GetChildGameObject(twoStarRewardPanel, "StarAnimated", true).SetActive(hasClearedTwoStar);
        GameUtils.GetChildGameObject(twoStarRewardPanel, "PrizeVisible", true).SetActive(hasClearedTwoStarFirstTime);
        int twoStarCardReward = hasClearedTwoStarFirstTime ? ChallengeController.Instance.GetTwoStarCardRewardAmount() : 0;
        GameUtils.GetChildGameObject(twoStarRewardPanel, "PrizeText", true).GetComponent<Text>().text = "+ " + twoStarCardReward;
        GameUtils.GetChildGameObject(twoStarRewardPanel, "PrizeWon", true).SetActive(originalStars >= 2 && hasClearedTwoStar);
        GameObject secondObjectiveOnPanel = GameUtils.GetChildGameObject(twoStarRewardPanel, "ObjectiveOn", true);
        secondObjectiveOnPanel.SetActive(hasClearedTwoStar);
        GameObject secondObjectiveOffPanel = GameUtils.GetChildGameObject(twoStarRewardPanel, "ObjectiveOff", true);
        secondObjectiveOffPanel.SetActive(!hasClearedTwoStar);
        GameObject activeSecondObjectivePanel = hasClearedTwoStar ? secondObjectiveOnPanel : secondObjectiveOffPanel;
        string secondObjectiveTextID = isLastStand ? LocalizationManager.LAST_STAND_SECOND_OBJECTIVE_DESC_TEXT : LocalizationManager.MISSION_SECOND_OBJECTIVE_DESC_TEXT;
        GameUtils.GetChildGameObject(activeSecondObjectivePanel, "ObjectiveDesc", true).GetComponent<LocalizedText>().LocalizedTextID = secondObjectiveTextID;

        GameObject threeStarRewardPanel = GameUtils.GetChildGameObject(matchEndPanel, "ThreeStarRewardPanel", true);
        bool hasClearedThreeStar = matchStars > 2;
        bool hasClearedThreeStarFirstTime = originalStars < 3 && hasClearedThreeStar;
        GameUtils.GetChildGameObject(threeStarRewardPanel, "StarOff", true).SetActive(!hasClearedThreeStar);
        GameUtils.GetChildGameObject(threeStarRewardPanel, "StarOn", true).SetActive(hasClearedThreeStar);
        GameUtils.GetChildGameObject(threeStarRewardPanel, "StarAnimated", true).SetActive(hasClearedThreeStar);
        GameUtils.GetChildGameObject(threeStarRewardPanel, "PrizeVisible", true).SetActive(hasClearedThreeStarFirstTime);
        int threeStarCreditReward = hasClearedThreeStarFirstTime ? ChallengeController.Instance.GetThreeStarCreditRewardAmount() : 0;
        GameUtils.GetChildGameObject(threeStarRewardPanel, "PrizeText", true).GetComponent<Text>().text = "+ " + threeStarCreditReward;
        GameUtils.GetChildGameObject(threeStarRewardPanel, "PrizeWon", true).SetActive(originalStars >= 3 && hasClearedThreeStar);
        GameObject thirdObjectiveOnPanel = GameUtils.GetChildGameObject(threeStarRewardPanel, "ObjectiveOn", true);
        thirdObjectiveOnPanel.SetActive(hasClearedThreeStar);
        GameObject thirdObjectiveOffPanel = GameUtils.GetChildGameObject(threeStarRewardPanel, "ObjectiveOff", true);
        thirdObjectiveOffPanel.SetActive(!hasClearedThreeStar);
        GameObject activeThirdObjectivePanel = hasClearedThreeStar ? thirdObjectiveOnPanel : thirdObjectiveOffPanel;
        string thirdObjectiveTextID = isLastStand ? LocalizationManager.LAST_STAND_THIRD_OBJECTIVE_DESC_TEXT : LocalizationManager.MISSION_THIRD_OBJECTIVE_DESC_TEXT;
        GameUtils.GetChildGameObject(activeThirdObjectivePanel, "ObjectiveDesc", true).GetComponent<LocalizedText>().LocalizedTextID = thirdObjectiveTextID;

        // Populate total rewards
        int totalCredits = (totalMatchCredits + threeStarCreditReward + lastStandBonusReward);
        int totalGold = (matchGold + oneStarGoldReward);
        int totalCards = (twoStarCardReward + grindCardReward);
        GameObject totalRewardPanel = GameUtils.GetChildGameObject(matchEndPanel, "TotalRewardPanel", true);
        GameUtils.GetChildGameObject(totalRewardPanel, "TotalCredit", true).GetComponent<Text>().text = "+" + totalCredits;
        GameUtils.GetChildGameObject(totalRewardPanel, "TotalGold", true).GetComponent<Text>().text = "+" + totalGold;
        GameUtils.GetChildGameObject(totalRewardPanel, "TotalCards", true).GetComponent<Text>().text = "+" + totalCards;

        // Populate the last stand info
        GameObject lastStandPanel = GameUtils.GetChildGameObject(matchEndPanel, "LastStandPanel", true);
        lastStandPanel.SetActive(isLastStand);
        GameUtils.GetChildGameObject(lastStandPanel, "ScoreText", true).GetComponent<Text>().text = currentScore.ToString();

        // Sound hook - Match Won/Lost sound
        AudioManager.Instance.PlaySound(matchStatus == EMatchStatus.Cleared ? AudioManager.ESoundType.MatchWon : AudioManager.ESoundType.MatchLost);
    }

    private void StartChallengeButtonClicked()
    {
        challengeStartAnimator.SetTrigger(SLIDE_OUT_TRIGGER);
    }

    private void MenuButtonPressed()
    {
        EnemyController.Instance.CleanUp();
        FlowStateController.Instance.SetState(FlowStateController.FlowStates.Menu);
    }
    
    private void RetryButtonPressed()
    {
        EnemyController.Instance.CleanUp();
        FlowStateController.Instance.SetState(FlowStateController.FlowStates.Battle);
    }

    private void AbortButtonPressed()
    {
        PauseButtonPressed(false);
        AbortMatch();
    }

    private void PauseOn()
    {
        PauseButtonPressed(true);
        FlowStateController.Instance.UnregisterBackButton(PauseOn);
        FlowStateController.Instance.RegisterBackButton(AbortButtonPressed);
    }

    private void PauseButtonPressed(bool pause)
    {
        if (!pause)
        {
            FlowStateController.Instance.UnregisterBackButton(AbortButtonPressed);
            FlowStateController.Instance.RegisterBackButton(PauseOn);
        }
        paused = pause;
        if (pauseOverlay)
        {
            pauseOverlay.SetActive(pause);
        }

        SetAudioValues();
    }

    private void SetAudioValues()
    {
        musicSlider.value = AudioManager.Instance.GetMusicVolumeJSON();
        soundSlider.value = AudioManager.Instance.GetSoundVolumeJSON();
    }

    private void MusicSliderValueChanged()
    {
        GameUtils.GetChildGameObject(musicSlider.transform.parent.gameObject, "TextPourcentage").GetComponent<Text>().text = ((int)(musicSlider.value * 100)) + "%";
        AudioManager.Instance.SetMusicVolume(musicSlider.value);
    }

    private void SoundSliderValueChanged()
    {
        GameUtils.GetChildGameObject(soundSlider.transform.parent.gameObject, "TextPourcentage").GetComponent<Text>().text = ((int)(soundSlider.value * 100)) + "%";
        AudioManager.Instance.SetSoundVolume(soundSlider.value);
    }

    private void WaveStartButtonClicked()
    {
        GiveCreditsForWaveTimer();
        timeBetweenWavesTimer = 0f;
        StartNextWave();
    }

    private void GiveCreditsForWaveTimer()
    {
        if (timeBetweenWavesTimer > 0)
        {
            int amount = Mathf.CeilToInt(timeBetweenWavesTimer) * GetWaveBonusCreditPerSecond();
            RecordCredits(amount, GameUtils.CreditsSource.WaveNextPressed);
            if (BoosterController.Instance.BoosterTypeActive(BoosterController.EBoosterPackType.Credits))
            {
                int bonus = Mathf.CeilToInt(amount * BoosterController.Instance.waveTimer) - amount;
                RecordCredits(bonus, GameUtils.CreditsSource.BoosterWaveNextPressed);
                amount += bonus;
            }
            Vector3 waveStartImagePos = GameUtils.GetChildGameObject(waveStartButton.gameObject, "Image").transform.position;
            Vector3 flyingTextPos = waveStartImagePos + new Vector3(10f, -50f);
            AddCredits(amount, flyingTextPos, GameUtils.CreditsSource.WaveNextPressed, FlyingTextObject.EFlyingTextDirection.Down);
        }
    }

#if UNITY_EDITOR || DEBUG_MOBILE
    private void ResolutionButtonPressed(SettingsController.EResolutionType resolutionType)
    {
        // Note :: This won't save the graphic setting
        SettingsController.Instance.ChangeResolution(resolutionType);
    }

    private void DebugCheatButton1Clicked()
    {
        // Allow instant win once the first wave has started
        if (wavesData == null)
            return;

        PauseButtonPressed(false);
        if (currentChallengeType == ChallengeData.EChallengeType.LastStand)
        {
            currentLastStandRoundIndex = 3;
        }

        // Simulate the kill rewards
        // For each wave
        for (int i = 0; i < wavesData.Length; ++i)
        {
            // For each enemy type
            for (int j = 0; j < wavesData[i].enemyInfo.Length; ++j)
            {
                int individualScore = EnemyController.Instance.GetEnemyData(wavesData[i].enemyInfo[j].type).GetEnemyAttributes(ChallengeController.Instance.GetCurrentChallengeDifficulty()).Score;
                int totalScore = individualScore * wavesData[i].enemyInfo[j].amount;
                RecordCredits(totalScore, GameUtils.CreditsSource.EnemyDestroyed);
                AddCredits(totalScore, Vector3.zero, GameUtils.CreditsSource.EnemyDestroyed, FlyingTextObject.EFlyingTextDirection.Left);
            }            
        }

        // Simulate wave skip rewards halven
        int waveSkipCredits = (int)ChallengeController.Instance.GetCurrentChallengeTimeBetweenWaves() * GetWaveBonusCreditPerSecond() * wavesData.Length;
        waveSkipCredits /= 2;
        RecordCredits(waveSkipCredits, GameUtils.CreditsSource.WaveNextPressed);
        AddCredits(waveSkipCredits, Vector3.zero, GameUtils.CreditsSource.WaveNextPressed, FlyingTextObject.EFlyingTextDirection.Left);

        // Simulate a double kill reward per wave
        int multiKillRewards = GameUtils.DOUBLE_KILL_CREDIT_REWARD * wavesData.Length;
        RecordCredits(multiKillRewards, GameUtils.CreditsSource.MultiKillReward);
        AddCredits(multiKillRewards, Vector3.zero, GameUtils.CreditsSource.MultiKillReward, FlyingTextObject.EFlyingTextDirection.Left);

        // Simulate one credit convoy per match halven
        int convoyCredits = LootController.Instance.creditLootValues.RollLootValue(ChallengeController.Instance.GetCurrentChallengeDifficulty());
        convoyCredits /= 2;
        RecordCredits(convoyCredits, GameUtils.CreditsSource.LootReward);
        AddCredits(convoyCredits, Vector3.zero, GameUtils.CreditsSource.LootReward, FlyingTextObject.EFlyingTextDirection.Left);

        ClearedWaves();
    }

    private void DebugCheatButton2Clicked()
    {
        EnemyController.Instance.SpawnEnemy(GameUtils.UnitType.CONVOY, Vector3.zero, true, false);
    }

    private void DebugWatchAdButtonClicked()
    {
        AdvertismentController.Instance.RequestAdvertisment(OnRewardAdClosedHandler, "DebugDecisionPoint", null);
    }

    private void DebugWatchAdResetButtonClicked()
    {
        AdvertismentController.Instance.ResetAdvertismentsWatched();
    }
#endif

    private void WatchAdButtonClicked()
    {
        AdvertismentController.Instance.RequestAdvertisment(OnRewardAdClosedHandler, "ConsumableDepleted", null);
    }

    private void HideGetMoreConsumablePanel()
    {
        getMoreConsumablePanel.SetActive(false);
        paused = false;
    }

    private void GetMoreBuyButtonClicked()
    {
        if (CurrencyController.Instance.Currency.gold >= GameUtils.CONSUMABLE_DEPLETED_GOLD_COST)
        {
            CurrencyController.Instance.ModifyGoldAmount(-GameUtils.CONSUMABLE_DEPLETED_GOLD_COST, GameUtils.GoldSource.ConsumableDepleted, DataController.SAVEPRIORITY.QUEUE);

            getMoreConsumableCounters[getMoreConsumableIndex]++;
            HideGetMoreConsumablePanel();
            ConsumableController.Instance.ActivateConsumable(getMoreConsumableIndex, true, ConsumableController.EConsumableTrigger.GOLD);
        }
    }

    public GameObject SpawnFlyingText(string msg, Vector3 pos, FlyingTextObject.EFlyingTextType type, Color fontColor, FontStyle fontStyle, FlyingTextObject.EFlyingTextBehaviour behaviour, FlyingTextObject.EFlyingTextDirection dir)
    {
        GameObject flyingText = GetFlyingTextFromPool();
        FlyingTextObject flyingTextObject = flyingText.GetComponent<FlyingTextObject>();

        FlyingTextObject.FlyingTextInfo info = new FlyingTextObject.FlyingTextInfo();
        info.direction = dir;
        info.position = pos;
        info.type = type;
        info.fontColor = fontColor;
        info.fontStyle = fontStyle;
        info.behaviour = behaviour;

        flyingTextObject.Init(info, msg);

        return flyingText;
    }

    public void RecordCredits(int amount, GameUtils.CreditsSource source)
    {
        switch(source)
        {
            case GameUtils.CreditsSource.EnemyDestroyed: killScore += amount; break;
            case GameUtils.CreditsSource.LootReward: lootCreditScore += amount; break;
            case GameUtils.CreditsSource.MultiKillReward: multiKillScore += amount; break;
            case GameUtils.CreditsSource.WaveNextPressed: waveBonusScore += amount; break;
            case GameUtils.CreditsSource.BoosterEnemeyDestroyed: boosterKillScore += amount; break;
            case GameUtils.CreditsSource.BoosterLootReward: boosterLootCreditScore += amount; break;
            case GameUtils.CreditsSource.BoosterMatchReward: boosterMatchRewardScore += amount; break;
            case GameUtils.CreditsSource.BoosterMultiKillReward: boosterMultiKillScore += amount; break;
            case GameUtils.CreditsSource.BoosterWaveNextPressed: boosterWaveBonusScore += amount; break;
            case GameUtils.CreditsSource.DailyMission: dailyMissionScore += amount; break;
        }
    }

    public void AddCredits(int amount, Vector3 pos, GameUtils.CreditsSource source, FlyingTextObject.EFlyingTextDirection dir)
    {
        SpawnFlyingText("+" + amount.ToString("D"),
                        pos,
                        FlyingTextObject.EFlyingTextType.Credits,
                        GameUtils.COLOR_YELLOW,
                        FontStyle.Normal,
                        FlyingTextObject.EFlyingTextBehaviour.Normal,
                        dir);

        currentScore += amount;
        CurrencyController.Instance.ModifyCreditsAmount(amount, source, DataController.SAVEPRIORITY.NORMAL);
    }

    public void AddGold(int amount, Vector3 pos, GameUtils.GoldSource source, FlyingTextObject.EFlyingTextDirection dir)
    {
        // If source was loot
        if (source == GameUtils.GoldSource.LootReward)
        {
            // Track the loot score in order to show in the match end stats
            lootGoldScore += amount;
        }

        SpawnFlyingText("+" + amount.ToString(),
                        pos,
                        FlyingTextObject.EFlyingTextType.Gold,
                        GameUtils.COLOR_YELLOW,
                        FontStyle.Normal,
                        FlyingTextObject.EFlyingTextBehaviour.Normal,
                        dir);

        currentScore += amount;
        CurrencyController.Instance.ModifyGoldAmount(amount, source, DataController.SAVEPRIORITY.NORMAL);
    }

    private int CountStars()
    {
        int stars = 0;

        if (currentChallengeType == ChallengeData.EChallengeType.LastStand)
        {
            if (currentLastStandRoundIndex == 1)
            {
                stars = 1;
            }
            else if (currentLastStandRoundIndex == 2)
            {
                stars = 2;
            }
            else if (currentLastStandRoundIndex >= 3)
            {
                stars = 3;
            }
        }
        else
        {
            if (matchStatus == EMatchStatus.Cleared)
            {
                // One star for clearing the level
                stars = 1;

                // Two Stars for having all bases alive
                if (BaseController.Instance.Bases.Count == amountOfBases)
                {
                    stars = 2;
                }

                // Three Stars if all bases are full health
                if (allBasesHealth == CountAllBasesHealth())
                {
                    stars = 3;
                }

                // Tutorial override - Always give 3 stars
                if (ChallengeController.Instance.IsTutorial())
                {
                    stars = 3;
                }
            }
        }        
        
        return stars;
    }

    private float CountAllBasesHealth()
    {
        float totalHealth = 0;
        for (int i = 0; i < BaseController.Instance.Bases.Count; ++i)
        {
            GameObject baseObject = BaseController.Instance.Bases.ElementAt(i).Value;
            if (baseObject != null)
            {
                totalHealth += baseObject.GetComponent<BaseObject>().Health;
            }
        }
        return totalHealth;
    }

    public void CheckIfClearedWaves()
    {
        if (matchStatus == EMatchStatus.Failed) return;

        // Infinite loop for Last Stands
        if (currentChallengeType == ChallengeData.EChallengeType.LastStand) return;

        if (IsCurrentWaveLast()
            && EnemyController.Instance.HasClearedAllEnemies() 
            && EnemyController.Instance.HasClearedAllLoot())
        {
            ClearedWaves();
        }
    }

    private bool IsCurrentWaveLast()
    {
        return (currentWaveIndex + 1) == wavesData.Length;        
    }

    public void ManageMultipleKills(int enemyKillCount, Vector3 pos)
    {
        int multipleKillReward = 0;
        string multipleKillTextID = "";
        
        switch (enemyKillCount)
        {
            case 2:
                multipleKillTextID = LocalizationManager.DOUBLE_KILL_TEXT;
                multipleKillReward = GameUtils.DOUBLE_KILL_CREDIT_REWARD;
                break;
            case 3:
                multipleKillTextID = LocalizationManager.TRIPLE_KILL_TEXT;
                multipleKillReward = GameUtils.TRIPLE_KILL_CREDIT_REWARD;
                break;
            case 4:
                multipleKillTextID = LocalizationManager.QUADRA_KILL_TEXT;
                multipleKillReward = GameUtils.QUADRA_KILL_CREDIT_REWARD;
                break;
            case 5:
                multipleKillTextID = LocalizationManager.PENTA_KILL_TEXT;
                multipleKillReward = GameUtils.PENTA_KILL_CREDIT_REWARD;
                break;
            case 6:
                multipleKillTextID = LocalizationManager.HEXA_KILL_TEXT;
                multipleKillReward = GameUtils.HEXA_KILL_CREDIT_REWARD;
                break;
            case 7:
                multipleKillTextID = LocalizationManager.HEPTA_KILL_TEXT;
                multipleKillReward = GameUtils.HEPTA_KILL_CREDIT_REWARD;
                break;
            case 8:
                multipleKillTextID = LocalizationManager.OCTA_KILL_TEXT;
                multipleKillReward = GameUtils.OCTA_KILL_CREDIT_REWARD;
                break;
            case 9:
                multipleKillTextID = LocalizationManager.NONA_KILL_TEXT;
                multipleKillReward = GameUtils.NONA_KILL_CREDIT_REWARD;
                break;
            case 10:
                multipleKillTextID = LocalizationManager.DECA_KILL_TEXT;
                multipleKillReward = GameUtils.DECA_KILL_CREDIT_REWARD;
                break;
        }
        
        // If there is atleast a double kill        
        if (multipleKillReward > 0)
        {
            RecordCredits(multipleKillReward, GameUtils.CreditsSource.MultiKillReward);
            if (BoosterController.Instance.BoosterTypeActive(BoosterController.EBoosterPackType.Credits))
            {
                int bonus = Mathf.CeilToInt(multipleKillReward * BoosterController.Instance.multiKillCredits) - multipleKillReward;
                RecordCredits(bonus, GameUtils.CreditsSource.BoosterMultiKillReward);
                multipleKillReward += bonus;
            }

            // Reward the multiple kill
            AddCredits(multipleKillReward, pos + new Vector3(0, 30, 0), GameUtils.CreditsSource.MultiKillReward, FlyingTextObject.EFlyingTextDirection.Up);

            // Spawn a flying text
            SpawnFlyingText(LocalizationManager.Instance.GetLocalizedString(multipleKillTextID),
                            pos,
                            FlyingTextObject.EFlyingTextType.Damage,
                            Color.red,
                            FontStyle.Normal,
                            FlyingTextObject.EFlyingTextBehaviour.Normal,
                            FlyingTextObject.EFlyingTextDirection.Up);
        }
    }

    public void CheckGetMoreConsumables(int consumableIndex)
    {
        // if already used in this match return
        if (!CanGetMoreConsumable(consumableIndex))
            return;

        // remember which index
        getMoreConsumableIndex = consumableIndex;

        // pause game
        paused = true;

        // set gold available
        getMoreGoldAvailableText.text = CurrencyController.Instance.Currency.gold.ToString();
        getMoreBuyCostText.text = GameUtils.CONSUMABLE_DEPLETED_GOLD_COST.ToString();

        // show panel
        getMoreConsumablePanel.SetActive(true);
        ConsumableController.Instance.SetObjectToConsumableButtonPosition(consumableIndex, getMoreOverlay);
    }

    public void EnableBattleUI(bool isEnable)
    {
        battleCanvas.GetComponent<CanvasGroup>().interactable = isEnable;
        battleCanvas.GetComponent<CanvasGroup>().blocksRaycasts = isEnable;
    }

    private int GetWaveBonusCreditPerSecond()
    {
        switch (ChallengeController.Instance.GetCurrentChallengeDifficulty())
        {
            case GameUtils.EGameDifficulty.Hard:
                return waveBonusCreditPerSecondHard;
            case GameUtils.EGameDifficulty.Insane:
                return waveBonusCreditPerSecondInsane;
            case GameUtils.EGameDifficulty.Normal:
            default:
                return waveBonusCreditPerSecondNormal;
        }
    }

    public bool CanGetMoreConsumable(int consumableIndex)
    {
        return getMoreConsumableCounters[consumableIndex] < GameUtils.GET_MORE_CONSUMABLE_LIMIT_PER;
    }

    public override void OnDestroy()
    {
        base.OnDestroy();
        if (!FlowStateController.ApplicationIsQuitting)
        {
            FlowStateController.Instance.UnregisterBackButton(PauseOn);
            FlowStateController.Instance.UnregisterBackButton(MenuButtonPressed);
            FlowStateController.Instance.UnregisterBackButton(AbortButtonPressed);
        }
    }

    public void PutFlyingTextInPool(GameObject flyingText)
    {
        flyingText.SetActive(false);
        inactiveFlyingText.Enqueue(flyingText);
    }

    public GameObject GetFlyingTextFromPool()
    {
        GameObject flyingText;
        if (inactiveFlyingText.Count > 0)
        {
            flyingText = inactiveFlyingText.Dequeue();
            flyingText.SetActive(true);
            return flyingText;
        }

        flyingText = (GameObject)Instantiate(Resources.Load("FlyingText"));
        flyingText.transform.parent = flyingTextContainer.transform;

        return flyingText;
    }
}

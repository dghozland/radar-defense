using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;
//#if RECEIPT_VALIDATION
using UnityEngine.Purchasing.Security;
//#endif

// Deriving the Purchaser class from IStoreListener enables it to receive messages from Unity Purchasing.
public class PurchaseController : UnitySingleton<PurchaseController>, IStoreListener
{
    private static IStoreController storeController;          // The Unity Purchasing system.
    private static IExtensionProvider storeExtensionProvider; // The store-specific Purchasing subsystems.
    private static List<ProductData> productData;

#if RECEIPT_VALIDATION
    private static CrossPlatformValidator validator;
#endif

    public override void Awake()
    {
        base.Awake();

        // load the product data
        productData = new List<ProductData>(Resources.LoadAll<ProductData>("Products"));

#if RECEIPT_VALIDATION
        validator = new CrossPlatformValidator(GooglePlayTangle.Data(), AppleTangle.Data(), Application.identifier);
#endif
    }

    void Start()
    { 
        // If we haven't set up the Unity Purchasing reference
        if (storeController == null)
        {
            // Begin to configure our connection to Purchasing
            InitializePurchasing();
        }
    }

    public void InitializePurchasing()
    {
        // If we have already connected to Purchasing ...
        if (IsInitialized())
        {
            // ... we are done here.
            return;
        }

        // Create a builder, first passing in a suite of Unity provided stores.
        var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

        // Add a product to sell / restore by way of its identifier, associating the general identifier
        // with its store-specific identifiers.
        for (int i = 0; i < productData.Count; ++i)
        {
            builder.AddProduct(productData[i].storeIdInternal, productData[i].productType, new IDs
            {
                { productData[i].googleStoreID, GooglePlay.Name },
                { productData[i].appleStoreID, AppleAppStore.Name }
            });
        }

        // Kick off the remainder of the set-up with an asynchrounous call, passing the configuration 
        // and this class' instance. Expect a response either in OnInitialized or OnInitializeFailed.
        UnityPurchasing.Initialize(this, builder);
    }


    public bool IsInitialized()
    {
        // Only say we are initialized if both the Purchasing references are set.
        return storeController != null && storeExtensionProvider != null;
    }

    public void BuyCashPack(string storeID)
    {
        BuyProductID(storeID);
    }

    private void BuyProductID(string productId)
    {
        // If Purchasing has been initialized ...
        if (IsInitialized())
        {
            // ... look up the Product reference with the general product identifier and the Purchasing 
            // system's products collection.
            UnityEngine.Purchasing.Product product = storeController.products.WithID(productId);

            // If the look up found a product for this device's store and that product is ready to be sold ... 
            if (product != null && product.availableToPurchase)
            {
                Debug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));
                // ... buy the product. Expect a response either through ProcessPurchase or OnPurchaseFailed 
                // asynchronously.
                storeController.InitiatePurchase(product);
            }
            // Otherwise ...
            else
            {
                // ... report the product look-up failure situation  
                Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
            }
        }
        // Otherwise ...
        else
        {
            // ... report the fact Purchasing has not succeeded initializing yet. Consider waiting longer or 
            // retrying initiailization.
            Debug.Log("BuyProductID FAIL. Not initialized.");
        }
    }

    // gets the price of the product from the store in the right currency
    public string GetProductPrice(string productId)
    {
        if (IsInitialized())
        {
            UnityEngine.Purchasing.Product product = storeController.products.WithID(productId);
            if (product != null && product.availableToPurchase)
            {
                return product.metadata.localizedPriceString;
            }
        }
        return string.Empty;
    }


    // Restore purchases previously made by this customer. Some platforms automatically restore purchases, like Google. 
    // Apple currently requires explicit purchase restoration for IAP, conditionally displaying a password prompt.
    public void RestorePurchases()
    {
        // If Purchasing has not yet been set up ...
        if (!IsInitialized())
        {
            // ... report the situation and stop restoring. Consider either waiting longer, or retrying initialization.
            Debug.Log("RestorePurchases FAIL. Not initialized.");
            return;
        }

        // If we are running on an Apple device ... 
        if (Application.platform == RuntimePlatform.IPhonePlayer ||
            Application.platform == RuntimePlatform.OSXPlayer)
        {
            // ... begin restoring purchases
            Debug.Log("RestorePurchases started ...");

            // Fetch the Apple store-specific subsystem.
            var apple = storeExtensionProvider.GetExtension<IAppleExtensions>();
            // Begin the asynchronous process of restoring purchases. Expect a confirmation response in 
            // the Action<bool> below, and ProcessPurchase if there are previously purchased products to restore.
            apple.RestoreTransactions((result) => {
                // The first phase of restoration. If no more responses are received on ProcessPurchase then 
                // no purchases are available to be restored.
                Debug.Log("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
            });
        }
        // Otherwise ...
        else
        {
            // We are not running on an Apple device. No work is necessary to restore purchases.
            Debug.Log("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
        }
    }

    public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
    {
        // Purchasing has succeeded initializing. Collect our Purchasing references.
        Debug.Log("OnInitialized: PASS");

        // Overall Purchasing system, configured with products for this application.
        storeController = controller;
        // Store specific subsystem, for accessing device-specific store features.
        storeExtensionProvider = extensions;
    }


    public void OnInitializeFailed(InitializationFailureReason error)
    {
        // Purchasing set-up has not succeeded. Check error for reason. Consider sharing this reason with the user.
        Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
    }


    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
    {
        IPurchaseReceipt receipt = null;
#if RECEIPT_VALIDATION
        if (Application.platform == RuntimePlatform.Android ||
			Application.platform == RuntimePlatform.IPhonePlayer)
        {
			try
            {
				var results = validator.Validate(args.purchasedProduct.receipt);
#if RELEASE_BETA
                Debug.Log("Receipt is valid. Contents:");
#endif
                foreach (IPurchaseReceipt productReceipt in results)
                {
                    receipt = productReceipt;
#if RELEASE_BETA
                    Debug.Log(productReceipt.productID);
					Debug.Log(productReceipt.purchaseDate);
					Debug.Log(productReceipt.transactionID);
#endif
                    bool validReceipt = false;
#if UNITY_ANDROID
                    GooglePlayReceipt google = productReceipt as GooglePlayReceipt;
					if (null != google)
                    {
  #if RELEASE_BETA
                        Debug.Log(google.purchaseState);
						Debug.Log(google.purchaseToken);
  #endif
                        if (google.purchaseState == GooglePurchaseState.Purchased)
                        {
                            validReceipt = true;
                        }
                    }
#elif UNITY_IOS
                    AppleInAppPurchaseReceipt apple = productReceipt as AppleInAppPurchaseReceipt;
					if (null != apple)
                    {
  #if RELEASE_BETA
                        Debug.Log(apple.originalTransactionIdentifier);
						Debug.Log(apple.subscriptionExpirationDate);
						Debug.Log(apple.cancellationDate);
						Debug.Log(apple.quantity);
  #endif
                        if (!string.IsNullOrEmpty(apple.originalTransactionIdentifier))
                        {
                            validReceipt = true;
                        }
					}
#endif
                    if (!validReceipt)
                    {
                        Debug.Log("Invalid receipt, no valid Receipt data, not unlocking content");

                        return PurchaseProcessingResult.Complete;
                    }
                }
            }
            catch (IAPSecurityException)
            {
                Debug.Log("Invalid receipt, not unlocking content");

				return PurchaseProcessingResult.Complete;
			}
		}
#endif

        for (int i = 0; i < productData.Count; ++i)
        {
            if (String.Equals(productData[i].storeIdInternal, args.purchasedProduct.definition.id, StringComparison.Ordinal))
            {
                Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
                PackController.Instance.RewardPackFromStore(productData[i].storeIdInternal, args, receipt);
                return PurchaseProcessingResult.Complete;
            }
        }

        // something went wront, the product did not exist in the product data
        Debug.Log(string.Format("ProcessPurchase: FAIL. Unrecognized product: '{0}'", args.purchasedProduct.definition.id));

        // Return a flag indicating whether this product has completely been received, or if the application needs 
        // to be reminded of this purchase at next app launch. Use PurchaseProcessingResult.Pending when still 
        // saving purchased products to the cloud, and when that save is delayed. 
        return PurchaseProcessingResult.Complete;
    }

    public void OnPurchaseFailed(UnityEngine.Purchasing.Product product, PurchaseFailureReason failureReason)
    {
        // A product purchase attempt did not succeed. Check failureReason for more detail. Consider sharing 
        // this reason with the user to guide their troubleshooting actions.
        Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
    }

}
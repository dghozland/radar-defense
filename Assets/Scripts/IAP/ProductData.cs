using UnityEngine;
using UnityEngine.Purchasing;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class ProductData : ScriptableObject
{
    [Tooltip("The name used in the Pack Data")]
    [SerializeField]
    public string storeIdInternal;

    [Tooltip("The type of product, consumable, nonconsumable or subscription")]
    [SerializeField]
    public ProductType productType;

    [Tooltip("The Id used in the Google Developer Console")]
    [SerializeField]
    public string googleStoreID;

    [Tooltip("The Id used in the Apple Developer Console")]
    [SerializeField]
    public string appleStoreID;

#if UNITY_EDITOR
    [MenuItem("Assets/Create/Gameplay/Product")]
    public static void CreateAsset()
    {
        GameUtils.CreateAsset<ProductData>();
    }
#endif
}

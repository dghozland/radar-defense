using System.Linq;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(RarityData))]
public class EditorRarityData : Editor
{
    private RarityData rarityData;

    public override void OnInspectorGUI()
    {
        rarityData = (RarityData)target;
        DrawWeaponRarityFields();
        DrawWeaponUpgradeRarityFields();
        DrawConsumableRarityFields();

        EditorUtils.DrawHorizontalSeperator();
        EditorGUILayout.BeginVertical();
        if (GUILayout.Button("Mark Dirty"))
        {
            EditorUtility.SetDirty(rarityData);
        }
        EditorGUILayout.EndVertical();
    }

    private void DrawWeaponRarityFields()
    {
        if (rarityData.weaponCardsRarities.Count == 0)
        {
            for (int j = (int)GameUtils.BaseID.MISSILE; j < (int)GameUtils.BaseID.CITY; ++j)
            {
                rarityData.weaponCardsRarities.Add(0);
            }
        }
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("WEAPON RARITIES", EditorUtils.WhiteLabel);
        EditorGUILayout.EndHorizontal();

        float total = 0;
        EditorGUILayout.BeginVertical();
        for (int j = (int)GameUtils.BaseID.MISSILE; j < (int)GameUtils.BaseID.CITY; ++j)
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(((GameUtils.BaseID)j).ToString(), GUILayout.Width(170));
            rarityData.weaponCardsRarities[j] = EditorGUILayout.IntSlider(rarityData.weaponCardsRarities[j], 0, 100);
            total += rarityData.weaponCardsRarities[j];
            EditorGUILayout.EndHorizontal();
        }
        if (total != 100)
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Total: " + total + " ERROR: MUST equal 100!!!", EditorUtils.RedLabel);
            EditorGUILayout.EndHorizontal();
        }
        else
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Total: " + total, EditorUtils.GreenLabel);
            EditorGUILayout.EndHorizontal();
        }
        EditorGUILayout.EndVertical();
    }

    private void DrawWeaponUpgradeRarityFields()
    {
        if (rarityData.weaponUpgradeCardsRarities.Count == 0)
        {
            for (int j = (int)GameUtils.EWeaponUpgrade.SPEED; j < (int)GameUtils.EWeaponUpgrade.COUNT; ++j)
            {
                rarityData.weaponUpgradeCardsRarities.Add(0);
            }
        }
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("WEAPON UPGRADE RARITIES", EditorUtils.WhiteLabel);
        EditorGUILayout.EndHorizontal();

        float total = 0;
        EditorGUILayout.BeginVertical();
        for (int j = (int)GameUtils.EWeaponUpgrade.SPEED; j < (int)GameUtils.EWeaponUpgrade.COUNT; ++j)
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(((GameUtils.EWeaponUpgrade)j).ToString(), GUILayout.Width(170));
            rarityData.weaponUpgradeCardsRarities[j] = EditorGUILayout.IntSlider(rarityData.weaponUpgradeCardsRarities[j], 0, 100);
            total += rarityData.weaponUpgradeCardsRarities[j];
            EditorGUILayout.EndHorizontal();
        }
        if (total != 100)
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Total: " + total + " ERROR: MUST equal 100!!!", EditorUtils.RedLabel);
            EditorGUILayout.EndHorizontal();
        }
        else
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Total: " + total, EditorUtils.GreenLabel);
            EditorGUILayout.EndHorizontal();
        }
        EditorGUILayout.EndVertical();
    }

    private void DrawConsumableRarityFields()
    {
        if (rarityData.consumableCardsRarities.Count == 0 || rarityData.consumableCardsRarities.Count < (int)GameUtils.EConsumable.Count)
        {
            rarityData.consumableCardsRarities.Clear();
            for (int j = (int)GameUtils.EConsumable.Slower; j < (int)GameUtils.EConsumable.Count; ++j)
            {
                rarityData.consumableCardsRarities.Add(0);
            }
        }
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("CONSUMABLE RARITIES", EditorUtils.WhiteLabel);
        EditorGUILayout.EndHorizontal();

        float total = 0;
        EditorGUILayout.BeginVertical();
        for (int j = (int)GameUtils.EConsumable.Slower; j < (int)GameUtils.EConsumable.Count; ++j)
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(((GameUtils.EConsumable)j).ToString(), GUILayout.Width(170));
            rarityData.consumableCardsRarities[j] = EditorGUILayout.IntSlider(rarityData.consumableCardsRarities[j], 0, 100);
            total += rarityData.consumableCardsRarities[j];
            EditorGUILayout.EndHorizontal();
        }
        if (total != 100)
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Total: " + total + " ERROR: MUST equal 100!!!", EditorUtils.RedLabel);
            EditorGUILayout.EndHorizontal();
        }
        else
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Total: " + total, EditorUtils.GreenLabel);
            EditorGUILayout.EndHorizontal();
        }
        EditorGUILayout.EndVertical();
    }
}

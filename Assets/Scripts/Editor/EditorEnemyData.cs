using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(EnemyData))]
public class EditorEnemyData : Editor
{
    private EnemyData enemyData;

    public override void OnInspectorGUI()
    {
        enemyData = (EnemyData)target;
        DrawDefaultInspector();
        DrawCustomInspector();
    }

    private void DrawCustomInspector()
    {
        EditorGUILayout.BeginVertical();


        if (GUILayout.Button("Mark Dirty"))
        {
            EditorUtility.SetDirty(enemyData);
        }

        EditorGUILayout.EndVertical();

    }
}
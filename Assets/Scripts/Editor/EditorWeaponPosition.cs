using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(WeaponPositionObject))]
public class EditorWeaponPosition : Editor
{
    private WeaponPositionObject positionObject;

    public override void OnInspectorGUI()
    {
        positionObject = (WeaponPositionObject)target;
        DrawDefaultInspector();
        CheckNode();
    }

    private void CheckNode()
    {
        EditorUtils.DrawHorizontalSeperator();
        EditorGUILayout.BeginVertical();
        EditorGUILayout.BeginHorizontal();
        if (!CheckNodeNearBy())
        {
            if (positionObject.Node == null)
            {
                GUILayout.Label("WARNING: NO NODE ATTACHED! For Weapons this can be OK!", EditorUtils.YellowLabel);
            }
            else if (CheckDistance())
            {
                GUILayout.Label("CAUTION: BuildingPosition & Node are too far from each other!", EditorUtils.YellowLabel);
                if (GUILayout.Button("Adjust"))
                {
                    positionObject.Node.transform.position = positionObject.transform.position;
                    UnityEditor.SceneManagement.EditorSceneManager.MarkAllScenesDirty();
                    EditorUtility.SetDirty(positionObject);
                }
            }
            else
            {
                GUILayout.Label("Node Attached and close in position - ALL OK!", EditorUtils.GreenLabel);
            }
        }
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.EndVertical();
        EditorUtils.DrawHorizontalSeperator();
    }

    private bool CheckDistance()
    {
        return (!GameUtils.CompareVectors(positionObject.transform.position, positionObject.Node.transform.position, 10f));
    }

    private bool CheckNodeNearBy()
    {
        var allPathNodes = GameObject.FindObjectsOfType<PathNode>();
        foreach (var pathNode in allPathNodes)
        {
            if (pathNode != positionObject.Node && GameUtils.CompareVectors(positionObject.transform.position, pathNode.transform.position, 10f))
            {
                GUILayout.Label("WARNING: NODE " + pathNode.name + " is near, attach?", EditorUtils.YellowLabel);
                if (GUILayout.Button("Attach"))
                {
                    positionObject.Node = pathNode;
                    UnityEditor.SceneManagement.EditorSceneManager.MarkAllScenesDirty();
                    EditorUtility.SetDirty(positionObject);
                }
                return true;
            }
        }
        return false;
    }
}

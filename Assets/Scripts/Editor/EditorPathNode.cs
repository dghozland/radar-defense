using UnityEditor;
using UnityEngine;
using System.Collections.Generic;

[CustomEditor(typeof(PathNode))]
public class EditorPathNode : Editor
{
    private PathNode node;
    private int index = 0;

    public override void OnInspectorGUI()
    {
        node = (PathNode)target;
        DrawDefaultInspector();
        DrawButtons();
        CheckPosition();
    }
    
    private void DrawButtons()
    {
        EditorUtils.DrawHorizontalSeperator();
        EditorGUILayout.BeginVertical();
        EditorGUILayout.BeginHorizontal();
        Object[] allPathObjects = GameObject.FindObjectsOfType(typeof(PathNode));
        List<string> names = new List<string>();
        foreach(var obj in allPathObjects)
        {
            names.Add(obj.name);
        }
        if (GUILayout.Button("Add New Connected Node"))
        {
            GameObject parent = GameObject.Find("PathSystem");
            if (parent==null)
            {
                parent = new GameObject("PathSystem");
                node.transform.parent = parent.transform;
            }
            GameObject newNode = (GameObject)Instantiate(Resources.Load("Node"));
            newNode.name = "Node_" + allPathObjects.Length;
            newNode.transform.position = node.transform.position + (Vector3.right * 100f);
            newNode.transform.parent = parent.transform;

            PathNode newPathNode = newNode.GetComponent<PathNode>();
            newPathNode.ConnectedNodes.Add(node);
            node.ConnectedNodes.Add(newPathNode);
            Selection.activeObject = newPathNode;
            Repaint();
            SceneView.RepaintAll();
        }
        EditorGUILayout.EndHorizontal();

        index = EditorGUILayout.Popup(index, names.ToArray());

        EditorGUILayout.BeginHorizontal();
        if (GUILayout.Button("Add Connection to") && names.Count > 0)
        {
            PathNode connectionNode = (PathNode)allPathObjects[index];
            connectionNode.ConnectedNodes.Add(node);
            node.ConnectedNodes.Add(connectionNode);
            Selection.activeObject = connectionNode;
            Repaint();
            SceneView.RepaintAll();
        }
        if (GUILayout.Button("Split Between") && names.Count > 0)
        {
            GameObject parent = GameObject.Find("PathSystem");
            PathNode splitNode = (PathNode)allPathObjects[index];
            GameObject newNode = (GameObject)Instantiate(Resources.Load("Node"));
            newNode.name = "Node_" + allPathObjects.Length;
            newNode.transform.position = splitNode.gameObject.transform.position + ((node.transform.position - splitNode.gameObject.transform.position) / 2f);
            newNode.transform.parent = parent.transform;

            PathNode newPathNode = newNode.GetComponent<PathNode>();
            newPathNode.ConnectedNodes.Add(node);
            newPathNode.ConnectedNodes.Add(splitNode);
            node.ConnectedNodes.Add(newPathNode);
            node.ConnectedNodes.Remove(splitNode);
            splitNode.ConnectedNodes.Add(newPathNode);
            splitNode.ConnectedNodes.Remove(node);
            Selection.activeObject = newPathNode;
            Repaint();
            SceneView.RepaintAll();
        }
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.EndVertical();
        EditorUtils.DrawHorizontalSeperator();
    }

    private void CheckPosition()
    {
        //if (!PlayfieldController.Instance.IsInsidePlayArea(node.transform.position))
        //{
        //    GUILayout.Label("CAUTION: Node " + node.name + " outside the playarea! Adjusting Position Automatically!");
        //    Vector3 pos = node.gameObject.transform.position;

        //    float minX = PlayfieldController.Instance.BottomLeft.x + PlayfieldController.Instance.WidthAdjust;
        //    float maxX = PlayfieldController.Instance.TopRight.x - PlayfieldController.Instance.WidthAdjust;
        //    float minY = PlayfieldController.Instance.BottomLeft.y + PlayfieldController.Instance.HeightAdjust;
        //    float maxY = PlayfieldController.Instance.TopRight.y - PlayfieldController.Instance.HeightAdjust;

        //    if (pos.x < minX) pos.x = minX + 0.1f;
        //    if (pos.y < minY) pos.y = minY + 0.1f;
        //    if (pos.x > maxX) pos.x = maxX - 0.1f;
        //    if (pos.y > maxY) pos.y = maxY - 0.1f;

        //    node.transform.position = pos;
        //    UnityEditor.SceneManagement.EditorSceneManager.MarkAllScenesDirty();
        //}
    }
}

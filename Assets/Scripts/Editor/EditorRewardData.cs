using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(RewardData))]
public class EditorRewardData : Editor
{
    private RewardData rewardData;

    public override void OnInspectorGUI()
    {
        rewardData = (RewardData)target;
        DrawDefaultInspector();
        DrawCustomInspector();
    }

    private void DrawCustomInspector()
    {
        EditorGUILayout.BeginVertical();
        //rewardData.rewardType = (RewardData.ERewardType)EditorGUILayout.EnumPopup("Reward Type", rewardData.rewardType);
        //switch (rewardData.rewardType)
        //{
        //    case RewardData.ERewardType.Pack:
        //        rewardData.upgradeCardAmount = EditorGUILayout.IntField("Upgrade Card Amount", rewardData.upgradeCardAmount);
        //        rewardData.consumableAmount = EditorGUILayout.IntField("Consumable Amount", rewardData.consumableAmount);
        //        if (rewardData.upgradeCardAmount > 0)
        //        {
        //            rewardData.specificWeapon = (GameUtils.BaseID)EditorGUILayout.EnumPopup("Specific Weapon", rewardData.specificWeapon);
        //            rewardData.specificWeaponUpgrade = (GameUtils.EWeaponUpgrade)EditorGUILayout.EnumPopup("Specific Weapon Upgrade", rewardData.specificWeaponUpgrade);
        //        }
        //        break;
        //    case RewardData.ERewardType.Credit:
        //    case RewardData.ERewardType.Gold:
        //        rewardData.currencyAmount = EditorGUILayout.IntField("Currency Amount", rewardData.currencyAmount);
        //        break;
        //}
        if (GUILayout.Button("Mark Dirty"))
        {
            EditorUtility.SetDirty(rewardData);
        }

        EditorGUILayout.EndVertical();

    }
}
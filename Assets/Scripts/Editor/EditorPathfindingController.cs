using UnityEditor;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;

[CustomEditor(typeof(PathFindingController))]
public class EditorPathfindingController : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        DrawButtons();
    }

    private void DrawButtons()
    {
        EditorUtils.DrawHorizontalSeperator();
        EditorGUILayout.BeginVertical();
        EditorGUILayout.BeginHorizontal();
        if (GUILayout.Button("Check Connections"))
        {
            CheckConnections();
        }
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.EndVertical();
        EditorUtils.DrawHorizontalSeperator();
    }

    private void CheckConnections()
    {
        List<PathNode> pathNodes = new List<PathNode>(FindObjectsOfType<PathNode>());
        for (int i = 0; i < pathNodes.Count; ++i)
        {
            PathNode node = pathNodes.ElementAt(i);
            List<PathNode> connections = node.ConnectedNodes;
            for (int j = 0; j < connections.Count; ++j)
            {
                PathNode connection = connections.ElementAt(j);
                if (!connection.ConnectedNodes.Contains(node))
                {
                    Debug.LogError("Node " + connection.name + " is missing reverse connection to " + node.name);
                }
            }
        }
        Debug.Log("Nodes Fine");
    }
}
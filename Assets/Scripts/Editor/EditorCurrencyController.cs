using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(CurrencyController))]
public class EditorCurrencyController : Editor
{
    private CurrencyController currencyController;

    public override void OnInspectorGUI()
    {
        currencyController = (CurrencyController)target;
        DrawDefaultInspector();
        DrawCurrencyButtons();
    }

    private void DrawCurrencyButtons()
    {
        EditorUtils.DrawHorizontalSeperator();
        EditorGUILayout.BeginVertical();
        EditorGUILayout.BeginHorizontal();

        if (GUILayout.Button("Save"))
        {
            currencyController.SaveCurrencies(DataController.SAVEPRIORITY.QUEUE);
        }

        if (GUILayout.Button("Load"))
        {
            currencyController.LoadCurrencies(null);
        }

        EditorGUILayout.EndHorizontal();
        EditorGUILayout.EndVertical();
    }
}
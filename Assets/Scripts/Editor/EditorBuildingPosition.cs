using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(BuildingPositionObject))]
public class EditorBuildingPosition : Editor
{
    private BuildingPositionObject positionObject;

    public override void OnInspectorGUI()
    {
        positionObject = (BuildingPositionObject)target;
        DrawDefaultInspector();
        CheckNode();
    }

    private void CheckNode()
    {
        EditorUtils.DrawHorizontalSeperator();
        EditorGUILayout.BeginVertical();
        EditorGUILayout.BeginHorizontal();
        if (!CheckNodeNearBy())
        {
            if (positionObject.Node == null)
            {
                GUILayout.Label("CAUTION: NO NODE ATTACHED! NOT ALLOWED - PLACE A NODE TO THE BUILDING!", EditorUtils.RedLabel);
            }
            else if (CheckDistance())
            {
                GUILayout.Label("WARNING: BuildingPosition & Node are too far from each other!", EditorUtils.YellowLabel);
                if (GUILayout.Button("Adjust"))
                {
                    positionObject.Node.transform.position = positionObject.transform.position;
                    UnityEditor.SceneManagement.EditorSceneManager.MarkAllScenesDirty();
                    EditorUtility.SetDirty(positionObject);
                }
            }
            else
            {
                GUILayout.Label("Node Attached and close in position - ALL OK!", EditorUtils.GreenLabel);
            }
        }
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.EndVertical();
        EditorUtils.DrawHorizontalSeperator();
    }

    private bool CheckDistance()
    {
        return (!GameUtils.CompareVectors(positionObject.transform.position, positionObject.Node.transform.position, 10f));
    }

    private bool CheckNodeNearBy()
    {
        var allPathNodes = GameObject.FindObjectsOfType<PathNode>();
        foreach (var pathNode in allPathNodes)
        {
            if (pathNode != positionObject.Node && GameUtils.CompareVectors(positionObject.transform.position, pathNode.transform.position, 10f))
            {
                GUILayout.Label("WARNING: NODE " + pathNode.name + " is near, attach?", EditorUtils.YellowLabel);
                if (GUILayout.Button("Attach"))
                {
                    positionObject.Node = pathNode;
                    UnityEditor.SceneManagement.EditorSceneManager.MarkAllScenesDirty();
                    EditorUtility.SetDirty(positionObject);
                }
                return true;
            }
        }
        return false;
    }
}

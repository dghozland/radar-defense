using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(PackData))]
public class EditorPackData : Editor
{
    private PackData packData;

    public override void OnInspectorGUI()
    {
        packData = (PackData)target;
        DrawDefaultInspector();
        DrawCustomInspector();
    }

    private void DrawCustomInspector()
    {
        EditorGUILayout.BeginVertical();

        if (packData.currencyType == GameUtils.ECurrencyType.Cash)
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Product Data", GUILayout.Width(170));
            packData.productData = (ProductData) EditorGUILayout.ObjectField(packData.productData, typeof(ProductData), false);
            EditorGUILayout.EndHorizontal();
            if (packData.productData == null)
            {
                EditorGUILayout.LabelField("YOU MUST ADD A PRODUCT DATA OBJECT!!!", EditorUtils.RedLabel);
            }
        }
        else
        {
            packData.productData = null;
        }

        if (GUILayout.Button("Mark Dirty"))
        {
            EditorUtility.SetDirty(packData);
        }

        EditorGUILayout.EndVertical();

    }
}
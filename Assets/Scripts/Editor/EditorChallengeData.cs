using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(ChallengeData))]
public class EditorChallengeData : Editor
{
    private ChallengeData challengeData;

    public override void OnInspectorGUI()
    {
        challengeData = (ChallengeData)target;
        DrawDefaultInspector();
        DrawCustomAddon();
    }

    private void DrawCustomAddon()
    {
        EditorGUILayout.BeginVertical();

        if (challengeData.challengeType == ChallengeData.EChallengeType.Mission)
        {
            EditorUtils.DrawHorizontalSeperator();

            EditorGUILayout.LabelField("Challenge Durations", EditorUtils.WhiteLabel);
            GUILayout.Label("Normal : " + GetChallengeMissionDuration(GameUtils.EGameDifficulty.Normal, ChallengeData.EChallengeType.Mission, false) + " sec (/w Time Between Waves : " + GetChallengeMissionDuration(GameUtils.EGameDifficulty.Normal, ChallengeData.EChallengeType.Mission, true) + " sec)");
            GUILayout.Label("Hard : " + GetChallengeMissionDuration(GameUtils.EGameDifficulty.Hard, ChallengeData.EChallengeType.Mission, false) + " sec (/w Time Between Waves : " + GetChallengeMissionDuration(GameUtils.EGameDifficulty.Hard, ChallengeData.EChallengeType.Mission, true) + " sec)");
            GUILayout.Label("Insane : " + GetChallengeMissionDuration(GameUtils.EGameDifficulty.Insane, ChallengeData.EChallengeType.Mission, false) + " sec (/w Time Between Waves : " + GetChallengeMissionDuration(GameUtils.EGameDifficulty.Insane, ChallengeData.EChallengeType.Mission, true) + " sec)");
        }
        else if (challengeData.challengeType == ChallengeData.EChallengeType.Operation)
        {
            EditorUtils.DrawHorizontalSeperator();

            EditorGUILayout.LabelField("Challenge Durations", EditorUtils.WhiteLabel);
            GUILayout.Label("Insane : " + GetChallengeMissionDuration(GameUtils.EGameDifficulty.Insane, ChallengeData.EChallengeType.Operation, false) + " sec (/w Time Between Waves : " + GetChallengeMissionDuration(GameUtils.EGameDifficulty.Insane, ChallengeData.EChallengeType.Operation, true) + " sec)");
        }

        if (challengeData.normalGrindCardRarities.Length > 0
            || challengeData.hardGrindCardRarities.Length > 0
            || challengeData.insaneGrindCardRarities.Length > 0)
        {
            EditorUtils.DrawHorizontalSeperator();

            EditorGUILayout.LabelField("Grind Card Rarities", EditorUtils.WhiteLabel);
            int normalGrindCardRarityTotal = GetGrindCardRarityTotal(GameUtils.EGameDifficulty.Normal);
            if (normalGrindCardRarityTotal == 0 || normalGrindCardRarityTotal == 100)
            {
                EditorGUILayout.LabelField("Normal : " + normalGrindCardRarityTotal, EditorUtils.GreenLabel);
            }
            else
            {
                EditorGUILayout.LabelField("Normal : " + normalGrindCardRarityTotal + " ERROR: MUST equal 100!!!", EditorUtils.RedLabel);
            }
            int hardGrindCardRarityTotal = GetGrindCardRarityTotal(GameUtils.EGameDifficulty.Hard);
            if (hardGrindCardRarityTotal == 0 || hardGrindCardRarityTotal == 100)
            {
                EditorGUILayout.LabelField("Hard : " + hardGrindCardRarityTotal, EditorUtils.GreenLabel);
            }
            else
            {
                EditorGUILayout.LabelField("Hard : " + hardGrindCardRarityTotal + " ERROR: MUST equal 100!!!", EditorUtils.RedLabel);
            }
            int insaneGrindCardRarityTotal = GetGrindCardRarityTotal(GameUtils.EGameDifficulty.Insane);
            if (insaneGrindCardRarityTotal == 0 || insaneGrindCardRarityTotal == 100)
            {
                EditorGUILayout.LabelField("Insane : " + insaneGrindCardRarityTotal, EditorUtils.GreenLabel);
            }
            else
            {
                EditorGUILayout.LabelField("Insane : " + insaneGrindCardRarityTotal + " ERROR: MUST equal 100!!!", EditorUtils.RedLabel);
            }
        }

        EditorGUILayout.EndVertical();
    }

    private float GetChallengeMissionDuration(GameUtils.EGameDifficulty difficulty, ChallengeData.EChallengeType challengeType, bool withTimeBetweenWaves)
    {
        float challengeDuration = 0f;
        ChallengeData.WaveData2[] wavesData = challengeData.GetWavesData(difficulty);
        for (int i = 0; i < wavesData.Length; ++i)
        {
            challengeDuration += wavesData[i].GetWaveDuration(difficulty, challengeType);

            if (withTimeBetweenWaves
                && (i+1) < wavesData.Length)
            {
                challengeDuration += challengeData.timeBetweenWaves;
            }
        }
        return challengeDuration;
    }

    private int GetGrindCardRarityTotal(GameUtils.EGameDifficulty difficulty)
    {
        int rarityTotal = 0;
        ChallengeData.GrindCardRarity[] grindCardRarities = challengeData.GetGrindCardRarities(difficulty);
        if (grindCardRarities != null)
        {
            for (int i = 0; i < grindCardRarities.Length; ++i)
            {
                rarityTotal += grindCardRarities[i].rarity;
            }
        }        
        return rarityTotal;
    }
}

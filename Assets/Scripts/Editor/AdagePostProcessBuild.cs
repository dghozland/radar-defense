#if UNITY_IOS
using System.IO;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEditor.iOS.Xcode;


public class AdagePostProcessBuild
{
    [PostProcessBuild]
    public static void ChangeXcodePlist(BuildTarget buildTarget, string pathToBuiltProject)
    {

        if (buildTarget == BuildTarget.iOS)
        {
            // Get plist
            string plistPath = pathToBuiltProject + "/Info.plist";
            PlistDocument plist = new PlistDocument();
            plist.ReadFromString(File.ReadAllText(plistPath));

            // Get root
            PlistElementDict rootDict = plist.root;

            // change the CFBundleVersion to only contain numbers
			string version = PlayerSettings.bundleVersion;
			int index = version.IndexOf ("-");
			if (index > 0) 
			{
				version = version.Substring (0, index);
			}
			UnityEngine.Debug.Log ("Version: " + version);
			rootDict.SetString("CFBundleShortVersionString", version);

            // Add Dict NSAllowsArbitraryLoads
            var newDict = rootDict.CreateDict("NSAppTransportSecurity");
            newDict.SetBoolean("NSAllowsArbitraryLoads", true);

            // no encryption
            rootDict.SetBoolean("ITSAppUsesNonExemptEncryption", false);

            // Write to file
            File.WriteAllText(plistPath, plist.WriteToString());
        }
    }
}
#endif

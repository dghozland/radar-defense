using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(ChallengeController))]
public class EditorChallengeController : Editor
{
    private ChallengeController challengeController;

    public override void OnInspectorGUI()
    {
        challengeController = (ChallengeController)target;
        DrawDefaultInspector();
        DrawChallengeButtons();
    }
    
    private void DrawChallengeButtons()
    {
        EditorUtils.DrawHorizontalSeperator();
        EditorGUILayout.BeginVertical();
        
        EditorGUILayout.BeginHorizontal();
        if (GUILayout.Button("Save"))
        {
            challengeController.SaveChallenges(DataController.SAVEPRIORITY.QUEUE);
        }
        if (GUILayout.Button("Load"))
        {
            challengeController.LoadChallenges(null);
        }
        EditorGUILayout.EndHorizontal();

        //EditorGUILayout.BeginHorizontal();
        //if (GUILayout.Button("Complete"))
        //{
        //    challengeController.CompleteAchievementProgression();
        //}
        //if (GUILayout.Button("Reset"))
        //{
        //    challengeController.ResetAchievementProgression();
        //}
        //EditorGUILayout.EndHorizontal();

        EditorGUILayout.EndVertical();
    }
}
#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

public class EditorUtils
{
    private static GUIStyle guiStyleTitle1;
    public static GUIStyle GuiStyleTitle1
    {
        get
        {
            if (guiStyleTitle1 == null)
            {
                guiStyleTitle1 = new GUIStyle();
                guiStyleTitle1.fontSize = 14;
                guiStyleTitle1.fontStyle = FontStyle.Bold;
                guiStyleTitle1.alignment = TextAnchor.MiddleCenter;
            }

            return guiStyleTitle1;
        }
    }

    private static GUIStyle redLabel;
    public static GUIStyle RedLabel
    {
        get
        {
            if (redLabel == null)
            {
                redLabel = new GUIStyle(EditorStyles.boldLabel);
                redLabel.normal.textColor = Color.red;
            }
            return redLabel;
        }
    }

    private static GUIStyle greenLabel;
    public static GUIStyle GreenLabel
    {
        get
        {
            if (greenLabel == null)
            {
                greenLabel = new GUIStyle(EditorStyles.boldLabel);
                greenLabel.normal.textColor = Color.green;
            }
            return greenLabel;
        }
    }

    private static GUIStyle yellowLabel;
    public static GUIStyle YellowLabel
    {
        get
        {
            if (yellowLabel == null)
            {
                yellowLabel = new GUIStyle(EditorStyles.boldLabel);
                yellowLabel.normal.textColor = Color.yellow;
            }
            return yellowLabel;
        }
    }

    private static GUIStyle whiteLabel;
    public static GUIStyle WhiteLabel
    {
        get
        {
            if (whiteLabel == null)
            {
                whiteLabel = new GUIStyle(EditorStyles.boldLabel);
                whiteLabel.normal.textColor = Color.white;
            }
            return whiteLabel;
        }
    }

    public static void DrawHorizontalSeperator()
    {
        GUILayout.Box("", new GUILayoutOption[] { GUILayout.ExpandWidth(true), GUILayout.Height(1) });
    }
}
#endif

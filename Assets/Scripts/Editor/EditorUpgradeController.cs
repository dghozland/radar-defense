using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

[CustomEditor(typeof(UpgradeController))]
public class EditorUpgradeController : Editor
{
    private bool isInit = false;

    private UpgradeController upgradeController;
    private int[] debugWeaponRanks;

    public override void OnInspectorGUI()
    {
        if (!isInit)
        {
            Init();
        }

        upgradeController = (UpgradeController)target;
        DrawDefaultInspector();
        
        if (Application.isPlaying)
        {
            DrawWeaponUpgrades();
            DrawWeaponButtons();
        }
        else
        {
            EditorUtils.DrawHorizontalSeperator();
            GUILayout.Label("Play the game to debug weapon upgrades.", EditorUtils.YellowLabel);
        }
    }

    private void Init()
    {
        debugWeaponRanks = new int[6];
        isInit = true;
    }

    #region Weapons

    private void DrawWeaponUpgrades()
    {
        if (upgradeController.WeaponUpgrades == null || CardsController.Instance.WeaponCards == null)
        {
            return;
        }
        for (int i = (int)GameUtils.BaseID.MISSILE; i < (int)GameUtils.BaseID.CITY; ++i)
        {
            GameUtils.BaseID weaponID = (GameUtils.BaseID)i;
            EditorUtils.DrawHorizontalSeperator();
            EditorGUILayout.BeginVertical();
            EditorGUILayout.BeginHorizontal();
            GUILayout.Label(weaponID.ToString(), GUILayout.Width(150));
            GUILayout.Label("LVL", GUILayout.Width(50));
            int oldDebugWeaponRank = debugWeaponRanks[i];
            debugWeaponRanks[i] = (int)GUILayout.HorizontalSlider(debugWeaponRanks[i], 0, 10);
            if (oldDebugWeaponRank != debugWeaponRanks[i])
            {
                upgradeController.SetWeaponUpgradesLevel(weaponID, debugWeaponRanks[i]);
            }
            EditorGUILayout.EndHorizontal();
            for (int j = (int)GameUtils.EWeaponUpgrade.SPEED; j < (int)GameUtils.EWeaponUpgrade.COUNT; ++j)
            {
                EditorGUILayout.BeginHorizontal();
                GameUtils.EWeaponUpgrade weaponUpgrade = (GameUtils.EWeaponUpgrade)j;
                GUILayout.Label(weaponUpgrade.ToString(), GUILayout.Width(150));
                GUILayout.Label(upgradeController.GetWeaponUpgradeLevel(weaponID, weaponUpgrade).ToString(), GUILayout.Width(50));
                GUILayout.Label("(" + CardsController.Instance.GetWeaponCards(weaponID, weaponUpgrade).ToString() + ")", GUILayout.Width(50));
                if (GUILayout.Button("Upgrade"))
                {
                    upgradeController.UpgradeWeapon(weaponID, weaponUpgrade, false);
                }
                EditorGUILayout.EndHorizontal();
            }
            EditorGUILayout.EndVertical();
        }
    }

    private void DrawWeaponButtons()
    {
        EditorUtils.DrawHorizontalSeperator();
        EditorGUILayout.BeginVertical();
        EditorGUILayout.BeginHorizontal();
        //if (GUILayout.Button("Load"))
        //{
        //    upgradeController.LoadWeaponUpgrades(null);
        //    CardsController.Instance.LoadWeaponCards(null);
        //}
        //if (GUILayout.Button("Save"))
        //{
        //    upgradeController.SaveWeaponUpgrades(DataController.SAVEPRIORITY.QUEUE);
        //    CardsController.Instance.SaveWeaponCards(DataController.SAVEPRIORITY.QUEUE);
        //}
        if (GUILayout.Button("Max"))
        {
            upgradeController.MaxWeaponUpgrades();
        }
        if (GUILayout.Button("Reset"))
        {
            upgradeController.ResetWeaponUpgrades();
            CardsController.Instance.ResetWeaponCards();
        }
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.BeginHorizontal();
        if (GUILayout.Button("GetPack x 10"))
        {
            for (int i = 0; i < 10; ++i)
            {
                CardsController.Instance.RollWeaponCards(4, new GameUtils.BaseID[0]);
            }
        }
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.BeginHorizontal();
        if (GUILayout.Button("GetPack x 100"))
        {
            for (int i = 0; i < 100; ++i)
            {
                CardsController.Instance.RollWeaponCards(4, new GameUtils.BaseID[0]);
            }
        }
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.EndVertical();
    }

    #endregion // Weapons    
}

using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Linq;
using System;

public class EditorTranslation : EditorWindow
{
    private string[] allLanguages;
    private List<Dictionary<string, string>> referenceLanguage;
    private List<Dictionary<string, string>> currentLanguage;
    private List<Dictionary<string, string>> referenceSearch;
    private List<Dictionary<string, string>> currentSearch;
    private int currentIndex = 0;
    private string setReferenceLanguage;
    private string setCurrentLanguage;
    private int languageIndex = 0;
    private string searchPattern = string.Empty;
    private string newSearchPattern = string.Empty;

    [MenuItem("Window/Translation")]
    static void Init()
    {
        EditorTranslation window = (EditorTranslation)EditorWindow.GetWindow(typeof(EditorTranslation));
        window.Show();
    }

    public void OnGUI()
    {
        EditorUtils.DrawHorizontalSeperator();

        LoadSection();

        EditorUtils.DrawHorizontalSeperator();

        if (referenceSearch != null && referenceSearch.Count > 0 && !string.IsNullOrEmpty(searchPattern))
        {
            ShowSearch();
        }
        else
        {
            ShowLocalization();
        }

        EditorUtils.DrawHorizontalSeperator();

        DoSearch();

    }

    private void LoadAllLanguageFiles()
    {
        allLanguages = System.IO.Directory.GetFiles(Application.dataPath + "/Resources/Localization", "*.json");
        referenceLanguage = null;
        currentLanguage = null;
        currentSearch = null;
        searchPattern = newSearchPattern = string.Empty;
        currentIndex = 0;
        referenceSearch = null;
        setReferenceLanguage = string.Empty;
        setCurrentLanguage = string.Empty; 
    }

    private void LoadSection()
    {
        if (allLanguages == null) LoadAllLanguageFiles();
        EditorGUILayout.BeginVertical();
        if (GUILayout.Button("Reload All Languages"))
        {
            LoadAllLanguageFiles();
        }
        for (int i = 0; i < allLanguages.Length; ++i)
        {
            if (string.IsNullOrEmpty(setReferenceLanguage))
            {
                setReferenceLanguage = allLanguages[i];
            }
            if (!string.IsNullOrEmpty(setReferenceLanguage) && referenceLanguage == null)
            {
                JsonUtils<List<Dictionary<string, string>>>.Load(allLanguages[i], out referenceLanguage, GameUtils.ELoadType.Override );
            }
            EditorGUILayout.BeginHorizontal();

            if (GUILayout.Toggle(setReferenceLanguage == allLanguages[i], allLanguages[i]))
            {
                if (setReferenceLanguage != allLanguages[i])
                {
                    if (allLanguages[i] == setCurrentLanguage)
                    {
                        setCurrentLanguage = null;
                        currentLanguage.Clear();
                        currentLanguage = null;
                    }
                    setReferenceLanguage = allLanguages[i];
                    JsonUtils<List<Dictionary<string, string>>>.Load(allLanguages[i], out referenceLanguage, GameUtils.ELoadType.Override);
                }
            }
            if (setReferenceLanguage == allLanguages[i])
            {
                if (GUILayout.Button("Save"))
                {
                    JsonUtils<List<Dictionary<string, string>>>.Save(allLanguages[i], referenceLanguage, true);
                }
            }
            else if (setCurrentLanguage == allLanguages[i])
            {
                if (GUILayout.Button("Save"))
                {
                    JsonUtils<List<Dictionary<string, string>>>.Save(allLanguages[i], currentLanguage, true);
                }
            }
            else
            {
                if (GUILayout.Button("Load"))
                {
                    setCurrentLanguage = allLanguages[i];
                    JsonUtils<List<Dictionary<string, string>>>.Load(allLanguages[i], out currentLanguage, GameUtils.ELoadType.Override);
                }
            }

            EditorGUILayout.EndHorizontal();
        }
        EditorUtils.DrawHorizontalSeperator();
        EditorGUILayout.BeginHorizontal();
        string[] languages = GameUtils.GetEnumNames<SystemLanguage>();
        languageIndex = EditorGUILayout.Popup(languageIndex, languages, GUILayout.Width(250));
        if (GUILayout.Button("Create New Language") && referenceLanguage != null)
        {
            JsonUtils<List<Dictionary<string, string>>>.Save(Application.dataPath + "/Resources/Localization/" + languages[languageIndex] + ".json", referenceLanguage, true);
        }
        EditorGUILayout.EndHorizontal();
        EditorUtils.DrawHorizontalSeperator();
        EditorGUILayout.BeginHorizontal();
        if (GUILayout.Button("Fix Languages") && referenceLanguage != null)
        {
            for (int i = 0; i < allLanguages.Length; ++i)
            {
                if (setReferenceLanguage == allLanguages[i]) continue;
                List<Dictionary<string, string>> tempLang;
                JsonUtils<List<Dictionary<string, string>>>.Load(allLanguages[i], out tempLang, GameUtils.ELoadType.Override);
                bool modified = false;

                for (int j = 0; j < referenceLanguage.Count; ++j)
                {
                    Dictionary<string, string> referenceElement = referenceLanguage.ElementAt(j);
                    bool found = false;
                    for (int k = 0; k < tempLang.Count; ++k)
                    {
                        Dictionary<string, string> tempElement = tempLang.ElementAt(k);
                        if (referenceElement["Key"] == tempElement["Key"])
                        {
                            found = true;
                            break;
                        }
                    }
                    if (!found)
                    {
                        tempLang.Insert(j, referenceElement);
                        Debug.Log("missing element added KEY: " + referenceElement["Key"]);
                        modified = true;
                    }
                }

                for (int j = 0; j < referenceLanguage.Count; ++j)
                {
                    Dictionary<string, string> tempElement = tempLang.ElementAt(j);
                    bool found = false;
                    for (int k = 0; k < referenceLanguage.Count; ++k)
                    {
                        Dictionary<string, string> referenceElement = referenceLanguage.ElementAt(k);
                        if (referenceElement["Key"] == tempElement["Key"])
                        {
                            found = true;
                            break;
                        }
                    }
                    if (!found)
                    {
                        Debug.Log("element not in reference, removing. KEY: " + tempElement["Key"]);
                        tempLang.RemoveAt(j);
                        modified = true;
                        j--;
                    }
                }

                if (modified)
                {
                    JsonUtils<List<Dictionary<string, string>>>.Save(allLanguages[i], tempLang, true);
                }
            }
        }
        EditorGUILayout.EndHorizontal();
        EditorUtils.DrawHorizontalSeperator();
        EditorGUILayout.EndVertical();
    }

    private void ShowLocalization()
    {
        if (referenceLanguage == null) return;
        Navigation(referenceLanguage);
        Dictionary<string, string> referenceElement = referenceLanguage.ElementAt(currentIndex);

        EditorUtils.DrawHorizontalSeperator();

        EditorGUILayout.BeginVertical();
        GUILayout.Label("Reference Language: " + setReferenceLanguage, EditorStyles.boldLabel);
        EditorGUILayout.BeginHorizontal();
        GUILayout.Label("KEY:", GUILayout.Width(150));
        referenceElement["Key"] = EditorGUILayout.TextField(referenceElement["Key"]);
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.BeginHorizontal();
        GUILayout.Label("VALUE:", GUILayout.Width(150));
        referenceElement["Value"] = EditorGUILayout.TextArea(referenceElement["Value"]);
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.EndVertical();

        EditorUtils.DrawHorizontalSeperator();

        if (currentLanguage == null) return;
        Dictionary<string, string> element = currentLanguage.ElementAt(currentIndex);
        EditorGUILayout.BeginVertical();
        GUILayout.Label("Selected Language: " + setCurrentLanguage, EditorStyles.boldLabel);
        EditorGUILayout.BeginHorizontal();
        GUILayout.Label("KEY:", GUILayout.Width(150));
        element["Key"] = EditorGUILayout.TextField(element["Key"]);
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.BeginHorizontal();
        GUILayout.Label("VALUE:", GUILayout.Width(150));
        element["Value"] = EditorGUILayout.TextArea(element["Value"]);
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.EndVertical();
    }

    private void ShowSearch()
    {
        Navigation(referenceSearch);
        if (referenceSearch == null || referenceSearch.Count == 0 || string.IsNullOrEmpty(searchPattern)) return;
        EditorUtils.DrawHorizontalSeperator();
        Dictionary<string, string> referenceElement = referenceSearch.ElementAt(currentIndex);

        EditorGUILayout.BeginVertical();
        GUILayout.Label("Reference Language: " + setReferenceLanguage, EditorStyles.boldLabel);
        EditorGUILayout.BeginHorizontal();
        GUILayout.Label("KEY:", GUILayout.Width(150));
        referenceElement["Key"] = EditorGUILayout.TextField(referenceElement["Key"]);
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.BeginHorizontal();
        GUILayout.Label("VALUE:", GUILayout.Width(150));
        referenceElement["Value"] = EditorGUILayout.TextArea(referenceElement["Value"]);
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.EndVertical();
 
        if (currentSearch == null || currentSearch.Count == 0 || string.IsNullOrEmpty(searchPattern)) return;
        EditorUtils.DrawHorizontalSeperator();
        int correspondingIndex = FindElementByKey(currentLanguage, referenceElement["Key"]);
        if (correspondingIndex != -1)
        {
            Dictionary<string, string> currentElement = currentSearch.ElementAt(correspondingIndex);

            EditorGUILayout.BeginVertical();
            GUILayout.Label("Reference Language: " + setReferenceLanguage, EditorStyles.boldLabel);
            EditorGUILayout.BeginHorizontal();
            GUILayout.Label("KEY:", GUILayout.Width(150));
            currentElement["Key"] = EditorGUILayout.TextField(currentElement["Key"]);
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.BeginHorizontal();
            GUILayout.Label("VALUE:", GUILayout.Width(150));
            currentElement["Value"] = EditorGUILayout.TextArea(currentElement["Value"]);
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.EndVertical();
        } 
    }

    private int FindElementByKey(List<Dictionary<string,string>> language, string key)
    {
        for (int i = 0; i < language.Count; ++i)
        {
            Dictionary<string, string> keyValue = language.ElementAt(i);
            if (keyValue.ContainsKey(key))
            {
                return i;
            }
        }
        return -1;
    }

    private void Navigation(List<Dictionary<string,string>> language)
    {
        EditorGUILayout.BeginVertical();
        EditorGUILayout.BeginHorizontal();
        if (GUILayout.Button("<<", GUILayout.Width(30)))
        {
            currentIndex = 0;
            searchPattern = string.Empty;
        }
        GUI.enabled = (currentIndex != 0);
        if (GUILayout.Button("<", GUILayout.Width(30)))
        {
            currentIndex--;
            searchPattern = string.Empty;
        }
        GUI.enabled = true;

        currentIndex = Int32.Parse(GUILayout.TextField(currentIndex.ToString(), GUILayout.Width(50)));
        currentIndex = Math.Min(language.Count - 1, currentIndex);
        currentIndex = Math.Max(0, currentIndex);
        GUILayout.Label(" of " + (language.Count - 1), GUILayout.Width(100));

        GUI.enabled = (currentIndex < language.Count - 1);
        if (GUILayout.Button(">", GUILayout.Width(30)))
        {
            currentIndex++;
            searchPattern = string.Empty;
        }
        GUI.enabled = true;
        if (GUILayout.Button(">>", GUILayout.Width(30)))
        {
            currentIndex = language.Count - 1;
            searchPattern = string.Empty;
        }
        if (GUILayout.Button("New Text", GUILayout.Width(100)))
        {
            if (referenceLanguage != null)
            {
                Dictionary<string, string> newDict = new Dictionary<string, string>();
                newDict.Add("Key", "");
                newDict.Add("Value", "");
                referenceLanguage.Add(newDict);
                currentIndex = referenceLanguage.Count - 1;
                searchPattern = string.Empty;
                currentLanguage = null;
                setCurrentLanguage = string.Empty;
                 
            }
        }
        GUILayout.Label("Search Key: ", GUILayout.Width(100));
        if (newSearchPattern != searchPattern)
        {
            searchPattern = newSearchPattern;
        }
        newSearchPattern = EditorGUILayout.TextField(searchPattern, GUILayout.Width(150));
        if (GUILayout.Button("Clear", GUILayout.Width(100)))
        {
            searchPattern = newSearchPattern = string.Empty;
            if (referenceSearch != null) referenceSearch.Clear();
            if (currentSearch != null) currentSearch.Clear();
            currentIndex = 0;
        }
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.EndVertical();
    }

    private void DoSearch()
    {
        if (string.IsNullOrEmpty(searchPattern)) return;
        if (referenceLanguage == null) return;
        if (referenceSearch == null) referenceSearch = new List<Dictionary<string, string>>();
        if (currentSearch == null) currentSearch = new List<Dictionary<string, string>>();
        if (newSearchPattern != searchPattern)
        {
            searchPattern = newSearchPattern;
            referenceSearch.Clear();
            for (int i = 0; i < referenceLanguage.Count; ++i)
            {
                Dictionary<string, string> element = referenceLanguage.ElementAt(i);
                if (element["Key"].IndexOf(searchPattern, StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    referenceSearch.Add(element);
                }
            }

            if (currentLanguage != null)
            {
                for (int i = 0; i < currentLanguage.Count; ++i)
                {
                    Dictionary<string, string> element = currentLanguage.ElementAt(i);
                    if (element["Key"].IndexOf(searchPattern, StringComparison.OrdinalIgnoreCase) >= 0)
                    {
                        currentSearch.Add(element);
                    }
                }
            }
            currentIndex = 0;
        }
    }
}
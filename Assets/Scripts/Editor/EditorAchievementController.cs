using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(AchievementController))]
public class EditorAchievementController : Editor
{
    private AchievementController achievementController;

    public override void OnInspectorGUI()
    {
        achievementController = (AchievementController)target;
        DrawTitle();
        DrawDefaultInspector();
        DrawAchievementButtons();
    }

    private void DrawTitle()
    {
        EditorUtils.DrawHorizontalSeperator();
        EditorGUILayout.BeginVertical();
        EditorGUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        GUILayout.Label("FOR DEBUG PURPOSES (ask Sacha for info)");
        GUILayout.FlexibleSpace();
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.EndVertical();
        EditorUtils.DrawHorizontalSeperator();
    }

    private void DrawAchievementButtons()
    {
        EditorUtils.DrawHorizontalSeperator();
        EditorGUILayout.BeginVertical();

        EditorGUILayout.BeginHorizontal();
        if (GUILayout.Button("Save"))
        {
            achievementController.SaveAchievements(DataController.SAVEPRIORITY.QUEUE);
        }
        if (GUILayout.Button("Load"))
        {
            achievementController.LoadAchievements(null);
        }
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        if (GUILayout.Button("Complete"))
        {
            achievementController.CompleteAchievementProgression();
        }
        if (GUILayout.Button("Reset"))
        {
            achievementController.ResetAchievementProgression();
        }
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.EndVertical();
    }
}
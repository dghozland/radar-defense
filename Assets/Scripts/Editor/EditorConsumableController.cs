using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(ConsumableController))]
public class EditorConsumableController : Editor
{
    private ConsumableController consumableController;

    public override void OnInspectorGUI()
    {
        consumableController = (ConsumableController)target;
        DrawDefaultInspector();
        DrawCurrencyButtons();
    }

    private void DrawCurrencyButtons()
    {
        EditorUtils.DrawHorizontalSeperator();
        EditorGUILayout.BeginVertical();
        EditorGUILayout.BeginHorizontal();

        if (GUILayout.Button("Save"))
        {
            consumableController.SaveConsumables(DataController.SAVEPRIORITY.QUEUE);
        }

        if (GUILayout.Button("Load"))
        {
            consumableController.LoadConsumables(null);
        }

        EditorGUILayout.EndHorizontal();
        EditorGUILayout.EndVertical();
    }
}
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

#pragma warning disable 0649

[RequireComponent(typeof(Button))]
public class ButtonSoundPlayer : MonoBehaviour
{
    [SerializeField]
    private AudioClip sound;

    private Button button;

    private void Awake()
    {
        button = GetComponent<Button>();
        button.onClick.AddListener(() => PlaySound());
    }

    private void PlaySound()
    {
        AudioManager.Instance.PlaySound(sound);
    }
}

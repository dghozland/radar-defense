using UnityEngine;
using System.Collections;
using System.IO;

#pragma warning disable 0649

[RequireComponent(typeof(AudioSource))]
public class AudioManager : UnitySingleton<AudioManager>
{
    public enum ESoundType
    {
        NewWave,
        NewEnemySpawn,
        Explosion,
        MissileAlert,
        LootCollect,
        BaseHit,
        BaseDestroy,
        MatchWon,
        MatchLost,
        ConsumableProduced,
        RadarScan,
        WeaponSelect,
        OpenPrize,
        OpenPack,
        OpenPackCardIn,
        SpecialAlert,
        RadarScanJammed
    }

    public class AudioData
    {
        public float musicVolume = 0.5f;
        public float soundVolume = 0.5f;

        public AudioData(float musicVolume, float soundVolume)
        {
            this.musicVolume = musicVolume;
            this.soundVolume = soundVolume;
        }
    }

    private static bool isInitialized = false;
    private AudioSource soundPlayer;
    private AudioSource musicPlayer;

    [SerializeField]
    private float musicFadeInDuration = 1f;

    [SerializeField]
    private float musicFadeOutDuration = 1f;

    [SerializeField]
    private AudioClip menuMusicClip;

    [SerializeField]
    private AudioClip[] battleMusicClips;

    [SerializeField]
    private AudioClip newWaveSound;

    [SerializeField]
    private AudioClip newEnemySpawnSound;
    
    [SerializeField]
    private AudioClip explosionSound;

    [SerializeField]
    private AudioClip missileAlertSound;

    [SerializeField]
    private AudioClip specialAlertSound;

    [SerializeField]
    private AudioClip lootCollectSound;

    [SerializeField]
    private AudioClip baseHitSound;

    [SerializeField]
    private AudioClip baseDestroySound;

    [SerializeField]
    private AudioClip matchWonSound;

    [SerializeField]
    private AudioClip matchLostSound;

    [SerializeField]
    private AudioClip consumableProducedSound;

    [SerializeField]
    private AudioClip radarScanSound;

    [SerializeField]
    private AudioClip radarScanJammedSound;

    [SerializeField]
    private AudioClip weaponSelectSound;

    [SerializeField]
    private AudioClip openPrizeSound;

    [SerializeField]
    private AudioClip openPackSound;

    [SerializeField]
    private AudioClip openPackCardInSound;

    // Note :: Using an initial audio data to control the initial volume
    private AudioData initialAudioData;
    private AudioData currentAudioJSON;

    private float musicFadeInTimer = 0f;
    private float musicFadeOutTimer = 0f;


    public override void Awake()
    {
        base.Awake();
        if (!isInitialized)
        {
            // Always keep the controller loaded through the scene changes
            DontDestroyOnLoad(gameObject);
            isInitialized = true;

            soundPlayer = GetComponents<AudioSource>()[0];
            musicPlayer = GetComponents<AudioSource>()[1];

            initialAudioData = new AudioData(musicPlayer.volume, soundPlayer.volume);

            LoadAudioData();
        }
    }

    public override bool Update()
    {
        if (!base.Update()) return false;

        UpdateMusicFade();        

        return true;
    }

    private void UpdateMusicFade()
    {
        if (musicFadeInTimer > 0f)
        {
            musicFadeInTimer -= Time.deltaTime;

            musicPlayer.volume = GetMusicVolume() * (1 - (musicFadeInTimer / musicFadeInDuration));

            // If fade in is done
            if (musicFadeInTimer <= 0f)
            {
                musicPlayer.volume = GetMusicVolume();
            }
        }

        if (musicFadeOutTimer > 0f)
        {
            musicFadeOutTimer -= Time.deltaTime;

            musicPlayer.volume = GetMusicVolume() * (musicFadeOutTimer / musicFadeOutDuration);

            // If fade out is done
            if (musicFadeInTimer <= 0f)
            {
                musicPlayer.volume = 0f;
                musicPlayer.Stop();
            }
        }
    }

    public float GetMusicVolumeJSON()
    {
        return currentAudioJSON.musicVolume;
    }

    private float GetMusicVolume()
    {
        return initialAudioData.musicVolume * currentAudioJSON.musicVolume;
    }

    public float GetSoundVolumeJSON()
    {
        return currentAudioJSON.soundVolume;
    }

    public float GetSoundVolume()
    {
        return initialAudioData.soundVolume * currentAudioJSON.soundVolume;
    }

    public void SetMusicVolume(float musicSliderValue)
    {
        currentAudioJSON.musicVolume = musicSliderValue;
        musicPlayer.volume = GetMusicVolume();
        SaveAudioData();
    }

    public void SetSoundVolume(float soundSliderValue)
    {
        currentAudioJSON.soundVolume = soundSliderValue;
        soundPlayer.volume = initialAudioData.soundVolume * currentAudioJSON.soundVolume;
        SaveAudioData();
    }

    public void PlaySound(AudioClip sound)
    {
        soundPlayer.PlayOneShot(sound);
    }

    public void PlaySound(ESoundType soundType)
    {
        AudioClip soundToPlay = null;
        switch(soundType)
        {
            case ESoundType.NewWave:
                soundToPlay = newWaveSound;
                break;
            case ESoundType.NewEnemySpawn:
                soundToPlay = newEnemySpawnSound;
                break;
            case ESoundType.Explosion:
                soundToPlay = explosionSound;
                break;
            case ESoundType.MissileAlert:
                soundToPlay = missileAlertSound;
                break;
            case ESoundType.LootCollect:
                soundToPlay = lootCollectSound;
                break;
            case ESoundType.BaseHit:
                soundToPlay = baseHitSound;
                break;
            case ESoundType.BaseDestroy:
                soundToPlay = baseDestroySound;
                break;
            case ESoundType.MatchWon:
                soundToPlay = matchWonSound;
                break;
            case ESoundType.MatchLost:
                soundToPlay = matchLostSound;
                break;
            case ESoundType.ConsumableProduced:
                soundToPlay = consumableProducedSound;
                break;
            case ESoundType.RadarScan:
                soundToPlay = radarScanSound;
                break;
            case ESoundType.WeaponSelect:
                soundToPlay = weaponSelectSound;
                break;
            case ESoundType.OpenPrize:
                soundToPlay = openPrizeSound;
                break;
            case ESoundType.OpenPack:
                soundToPlay = openPackSound;
                break;
            case ESoundType.OpenPackCardIn:
                soundToPlay = openPackCardInSound;
                break;
            case ESoundType.SpecialAlert:
                soundToPlay = specialAlertSound;
                break;
            case ESoundType.RadarScanJammed:
                soundToPlay = radarScanJammedSound;
                break;
        }
        soundPlayer.PlayOneShot(soundToPlay);
    }

    public void PlayMenuMusic()
    {
        musicPlayer.clip = menuMusicClip;
        PlayMusic();
    }

    public void PlayBattleMusic()
    {
        musicPlayer.clip = battleMusicClips[Random.Range(0, battleMusicClips.Length)];
        PlayMusic();
    }

    private void PlayMusic()
    {
        // Start music fade in
        musicFadeInTimer = musicFadeInDuration;

        musicPlayer.volume = 0f;
        musicPlayer.Play();
    }

    public void FadeOutBattleMusic()
    {
        // Start music fade out
        musicFadeOutTimer = musicFadeOutDuration;
    }

    #region Data Handling
    private void SaveAudioData()
    {
        JsonUtils<AudioData>.Save(GameUtils.AUDIO_SETTINGS_JSON, currentAudioJSON);
    }

    private void LoadAudioData()
    {
        System.Exception ex = JsonUtils<AudioData>.Load(GameUtils.AUDIO_SETTINGS_JSON, out currentAudioJSON, GameUtils.ELoadType.DynamicData);
        if (ex != null
            && ex.GetType() == typeof(FileLoadException))
        {
            ResetAudioData();
            SaveAudioData();
        }
    }

    private void ResetAudioData()
    {
        currentAudioJSON = new AudioData(0.5f, 0.5f);
    }
    #endregion
}

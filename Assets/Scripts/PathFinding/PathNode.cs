using UnityEngine;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class PathNode : MonoBehaviour
{
    public enum PathNodeType
    {
        NORMAL,
        START,
        END
    }

    [SerializeField]
    private List<PathNode> connectedNodes = new List<PathNode>();

    [SerializeField]
    private PathNodeType nodeType = PathNodeType.NORMAL;

    public List<PathNode> ConnectedNodes
    {
        get { return connectedNodes; }
    }

    public PathNodeType NodeType
    {
        get { return nodeType; }
        set { nodeType = value; }
    }

    public bool IsScaled
    {
        get
        {
            if (scaleObject != null)
            {
                return scaleObject.IsScaled;
            }
            return false;
        }
    }

    public float f;
    public PathNode parent;
    private ScaleObject scaleObject;
    private bool init = false;

    public void Update()
    {
        if (!init)
        {
            PathFindingController.Instance.RegisterNode(this);
            scaleObject = gameObject.GetComponent<ScaleObject>();
            init = true;
        }
    }

#if UNITY_EDITOR
    public void OnDrawGizmos()
    {
        for (int i = 0; i < connectedNodes.Count; ++i)
        {
            if (connectedNodes[i] == null)
            {
                Debug.LogError("There is a connected node null in node " + name + " at index = " + i + ". Please add a node or remove the element.");
                return;
            }

            float deltaX = connectedNodes[i].transform.position.x - transform.position.x;
            Color lineColor = (deltaX < 0) ? Color.red : Color.blue;
            Vector3 right = (transform.position - connectedNodes[i].transform.position).normalized;
            right = Quaternion.AngleAxis(-90, Vector3.forward) * right * 2f;
            Debug.DrawLine(transform.position + right, connectedNodes[i].transform.position + right, lineColor);
        }
    }
#endif
}

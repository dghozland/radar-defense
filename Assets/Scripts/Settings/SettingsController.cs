using UnityEngine;
using System.Collections;
using System.IO;

public class SettingsController : UnitySingleton<SettingsController>
{
    private static bool isInitialized = false;

    public enum EGraphicSetting
    {
        Performance,
        Quality,
        Normal
    }

    public class GraphicSettingsData
    {
        public EGraphicSetting graphicSetting;
        public bool dragAndRelease;

        public GraphicSettingsData()
        {
            // screen smaller than FullHD should not reduce by default
            if (Screen.currentResolution.width <= 1920)
            {
                graphicSetting = EGraphicSetting.Quality;
            }
            else if (Screen.currentResolution.width > 1920 && Screen.currentResolution.width < 2048)
            {
                graphicSetting = EGraphicSetting.Normal;
            }
            else
            {
                graphicSetting = EGraphicSetting.Performance;
            }

            dragAndRelease = true;
        }
    }
    
    private GraphicSettingsData currentGraphicSettingsJSON;

    public enum EResolutionType
    {
        Full,
        TwoThirds,
        Half
    }

    private Resolution fullResolution;

    public override void Awake()
    {
        base.Awake();
        if (!isInitialized)
        {
            // Always keep the controller loaded through the scene changes
            DontDestroyOnLoad(gameObject);
            isInitialized = true;

            SetFullResolution(Screen.currentResolution);

            LoadGraphicSettingsData();

            // Always keep the target frame rate to 35
            // Note :: This will increase drastically the performance
            // and it will overheat the phones alot less.
            Application.targetFrameRate = 35;
        }
    }

    public void SetFullResolution(Resolution fullResolution)
    {
        this.fullResolution = fullResolution;
    }
    
    public void ChangeResolution(EResolutionType resolutionType)
    {
        switch (resolutionType)
        {
            case EResolutionType.Full:
                Screen.SetResolution(fullResolution.width, fullResolution.height, true);
                break;
            case EResolutionType.TwoThirds:
                Screen.SetResolution(fullResolution.width / 3 * 2, fullResolution.height / 3 * 2, true);
                break;
            case EResolutionType.Half:
                Screen.SetResolution(fullResolution.width / 2, fullResolution.height / 2, true);
                break;
        }
    }

    public EGraphicSetting GetGraphicSettings()
    {
        return currentGraphicSettingsJSON.graphicSetting;
    }

    public bool GetDragAndRelease()
    {
        return currentGraphicSettingsJSON.dragAndRelease;
    }

    public void SetDragAndRelease(bool active)
    {
        if (currentGraphicSettingsJSON.dragAndRelease != active)
        {
            currentGraphicSettingsJSON.dragAndRelease = active;
            SaveGraphicSettingsData();
        }
    }

    public void SetGraphicSettings(EGraphicSetting newGraphicSetting)
    {
        // Only save if different graphic setting
        if (currentGraphicSettingsJSON.graphicSetting != newGraphicSetting)
        {
            currentGraphicSettingsJSON.graphicSetting = newGraphicSetting;
            SaveGraphicSettingsData();
        }

        if (newGraphicSetting == EGraphicSetting.Performance)
        {
            // Go half resolution
            ChangeResolution(EResolutionType.Half);
        }
        else if (newGraphicSetting == EGraphicSetting.Quality)
        {
            // Go full resolution
            ChangeResolution(EResolutionType.Full);
        }
        else if (newGraphicSetting == EGraphicSetting.Normal)
        {
            // Go full resolution
            ChangeResolution(EResolutionType.Full);
        }
    }

    #region Data Handling
    private void SaveGraphicSettingsData()
    {
        JsonUtils<GraphicSettingsData>.Save(GameUtils.GRAPHIC_SETTINGS_JSON, currentGraphicSettingsJSON);
    }

    private void LoadGraphicSettingsData()
    {
        System.Exception ex = JsonUtils<GraphicSettingsData>.Load(GameUtils.GRAPHIC_SETTINGS_JSON, out currentGraphicSettingsJSON, GameUtils.ELoadType.DynamicData);
        if (ex != null
            && ex.GetType() == typeof(FileLoadException))
        {
            ResetGraphicSettingsData();
            SaveGraphicSettingsData();
        }

        SetGraphicSettings(currentGraphicSettingsJSON.graphicSetting);
    }

    private void ResetGraphicSettingsData()
    {
        currentGraphicSettingsJSON = new GraphicSettingsData();
    }
    #endregion
}

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Explosion : MonoBehaviour
{
    private float lifeTimer;
    private float scale;
    private float explosionGroundPower;
    private float explosionAirPower;
    private float explosionRadius;
    private int enemyKillCount;
    private bool explosionHit;

    private Sprite miss00;
    private Sprite miss01;
    private Sprite miss02;
    private Sprite hit00;
    private Sprite hit01;
    private Sprite hit02;
    private Image image00;
    private Image image01;
    private Image image02;
    //
    private ParticleSystem particle01;
    private ParticleSystem particle02;

    [SerializeField]
    private float explosionTime = 1f;

    private List<GameObject> receivedDamage = new List<GameObject>();

    // Use this for initialization

    public void Awake()
    {
        particle01 = GameUtils.GetChildGameObject(gameObject, "particle01").GetComponent<ParticleSystem>();
        particle02 = GameUtils.GetChildGameObject(gameObject, "particle02").GetComponent<ParticleSystem>();
        //

        lifeTimer = Time.time + explosionTime;
        enemyKillCount = 0;
        receivedDamage.Clear();

        // REFACTOR :: Have both image in the prefab and turn them on off
        // in order to remove images from resources
        miss00 = (Sprite)Resources.Load("Images/gs_miss00", typeof(Sprite));
        miss01 = (Sprite)Resources.Load("Images/gs_miss01", typeof(Sprite));
        miss02 = (Sprite)Resources.Load("Images/gs_miss02", typeof(Sprite));
        hit00 = (Sprite)Resources.Load("Images/gs_hit00", typeof(Sprite));
        hit01 = (Sprite)Resources.Load("Images/gs_hit01", typeof(Sprite));
        hit02 = (Sprite)Resources.Load("Images/gs_hit02", typeof(Sprite));

        image00 = GameUtils.GetChildGameObject(gameObject, "gs_explosionCircle00").GetComponent<Image>();
        image01 = GameUtils.GetChildGameObject(gameObject, "gs_explosionCircle01").GetComponent<Image>();
        image02 = GameUtils.GetChildGameObject(gameObject, "gs_explosionCircle02").GetComponent<Image>();

    }

    public void Init(float groundPower, float airPower, float radius)
    {
        receivedDamage.Clear();
        lifeTimer = Time.time + explosionTime;
        enemyKillCount = 0;
        explosionHit = false;

        explosionGroundPower = groundPower;
        explosionAirPower = airPower;
        explosionRadius = radius;
        scale = explosionRadius / GameUtils.RADIUS_FACTOR;
        gameObject.transform.localScale = new Vector3(scale, scale, 0);
        //particles.startSize = scale * 40f;

        image00.sprite = miss00;
        image01.sprite = miss01;
        image02.sprite = miss02;

        //
        particle01.Stop();
        particle02.Play();
        particle01.startSize = scale * 40f;
        particle02.startSize = scale * 40f;
        //



        // Sound hook - Explosion sound
        AudioManager.Instance.PlaySound(AudioManager.ESoundType.Explosion);
    }

	public void Update()
    {
        if (WaveController.Instance.Paused)
        {
            // make sure explosion continues after pause
            lifeTimer += Time.deltaTime;
            return;
        }

        // If life timer expired
        if (lifeTimer < Time.time)
        {
            WaveController.Instance.ManageMultipleKills(enemyKillCount, transform.position);

            CleanObject();
        }
	}
    
    public void OnTriggerEnter2D(Collider2D coll)
    {
        CheckTrigger(coll);
    }
    
    private void CheckTrigger(Collider2D coll)
    {
        if (coll.CompareTag(GameUtils.GROUND_TAG) || coll.CompareTag(GameUtils.AIR_TAG))
        {
            MovingEnemyObject enemyObject = coll.gameObject.GetComponent<MovingEnemyObject>();
            if (enemyObject && !receivedDamage.Contains(coll.gameObject))
            {
                // add to list to make sure same explosion cannot deal damage more than once to one enemy
                receivedDamage.Add(coll.gameObject);

                if (!explosionHit)
                {
                    // Change the visual of the explosion
                    image00.sprite = hit00;
                    image01.sprite = hit01;
                    image02.sprite = hit02;
                    explosionHit = true;

                    //
                    particle02.Stop();
                    particle01.Play();
                    //
                }

                float damage = coll.CompareTag(GameUtils.GROUND_TAG) ? explosionGroundPower : explosionAirPower;
                if (damage > 0f)
                {
                    float sqrDist = GameUtils.GetSqrDistanceBetweenVectors(coll.transform.position, transform.position);
                    float sqrCritDist = (explosionRadius / 4f) * (explosionRadius / 4f) * PlayfieldController.Instance.WidthScale;
                    bool isDirectHit = sqrDist < sqrCritDist;

                    // Reduce enemy's HP based on the damage type
                    enemyObject.ReceiveDamage(damage, isDirectHit);
                }
                
                // Increment kill count to show multiple kills notice
                if (enemyObject.IsDead)
                {
                    enemyKillCount++;
                }
            }
        }
    }

    private void CleanObject()
    {
        WeaponController.Instance.PutExplosionToPool(gameObject);
    }
}

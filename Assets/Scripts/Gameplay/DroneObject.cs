using UnityEngine;
using System.Collections;

public class DroneObject : SightRadiusObject
{
    private const float DEPLOY_DURATION = 1f;

    private GameObject sightRadiusGO;
    private AudioSource droneAudio;
    private float deployTimer;

    private void Awake()
    {
        droneAudio = GetComponent<AudioSource>();
        sightRadiusGO = GameUtils.GetChildGameObject(gameObject, "SightRadius");
        deployTimer = 0f;
        sightRadiusGO.transform.localScale = Vector3.zero;
    }
    
    private void Start()
    {
        droneAudio.volume = AudioManager.Instance.GetSoundVolume();
        deployTimer = DEPLOY_DURATION;
        transform.parent = GameObject.Find("RadarScanMask").transform;
    }

    private void Update()
    {
        if (WaveController.Instance.Paused)
        {
            droneAudio.Pause();
        }
        else if (!droneAudio.isPlaying)
        {
            droneAudio.UnPause();
        }

        if (deployTimer > 0f)
        {
            deployTimer -= Time.deltaTime;

            UpdateScale(deployTimer <= 0f ? 0f : (deployTimer / DEPLOY_DURATION));
        }
    }

    private void UpdateScale(float t)
    {
        float adjustment = sightRadius * GameUtils.SIGHT_RADIUS_FACTOR;
        Vector3 localScale = sightRadiusGO.transform.localScale;
        localScale.x = localScale.y = adjustment * 2 * (1 - t);
        sightRadiusGO.transform.localScale = localScale;
    }
}

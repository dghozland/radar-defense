using UnityEngine;
using System.Collections.Generic;

public class MovingObject : MonoBehaviour
{
    protected Vector3 targetPosition;
    protected GameObject targetBase;
    protected Vector3 startPosition;
    protected float speed;
    protected Vector3 direction;
    protected bool initialized;
    protected Rigidbody2D physicsBody;
    protected SpriteRenderer spriteRenderer;
    protected List<PathNode> path;
    protected int pathTargetId;
    protected float lifeTimer;
    // trails
    protected Vector3 lastTrailPosition;
    protected Quaternion lastTrailRotation;
    protected Queue<GameObject> activeWeaponTrails;
    protected Queue<GameObject> activeBonusTrails;
    protected Queue<GameObject> activeEnemyTrails;

    protected bool continueAfterTarget;
    private const int maxDistanceRemainingSqr = 2;

    [SerializeField]
    protected int trailCount = GameUtils.DEFAULT_TRAIL_COUNT;

    protected int currentTrailCount;

    public GameObject TargetBase
    {
        get { return targetBase; }
        set { targetBase = value; }
    }

    public Vector3 TargetPosition
    {
        get { return targetPosition; }
        set { targetPosition = value; }
    }

    public Vector3 StartPosition
    {
        set { startPosition = value; }
    }

    public List<PathNode> Path
    {
        get { return path; }
        set { path = value; }
    }

    public float SetSpeed
    {
        set { speed = value; }
    }

    public virtual void Init()
    {
        InitPath();

        lastTrailPosition = transform.position;
        lastTrailRotation = transform.rotation;
        if (activeWeaponTrails == null)
        {
            activeWeaponTrails = new Queue<GameObject>();
        }
        if (activeBonusTrails == null)
        {
            activeBonusTrails = new Queue<GameObject>();
        }
        if (activeEnemyTrails == null)
        {
            activeEnemyTrails = new Queue<GameObject>();
        }

        physicsBody = GetComponent<Rigidbody2D>();

        if (spriteRenderer == null)
        {
            spriteRenderer = GetComponent<SpriteRenderer>();
            spriteRenderer.sortingOrder = 11; // TEMP :: Sorting layers
        }

        RotateObject();
        initialized = true;
        continueAfterTarget = false;
        currentTrailCount = trailCount;
    }

    public void InitPath()
    {
        if (path == null)
        {
            transform.position = startPosition;
            direction = Vector3.Normalize(targetPosition - startPosition);
        }
        else
        {
            if (path.Count < 2)
            {
                Debug.LogError("Path is too short, only 1 node, FIX IT!");
                return;
            }
            startPosition = path[0].transform.position;
            targetPosition = path[1].transform.position;
            transform.position = path[0].transform.position;
            direction = Vector3.Normalize(path[1].transform.position - path[0].transform.position);
            pathTargetId = 1;
        }
    }

    protected virtual GameObject GetTrail(int trailCount)
    {
        if (WeaponController.Instance.inactiveWeaponTrails.Count > 0)
        {
            // return trail from inactive queue
            GameObject trail = WeaponController.Instance.inactiveWeaponTrails.Dequeue();
            activeWeaponTrails.Enqueue(trail);
            AdjustTransparency(trail);
            return trail;
        }
        else if ((activeWeaponTrails.Count) < trailCount)
        {
            // new trail
            GameObject trail = (GameObject)Instantiate(Resources.Load("WeaponTrail"), lastTrailPosition, lastTrailRotation);
            trail.GetComponent<SpriteRenderer>().sortingOrder = 10; // TEMP :: Sorting layers
            ParentTrail(trail);
            AdjustTransparency(trail);
            activeWeaponTrails.Enqueue(trail);
            return trail;
        }
        // Obsolete code new trail system
        // Debug.LogError("Should never happen! FIX IT");
        return null;
    }

    protected void AdjustTransparency(GameObject trail)
    {
        RadarObject movingObjectRadar = gameObject.GetComponent<RadarObject>();
        RadarObject trailRadar = trail.GetComponent<RadarObject>();
        trailRadar.Inherit(movingObjectRadar);
    }

    protected virtual void ParentTrail(GameObject trail)
    {
        GameObject container = GameObject.Find("GreenTrailContainer");
        if (container == null)
        {
            container = new GameObject("GreenTrailContainer");
        }
        trail.transform.parent = container.transform;
        container.SetActive(false);
        container.SetActive(true);
    }

    public virtual void Update()
    {
        if (WaveController.Instance.Paused) return;
        if (!initialized) return;

        lifeTimer += Time.deltaTime;
        UpdateTrails(currentTrailCount, ref activeWeaponTrails, ref WeaponController.Instance.inactiveWeaponTrails);
        CheckOutside();
    }

    public virtual void FixedUpdate()
    {
        if (WaveController.Instance.Paused) return;
        if (!initialized) return;

        MoveObject();
        RotateObject();
    }

    protected virtual void CheckOutside()
    {
        if (!PlayfieldController.Instance.IsInsidePlayArea(transform.position))
        {
            // If just spawned
            // Note :: This condition could be removed if all nodes
            // would be guaranteed to be inside the play area
            if (lifeTimer < 1f)
            {
                Debug.LogWarning("RADAR WARNING :: There is/are node(s) set outside of the play area, please fix this issue! (Side effect : Enemy unit destroyed)");
                return;
            }

            ClearObject();
        }
    }

    public virtual void OnTriggerEnter2D(Collider2D coll)
    {
        float power = 1;
        MovingEnemyObject movingEnemyObject = GetComponent<MovingEnemyObject>();
        if (movingEnemyObject)
        {
            if (movingEnemyObject.GetPower() == 0 || movingEnemyObject.EnemyData.isUnitBonus)
            {
                // if doesn't do damage or is bonus unit, don't destroy bases!
                return;
            }

            // get the power of the unit
            power = movingEnemyObject.GetPower();
        }
        // rest does!
        if (coll.CompareTag(GameUtils.BASE_TAG) && coll.GetType()==typeof(BoxCollider2D))
        {
            //Debug.Log("Base Hit");
            BaseObject baseObject = coll.GetComponent<BaseObject>();
            baseObject.Health -= power;
            ClearObject();
        }
    }

    protected virtual void MoveObject()
    {
        if (path != null)
        {
            CheckReachedTargetID();
        }

        Vector3 newPosition = transform.position + direction * Time.deltaTime * speed;
        if (!continueAfterTarget && (Vector3.SqrMagnitude(newPosition - transform.position) > Vector3.SqrMagnitude(targetPosition - transform.position)))
        {
            newPosition = targetPosition;
        }
        if (physicsBody)
        {
            physicsBody.MovePosition(newPosition);
        }
        else
        {
            transform.position = newPosition;
        }
    }

    private void CheckReachedTargetID()
    {
        int distanceTraveled = (int)Vector3.SqrMagnitude(transform.position - path[pathTargetId - 1].transform.position);
        int distanceBetweenNodes = (int)Vector3.SqrMagnitude(path[pathTargetId].transform.position - path[pathTargetId - 1].transform.position);
        int distanceRemaining = (int)Vector3.SqrMagnitude(path[pathTargetId].transform.position - transform.position);
        if ((distanceTraveled >= distanceBetweenNodes || distanceRemaining < maxDistanceRemainingSqr) && (path.Count - 1) > pathTargetId)
        {
            // select next target node
            pathTargetId++;
            startPosition = targetPosition;
            targetPosition = path[pathTargetId].transform.position;
            direction = Vector3.Normalize(path[pathTargetId].transform.position - path[pathTargetId - 1].transform.position);
        }
        else if (distanceTraveled >= distanceBetweenNodes && pathTargetId == (path.Count - 1))
        {
            // reached final destination
            // remove unit
            ClearObject();
        }
    }

    public PathNode GetNextPathNode()
    {
        return path[pathTargetId];
    }

    protected virtual void RotateObject()
    {
        float angle = Vector3.Angle(Vector3.right, direction);
        if (direction.y < 0)
        {
            angle *= -1f;
        }
        if (direction.x < 0)
        {
            spriteRenderer.flipY = true;
        }

        transform.rotation = Quaternion.Euler(0f, 0f, angle);
    }

    private void UpdateTrailCount(int trailCount, ref Queue<GameObject> activeTrails, ref Queue<GameObject> inactiveTrails)
    {
        while (activeTrails.Count >= trailCount && activeTrails.Count != 0)
        {
            GameObject tempTrail = activeTrails.Dequeue();
            tempTrail.SetActive(false);
            inactiveTrails.Enqueue(tempTrail);
        }
    }

    protected void UpdateTrails(int trailCount, ref Queue<GameObject> activeTrails, ref Queue<GameObject> inactiveTrails)
    {
        // only show half the trails unless we are in Quality mode
        // this is for weapons only in other modes, in Performance trails for enemies are disabled!
        int useTrailCount = trailCount;
        if (SettingsController.Instance.GetGraphicSettings() != SettingsController.EGraphicSetting.Quality)
        {
            useTrailCount /= 2;
        }

        UpdateTrailCount(useTrailCount, ref activeTrails, ref inactiveTrails);

        while ((transform.position - lastTrailPosition).sqrMagnitude >= (GameUtils.TRAIL_DISTANCE_SQR * PlayfieldController.Instance.WidthScale))
        {
            UpdateTrailCount(useTrailCount, ref activeTrails, ref inactiveTrails);
            GameObject trail = GetTrail(useTrailCount);
            if (trail == null) return;

            trail.SetActive(true);
            trail.transform.position = lastTrailPosition;
            trail.transform.localRotation = lastTrailRotation;
            lastTrailPosition = lastTrailPosition + ((gameObject.transform.position - lastTrailPosition).normalized * (GameUtils.TRAIL_DISTANCE * PlayfieldController.Instance.WidthScale));
            lastTrailRotation = spriteRenderer.gameObject.transform.rotation;

            // Update trail transparency
            GameObject[] activeTrailsArray = activeTrails.ToArray();
            for (int i = 0; i < activeTrailsArray.Length; ++i)
            {
                Color activeTrailColor = activeTrailsArray[i].GetComponent<SpriteRenderer>().color;
                int diff = activeTrailsArray.Length - 1 - i;
                float fadePerTrail = 1 / (float)useTrailCount;
                activeTrailColor.a = 1f - (diff * fadePerTrail);
                activeTrailsArray[i].GetComponent<SpriteRenderer>().color = activeTrailColor;
            }
        }        
    }

    public virtual void ClearObject()
    {
        if (WeaponController.ApplicationIsQuitting) return;

#if UNITY_EDITOR
        Debug.Log("Clearing Object: " + gameObject.name);
#endif

        while (activeWeaponTrails != null && activeWeaponTrails.Count > 0)
        {
            GameObject tempTrail = activeWeaponTrails.Dequeue();
            if (tempTrail != null)
            {
                tempTrail.SetActive(false);
                WeaponController.Instance.inactiveWeaponTrails.Enqueue(tempTrail);
            }
        }
    }
}

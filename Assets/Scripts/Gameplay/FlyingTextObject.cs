using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FlyingTextObject : MonoBehaviour
{
    private const float FLYING_TEXT_TIMER = 1.5f;
    private const float FLYING_TEXT_SPEED = 35f;
    // get the width of the text
    private float totalWidth = 0;
    private float totalHeight = 0;

    private GameObject goldImage;
    private GameObject creditsImage;
    private GameObject goldText;
    private GameObject damageText;
    private RectTransform rectGoldImage;
    private RectTransform rectCreditsImage;
    private RectTransform rectGoldText;
    private RectTransform rectDamageText;

    public struct FlyingTextInfo
    {
        public Vector3 position;
        public Color fontColor;
        public EFlyingTextType type;
        public FontStyle fontStyle;
        public EFlyingTextBehaviour behaviour;
        public EFlyingTextDirection direction;
    }

    public enum EFlyingTextType
    {
        Damage,
        DamageBold,
        Heal,
        Gold,
        Credits
    }

    public enum EFlyingTextBehaviour
    {
        Normal,
        FastAndHold
    }

    public enum EFlyingTextDirection
    {
        Up,
        Down,
        Right,
        Left
    }

    private float timer;
    private EFlyingTextBehaviour flyingTextBehaviour = EFlyingTextBehaviour.Normal;
    private EFlyingTextDirection flyingTextDirection = EFlyingTextDirection.Up;

    public void Awake()
    {
        timer = FLYING_TEXT_TIMER;

        goldImage = GameUtils.GetChildGameObject(gameObject, "Gold", true);
        creditsImage = GameUtils.GetChildGameObject(gameObject, "Credits", true);
        goldText = GameUtils.GetChildGameObject(gameObject, "GoldText", true);
        damageText = GameUtils.GetChildGameObject(gameObject, "DamageText", true);
        rectGoldImage = goldImage.GetComponent<RectTransform>();
        rectCreditsImage = creditsImage.GetComponent<RectTransform>();
        rectGoldText = goldText.GetComponent<RectTransform>();
        rectDamageText = damageText.GetComponent<RectTransform>();
    }

    public void Init(FlyingTextInfo info, string message)
    {
        gameObject.transform.position = info.position;
        gameObject.transform.rotation = Quaternion.identity;

        timer = FLYING_TEXT_TIMER;

        goldImage.SetActive(false);
        creditsImage.SetActive(false);
        goldText.SetActive(false);
        damageText.SetActive(false);

        flyingTextDirection = info.direction;
        flyingTextBehaviour = info.behaviour;

        switch (info.type)
        {
            case EFlyingTextType.Credits:
                {
                    creditsImage.SetActive(true);
                    goldText.SetActive(true);
                    goldText.GetComponent<Text>().text = message;
                    break;
                }
            case EFlyingTextType.Gold:
                {
                    goldImage.SetActive(true);
                    goldText.SetActive(true);
                    goldText.GetComponent<Text>().text = message;
                    break;
                }
            case EFlyingTextType.Damage:
            default:
                {
                    damageText.SetActive(true);
                    Text textComponent = damageText.GetComponent<Text>();
                    textComponent.text = message;
                    textComponent.color =info.fontColor;
                    textComponent.fontStyle = info.fontStyle;
                    break;
                }
        }
        GetHeightAndWidth();
    }

    public void Update()
    {
        if (WaveController.Instance.Paused) return;

        timer -= Time.deltaTime;

        MoveText();

        if (timer <= 0f)
        {
            ClearObject();
        }
    }

    private void GetHeightAndWidth()
    {
        totalWidth = 0;
        totalHeight = 0;
        if (creditsImage.activeInHierarchy)
        {
            totalWidth += LayoutUtility.GetPreferredWidth(rectCreditsImage);
        }
        if (goldImage.activeInHierarchy)
        {
            totalWidth += LayoutUtility.GetPreferredWidth(rectGoldImage);
        }
        if (goldText.activeInHierarchy)
        {
            totalWidth += LayoutUtility.GetPreferredWidth(rectGoldText);
            totalHeight += LayoutUtility.GetPreferredHeight(rectGoldText);
        }
        if (damageText.activeInHierarchy)
        {
            totalWidth += LayoutUtility.GetPreferredWidth(rectDamageText);
            totalHeight += LayoutUtility.GetPreferredHeight(rectDamageText);
        }
    }

    private Vector3 FixTextPosition(Vector3 position)
    {
        Vector2 newPos = position;

        // check left side
        if (PlayfieldController.Instance.BottomLeft.x + 20f > (newPos.x - (totalWidth / 2)))
        {
            float diff = (PlayfieldController.Instance.BottomLeft.x - (newPos.x - (totalWidth / 2)));
            // move right
            newPos = new Vector2(newPos.x + diff + 20f, newPos.y);
        }
        // check right side
        if (PlayfieldController.Instance.TopRight.x - 20f < (newPos.x + (totalWidth / 2)))
        {
            float diff = ((newPos.x + (totalWidth / 2)) - PlayfieldController.Instance.TopRight.x);
            // move left
            newPos = new Vector2(newPos.x - diff - 20f, newPos.y);
        }
        // check top side
        if (PlayfieldController.Instance.TopRight.y - 10f < (newPos.y + (totalHeight / 2)))
        {
            float diff = ((newPos.y + (totalHeight / 2)) - PlayfieldController.Instance.TopRight.y);
            // move down
            newPos = new Vector2(newPos.x, newPos.y - diff - 10f);
        }
        // check bottom side
        if (PlayfieldController.Instance.BottomLeft.y + 10f > (newPos.y - (totalHeight / 2)))
        {
            float diff = (PlayfieldController.Instance.BottomLeft.y - (newPos.y - (totalHeight / 2)));
            // move up
            newPos = new Vector2(newPos.x, newPos.y + diff + 10f);
        }

        return newPos;
    }

    private void MoveText()
    {
        // Manage the speed
        float flyingTextSpeed = FLYING_TEXT_SPEED;
        switch (flyingTextBehaviour)
        {
            case EFlyingTextBehaviour.Normal:
                break;                
            case EFlyingTextBehaviour.FastAndHold:
                if (timer <= (FLYING_TEXT_TIMER * (2f / 3f))) return;
                flyingTextSpeed *= 3f;
                break;                
        }

        // Manage the direction
        Vector3 position = gameObject.transform.position;
        switch (flyingTextDirection)
        {
            case EFlyingTextDirection.Up:                
                position.y += Time.deltaTime * flyingTextSpeed;
                break;
            case EFlyingTextDirection.Down:
                position.y += Time.deltaTime * flyingTextSpeed * -1;
                break;
            case EFlyingTextDirection.Right:
                position.x += Time.deltaTime * flyingTextSpeed;
                break;
            case EFlyingTextDirection.Left:
                position.x += Time.deltaTime * flyingTextSpeed * -1;
                break;
        }

        gameObject.transform.position = FixTextPosition(position);              
    }

    public void ClearObject()
    {
        WaveController.Instance.PutFlyingTextInPool(gameObject);
    }
}

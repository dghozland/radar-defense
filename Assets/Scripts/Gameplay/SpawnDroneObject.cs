using UnityEngine;

public class SpawnDroneObject : SpawnObject
{
    public override void Init()
    {
        base.Init();
    }

    public override void CleanObject()
    {
        gameObject.SetActive(false);
    }
}

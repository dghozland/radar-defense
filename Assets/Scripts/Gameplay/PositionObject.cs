using UnityEngine;
#pragma warning disable 0649

public class PositionObject : MonoBehaviour
{
    [SerializeField]
    private PathNode node;

    public PathNode Node
    {
        get { return node; }
        set { node = value; }
    }
}

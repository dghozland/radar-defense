using UnityEngine;
using System.Collections;

public class SpawnTargetObject : SpawnObject
{
    public override void Init()
    {
        base.Init();
        gameObject.GetComponent<Animator>().Play("ProjectileTargetMarker", 0, 0);
    }

    public override void CleanObject()
    {
        // put back to pool
        WeaponController.Instance.PutTargetMarkerToPool(gameObject);
    }
}


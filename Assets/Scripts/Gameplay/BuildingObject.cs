using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BuildingObject : BaseObject
{
    private Texture2D buildingDestroyedTexture;
    private GameUtils.EConsumable consumableProduction;
    private float consumableLifeTime;
    private float consumableTimeProduction;
    private float consumableProductionTimer;

    private Image consumableProductionRadialBar;
    private GameObject consumableProductionPanel;

    private int consumableReadyAmount;

    public GameUtils.EConsumable ConsumableProduction
    {
        get { return consumableProduction; }
    }

    public override bool Update()
    {
        if (!base.Update()) return false;

        UpdateTimeProduction();

        return true;
    }

    protected override bool InitBase()
    {
        if (!BaseController.Instance.Initialized) return false;
        if (!BuildingController.Instance.Initialized) return false;
        
        base.InitBase();
        
        return true;
    }

    public void InitBuildingData(BuildingData buildingData)
    {
        GameUtils.GetChildGameObject(gameObject, "BuildingIcon").GetComponent<SpriteRenderer>().sprite = buildingData.baseImage;
        buildingDestroyedTexture = buildingData.buildingDestroyedTexture;
        baseID = buildingData.baseID;
        health = MaxHealth = buildingData.maxHealth;
        SightRadius = buildingData.sightRadius;
        consumableLifeTime = buildingData.consumableLifeTime;
        consumableProduction = buildingData.consumableProduction;
        consumableTimeProduction = buildingData.consumableTimeProduction;
        GameUtils.GetChildGameObject(gameObject, "ConsumableImage").GetComponent<Image>().sprite = buildingData.consumableImage;
        GameUtils.GetChildGameObject(gameObject, "ConsumableRegen").GetComponent<Image>().sprite = buildingData.consumableImage;

        // Set the base name
        baseName = GameUtils.GetChildGameObject(gameObject, "BaseName").GetComponent<Text>();
        baseName.text = LocalizationManager.Instance.GetLocalizedString(buildingData.baseNameID);

        InitConsumableProduction();
    }

    private void InitConsumableProduction()
    {
        // Container of the consumable production system
        consumableProductionPanel = GameUtils.GetChildGameObject(gameObject, "ConsumablePanel");
        if (consumableProduction == GameUtils.EConsumable.Count)
        {
            consumableProductionPanel.SetActive(false);
        }
        else
        {
            consumableProductionPanel.SetActive(true);

            // Init the production radial bar
            consumableProductionRadialBar = GameUtils.GetChildGameObject(consumableProductionPanel, "ConsumableRegen").GetComponent<Image>();
            consumableProductionRadialBar.fillAmount = 1;

            // Init production timer
            consumableProductionTimer = GetConsumableTimeProductionWithDifficulty();
        }

        consumableReadyAmount = 0;
    }

    private void UpdateTimeProduction()
    {
        if (!WeaponController.Instance.WeaponsPlaced()) return;

        // TEMP :: Missing visuals, will enabled soon!
        if (consumableProductionTimer > 0f /*&& !isDamaged*/)
        {
            consumableProductionTimer -= Time.deltaTime;
            
            if (consumableProductionTimer <= 0f)
            {
                SpawnConsumable();

                // Reset timer
                consumableProductionTimer += consumableTimeProduction;
            }

            UpdateProductionRadialBar();
        }
    }

    private void UpdateProductionRadialBar()
    {
        consumableProductionRadialBar.fillAmount = consumableProductionTimer / GetConsumableTimeProductionWithDifficulty();
    }

    public void SpawnConsumable()
    {
        GameObject consumableGO = LootController.Instance.GetLootFromPool();
        consumableGO.transform.position = transform.position;
        consumableGO.GetComponent<LootObject>().Init(ConvertConsumableToLootType(consumableProduction), consumableLifeTime, false);

        // Sound hook - Consumable Produced sound
        AudioManager.Instance.PlaySound(AudioManager.ESoundType.ConsumableProduced);

        // Hide the consumable production icon
        ManageConsumableProductionUIVisibility(1);
    }

    public void ManageConsumableProductionUIVisibility(int consumableReadyToAdd)
    {
        consumableReadyAmount += consumableReadyToAdd;
        consumableProductionPanel.SetActive(consumableReadyAmount == 0);
    }
        
    private LootObject.ELootType ConvertConsumableToLootType(GameUtils.EConsumable consumableType)
    {
        switch (consumableType)
        {
            case GameUtils.EConsumable.OhShit:
                return LootObject.ELootType.OhShit;
            case GameUtils.EConsumable.Pulse:
                return LootObject.ELootType.Pulse;
            case GameUtils.EConsumable.Reload:
                return LootObject.ELootType.Reload;
            case GameUtils.EConsumable.Slower:
                return LootObject.ELootType.Slower;
            case GameUtils.EConsumable.Repair:
            default:
                return LootObject.ELootType.Repair;
        }
    }

    private float GetConsumableTimeProductionWithDifficulty()
    {
        return consumableTimeProduction * GameUtils.GetConsumableProductionTimeMultiplier(ChallengeController.Instance.GetCurrentChallengeDifficulty());
    }

    protected override void BaseDestroyed(float value)
    {
        base.BaseDestroyed(value);

        // Spawn the base destroyed prefab
        //GameObject prefabBaseDestroyed = (GameObject)Instantiate(Resources.Load("prefab_baseDestroyed"), transform.position, Quaternion.identity, GameObject.Find("BattleUICanvas").transform);
        GameObject prefabBaseDestroyed = (GameObject)Instantiate(Resources.Load("BaseDestroyedCanvas"), transform.position, Quaternion.identity, GameObject.Find("Bases").transform);
        RawImage[] baseDestroyedParts = prefabBaseDestroyed.GetComponentsInChildren<RawImage>();
        for (int i = 0; i < baseDestroyedParts.Length; ++i)
        {
            baseDestroyedParts[i].texture = buildingDestroyedTexture;
        }
    }
}

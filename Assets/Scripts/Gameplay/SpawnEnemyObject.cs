using UnityEngine;
using System.Collections;

public class SpawnEnemyObject : SpawnObject
{
    public override void Init()
    {
        base.Init();
        gameObject.GetComponent<Animator>().Play("NewEnemySpawn", 0, 0);
    }

    public override void CleanObject()
    {
        // put back to pool
        EnemyController.Instance.PutSpawnInPool(gameObject);
    }
}


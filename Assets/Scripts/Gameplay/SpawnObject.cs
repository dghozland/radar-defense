using UnityEngine;

public class SpawnObject : MonoBehaviour
{
    private float lifeTimer;

    [SerializeField]
    private float spawnTime = 1f;

    public float SpawnTime
    {
        get { return spawnTime; }
        set
        {
            spawnTime = value;
            lifeTimer = Time.time + spawnTime;
        }
    }

    public virtual void Awake()
    {
        Init();
    }

    public virtual void Init()
    {
        lifeTimer = Time.time + spawnTime;
    }

    public void Update()
    {
        if (WaveController.Instance.Paused)
        {
            // make sure spawn continues after pause
            lifeTimer += Time.deltaTime;
            return;
        }

        if (lifeTimer < Time.time)
        {
            CleanObject();
        }
    }

    public virtual void CleanObject() { }
}

using UnityEngine;
using System.Collections;
using UnityEngine.UI;

// REFACTOR :: 
// The right way would have been to have a function in SightRadiusObject that would fade out / scale down
// the sight radius object, but it should have been done like the following :
// All SightRadius GameObjects holds 2 Gameobjects atleast, one for the image and one for the sightradius image, 
// that would also contain the circle collider 2D.
// FOR NOW, only the drone has been re-done like that, all the bases (buildings and weapons) are still on the old system.

public class SightRadiusObject : MonoBehaviour
{
    [SerializeField]
    private bool isNewSightRadiusSystem = false;

    [SerializeField]
    protected float sightRadius = 1f;
    
    public float SightRadius
    {
        get { return sightRadius; }
        set
        {
            sightRadius = value;
            InitializeSightRadius();
        }
    }
        
    private void InitializeSightRadius()
    {
        if (!isNewSightRadiusSystem)
        {
            float adjustment = sightRadius * GameUtils.SIGHT_RADIUS_FACTOR;
            CircleCollider2D collider = GetComponent<CircleCollider2D>();
            collider.radius = adjustment;
            GameObject sightRadiusGO = GameObject.Find(name + "/SightRadius");
            Vector3 localScale = sightRadiusGO.transform.localScale;
            localScale.x = localScale.y = adjustment * 2;
            sightRadiusGO.transform.localScale = localScale;

        }      
    }
}

using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using UnityEngine.UI;
using System.Collections.Generic;

// REFACTOR :: 
// 1) Move WeaponController Stat Getters to here 
// 2) Move many functions such as FireWeapon, etc
// 3) Use a WeaponData structure for all stats 
// (or create new WeaponData for dynamics stats, see Achievement and AchievementData)
public class WeaponObject : BaseObject
{
    private Texture2D weaponTopDestroyedTexture;
    private Texture2D weaponBottomDestroyedTexture;
    private int currentAmmo = 1;
    private float cooldown;
    private AudioClip fireSound;
    private AudioClip emptySound;

    // UI elements
    private SpriteRenderer weaponImageTop;
    private SpriteRenderer weaponImageBottom;
    private Text ammoText;
    private Image ammoRegenRadialBar;
    private Image cooldownRadialBar;

#if ENABLE_WEAPON_AI
    // Weapon AI variables
    private EWeaponAIState weaponAIState;
    private MovingEnemyObject currentEnemyTarget;
    private float updateTargetTimer;    
    private EDamageSpecialization damageSpecialization;
    private Dictionary<GameObject, float> potentialTargets;
    private float lockOnTimer;
    private GameObject targetAIMarker;
    private float weaponAIMaxRange;
    private float delayBetweenShotTimer;
    
    public enum EWeaponAIState
    {
        Off,
        Idle,
        LockOn,
        Cooldown
    }

    public enum EDamageSpecialization
    {
        Ground,
        Air,
        Both
    }
#endif

    public int CurrentAmmo
    {
        get { return currentAmmo; }
        set
        {
            currentAmmo = value;

            // Update the ammo amount text
            ammoText.text = currentAmmo.ToString();
        }
    }

    public float Cooldown
    {
        get { return cooldown; }
        set { cooldown = value; }
    }

    public AudioClip FireSound
    {
        get { return fireSound; }
        set { fireSound = value; }
    }
    
    public AudioClip EmptySound
    {
        get { return emptySound; }
        set { emptySound = value; }
    }

    public SpriteRenderer WeaponImageTop
    {
        get { return weaponImageTop; }
    }

    public SpriteRenderer WeaponImageBottom
    {
        get { return weaponImageBottom; }
    }

    public Image AmmoRegenRadialBar
    {
        get { return ammoRegenRadialBar; }
    }

    public Text AmmoText
    {
        get { return ammoText; }
    }

    public Image CooldownRadialBar
    {
        get { return cooldownRadialBar; }
    }

#if ENABLE_WEAPON_AI
    public MovingEnemyObject CurrentEnemyTarget
    {
        get { return currentEnemyTarget; }
        set { currentEnemyTarget = value; }
    }

    public EWeaponAIState WeaponAIState
    {
        get { return weaponAIState; }
        set
        {
            weaponAIState = value;

            // - Off state -
            if (weaponAIState == EWeaponAIState.Off)
            {
                CleanTargetAIMarker();

                // Remove the current target
                currentEnemyTarget = null;
            }
            // - Idle state -
            else if (weaponAIState == EWeaponAIState.Idle)
            {
                CleanTargetAIMarker();

                // Remove the current target
                currentEnemyTarget = null;

                updateTargetTimer = WeaponController.Instance.WeaponAIUpdateTargetDelay;
            }
            // - Lock-on state -
            else if (weaponAIState == EWeaponAIState.LockOn)
            {
                // Set the lock on timer
                lockOnTimer = WeaponController.Instance.WeaponAILockOnDuration;
                
                // Add a marker
                targetAIMarker = WeaponController.Instance.GetTargetAIMarkerFromPool();
                targetAIMarker.transform.position = currentEnemyTarget.transform.position;
                targetAIMarker.GetComponent<SpawnTargetAIObject>().Init();
            }
            // - Cooldown state -
            else if (weaponAIState == EWeaponAIState.Cooldown)
            {
                // Fire the weapon
                WeaponController.Instance.AIFireWeapon(baseID, currentEnemyTarget.transform.position);
            }

            //Debug.Log("WeaponID = " + baseID + "/// AI state = " + weaponAIState);
        }
    }
#endif

    private void Awake()
    {        
        // Init UI elements
        weaponImageTop = GameUtils.GetChildGameObject(gameObject, "WeaponIconTop").GetComponent<SpriteRenderer>();
        weaponImageBottom = GameUtils.GetChildGameObject(gameObject, "WeaponIconBottom").GetComponent<SpriteRenderer>();
        ammoText = GameUtils.GetChildGameObject(gameObject, "AmmoValue").GetComponent<Text>();
        ammoRegenRadialBar = GameUtils.GetChildGameObject(gameObject, "AmmoRegen").GetComponent<Image>();
        cooldownRadialBar = GameUtils.GetChildGameObject(gameObject, "CooldownRadialBar").GetComponent<Image>();

#if ENABLE_WEAPON_AI
        WeaponAIState = EWeaponAIState.Off;
        currentEnemyTarget = null;
        potentialTargets = new Dictionary<GameObject, float>();
        lockOnTimer = 0f;
        delayBetweenShotTimer = 0f;
#endif
    }

    protected override bool InitBase()
    {
        if (!BaseController.Instance.Initialized) return false;
        if (!WeaponController.Instance.Initialized) return false;

        WeaponController.Instance.RegisterWeapon(baseID, gameObject, new UnityAction(WeaponPressed));
        
        base.InitBase();

        return true;
    }

    public void InitWeaponData(WeaponData weaponData)
    {
        weaponImageTop.sprite = weaponData.weaponImageTop;
        weaponImageBottom.sprite = weaponData.weaponImageBottom;
        weaponTopDestroyedTexture = weaponData.weaponTopDestroyedTexture;
        weaponBottomDestroyedTexture = weaponData.weaponBottomDestroyedTexture;
        SightRadius = WeaponController.Instance.GetWeaponBaseStat(baseID, GameUtils.EWeaponUpgrade.VISIBILITY_RADIUS);
        health = MaxHealth = WeaponController.Instance.GetWeaponBaseStat(baseID, GameUtils.EWeaponUpgrade.HEALTH);
        currentAmmo = (int)WeaponController.Instance.GetWeaponBaseStat(baseID, GameUtils.EWeaponUpgrade.RELOAD);
        cooldown = weaponData.cooldown;
        fireSound = weaponData.fireSound;
        emptySound = weaponData.emptySound;

#if ENABLE_WEAPON_AI
        // Set the damage specialization
        float baseGroundDamage = WeaponController.Instance.GetWeaponBaseStat(baseID, GameUtils.EWeaponUpgrade.GROUND);
        float baseAirDamage = WeaponController.Instance.GetWeaponBaseStat(baseID, GameUtils.EWeaponUpgrade.AIR);
        if (baseGroundDamage > baseAirDamage)
        {
            damageSpecialization = EDamageSpecialization.Ground;
        }
        else if (baseGroundDamage < baseAirDamage)
        {
            damageSpecialization = EDamageSpecialization.Air;
        }
        else
        {
            damageSpecialization = EDamageSpecialization.Both;
        }
        
        weaponAIMaxRange = WeaponController.Instance.GetWeaponAIRange(baseID);
#endif

        // Set the base name
        baseName = GameUtils.GetChildGameObject(gameObject, "BaseName").GetComponent<Text>();
        baseName.text = LocalizationManager.Instance.GetLocalizedString(weaponData.baseNameID);
    }

#if ENABLE_WEAPON_AI
    public override bool Update()
    {
        if (!base.Update())
            return false;

        if (WeaponController.Instance.ActiveWeapons[baseID].IsDead)
            return false;

        UpdateWeaponAIState();

        return true;
    }
    
    private void UpdateWeaponAIState()
    {
        // As soon as the weapon is selected, put weapon AI off
        if (WeaponController.Instance.SelectedWeaponID == baseID
            && WeaponAIState != EWeaponAIState.Off)
        {
            WeaponAIState = EWeaponAIState.Off;
        }
        
        switch (WeaponAIState)
        {
            case EWeaponAIState.Off:
                UpdateWeaponAIOff();
                return;
            case EWeaponAIState.Idle:
                UpdateWeaponAIIdle();
                break;
            case EWeaponAIState.LockOn:
                UpdateWeaponAILockOn();
                break;
            case EWeaponAIState.Cooldown:
                UpdateWeaponAICooldown();
                break;
        }
    }

    private void UpdateWeaponAIOff()
    {
        // Only update AI if not selected
        if (WeaponController.Instance.SelectedWeaponID != baseID)
        {
            WeaponAIState = EWeaponAIState.Idle;
        }
    }

    private void UpdateWeaponAIIdle()
    {
        // Search for a target each X seconds
        if (updateTargetTimer > 0f)
        {
            updateTargetTimer -= Time.deltaTime;

            if (updateTargetTimer <= 0f)
            {
                UpdateAITarget();
            }                
        }
    }
    
    private void UpdateWeaponAILockOn()
    {
        if (lockOnTimer > 0f)
        {
            lockOnTimer -= Time.deltaTime;

            // Check if the target is dead
            if (currentEnemyTarget.IsDead)
            {
                // Set the AI state to idle
                WeaponAIState = EWeaponAIState.Idle;

                return;
            }

            // Lock on the target
            WeaponController.Instance.RotateWeapon(baseID, currentEnemyTarget.transform.position);

            // Position the marker
            targetAIMarker.transform.position = currentEnemyTarget.transform.position;
            
            // Once lock-on duration is done
            if (lockOnTimer <= 0f)
            {
                // Set the AI state to cooldown
                WeaponAIState = EWeaponAIState.Cooldown;
            }
        }
    }

    private void UpdateWeaponAICooldown()
    {
        // If there's a delay between shot to apply
        if (delayBetweenShotTimer > 0f)
        {
            delayBetweenShotTimer -= Time.deltaTime;

            if (delayBetweenShotTimer <= 0f)
            {
                // Set the AI state to lock-on
                WeaponAIState = EWeaponAIState.LockOn;
            }
            
            return;
        }

        // If cooldown is done and has ammo
        if (cooldownRadialBar.fillAmount <= 0f
            && currentAmmo > 0)
        {
            // If there's no target
            // or the current target is dead or not visible
            if (currentEnemyTarget == null
                || (currentEnemyTarget.IsDead
                || !IsEnemyVisibleForWeaponAI(currentEnemyTarget.gameObject)))
            {
                // Set the AI state to idle
                WeaponAIState = EWeaponAIState.Idle;
            }
            else
            {
                // Give a small delay before doing another shot
                delayBetweenShotTimer = WeaponController.Instance.WeaponAIShotDelay;                
            }
        }
    }

    private void UpdateAITarget()
    {
        // Clear potential targets
        potentialTargets.Clear();

        // For each alive enemies on the field
        foreach (var enemy in EnemyController.Instance.AliveEnemies)
        {
            // Don't get dead enemies in target
            if (!EnemyController.Instance.IsEnemyAlive(enemy))
                continue;

            // Don't target an already targeted enemy
            if (WeaponController.Instance.IsEnemyAlreadyTargeted(enemy))
                continue;

            // If enemy is not visible
            if (!IsEnemyVisibleForWeaponAI(enemy))
                continue;

            // Ignore enemy targets that are too far
            float sqrDistWithEnemy = GameUtils.GetSqrDistanceBetweenVectors(transform.position, enemy.transform.position);
            float sqrDistMaxRange = (weaponAIMaxRange * weaponAIMaxRange);
            if (sqrDistWithEnemy > sqrDistMaxRange)
                continue;

            // Target prioritization value 
            float targetPriorityValue = sqrDistWithEnemy;

            // If enemy's target is current weapon
            bool isWeaponTarget = enemy.GetComponent<MovingEnemyObject>().TargetBase == gameObject;
            if (isWeaponTarget)
            {
                targetPriorityValue /= WeaponController.Instance.WeaponAIBeingTargetedDivider;
            }            

            // If damage specialization
            bool isEnemyAir = enemy.CompareTag(GameUtils.AIR_TAG);
            if ((damageSpecialization == EDamageSpecialization.Air && isEnemyAir)
                || (damageSpecialization == EDamageSpecialization.Ground && !isEnemyAir))
            {
                // Divide the target value
                targetPriorityValue /= WeaponController.Instance.WeaponAIDamageSpecializationDivider;
            }
            // If not damage specialist and is not being targeted
            else if (damageSpecialization != EDamageSpecialization.Both
                && !isWeaponTarget)
            {
                // Note :: i.e. We don't want to have a flak shooting
                // a tank unless the tank is targeting the flak.

                // Don't add the enemy target
                continue;
            }

            // Take into account the enemy's speed value
            targetPriorityValue /= enemy.GetComponent<MovingEnemyObject>().GetSpeed();

            // Add the target to the potential target candidates
            potentialTargets.Add(enemy, targetPriorityValue);
        }

        // Target the target with the lowest value
        float lowestTargetValue = float.MaxValue;
        foreach (KeyValuePair<GameObject, float> potentialTarget in potentialTargets)
        {
            if (potentialTarget.Value < lowestTargetValue)
            {
                lowestTargetValue = potentialTarget.Value;
                currentEnemyTarget = potentialTarget.Key.GetComponent<MovingEnemyObject>();
            }
        }

        // If found a target
        if (currentEnemyTarget != null)
        {
            // Set the AI state to Lock-on
            WeaponAIState = EWeaponAIState.LockOn;

            //Debug.Log(baseID + " lock-on " + currentEnemyTarget);                 
        }
        else
        {
            // Restart the timer
            updateTargetTimer = WeaponController.Instance.WeaponAIUpdateTargetDelay;
        }
    }

    private void CleanTargetAIMarker()
    {
        if (targetAIMarker == null)
            return;

        targetAIMarker.GetComponent<SpawnTargetAIObject>().CleanObject();
        //targetAIMarker = null;
    }

    private bool IsEnemyVisibleForWeaponAI(GameObject enemyGO)
    {
        return enemyGO.GetComponent<RadarObject>().GetCanvasGroupAlpha() >= WeaponController.Instance.WeaponAIEnemyAlphaThreshold;
    }
#endif

    private void WeaponPressed()
    {
        if (isDead) return;

        WeaponController.Instance.SelectWeapon(baseID);
    }

    protected override void BaseDestroyed(float value)
    {
        base.BaseDestroyed(value);

        WeaponController.Instance.UnregisterWeapon(baseID);

        // Spawn the weapon top destroyed prefab
        GameObject weaponTopDestroyedPrefab = (GameObject)Instantiate(Resources.Load("BaseDestroyedCanvas"), transform.position, weaponImageTop.transform.rotation, GameObject.Find("Bases").transform);
        RawImage[] weaponTopDestroyedParts = weaponTopDestroyedPrefab.GetComponentsInChildren<RawImage>();
        for (int i = 0; i < weaponTopDestroyedParts.Length; ++i)
        {
            weaponTopDestroyedParts[i].texture = weaponTopDestroyedTexture;
        }

        // Spawn the weapon bottom destroyed prefab
        // Note :: Only if existing
        if (weaponBottomDestroyedTexture)
        {
            GameObject weaponBottomDestroyedPrefab = (GameObject)Instantiate(Resources.Load("BaseDestroyedCanvas"), transform.position, Quaternion.identity, GameObject.Find("Bases").transform);
            RawImage[] weaponBottomDestroyedParts = weaponBottomDestroyedPrefab.GetComponentsInChildren<RawImage>();
            for (int i = 0; i < weaponBottomDestroyedParts.Length; ++i)
            {
                weaponBottomDestroyedParts[i].texture = weaponBottomDestroyedTexture;
            }
        }        
    }

    public void SetVisibility(bool isVisible)
    {
        GameUtils.GetChildGameObject(gameObject, "WeaponCanvas", true).gameObject.SetActive(isVisible);
        GameUtils.GetChildGameObject(gameObject, "WeaponIconTop", true).gameObject.SetActive(isVisible);
        GameUtils.GetChildGameObject(gameObject, "WeaponIconBottom", true).gameObject.SetActive(isVisible);
        GameUtils.GetChildGameObject(gameObject, "SightRadius", true).gameObject.SetActive(isVisible);
    }
}

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

#pragma warning disable 0649

public class MovingEnemyObject : MovingObject
{
    private const float DEATH_DURATION = 2f;

    private EnemyData enemyData;
    private EnemyController.EEnemyRank enemyRank;
    private bool isEnemyFromWave;
    private bool isBossUnit;

    // UI elements
    private Slider healthGreenBar;
    private Slider healthRedBar;
    private Image imageGreen;

    [SerializeField]
    private Color greenBar;
    [SerializeField]
    private Color yellowBar;

    private Text enemyName;
    private Image slowIcon;
    private Image shieldIcon;
    private Image repairIcon;
    private GameObject deathAnimContainer;

    private float healthCurrent;
    private float healthLostRate;
    private float unitSpawnTimer;
    private int unitSpawnCount;
    private bool isSlow;
    private bool isDead;
    private float deadTimer;
    private GameObject damageFlyingText;
    private float damageFlyingTextValue;
    private bool cleanUp;
    private Collider2D rectCollider;
    private RadarObject radarObject;
    private float timeToDeploySpecial;
    private bool specialActive;
    private float slowTimer;
    private bool hiddenKill;

    public EnemyData EnemyData
    {
        get { return enemyData; }
    }
        
    public bool IsDead
    {
        get { return isDead; }
        set
        {
            isDead = value;
            rectCollider.enabled = false;
            deadTimer = DEATH_DURATION;
        }
    }

    public bool IsSlow
    {
        get { return isSlow; }
        set
        {
            // If new isSlow value
            if (value != isSlow)
            {
                if (value)
                {
                    speed *= ConsumableController.Instance.SlowerMultiplier;
                }
                else
                {
                    speed /= ConsumableController.Instance.SlowerMultiplier;
                }
            }

            isSlow = value;
            
            if (isSlow)
            {
                // Re-do duration
                slowTimer = ConsumableController.Instance.SlowerDuration;
            }

            GetComponent<Animator>().enabled = isSlow;
            slowIcon.enabled = isSlow;
        }
    }

    private void Reset()
    {
        healthLostRate = 0f;
        unitSpawnTimer = 0f;
        unitSpawnCount = 1;
        isSlow = false;
        isDead = false;
        deadTimer = 0f;
        cleanUp = false;
        timeToDeploySpecial = -1f;
        specialActive = false;
        slowTimer = 0f;
        damageFlyingText = null;
        hiddenKill = false;

        if (spriteRenderer == null)
        {
            // the sprite is in a child object
            spriteRenderer = GetComponentInChildren<SpriteRenderer>();
        }
        spriteRenderer.gameObject.SetActive(true);

        if (rectCollider == null)
        {
            rectCollider = GetComponent<Collider2D>();
        }
        rectCollider.enabled = true;

        if (radarObject == null)
        {
            radarObject = GetComponent<RadarObject>();
        }
        radarObject.Reset();

        if (enemyData.spawnType != GameUtils.UnitType.NONE)
        {
            unitSpawnTimer = enemyData.unitSpawnTimer;
            unitSpawnCount = enemyData.unitSpawnCount;
        }

        if (enemyData.bossSpecial != GameUtils.EBossSpecial.None)
        {
            timeToDeploySpecial = enemyData.timeToDeploySpecial;
        }
    }
    
    public override void Init()
    {
        Reset();

        base.Init();

        //
        if (GetComponent<TrailRenderer>() != null)
        {
            GetComponent<TrailRenderer>().sortingOrder = 11;
        }
    }

    public void InitEnemy(EnemyData enemyData, GameUtils.EGameDifficulty difficulty, EnemyController.EEnemyRank enemyRank, bool isEnemyFromWave)
    {
        this.enemyData = enemyData;
        this.enemyRank = enemyRank;
        this.isEnemyFromWave = isEnemyFromWave;
        isBossUnit = GameUtils.IsBossUnit(enemyData.enemyType);

        Init();

        continueAfterTarget = enemyData.continueAfterTarget;
        gameObject.tag = enemyData.isUnitAir ? GameUtils.AIR_TAG : GameUtils.GROUND_TAG;
        spriteRenderer.sortingOrder = enemyData.isUnitAir ? 3 : 2;
        spriteRenderer.sprite = enemyData.enemyNormalSprite;
        spriteRenderer.color = enemyData.enemyColor;
        GetComponent<CircleCollider2D>().radius = enemyData.colliderRadius;

        // Death anim
        deathAnimContainer = GameUtils.GetChildGameObject(gameObject, "DeathAnimContainer", true);
        GameObject prefabEnemyDestroyed = GameUtils.GetChildGameObject(deathAnimContainer, "prefab_enemyDestroyed", true);
        RawImage[] enemyDestroyedParts = prefabEnemyDestroyed.GetComponentsInChildren<RawImage>();
        for (int i = 0; i < enemyDestroyedParts.Length; ++i)
        {
            enemyDestroyedParts[i].texture = enemyData.enemyDestroyedTexture;
            enemyDestroyedParts[i].color = enemyData.enemyColor;
        }
        deathAnimContainer.SetActive(false);

        // Note :: We are not using directly the stats
        // except for the health and speed
        healthCurrent = GetHealthMax();
        speed = GetSpeed() * PlayfieldController.Instance.WidthScale;

        // Initialize the health bars
        healthGreenBar = GameUtils.GetChildGameObject(gameObject, "HealthBarGreen").GetComponent<Slider>();
        healthGreenBar.value = healthGreenBar.maxValue = healthCurrent;
        imageGreen = GameUtils.GetChildGameObject(healthGreenBar.gameObject, "Fill").GetComponent<Image>();
        imageGreen.color = greenBar;
        healthRedBar = GameUtils.GetChildGameObject(gameObject, "HealthBarRed").GetComponent<Slider>();
        healthRedBar.value = healthRedBar.maxValue = healthCurrent;

        // Set the enemy name
        enemyName = GameUtils.GetChildGameObject(gameObject, "EnemyName").GetComponent<Text>();
        enemyName.text = LocalizationManager.Instance.GetLocalizedString(enemyData.enemyNameID) + " Mk" + ((int)difficulty + 1);

        // Set the enemy rank UI        
        GameUtils.GetChildGameObject(gameObject, "EnemyVeteranRank", true).SetActive(enemyRank == EnemyController.EEnemyRank.Veteran);
        GameUtils.GetChildGameObject(gameObject, "EnemyEliteRank", true).SetActive(enemyRank == EnemyController.EEnemyRank.Elite);
        GameUtils.GetChildGameObject(gameObject, "EnemyHeroRank", true).SetActive(enemyRank == EnemyController.EEnemyRank.Hero);

        // Set the info icon color
        Image infoIcon = GameUtils.GetChildGameObject(gameObject, "InfoIcon").GetComponent<Image>();
        infoIcon.color = Color.red;

        // Set the slow icon
        slowIcon = GameUtils.GetChildGameObject(gameObject, "SlowIcon").GetComponent<Image>();
        slowIcon.enabled = false;

        // Set the shield icon
        shieldIcon = GameUtils.GetChildGameObject(gameObject, "ShieldIcon").GetComponent<Image>();
        shieldIcon.enabled = false;

        // Set the shield icon
        repairIcon = GameUtils.GetChildGameObject(gameObject, "RepairIcon").GetComponent<Image>();
        repairIcon.enabled = false;

        if (enemyData.bossSpecial != GameUtils.EBossSpecial.None && enemyData.bossSpecial != GameUtils.EBossSpecial.Bomb)
        {
            EnemyController.Instance.SetSpecial(enemyData.bossSpecial, enemyData.specialRepeatTime, enemyData.specialValue);
        }
    }

    public override void Update()
    {
        if (WaveController.Instance.Paused) return;
        if (!initialized) return;

        if (!isDead)
        {
            lifeTimer += Time.deltaTime;

            UpdateUnitSpawn();
            UpdateDeployTimer();
            UpdateSpecialsIcons();
            UpdateSlow();

            // Note :: Not calling the base.Update(),
            // instead we're moving the enemy object from the fixedUpdate
            // in order to gain performance since the fixedUpdate is called less.
            CheckOutside();
        }
        else
        {
            UpdateDeathState();
        }
    }

    protected override void CheckOutside()
    {
        if (!PlayfieldController.Instance.IsInsidePlayArea(transform.position))
        {
            // If just spawned
            // Note :: This condition could be removed if all nodes
            // would be guaranteed to be inside the play area
            if (lifeTimer < 1f)
            {
                Debug.LogWarning("RADAR WARNING :: There is/are node(s) set outside of the play area, please fix this issue! (Side effect : Enemy unit destroyed) \nPosition: " + transform.position.ToString() + " enemy: " + gameObject.name);

                return;
            }

            if (!enemyData.returnFromBorder)
            {
                SpecialActive(false);
                ClearObject();
            }
            else
            {
                // set startposition to be close to border
                // this is needed if the unit is changing target to adjust for the new start position
                transform.position = GameUtils.FindBorderPosition(transform.position, -direction) + (direction * 25f * PlayfieldController.Instance.WidthScale);

                // reset the spawn timer
                unitSpawnTimer = enemyData.unitSpawnTimer;
                unitSpawnCount = enemyData.unitSpawnCount;
                // reset the depoy timer
                timeToDeploySpecial = enemyData.timeToDeploySpecial;
                // stop the deploy
                SpecialActive(false);

                CleanTrails();
            }
        }
    }

    private void SpecialActive(bool active)
    {
        // cannot deactivate if not active
        if (!active && !specialActive) return;
        // only do if there is a special
        if (enemyData.bossSpecial != GameUtils.EBossSpecial.None)
        {
            specialActive = active;
            EnemyController.Instance.SetSpecialActive(enemyData.bossSpecial, active);
        }
    }

    private bool SpecialDeployedStop()
    {
        if ((enemyData.bossSpecial == GameUtils.EBossSpecial.Repair ||
            enemyData.bossSpecial == GameUtils.EBossSpecial.Shield ||
            enemyData.bossSpecial == GameUtils.EBossSpecial.JamRadar) 
                && !enemyData.isUnitAir
                && timeToDeploySpecial <= 0f)
        {
            return true;
        }
        return false;
    }

    public override void FixedUpdate()
    {
        if (WaveController.Instance.Paused) return;
        if (!initialized) return;

        if (!isDead && !SpecialDeployedStop()) 
        {
            MoveObject();
            RotateObject();
        }
        else
        {
            currentTrailCount = 0;
        }
        UpdateHealthRedBar();

        // don't show trails for enemies unless in Quality setting
        if (SettingsController.Instance.GetGraphicSettings() != SettingsController.EGraphicSetting.Quality) return;

        if (enemyData.isUnitBonus)
        {
            UpdateTrails(currentTrailCount, ref activeBonusTrails, ref WeaponController.Instance.inactiveBonusTrails);
        }
        else
        {
            UpdateTrails(currentTrailCount, ref activeEnemyTrails, ref WeaponController.Instance.inactiveEnemyTrails);
        }
    }

    protected override void MoveObject()
    {
        // if base is destroyed aquire new base as target
        if (enemyData.movementType == GameUtils.EMovementType.StraightToBase && targetBase != null)
        {
            BaseObject baseObject = targetBase.GetComponent<BaseObject>();
            if (baseObject == null || baseObject.IsDead)
            {
#if UNITY_EDITOR
                Debug.Log("MoveObject() " + gameObject.name + ": Need new target base!");
#endif
                GameObject newTargetBase = EnemyController.Instance.GetTargetBase(GameUtils.ETargetBaseType.All);
                if (newTargetBase)
                {
                    // only set if there is a base to go to
                    targetBase = newTargetBase;
                    targetPosition = targetBase.transform.position;
                    direction = Vector3.Normalize(targetPosition - transform.position);
                }
            }
        }
        base.MoveObject();
    }

    private void UpdateUnitSpawn()
    {
        if (enemyData.spawnType != GameUtils.UnitType.NONE && unitSpawnCount > 0)
        {
            unitSpawnTimer -= Time.deltaTime;
            if (unitSpawnTimer <= 0)
            {
                unitSpawnCount--;
                // Spawn an Unit
                EnemyController.Instance.SpawnEnemy(enemyData.spawnType, transform.position, false, enemyData.destroyOnSpawn, enemyRank);

                if (enemyData.destroyOnSpawn)
                {
                    SpecialActive(false);
                    ClearObject();
                }

                // Restart unit spawn timer
                unitSpawnTimer = enemyData.unitSpawnTimer;
            }
        }
    }

    private void UpdateDeployTimer()
    {
        if (enemyData.bossSpecial != GameUtils.EBossSpecial.None && timeToDeploySpecial > 0)
        {
            timeToDeploySpecial -= Time.deltaTime;
            if (timeToDeploySpecial <= 0)
            {
                SpecialActive(true);
            }
        }
    }

    private void UpdateSpecialsIcons()
    {
        if (EnemyController.Instance.IsSpecialActive(GameUtils.EBossSpecial.Shield))
        {
            shieldIcon.enabled = true;
        }
        else
        {
            shieldIcon.enabled = false;
        }

        if (EnemyController.Instance.IsSpecialActive(GameUtils.EBossSpecial.Repair))
        {
            repairIcon.enabled = true;
        }
        else
        {
            repairIcon.enabled = false;
        }
    }

    private void UpdateSlow()
    {
        if (slowTimer > 0f)
        {
            slowTimer -= Time.deltaTime;

            if (slowTimer <= 0f)
            {
                IsSlow = false;
            }
        }
    }

    private void UpdateDeathState()
    {
        if (deadTimer > 0f)
        {
            deadTimer -= Time.deltaTime;
            
            if (deadTimer <= 0f)
            {
                DestroyEnemy();
            }
        }
    }

    private void UpdateHealthRedBar()
    {
        // If recently lost health
        if (healthRedBar.value > healthCurrent)
        {
            // If just started to lose health
            if (healthLostRate == 0f)
            {
                // Set the health lost rate only once per damage taken
                healthLostRate = healthRedBar.value - Mathf.Clamp(healthCurrent, 0f, healthRedBar.value);
            }
            healthRedBar.value -= healthLostRate * Time.deltaTime;
        }
        else
        {
            // Reset the health lost rate
            healthLostRate = 0f;
        }
    }

    protected override void RotateObject()
    {
        float angle = Vector3.Angle(Vector3.right, direction);
        if (direction.y < 0)
        {
            angle *= -1f;
        }
        if (direction.x < 0)
        {
            spriteRenderer.flipY = true;
        }

        spriteRenderer.gameObject.transform.rotation = Quaternion.Euler(0f, 0f, angle);
    }

    private GameObject GetTrailInternal(ref Queue<GameObject> activeTrails, ref Queue<GameObject> inactiveTrails, int trailCount, Color color)
    {
        if (inactiveTrails.Count > 0)
        {
            // return trail from inactive queue
            GameObject trail = inactiveTrails.Dequeue();
            trail.GetComponent<SpriteRenderer>().color = color;
            activeTrails.Enqueue(trail);
            AdjustTransparency(trail);
            return trail;
        }
        else if ((activeTrails.Count) < trailCount)
        {
            // new trail
            GameObject trail = (GameObject)Instantiate(Resources.Load("EnemyTrail"), lastTrailPosition, lastTrailRotation);
            trail.GetComponent<SpriteRenderer>().color = color;
            ParentTrail(trail);
            AdjustTransparency(trail);
            activeTrails.Enqueue(trail);
            return trail;
        }
        return null;
    }

    protected override GameObject GetTrail(int trailCount)
    {
        if (enemyData.isUnitBonus)
        {
            return GetTrailInternal(ref activeBonusTrails, ref WeaponController.Instance.inactiveBonusTrails, trailCount, enemyData.trailColor);
        }
        else
        {
            return GetTrailInternal(ref activeEnemyTrails, ref WeaponController.Instance.inactiveEnemyTrails, trailCount, enemyData.trailColor);
        }
    }

    protected override void ParentTrail(GameObject trail)
    {
        string containerName = enemyData.isUnitBonus ? "BlueTrailContainer" : "OrangeTrailContainer";
        GameObject container = GameObject.Find(containerName);
        if (container == null)
        {
            GameObject radarScanMask = GameObject.Find("RadarScanMask");
            container = new GameObject(containerName);
            container.transform.parent = radarScanMask.transform;
        }
        trail.transform.parent = container.transform;
    }

    public void Heal(float healPercentage)
    {
        if (healPercentage <= 0f) return;
        if (isDead) return;

        float healValue = (healPercentage / 100f) * GetHealthMax();

        if (radarObject.SpritesEnabled && healthCurrent < GetHealthMax())
        {
            WaveController.Instance.SpawnFlyingText(healValue.ToString(),
                        transform.position,
                        FlyingTextObject.EFlyingTextType.Heal,
                        GameUtils.COLOR_YELLOW,
                        FontStyle.Normal,
                        FlyingTextObject.EFlyingTextBehaviour.Normal,
                        FlyingTextObject.EFlyingTextDirection.Up);
        }

        healthCurrent += healValue;
        healthCurrent = Mathf.Min(healthCurrent, GetHealthMax());

        healthGreenBar.value = healthCurrent;
        if (healthCurrent == GetHealthMax())
        {
            imageGreen.color = greenBar;
        }
    }

    public void ReceiveDamage(float damage, bool isCritical = false)
    {
        if (damage <= 0f) return;

        // Do not update the health if dead already
        // Note :: This condition is needed since there are two ways to damage an enemy.
        // The first one is from the Explosion and the second one from the HitObject (hit VFX).
        // For almost all weapons (except laser), they will both try to lower the health,
        // where they would award double the reward by calling EnemyDied() twice.
        if (isDead) return;

        if (isCritical)
        {
            damage *= GameUtils.DIRECT_HIT_DAMAGE_MULTIPLIER;
            damage = Mathf.RoundToInt(damage);
        }

        float shieldedDamage = EnemyController.Instance.IsSpecialActive(GameUtils.EBossSpecial.Shield) ? damage / EnemyController.Instance.GetSpecialValue(GameUtils.EBossSpecial.Shield) : damage;

        healthCurrent -= shieldedDamage;
        
        // Update the health bar
        healthGreenBar.value = healthCurrent;

        imageGreen.color = yellowBar;

        if (healthCurrent <= 0)
        {
            EnemyDied();
        }

        // If there's already an existing damage flying text
        if (damageFlyingText)
        {
            // Increment the damage value shown
            damageFlyingTextValue += shieldedDamage;
            damageFlyingText.GetComponentInChildren<Text>().text = damageFlyingTextValue.ToString("F1");
        }
        else
        {
            // Only spawn damage flying text if the enemy is visible or dead
            if (radarObject.SpritesEnabled
                || isDead)
            {
                // Show damage with flying text
                FlyingTextObject.EFlyingTextBehaviour behaviour = isCritical ? FlyingTextObject.EFlyingTextBehaviour.FastAndHold : FlyingTextObject.EFlyingTextBehaviour.Normal;
                Color fontColor = isCritical ? Color.red : GameUtils.COLOR_ORANGE;
                FontStyle fontStyle = isCritical? FontStyle.Bold: FontStyle.Normal;
                damageFlyingText = WaveController.Instance.SpawnFlyingText(shieldedDamage.ToString("F1"), 
                                                                           transform.position, 
                                                                           FlyingTextObject.EFlyingTextType.Damage, 
                                                                           fontColor,
                                                                           fontStyle,
                                                                           behaviour,
                                                                           FlyingTextObject.EFlyingTextDirection.Up);
                damageFlyingTextValue = shieldedDamage;
            }
        }
    }
    
    private void EnemyDied()
    {
        //Debug.Log("Enemy " + name + " has been killed.");
        IsDead = true;

        if (!radarObject.SpritesEnabled)
        {
            hiddenKill = true;
        }
        
        // Sound hook - Enemy death sound
        AudioManager.Instance.PlaySound(enemyData.enemyDeathSound);

        // Enable the death anim with the right direction
        deathAnimContainer.SetActive(true);
        GameObject prefabEnemyDestroyed = GameUtils.GetChildGameObject(deathAnimContainer, "prefab_enemyDestroyed", true);
        prefabEnemyDestroyed.transform.rotation = spriteRenderer.transform.rotation;
        spriteRenderer.gameObject.SetActive(false);

        // Reveal one last time on radar
        radarObject.Death(1f);
    }

    private void DestroyEnemy()
    {
        int creditsToAdd = GetScore();
        if (creditsToAdd > 0)
        {
            if (hiddenKill)
            {
                creditsToAdd = Mathf.CeilToInt(creditsToAdd * GameUtils.HIDDEN_KILL_MULTIPLIER);
            }
            WaveController.Instance.RecordCredits(creditsToAdd, GameUtils.CreditsSource.EnemyDestroyed);
            if (BoosterController.Instance.BoosterTypeActive(BoosterController.EBoosterPackType.Credits))
            {
                int bonus = Mathf.CeilToInt(creditsToAdd * BoosterController.Instance.killCredits) - creditsToAdd;
                WaveController.Instance.RecordCredits(bonus, GameUtils.CreditsSource.BoosterEnemeyDestroyed);
                creditsToAdd += bonus;
            }
            WaveController.Instance.AddCredits(creditsToAdd, transform.position, GameUtils.CreditsSource.EnemyDestroyed, FlyingTextObject.EFlyingTextDirection.Up);
        }			

        // Achievement hook - Kill type
        AchievementProgress(enemyData.enemyType);

        // Achievement hook - Kills
        AchievementController.Instance.UpdateAchievementProgression(AchievementData.EAchievementType.Kills, 1, AchievementData.EAchievementOverride.Progress);

        // If unit is a bonus type, reward bonus
        if (enemyData.isUnitBonus)
        {
            LootController.Instance.SpawnLoot(transform.position);
        }

        SpecialActive(false);

        ClearObject();
    }

    private void AchievementProgress(GameUtils.UnitType unitType)
    {
        AchievementData.EAchievementType achievementType = AchievementData.EAchievementType.Kills;
        switch (unitType)
        {
            case GameUtils.UnitType.INFANTRY:           achievementType = AchievementData.EAchievementType.Infantry; break;
            case GameUtils.UnitType.LAV:                achievementType = AchievementData.EAchievementType.LAV; break;
            case GameUtils.UnitType.TANK:               achievementType = AchievementData.EAchievementType.Tank; break;
            case GameUtils.UnitType.HEAVYTANK:          achievementType = AchievementData.EAchievementType.HeavyArmor; break;
            case GameUtils.UnitType.CONVOY:             achievementType = AchievementData.EAchievementType.Convoy; break;
            case GameUtils.UnitType.TRANSPORTPLANE:     achievementType = AchievementData.EAchievementType.Transporter; break;
            case GameUtils.UnitType.FIGHTERJET:         achievementType = AchievementData.EAchievementType.Fighter; break;
            case GameUtils.UnitType.BOMBER:             achievementType = AchievementData.EAchievementType.Bomber; break;
            case GameUtils.UnitType.BALLISTICMISSILE:   achievementType = AchievementData.EAchievementType.BallisticMissile; break;
            case GameUtils.UnitType.HELICOPTER:         achievementType = AchievementData.EAchievementType.Helicopter; break;
            case GameUtils.UnitType.JEEP:               achievementType = AchievementData.EAchievementType.Jeep; break;
            case GameUtils.UnitType.DRONE:              achievementType = AchievementData.EAchievementType.Drone; break;
            case GameUtils.UnitType.ASSAULT_HELICOPTER: achievementType = AchievementData.EAchievementType.AssaultHelicopter; break;
            case GameUtils.UnitType.BUGGY:              achievementType = AchievementData.EAchievementType.Buggy; break;
            default: return;
        }
        AchievementController.Instance.UpdateAchievementProgression(achievementType, 1, AchievementData.EAchievementOverride.Progress);
    }

    public override void OnTriggerEnter2D(Collider2D coll)
    {
        if (EnemyData.movementType == GameUtils.EMovementType.StraightToBase && targetBase != null)
        {
            // only collide with the base that is the target
            if (coll.CompareTag(GameUtils.BASE_TAG) && coll.GetType() == typeof(BoxCollider2D) && coll.gameObject != targetBase)
            {
#if UNITY_EDITOR
                Debug.Log("Collision by " + gameObject.name + " with a base '" + coll.gameObject.name + "' that is not the target '" + targetBase.name + "' ! Not doing damage!");
#endif
                return;
            }
        }

        base.OnTriggerEnter2D(coll);

        if(coll.CompareTag(GameUtils.SLOWER_TAG))
        {
            IsSlow = true;          
        }

        // BOSS :: Bomber
        if (coll.CompareTag(GameUtils.BASE_TAG) && coll.GetType() == typeof(BoxCollider2D) && enemyData.bossSpecial == GameUtils.EBossSpecial.Bomb)
        {
            //Debug.Log("Base Hit");
            BaseObject baseObject = coll.GetComponent<BaseObject>();
            baseObject.Health -= enemyData.specialValue;
        }
    }

    private EnemyAttributes GetAttributes()
    {
        return enemyData.GetEnemyAttributes(ChallengeController.Instance.GetCurrentChallengeDifficulty());
    }

    public float GetPower()
    {
        return GetAttributes().Power;
    }

    public float GetHealthMax()
    {
        return GetAttributes().HealthMax * EnemyController.Instance.GetEnemyHealthMaxRankMultiplier(enemyRank, isBossUnit);
    }

    public float GetSpeed()
    {
        return GetAttributes().Speed * EnemyController.Instance.GetEnemySpeedRankMultiplier(enemyRank);
    }

    public int GetScore()
    {
        return (int)(GetAttributes().Score * EnemyController.Instance.GetEnemyScoreRankMultiplier(enemyRank));
    }

    private void CleanTrails()
    {
        while (activeBonusTrails != null && activeBonusTrails.Count > 0)
        {
            GameObject tempTrail = activeBonusTrails.Dequeue();
            if (tempTrail != null)
            {
                tempTrail.SetActive(false);
                WeaponController.Instance.inactiveBonusTrails.Enqueue(tempTrail);
            }
        }
        while (activeEnemyTrails != null && activeEnemyTrails.Count > 0)
        {
            GameObject tempTrail = activeEnemyTrails.Dequeue();
            if (tempTrail != null)
            {
                tempTrail.SetActive(false);
                WeaponController.Instance.inactiveEnemyTrails.Enqueue(tempTrail);
            }
        }
    }

    public void CleanUp()
    {
        cleanUp = true;
    }

    public override void ClearObject()
    {
        isDead = true;

        EnemyController.Instance.PutEnemyInPool(enemyData.enemyType, gameObject);
        base.ClearObject();

        // don't do these things if we are cleaning up
        if (cleanUp) return;

        // Remove this enemy from the alive enemies
        if (!EnemyController.ApplicationIsQuitting)
        {
            EnemyController.Instance.AliveEnemies.Remove(gameObject);

            // If enemy spawned from wave settings
            if (isEnemyFromWave)
            {
                EnemyController.Instance.NumAliveEnemiesFromWave--;
            }            
        }

        // check if the wave is finished
        if (!WaveController.ApplicationIsQuitting && !BaseController.ApplicationIsQuitting && !AudioManager.ApplicationIsQuitting)
        {
            WaveController.Instance.CheckIfClearedWaves();
        }

        if (EnemyController.ApplicationIsQuitting) return;

        CleanTrails();
    }
}

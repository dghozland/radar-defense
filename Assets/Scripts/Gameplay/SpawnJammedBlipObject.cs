using UnityEngine;
using System.Collections;

public class SpawnJammedBlipObject : SpawnObject
{
    public override void Init()
    {
        base.Init();
        gameObject.GetComponent<Animator>().Play("JammedBlip", 0, 0);
    }

    public override void CleanObject()
    {
        // put back to pool
        EnemyController.Instance.PutJammedBlipInPool(gameObject);
    }
}


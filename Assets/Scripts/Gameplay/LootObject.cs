using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections.Generic;

public class LootObject : MonoBehaviour
{
    public enum ELootType
    {
        Credits,
        Gold,
        MatchBoost,
        Drone,
        Pulse,
        Reload,
        Repair,
        Slower,
        OhShit
    }

    private Button lootButton;
    private ELootType lootType;    
    private float lifeTimer;

    private void Awake()
    {
        lootButton = GetComponentInChildren<Button>();
        lootButton.onClick.AddListener(delegate { LootButtonClicked(); });
    }

    public void Init(ELootType lootType, float lifeTime, bool registerLoot)
    {
        if (registerLoot) EnemyController.Instance.RegisterLoot(gameObject);
        this.lootType = lootType;

        // Set the loot icon
        GameUtils.GetChildGameObject(gameObject, "LootImage").GetComponent<Image>().sprite = GetLootIcon(lootType);

        // Set the loot effect color
        GameObject lootEffectContainer = GameUtils.GetChildGameObject(gameObject, "targetOut");
        Image[] lootEffectImages = lootEffectContainer.GetComponentsInChildren<Image>();
        for (int i = 0; i < lootEffectImages.Length; ++i)
        {
            lootEffectImages[i].color = GetLootColor(lootType);
        }

        // Set the loot life
        lifeTimer = lifeTime;
    }

    private void Update()
    {
        lifeTimer -= Time.deltaTime;
        
        // If life ended
        if (lifeTimer <= 0f)
        {
            ClearObject();
        }
    }

    private void LootButtonClicked()
    {
        CollectLoot();
    }

    private void CollectLoot()
    {
        switch (lootType)
        {
            case ELootType.Credits:
                int lootCreditAmount = LootController.Instance.creditLootValues.RollLootValue(ChallengeController.Instance.GetCurrentChallengeDifficulty());
                WaveController.Instance.RecordCredits(lootCreditAmount, GameUtils.CreditsSource.LootReward);
                if (BoosterController.Instance.BoosterTypeActive(BoosterController.EBoosterPackType.Credits))
                {
                    int bonus = Mathf.CeilToInt(lootCreditAmount * BoosterController.Instance.lootCredits) - lootCreditAmount;
                    WaveController.Instance.RecordCredits(bonus, GameUtils.CreditsSource.BoosterLootReward);
                    lootCreditAmount += bonus;
                }
                WaveController.Instance.AddCredits(lootCreditAmount, transform.position, GameUtils.CreditsSource.LootReward, FlyingTextObject.EFlyingTextDirection.Up);
                break;
            case ELootType.Gold:
                int lootGoldAmount = LootController.Instance.goldLootValues.RollLootValue(ChallengeController.Instance.GetCurrentChallengeDifficulty());
                WaveController.Instance.AddGold(lootGoldAmount, transform.position, GameUtils.GoldSource.LootReward, FlyingTextObject.EFlyingTextDirection.Up);
                break;
            case ELootType.MatchBoost:
                BoosterController.Instance.AwardBattleBoost(transform.position);
                break;
            case ELootType.Drone:
                ConsumableController.Instance.ActivateDroneConsumable(transform.position);
                break;
            case ELootType.OhShit:
                ConsumableController.Instance.ActivateOhShitConsumable(true);
                BuildingController.Instance.ConsumableConsumed(lootType);
                break;
            case ELootType.Pulse:
                ConsumableController.Instance.ActivatePulseConsumable(true);
                BuildingController.Instance.ConsumableConsumed(lootType);
                break;
            case ELootType.Reload:
                ConsumableController.Instance.ActivateReloadConsumable(true);
                BuildingController.Instance.ConsumableConsumed(lootType);
                break;
            case ELootType.Repair:
                ConsumableController.Instance.ActivateRepairConsumable(true);
                BuildingController.Instance.ConsumableConsumed(lootType);
                break;
            case ELootType.Slower:
                ConsumableController.Instance.ActivateSlowerConsumable(true);
                BuildingController.Instance.ConsumableConsumed(lootType);
                break;
        }

        // Sound hook - Loot collect
        AudioManager.Instance.PlaySound(AudioManager.ESoundType.LootCollect);

        ClearObject();
    }

    private Sprite GetLootIcon(ELootType lootType)
    {
        Sprite lootIcon = null;
        switch (lootType)
        {
            case ELootType.Credits:
                lootIcon = (Sprite)Resources.Load("Images/LootCreditsIcon", typeof(Sprite));
                break;
            case ELootType.Gold:
                lootIcon = (Sprite)Resources.Load("Images/LootGoldIcon", typeof(Sprite));
                break;
            case ELootType.MatchBoost:
                lootIcon = (Sprite)Resources.Load("Images/LootMatchBoostIcon", typeof(Sprite));
                break;
            case ELootType.Drone:
                lootIcon = (Sprite)Resources.Load("Images/LootDroneIcon", typeof(Sprite));
                break;
            case ELootType.OhShit:
                lootIcon = (Sprite)Resources.Load("Images/LootMissileIcon", typeof(Sprite));
                break;
            case ELootType.Pulse:
                lootIcon = (Sprite)Resources.Load("Images/LootPulseIcon", typeof(Sprite));
                break;
            case ELootType.Reload:
                lootIcon = (Sprite)Resources.Load("Images/LootAmmoIcon", typeof(Sprite));
                break;
            case ELootType.Repair:
                lootIcon = (Sprite)Resources.Load("Images/LootRepairIcon", typeof(Sprite));
                break;
            case ELootType.Slower:
                lootIcon = (Sprite)Resources.Load("Images/LootSlowIcon", typeof(Sprite));
                break;
        }

        return lootIcon;
    }

    private Color GetLootColor(ELootType lootType)
    {
        Color lootColor = Color.white;
        switch (lootType)
        {
            case ELootType.Credits:
                lootColor = GameUtils.COLOR_CYAN_CREDITS;
                break;
            case ELootType.Gold:
                lootColor = GameUtils.COLOR_YELLOW_GOLD;
                break;
            case ELootType.MatchBoost:
                lootColor = GameUtils.COLOR_GREEN_BOOST;
                break;
            case ELootType.Drone:
                lootColor = GameUtils.COLOR_RED_SUPPORT;
                break;
            case ELootType.OhShit:
            case ELootType.Pulse:
            case ELootType.Reload:
            case ELootType.Repair:
            case ELootType.Slower:
                lootColor = GameUtils.COLOR_RED_SUPPORT;
                break;
        }

        return lootColor;
    }

    public void ClearObject()
    {
        EnemyController.Instance.UnregisterLoot(gameObject);
        LootController.Instance.PutLootInPool(gameObject);
        WaveController.Instance.CheckIfClearedWaves();
    }
}

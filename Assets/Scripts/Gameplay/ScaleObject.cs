using UnityEngine;

public class ScaleObject : MonoBehaviour
{
    private bool positioned = false;
    private bool scaled = false;

    [SerializeField]
    private bool scalePosition = true;
    [SerializeField]
    private bool scaleSize = true;
    [SerializeField]
    private bool inverseSize = false;

    public bool IsScaled
    {
        get { return positioned && scaled; }
    }

    public void Awake()
    {
        ApplyScale();
    }
        
    private void Update()
    {
        if (!positioned)
        {
            ApplyPosition();
        }
        if (!scaled)
        {
            ApplyScale();
        }
    }

    private void ApplyPosition()
    {
        if (PlayfieldController.Instance.Initialized)
        {
            if (scalePosition)
            {
                // do the position adjustment
                Vector3 position = gameObject.transform.position;
                gameObject.transform.position = PlayfieldController.Instance.AdjustScalePosition(position);
            }

            positioned = true;
        }
    }

    private void ApplyScale()
    {
        if (PlayfieldController.Instance.Initialized)
        {
            if (scaleSize && !inverseSize)
            {
                // do the scale
                Vector3 localScale = gameObject.transform.localScale;
                gameObject.transform.localScale = PlayfieldController.Instance.AdjustScaleSize(localScale);
            }
            if (scaleSize && inverseSize)
            {
                // do the scale
                Vector3 localScale = gameObject.transform.localScale;
                gameObject.transform.localScale = PlayfieldController.Instance.AdjustScaleSizeInverse(localScale);
            }

            scaled = true;
        }
    }
}

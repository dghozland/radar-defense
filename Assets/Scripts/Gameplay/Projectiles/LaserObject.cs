using UnityEngine;
using System.Collections.Generic;

#pragma warning disable 0649

public class LaserObject : MovingWeaponObject
{
    [SerializeField]
    private Color laserHitColor;

    [SerializeField]
    private Color laserNormalColor;

    [SerializeField]
    private float muzzleOffset = 25f;

    private Vector3 intersection = new Vector3();
    private float chargeTime;
    private float shotTime;
    private float laserWidth;
    private bool drawn;
    private bool fire;
    private List<Collider2D> hitObjects = new List<Collider2D>();
    private BoxCollider2D collisionComponent;
    private Animation anim;
    private bool laserHit;
    private int enemyKillCount;
    private float chargeSpeed;
    private bool activeLines;

    public override void Update()
    {
        if (WaveController.Instance.Paused) return;
        if (!initialized) return;

        UpdateLaser();
    }

    public override void FixedUpdate()
    {
        if (WaveController.Instance.Paused) return;
        if (!initialized) return;

        RotateObject();
    }

    public override void Init()
    {
        base.Init();
        targetEnemy = null;
        GetComponent<SpriteRenderer>().color = laserNormalColor;
        chargeSpeed = WeaponController.Instance.FindWeaponData(GameUtils.BaseID.LASER).speed / speed;
        laserWidth = (GameUtils.LASER_BASIC_WIDTH + (Radius / GameUtils.RADIUS_FACTOR)) * PlayfieldController.Instance.WidthScale;

        // find a direction for the laser
        FindEnemyInCone();
        if (targetEnemy != null)
        {
            CreateConeLines(startPosition);
            ActivateLines(true);
            activeLines = true;
        }
        else
        {
            ActivateLines(false);
            activeLines = false;
        }

        chargeTime = speed;
        enemyDetectedAnimTimer = speed;
        enemyDetectionAnimDuration = speed;
        shotTime = GameUtils.LASER_SHOT_TIME;
        drawn = false;
        fire = false;
        hitObjects.Clear();
        laserHit = false;
        enemyKillCount = 0;
        collisionComponent = GetComponent<BoxCollider2D>();
        anim = GetComponent<Animation>();
        
        // Disable collision during charging
        collisionComponent.enabled = false;

        ChargeLaser();
    }

    private void ActivateLines(bool active)
    {
        if (lineOneGO && lineTwoGO)
        {
            lineOneGO.SetActive(active);
            lineTwoGO.SetActive(active);
        }
    }

    private void UpdateLaser()
    {
        if (chargeTime > 0f)
        {
            chargeTime -= Time.deltaTime;
            ChargeLaser();
            return;
        }
        if (shotTime > 0f)
        {
            shotTime -= Time.deltaTime;
            FireLaser();
            return;
        }

        WaveController.Instance.ManageMultipleKills(enemyKillCount, transform.position);
        ClearObject();
    }

    private void ChargeLaser()
    {
        // follow the enemy if there is one
        RotateLaser();
        if (activeLines)
        {
            UpdateLines(startPosition, laserWidth);
            // update colors of line0 and line1
            UpdateOuterLineColor();
        }

        // position the laser
        intersection = GameUtils.FindBorderPosition(startPosition, direction);
        Vector3 middle = startPosition + ((intersection - startPosition) / 2);
        transform.position = middle;
        Vector3 localScale = transform.localScale;
        localScale.x = Vector3.Magnitude(startPosition - intersection);
        localScale.y = Mathf.Lerp(laserWidth, 0, chargeTime/speed);
        transform.localScale = localScale;

        if (drawn) return;
        drawn = true;
        // speed up charge according to speed value
        foreach (AnimationState state in anim)
        {
            state.speed = chargeSpeed;
        }
    }

    private void UpdateOuterLineColor()
    {
        if (lineOne && lineTwo)
        {
            lineOne.SetColors(spriteRenderer.color, spriteRenderer.color);
            lineTwo.SetColors(spriteRenderer.color, spriteRenderer.color);
        }
    }

    private void FireLaser()
    {
        if (fire) return;
        fire = true;

        ActivateLines(false);

        // enable collision again
        collisionComponent.enabled = true;
        
        // only the charge is affected by the speed, when shot slow down to normal speed
        foreach (AnimationState state in anim)
        {
            state.speed = 1;
        }

        // Sound hook - Play extra sound for laser shot
        AudioManager.Instance.PlaySound(WeaponController.Instance.FindWeaponData(GameUtils.BaseID.LASER).extraSound);
    }

#if UNITY_EDITOR
    public void OnDrawGizmos()
    {
        Debug.DrawLine(startPosition, intersection, Color.magenta);
    }
#endif

    public override void OnTriggerEnter2D(Collider2D coll)
    {
        CheckCollision(coll);
    }

    public void OnTriggerStay2D(Collider2D coll)
    {
        CheckCollision(coll);
    }

    private void CheckCollision(Collider2D coll)
    {
        if (coll.CompareTag(GameUtils.AIR_TAG) || coll.CompareTag(GameUtils.GROUND_TAG))
        {
            // Note :: Because the collision is checked OnTriggerStay2D,
            // do not damage the target another time.
            if (hitObjects.Contains(coll)) return;

            MovingEnemyObject enemyObject = coll.gameObject.GetComponent<MovingEnemyObject>();
            if (enemyObject)
            {
                if (!laserHit)
                {
                    // Change the visual of the laser
                    GetComponent<SpriteRenderer>().color = laserHitColor;
                    laserHit = true;
                }

                float damage = coll.CompareTag(GameUtils.GROUND_TAG) ? GroundPower : AirPower;
                if (damage > 0f)
                {
                    bool isDirectHit = shotTime > GameUtils.LASER_SHOT_TIME * 0.9f;

                    // Reduce enemy's HP based on the damage type
                    enemyObject.ReceiveDamage(damage, isDirectHit);

                    // Increment kill count to show multiple kills notice
                    if (enemyObject.IsDead)
                    {
                        enemyKillCount++;
                    }
                }

                hitObjects.Add(coll);
            }
        }
    }

    private void FindEnemyInCone()
    {
        base.targetEnemy = null;
        float currentAngle = float.PositiveInfinity;
        Vector3 directionToEnemy = direction;
        
        foreach (var enemy in EnemyController.Instance.AliveEnemies)
        {
            // don't get dead enemies in target
            if (!CheckTargetAlive(enemy)) continue;

            // vector between enemy and missile
            directionToEnemy = (enemy.transform.position - gameObject.transform.position).normalized;
            float projection = Vector2.Dot(directionToEnemy, direction);

            // enemy behind laser
            if (projection < 0) continue;

            // check angle
            float angle = Mathf.Abs(Vector2.Angle(direction, directionToEnemy));

            // give a bit more to the angle because the players eye is not as accurate as the math
            if (angle < laserWidth && angle < currentAngle)
            {
                currentAngle = angle;
                targetEnemy = enemy;
            }
        }
    }

    private void RotateLaser()
    {
        // make sure this does not break when weapon is destroyed
        if (!WeaponController.Instance.ActiveWeapons.ContainsKey(GameUtils.BaseID.LASER)) return;

        WeaponObject weaponObject = WeaponController.Instance.ActiveWeapons[GameUtils.BaseID.LASER];
        if (weaponObject == null) return;

        startPosition = weaponObject.gameObject.transform.position;
        if (targetEnemy)
        {
            targetPosition = targetEnemy.transform.position;
            Vector3 targetDirection = (targetPosition - startPosition).normalized;

            // the speed to rotate towards the enemy
            float step = (Mathf.Deg2Rad * laserWidth) / speed * Time.deltaTime;
            direction = Vector3.RotateTowards(direction, targetDirection, step, 0f);
        }
        // the start offset of the laser
        weaponObject.WeaponImageTop.transform.localRotation = Quaternion.LookRotation(Vector3.forward, targetPosition - weaponObject.WeaponImageTop.transform.position);
        startPosition += direction * muzzleOffset * PlayfieldController.Instance.WidthScale;
    }
}

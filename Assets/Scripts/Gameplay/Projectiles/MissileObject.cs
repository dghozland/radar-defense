using UnityEngine;
using System;

#pragma warning disable 0649

public class MissileObject : MovingWeaponObject
{
    private float closeDistanceSqr;
    private bool firstCheck;

    public override void Init()
    {
        base.Init();

        // create cone lines
        CreateConeLines(gameObject.transform.position);

        continueAfterTarget = true;
        targetEnemy = null;
        closeDistanceSqr = GameUtils.MISSILE_CLOSE_DISTANCE * GameUtils.MISSILE_CLOSE_DISTANCE * PlayfieldController.Instance.WidthScale;
        firstCheck = false;
    }

    public override void Update()
    {
        if (WaveController.Instance.Paused) return;
        if (!initialized) return;
        base.Update();

        UpdateLines(gameObject.transform.position, GameUtils.MISSILE_OPEN_ANGLE);

        // if we have a target don't check for a new one
        if (CheckTargetAlive(targetEnemy))
        {
            return;   
        }
        // no target, try to find a new one
        targetEnemy = null;
        CheckEnemyInCone();
        firstCheck = true;
    }
    
    private void CheckEnemyInCone()
    {
        foreach (var enemy in EnemyController.Instance.AliveEnemies)
        {
            // don't get dead enemies in target
            if (!CheckTargetAlive(enemy)) continue;

            // vector between enemy and missile
            Vector3 between = enemy.transform.position - gameObject.transform.position;
            float projection = Vector2.Dot(between, direction);

            // enemy behind missile
            if (projection < 0) continue;

            float checkAngle;

            // depending on distance use different angle
            float distanceSqr = Vector3.SqrMagnitude(between);
            if (!firstCheck && distanceSqr < closeDistanceSqr)
            {
                // enemy close use wider angle
                checkAngle = GameUtils.MISSILE_OPEN_ANGLE_CLOSE + GameUtils.MISSILE_OPEN_ANGLE_EPSILON;
            }
            else
            {
                // enemy far, use standard angle
                checkAngle = GameUtils.MISSILE_OPEN_ANGLE + GameUtils.MISSILE_OPEN_ANGLE_EPSILON;
            }

            // check angle
            float angle = Vector2.Angle(direction, between);

            // give a bit more to the angle because the players eye is not as accurate as the math
            if (Mathf.Abs(angle) < checkAngle)
            {
                // If there's already a target locked
                if (targetEnemy)
                {
                    // if current target is closer than new found enemy, keep it
                    if (Vector2.SqrMagnitude(targetEnemy.transform.position - gameObject.transform.position) < Vector2.SqrMagnitude(enemy.transform.position - gameObject.transform.position)) continue;
                }
                // If first target
                else
                {
                    // Start the enemy detected anim
                    enemyDetectedAnimTimer = enemyDetectionAnimDuration;
                }

                // new enemy target
                targetEnemy = enemy;
            }
        }
    }
}

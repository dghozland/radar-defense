using UnityEngine;

public class RailgunObject : MovingWeaponObject
{
    public override void Init()
    {
        base.Init();
        hasExploded = false;
    }

    public override void Update()
    {
        if (WaveController.Instance.Paused) return;
        if (!initialized) return;

        float totalDistanceSqr = Vector3.SqrMagnitude(targetPosition - startPosition);
        float distanceTraveledSqr = Vector3.SqrMagnitude(transform.position - startPosition);
        if (totalDistanceSqr <= distanceTraveledSqr)
        {
            // explode the rail shells
            transform.position = targetPosition;
            explodeInSeconds -= Time.deltaTime;
            if (!hasExploded)
            {
                ClearObject();
                SpawnExplosion();
                hasExploded = true;
            }
        }
        else
        {
            // only move if not at target yet.
            transform.position += direction * Time.deltaTime * speed;
        }

        if (explodeInSeconds < 0f)
        {
            ClearObject();
        }
        CheckOutside();
        UpdateTrails(currentTrailCount, ref activeWeaponTrails, ref WeaponController.Instance.inactiveWeaponTrails);
    }

    public override void FixedUpdate()
    {
        if (WaveController.Instance.Paused) return;
        if (!initialized) return;

        RotateObject();
    }
}

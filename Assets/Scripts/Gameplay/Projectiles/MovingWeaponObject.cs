using UnityEngine;

public class MovingWeaponObject : MovingObject
{
    [SerializeField]
    protected float enemyDetectionAnimDuration = 1f;

    [SerializeField]
    protected float explodeTimer = 0.25f;

    [SerializeField]
    private bool explodeOnTrigger = false;

    [SerializeField]
    protected GameUtils.LineParameters lineParameters;

    protected GameObject lineOneGO;
    protected GameObject lineTwoGO;
    protected LineRenderer lineOne;
    protected LineRenderer lineTwo;
    protected float enemyDetectedAnimTimer = 0f;

    private Vector3 lastDirOne = Vector3.zero;
    private Vector3 lastDirTwo = Vector3.zero;

    private bool explodeOnTargetOnly;
    
    private float radius;
    private float groundPower;
    private float airPower;
    protected bool hasExploded;
    protected GameObject targetEnemy;
    protected float explodeInSeconds;

    private GameUtils.BaseID weaponID;

    public GameUtils.BaseID WeaponID
    {
        set { weaponID = value; }
        get { return weaponID; }
    }

    public bool ExplodeOnTargetOnly
    {
        set { explodeOnTargetOnly = value; }
    }

    public override void Init()
    {
        base.Init();
        enemyDetectedAnimTimer = 0f;
        explodeInSeconds = explodeTimer;
        explodeOnTargetOnly = false;
        lastDirOne = Vector3.zero;
        lastDirTwo = Vector3.zero;
        //
        if (GetComponent<TrailRenderer>() != null)
        {
            GetComponent<TrailRenderer>().sortingOrder = 11;
        }
    }

    public float GroundPower
    {
        get { return groundPower; }
        set { groundPower = value; }
    }

    public float AirPower
    {
        get { return airPower; }
        set { airPower = value; }
    }

    public float Radius
    {
        get { return radius; }
        set { radius = value; }
    }

    public GameObject TargetEnemy
    {
        get { return targetEnemy; }
        set { targetEnemy = value; }
    }

    public void Awake()
    {
        if (lineParameters.lineMaterial == null)
        {
            lineParameters.lineMaterial = new Material(Shader.Find("Sprites/Default"));
        }
    }

    protected override void MoveObject()
    {
        if (targetEnemy)
        {
            targetPosition = targetEnemy.transform.position;
            direction = Vector3.Normalize(targetPosition - transform.position);
        }
        base.MoveObject();
    }

    public override void OnTriggerEnter2D(Collider2D coll)
    {
        // If does not explode on entering a trigger
        if (!explodeOnTrigger)
            return;

        // If already exploded
        if (hasExploded)
            return;

        // If is set to explode on the enemy target only and collider is not this target
        // Note :: Used by the homing missile only!
        if (explodeOnTargetOnly && coll.gameObject != targetEnemy)
            return;

        if (coll.CompareTag(GameUtils.AIR_TAG) || coll.CompareTag(GameUtils.GROUND_TAG))
        {
            ClearObject();
            SpawnExplosion();
        }
    }

    protected void SpawnExplosion()
    {
        WeaponController.Instance.SpawnExplosion(transform.position, GroundPower, AirPower, Radius);
    }

    public override void ClearObject()
    {
        WeaponController.Instance.PutProjectileInPool(WeaponID, gameObject);
        base.ClearObject();
    }

    protected bool CheckTargetAlive(GameObject enemy)
    {
        if (enemy)
        {
            MovingEnemyObject enemyObject = enemy.GetComponent<MovingEnemyObject>();
            if (!enemyObject.IsDead)
            {
                return true;
            }
        }
        return false;
    }

    protected void CreateConeLines(Vector3 startPosition)
    {
        // create cone lines
        if (lineOneGO == null)
        {
            lineOneGO = new GameObject("LineOne");
            lineOneGO.transform.parent = gameObject.transform;
            lineOne = lineOneGO.AddComponent<LineRenderer>();
            lineOne.material = lineParameters.lineMaterial;
            lineOne.SetColors(lineParameters.lineLargeColor, lineParameters.lineLargeColor);
            lineOne.SetWidth(lineParameters.lineLargeWidth, lineParameters.lineLargeWidth);
            lineOne.sortingOrder = 10;
        }

        lineOneGO.transform.position = startPosition;

        if (lineTwoGO == null)
        {
            lineTwoGO = new GameObject("LineTwo");
            lineTwoGO.transform.parent = gameObject.transform;
            lineTwo = lineTwoGO.AddComponent<LineRenderer>();
            lineTwo.material = lineParameters.lineMaterial;
            lineTwo.SetColors(lineParameters.lineLargeColor, lineParameters.lineLargeColor);
            lineTwo.SetWidth(lineParameters.lineLargeWidth, lineParameters.lineLargeWidth);
            lineTwo.sortingOrder = 10;
        }

        lineTwoGO.transform.position = startPosition;
    }

    protected void UpdateLines(Vector3 startPosition, float openAngle)
    {
        if (!targetEnemy)
        {
            SetLinesPositionsWithOpenAngle(startPosition, openAngle, 1f);
        }
        else
        {
            if (enemyDetectedAnimTimer > 0f)
            {
                SetLinesPositionsWithOpenAngle(startPosition, openAngle, enemyDetectedAnimTimer / enemyDetectionAnimDuration);
                enemyDetectedAnimTimer -= Time.deltaTime;
                enemyDetectedAnimTimer = Mathf.Max(enemyDetectedAnimTimer, 0);
            }
            else
            {
                SetLinesPositions(startPosition, targetEnemy.transform.position, targetEnemy.transform.position);
            }
        }
    }

    private void SetLinesPositionsWithOpenAngle(Vector3 startPosition, float openAngle, float t)
    {
        Vector3 dirOne = Vector3.Normalize(Quaternion.AngleAxis(-openAngle * t, Vector3.forward) * direction);
        Vector3 dirTwo = Vector3.Normalize(Quaternion.AngleAxis(openAngle * t, Vector3.forward) * direction);

        if (lastDirOne != Vector3.zero && GameUtils.AngleDir(dirOne, lastDirOne) < 0)
        {
            dirOne = lastDirOne;
        }
        lastDirOne = dirOne;
        if (lastDirTwo != Vector3.zero && GameUtils.AngleDir(dirTwo, lastDirTwo) > 0)
        {
            dirTwo = lastDirTwo;
        }
        lastDirTwo = dirTwo;

        Vector3 lineOneEndPos = GameUtils.FindBorderPosition(startPosition, dirOne);
        Vector3 lineTwoEndPos = GameUtils.FindBorderPosition(startPosition, dirTwo);

        SetLinesPositions(startPosition, lineOneEndPos, lineTwoEndPos);
    }

    private void SetLinesPositions(Vector3 startPosition, Vector3 lineOneEndPos, Vector3 lineTwoEndPos)
    {
        lineOne.SetPosition(0, startPosition);
        lineOne.SetPosition(1, lineOneEndPos);

        lineTwo.SetPosition(0, startPosition);
        lineTwo.SetPosition(1, lineTwoEndPos);
    }
}

using UnityEngine;

public class ArtilleryObject : MovingWeaponObject
{
    private bool explode;
    private float totalDistance;
    private float rotationPerUnit;
    private float amplitude;
    private Vector3 right;
    private Vector3 originalPosition;
    private Vector3 lastPosition;

    public override void Init()
    {
        base.Init();
        explode = false;
        amplitude = GameUtils.CURVE_AMPLITUDE_ARTILLERY * PlayfieldController.Instance.HeightScale;
        if (direction.x < 0)
        {
            amplitude *= -1f;
        }
        right = Quaternion.AngleAxis(90f, Vector3.forward) * direction;
        originalPosition = startPosition;
        totalDistance = Vector3.Magnitude(targetPosition - startPosition);
        rotationPerUnit = Mathf.PI / totalDistance;
        lastPosition = startPosition;
    }

    public override void Update()
    {
        if (WaveController.Instance.Paused) return;
        if (!initialized) return;

        float totalDistanceSqr = Vector3.SqrMagnitude(targetPosition - startPosition);
        float distanceTraveledSqr = Vector3.SqrMagnitude(transform.position - startPosition);
        if (totalDistanceSqr <= distanceTraveledSqr)
        {
            // explode the artillery shells
            transform.position = targetPosition;
            explodeInSeconds -= Time.deltaTime;
            if (!explode)
            {
                ClearObject();
                SpawnExplosion();
                explode = true;
            }
        }
        else
        {
            // only move if not at target yet.
            originalPosition += direction * Time.deltaTime * speed;
            float distanceTraveled = Vector3.Magnitude(transform.position - startPosition);
            float currentAngle = rotationPerUnit * distanceTraveled;
            lastPosition = transform.position;
            transform.position = originalPosition + (right * Mathf.Sin(currentAngle) * amplitude);
        }

        if (explodeInSeconds < 0f)
        {
            ClearObject();
        }

        CheckOutside();
        UpdateTrails(currentTrailCount, ref activeWeaponTrails, ref WeaponController.Instance.inactiveWeaponTrails);
    }

    public override void FixedUpdate()
    {
        if (WaveController.Instance.Paused) return;
        if (!initialized) return;

        RotateObject();
    }

    protected override void RotateObject()
    {
        if (explode) return;
        Vector3 currentDirection = (transform.position - lastPosition).normalized;
        float angle = Vector3.Angle(Vector3.right, currentDirection);
        if (currentDirection.y < 0)
        {
            angle *= -1f;
        }
        if (currentDirection.x < 0)
        {
            spriteRenderer.flipY = true;
        }

        transform.rotation = Quaternion.Euler(0f, 0f, angle);
    }
}

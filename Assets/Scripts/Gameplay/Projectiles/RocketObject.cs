using UnityEngine;

public class RocketObject : MovingWeaponObject
{
    private int swings;
    private float totalDistance;
    private float roatationPerUnit;
    private float amplitude;
    private Vector3 right;
    private Vector3 originalPosition;
    private Vector3 lastPosition;

    [SerializeField]
    private int maxSwings = 7;

    public override void Init()
    {
        base.Init();
        hasExploded = false;
        swings = Random.Range(1, maxSwings);
        amplitude = Random.Range(3f, 20f) * PlayfieldController.Instance.HeightScale;
        right = Quaternion.AngleAxis(90f, Vector3.forward) * direction;
        originalPosition = startPosition;
        totalDistance = Vector3.Magnitude(targetPosition - startPosition);
        roatationPerUnit = (Mathf.PI * 2) / (totalDistance / swings);
    }

    public override void Update()
    {
        if (WaveController.Instance.Paused) return;
        if (!initialized) return;

        float totalDistanceSqr = Vector3.SqrMagnitude(targetPosition - startPosition);
        float distanceTraveledSqr = Vector3.SqrMagnitude(transform.position - startPosition);
        if (totalDistanceSqr <= distanceTraveledSqr)
        {
            // explode the Rockets
            transform.position = targetPosition;
            explodeInSeconds -= Time.deltaTime;
            if (!hasExploded)
            {
                ClearObject();
                SpawnExplosion();
                hasExploded = true;
            }
        }
        else
        {
            // only move if not at target yet.
            originalPosition += direction * Time.deltaTime * speed;
            float distanceTraveled = Vector3.Magnitude(transform.position - startPosition);
            float currentAngle = roatationPerUnit * distanceTraveled;
            lastPosition = transform.position;
            transform.position = originalPosition + (right * Mathf.Sin(currentAngle) * amplitude);
        }

        if (explodeInSeconds < 0f)
        {
            ClearObject();
        }
        CheckOutside();
        UpdateTrails(currentTrailCount, ref activeWeaponTrails, ref WeaponController.Instance.inactiveWeaponTrails);
    }

    public override void FixedUpdate()
    {
        if (WaveController.Instance.Paused) return;
        if (!initialized) return;

        RotateObject();
    }

    protected override void RotateObject()
    {
        if (hasExploded) return;

        Vector3 currentDirection = (transform.position - lastPosition).normalized;
        float angle = Vector3.Angle(Vector3.right, currentDirection);
        if (currentDirection.y < 0)
        {
            angle *= -1f;
        }
        if (currentDirection.x < 0)
        {
            spriteRenderer.flipY = true;
        }

        transform.rotation = Quaternion.Euler(0f, 0f, angle);
    }
}

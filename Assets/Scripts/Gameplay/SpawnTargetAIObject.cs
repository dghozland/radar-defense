using UnityEngine;
using System.Collections;

public class SpawnTargetAIObject : SpawnObject
{
#if ENABLE_WEAPON_AI
    public override void Init()
    {
        base.Init();
        gameObject.GetComponent<Animator>().Play("ProjectileTargetAIMarker", 0, 0);
    }

    public override void CleanObject()
    {
        // put back to pool
        WeaponController.Instance.PutTargetAIMarkerToPool(gameObject);
    }
#endif
}


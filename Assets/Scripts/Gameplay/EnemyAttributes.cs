﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class EnemyAttributes
{
    [SerializeField]
    private float health = 1f;

    [SerializeField]
    private float power = 1f;
    
    [SerializeField]
    private float speed = 1f;

    [SerializeField]
    private int score = 0;

    public float HealthMax
    {
        get { return health; }
    }

    public float Power
    {
        get { return power; }
    }

    public float Speed
    {
        get { return speed; }
    }

    public int Score
    {
        get { return score; }
    }
}

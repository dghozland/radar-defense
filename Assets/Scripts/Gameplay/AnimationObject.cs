using UnityEngine;

public class AnimationObject : MonoBehaviour
{
    private Animation anim;
    private Animator animator;
    private float speed;

    public void Awake()
    {
        anim = GetComponent<Animation>();
        animator = GetComponent<Animator>();
    }

    public void Update()
    {
        if (WaveController.Instance.Paused)
        {
            if (anim != null && anim.enabled)
            {
                // pause Animation
                anim.enabled = false;
            }
            if (animator != null && animator.speed > 0)
            {
                speed = animator.speed;
                animator.speed = 0;
            }        
        }
        else
        {
            if (anim != null && !anim.enabled)
            {
                // unpause Animation         
                anim.enabled = true;
            }
            if (animator != null && animator.speed == 0)
            {
                animator.speed = speed;
            }
        }
    }
}

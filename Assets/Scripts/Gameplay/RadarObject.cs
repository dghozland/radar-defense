using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RadarObject : MonoBehaviour
{
    private SpriteRenderer[] spriteRenderers;
    private CanvasGroup canvasGroup;

    private bool isDisabled;
    private float revealTimer;
    private float fadeTimer;
    private float halfSecondTimer;
    private bool blipPlayed;
    private bool soundPlayed;
    private bool isTrail;
    private bool isAlwaysVisible;
    private float trailStartColorAlpha;
    private float trailEndColorAlpha;

    [SerializeField]
    private float defaultFadeDuration = 1f;


    [SerializeField]
    private bool alwaysVisible = false;

    public bool SpritesEnabled
    {
        get
        {
            for (int i = 0; i < spriteRenderers.Length; ++i)
            {
                if (spriteRenderers[i].enabled) return true;
            }
            return false;
        }
    }
    

    public float Transparency
    {
        get
        {
            var c = spriteRenderers[0].material.color;
            return c.a;
        }
    }

    public bool isVisible()
    {
        if (canvasGroup)
        {
            return canvasGroup.alpha > 0f;
        }
        else
        {
            return spriteRenderers[0].enabled;
        }
    }

    public float GetCanvasGroupAlpha()
    {
        if (canvasGroup)
        {
            return canvasGroup.alpha;
        }

        return 0f;
    }

    public void Reset()
    {
        isDisabled = false;
        revealTimer = 0f;
        fadeTimer = 0f;
        halfSecondTimer = 0.5f;
        blipPlayed = false;
        soundPlayed = false;
        isTrail = gameObject.name.Contains("Trail");
        isAlwaysVisible = alwaysVisible;
        SetTransparency(isAlwaysVisible ? 1f : 0f);
        if (!EnemyController.Instance.IsSpecialActive(GameUtils.EBossSpecial.JamRadar) && !isAlwaysVisible)
        {
            SetTransparency(0);
        }
    }

    public void Awake()
    {
        spriteRenderers = GetComponentsInChildren<SpriteRenderer>();
        canvasGroup = GetComponentInChildren<CanvasGroup>();

        TrailRenderer[] trailRenderers = GetComponentsInChildren<TrailRenderer>();
        // Note :: Should have one trail renderer at maximum
        for (int i = 0; i < trailRenderers.Length; ++i)
        {
            // i.e. 0.78f & 0.39f
            trailStartColorAlpha = trailRenderers[i].startColor.a;
            trailEndColorAlpha = trailRenderers[i].endColor.a;
        }

        Reset();
    }

    public void Update()
    {
        if (WaveController.Instance.Paused) return;
        
        UpdateTransparency();
        UpdatePhysics();
    }
    
    private void UpdateTransparency()
    {
        // Reveal the unit
        if (revealTimer > 0f)
        {
            revealTimer -= Time.deltaTime;

            // If reveal state is over
            if (revealTimer <= 0f)
            {
                // Start the fade state
                fadeTimer = defaultFadeDuration;
            }
        }

        // Fade the unit
        if (fadeTimer > 0f)
        {
            fadeTimer -= Time.deltaTime;

            // Clamp timer to 0 in order to set
            // the transparency to 0 when fade is over
            if (fadeTimer < 0f)
            {
                fadeTimer = 0f;
                blipPlayed = false;
                soundPlayed = false;
            }

            // don't show the radar object if jammed, and also don't show the trails!
            if (EnemyController.Instance.IsSpecialActive(GameUtils.EBossSpecial.JamRadar) && (blipPlayed || isTrail) && !isDisabled)
            {
                SetTransparency(0);
            }
            else
            {
                SetTransparency(fadeTimer / defaultFadeDuration);
            }
        }
    }

    private void UpdatePhysics()
    {
        // Every half second renew the physics
        // Note :: For an object that is not moving (bases), colliders
        // deactivate after a certain period of time and this is to prevent that.
        if (halfSecondTimer > 0f)
        {
            halfSecondTimer -= Time.deltaTime;
        }
        else
        {
            halfSecondTimer = 0.5f;
            //RadarObject radarObject = GetComponent<RadarObject>();
            //radarObject.DisableRadar(false);
            Rigidbody2D rigid2D = GetComponent<Rigidbody2D>();
            if (rigid2D) rigid2D.WakeUp();
        }
    }

    private void SetTransparency(float value)
    {
        // Clamp the value
        value = Mathf.Clamp01(value);

        for (int i = 0; i < spriteRenderers.Length; ++i)
        {
            if (value > 0)
            {
                // enable and set transparency
                spriteRenderers[i].enabled = true;
                var c = spriteRenderers[i].material.color;
                c.a = value;
                spriteRenderers[i].material.color = c;
            }
            else
            {
                // for optimization purpose we must not touch the material
                // so that unity can use batching
                spriteRenderers[i].enabled = false;
            }
        }
        
        // For the info panel
        if (canvasGroup)
        {
            canvasGroup.alpha = value;
        }

        // For the particles
        ParticleSystem[] visualEffects = GetComponentsInChildren<ParticleSystem>();
        for (int i = 0; i < visualEffects.Length; ++i)
        {
            var c = visualEffects[i].startColor;
            c.a = value;
            visualEffects[i].startColor = c;
        }

        // For the trail renderers
        TrailRenderer[] trailRenderers = GetComponentsInChildren<TrailRenderer>();
        for (int i = 0; i < trailRenderers.Length; ++i)
        {
            var startColor = trailRenderers[i].startColor;
            startColor.a = value * trailStartColorAlpha;
            trailRenderers[i].startColor = startColor;

            var endColor = trailRenderers[i].endColor;
            endColor.a = value * trailEndColorAlpha;
            trailRenderers[i].endColor = endColor;
        }
    }

    
    public void Reveal(float revealDuration, bool alwaysReveal, bool disable)
    {
        // Don't reveal the unit is disabled
        if (isDisabled) return;

        // Do not override a better reveal timer
        if (revealTimer < revealDuration)
        {
            revealTimer = revealDuration;
            fadeTimer = 0f;
            // only reveal if not jammed
            if (alwaysReveal || !EnemyController.Instance.IsSpecialActive(GameUtils.EBossSpecial.JamRadar) || disable)
            {
                SetTransparency(1);
                if (!soundPlayed && !isTrail)
                {
                    soundPlayed = true;
                    RadarController.Instance.PlayDetectedSound();
                }
            }
            else if (!blipPlayed && !isTrail)
            {
                blipPlayed = true;

                // do blip
                GameObject spawn = EnemyController.Instance.GetJammedBlipFromPool();
                spawn.transform.position = transform.position;
                spawn.GetComponent<SpawnJammedBlipObject>().Init();

                // Play new enemy spawn sound
                RadarController.Instance.PlayJammedDetectedSound();
            }
        }

        if (disable && !isAlwaysVisible)
        {
            isDisabled = true;
        }
    }

    // Last fade out
    public void Death(float deathDuration)
    {
        isAlwaysVisible = false;
        Reveal(deathDuration, true, true);
    }

    public void Inherit(RadarObject radarObject)
    {
        revealTimer = radarObject.revealTimer;
        fadeTimer = radarObject.fadeTimer;
        SetTransparency(radarObject.Transparency);
        bool enabled = radarObject.SpritesEnabled;
        for (int i = 0; i < spriteRenderers.Length; ++i)
        {
            spriteRenderers[i].enabled = enabled;
        }
    }

    public void OnTriggerEnter2D(Collider2D coll)
    {
        if (isAlwaysVisible) return;
        
        if (coll.CompareTag(GameUtils.PULSE_TAG))
        {
            // Reveal the unit with the pulse duration
            Reveal(ConsumableController.Instance.PulseDuration, true, false);
        }

        // if not jammed, don't go into the rest
        if (!EnemyController.Instance.IsSpecialActive(GameUtils.EBossSpecial.JamRadar)) return;

        if (coll.CompareTag(GameUtils.RADAR_SCAN_TAG)) 
        {
            // normal radar scan should only case the jammed blip
            Reveal(RadarController.Instance.GetRevealTimer(), false, false);
        }
    }
    
    public void OnTriggerStay2D(Collider2D coll)
    {
        if (IsColliderSightRadius(coll))
        {
            // inside the base radius or a drone it should reveal normal
            Reveal(1f, true, false);
        }

        if (EnemyController.Instance.IsSpecialActive(GameUtils.EBossSpecial.JamRadar)) return;

        if (coll.CompareTag(GameUtils.RADAR_SCAN_TAG))
        {
            // reveal if in scanner
            Reveal(RadarController.Instance.GetRevealTimer(), false, false);
        }
    }

    private bool IsColliderSightRadius(Collider2D coll)
    {
        return coll.CompareTag(GameUtils.BASE_TAG) && coll.GetType() == typeof(CircleCollider2D);
    }
}


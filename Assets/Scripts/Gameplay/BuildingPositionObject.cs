using UnityEngine;
#pragma warning disable 0649

public class BuildingPositionObject : PositionObject
{
    [SerializeField]
    private GameUtils.BaseID buildingType;

    public GameUtils.BaseID BuildingType
    {
        get { return buildingType; }
    }
}

using UnityEngine;
using UnityEngine.Events;
using System;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

#pragma warning disable 0414
#pragma warning disable 0649

// REFACTOR :: Put all the health related stuff into a class
// (currently used by BaseObject & MovingEnemyObject) and
// the info panel spawned at runtime from a prefab.
public class BaseObject : SightRadiusObject
{
    private const float DEATH_DURATION = 2f;
    private readonly string DAMAGE_BASE_TRIGGER = "DamageBase";
    private readonly string DESTROY_BASE_TRIGGER = "DestroyBase";

    private bool initialized = false;

    // UI elements
    private Slider healthRedBar;
    private Slider healthGreenBar;
    protected Text baseName;
    private Image healthGreenBarFill;

    [SerializeField]
    private Color fullHealthBarColor;
    [SerializeField]
    private Color damagedHealthBarColor;

    private BoxCollider2D damageCollider;
    private Animator animator;

    [SerializeField]
    protected GameUtils.BaseID baseID = GameUtils.BaseID.NONE;

    [SerializeField]
    protected float health;

    protected bool isDead = false;
    public bool isDamaged = false;
    private float deadTimer = 0f;
    private float healthLostRate = 0f;    
    private bool takeDamageSilently = false;

    protected float maxHealth;
    private PathNode node;

    public bool IsDead
    {
        get { return isDead; }
    }

    public PathNode Node
    {
        get { return node; }
        set { node = value; }
    }

    public float Health
    {
        set
        {
            // If base got hit but not destroyed
            if (value < Health && value > 0f)
            {
                BaseDamaged(value);
            }
            // If base got destroyed
            else if (value <= 0f)
            {
                BaseDestroyed(value);
            }

            // If base got healed
            if (health < value && health < maxHealth)
            {
                // Spawn a heal text
                WaveController.Instance.SpawnFlyingText((value - health).ToString(),
                                        transform.position,
                                        FlyingTextObject.EFlyingTextType.Heal,
                                        GameUtils.COLOR_YELLOW,
                                        FontStyle.Normal,
                                        FlyingTextObject.EFlyingTextBehaviour.Normal,
                                        FlyingTextObject.EFlyingTextDirection.Up);
            }

            // Set the new value and clamp if needed
            health = Mathf.Min(value, maxHealth);
            
            // If base has been fully repaired
            if (health == maxHealth)
            {
                BaseFullyRepaired();
            }

            // Update the health bar
            healthGreenBar.value = health;
        }
        get { return health; }
    }

    // Note :: For tutorial
    public void ChangeHealthSilently(float valueToAdd)
    {
        takeDamageSilently = true;
        Health += valueToAdd;
        takeDamageSilently = false;
    }

    public GameUtils.BaseID BaseID
    {
        get { return baseID; }
        set { baseID = value; }
    }

    public float MaxHealth
    {
        get { return maxHealth; }
        set
        {
            maxHealth = value;

            // Initialize the health bars
            healthGreenBar = GameUtils.GetChildGameObject(gameObject, "HealthBarGreen").GetComponent<Slider>();
            healthGreenBar.value = healthGreenBar.maxValue = maxHealth;
            healthGreenBarFill = GameUtils.GetChildGameObject(healthGreenBar.gameObject, "Fill").GetComponent<Image>();
            healthGreenBarFill.color = fullHealthBarColor;

            healthRedBar = GameUtils.GetChildGameObject(gameObject, "HealthBarRed").GetComponent<Slider>();
            healthRedBar.value = healthRedBar.maxValue = maxHealth;
        }
    }
    
    public virtual bool Update()
    {
        if (WaveController.Instance.Paused) return false;
        if (!DataController.Instance.AllLoaded) return false;
        if (!ChallengeController.Instance.IsSceneSetup) return false;
        if (!initialized && baseID != GameUtils.BaseID.NONE)
        {
            InitBase();
        }

        UpdateHealthRedBar();
        UpdateDeathState();

        return true;
    }

    protected virtual bool InitBase()
    {
        if (!BaseController.Instance.Initialized) return false;

        // Register the base    
        BaseController.Instance.RegisterBase(baseID, gameObject);
        
        // Set the info icon color
        Image infoIcon = GameUtils.GetChildGameObject(gameObject, "InfoIcon").GetComponent<Image>();
        infoIcon.color = Color.green;
        
        // Set components
        damageCollider = GetComponent<BoxCollider2D>();
        animator = GetComponent<Animator>();

        initialized = true;

        return true;
    }

    private void UpdateHealthRedBar()
    {
        // If recently lost health
        if (healthRedBar.value > health)
        {
            // If just started to lose health
            if (healthLostRate == 0f)
            {
                // Set the health lost rate only once per damage taken
                healthLostRate = healthRedBar.value - Mathf.Clamp(health, 0f, healthRedBar.value);
            }
            healthRedBar.value -= healthLostRate * Time.deltaTime;
        }
        else
        {
            // Reset the health lost rate
            healthLostRate = 0f;
        }
    }

    private void UpdateDeathState()
    {
        if (isDead && deadTimer > 0f)
        {
            deadTimer -= Time.deltaTime;

            if (deadTimer <= 0f)
            {
                RemoveBase();
            }
        }
    }

    protected virtual void RemoveBase()
    {
        gameObject.SetActive(false);

        BaseController.Instance.UnregisterBase(baseID);
    }

    protected virtual void BaseFullyRepaired()
    {
        isDamaged = false;

        // Change the bar color to green
        healthGreenBarFill.color = fullHealthBarColor;
    }
    
    protected virtual void BaseDamaged(float value)
    {
        isDamaged = true;

        if (!takeDamageSilently)
        {
            // Sound hook - Base hit sound
            AudioManager.Instance.PlaySound(AudioManager.ESoundType.BaseHit);

            // Show disturbance on the playfield
            PlayfieldController.Instance.DisturbPlayField();

            // Play the damaged anim
            animator.SetTrigger(DAMAGE_BASE_TRIGGER);
        }        
        
        // Change the bar color to yellow
        healthGreenBarFill.color = damagedHealthBarColor;
    }

    protected virtual void BaseDestroyed(float value)
    {
        // Sound hook - Base destroy sound
        AudioManager.Instance.PlaySound(AudioManager.ESoundType.BaseDestroy);

        // Show disturbance on the playfield
        PlayfieldController.Instance.DisturbPlayField();

        // Play the damaged anim
        animator.SetTrigger(DESTROY_BASE_TRIGGER);

        //// Play a particle effect
        //GameObject baseDestroyedEffect = Instantiate(Resources.Load<GameObject>("Particles/VFX_BaseDestroyed"));
        //baseDestroyedEffect.transform.parent = transform;
        //baseDestroyedEffect.transform.localPosition = Vector3.zero;

        isDead = true;
        damageCollider.enabled = false;
        deadTimer = DEATH_DURATION;
        GetComponent<RadarObject>().Death(1f);

        // Remove the node from the path finding controller
        PathFindingController.Instance.RemoveAtNode(gameObject);
    }

    public void ResetHealth()
    {
        Health = maxHealth;
    }
}

﻿using UnityEngine;

public class DebugUtils
{
    public static void DrawPositionCross(Vector3 pos, Color color, float size = 10f)
    {
        Debug.DrawLine(pos - Vector3.right * size, pos + Vector3.right * size, color);
        Debug.DrawLine(pos - Vector3.up * size, pos + Vector3.up * size, color);
        Debug.DrawLine(pos - Vector3.forward * size, pos + Vector3.forward * size, color);
    }

    public static void DrawPositionCrossWithDuration(Vector3 pos, Color color, float duration, float size = 10f)
    {
        Debug.DrawLine(pos - Vector3.right * size, pos + Vector3.right * size, color, duration);
        Debug.DrawLine(pos - Vector3.up * size, pos + Vector3.up * size, color, duration);
        Debug.DrawLine(pos - Vector3.forward * size, pos + Vector3.forward * size, color, duration);
    }
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

#pragma warning disable 0649

// WIP :: 
// 1) Need to verify scaling
// 2) Need to take into account random space between elements
// -- POSSIBLE SOLUTION : Check the center of the current element in the drag event.
// -- It could remove the 'itemPerRow', 'itemTemplateHeight', 'topOffset' and 'bottomOffset'.

[RequireComponent(typeof(ScrollRect))]
public class ScrollViewPrime : MonoBehaviour
{
    private readonly float SLOW_UPDATE_SPAN = 0.05f;

    private ScrollRect scrollRect;
    private RectTransform content;
    private List<Button> buttonList;
    
    [SerializeField]
    private int itemPerRow = 4;

    [SerializeField]
    private int itemTemplateHeight = 200;

    [SerializeField]
    private int topOffset = 0;

    [SerializeField]
    private int bottomOffset = 0;

    private float scrollViewHeight;
    private float slowUpdateTimer;

    private void Awake()
    {
        scrollRect = GetComponent<ScrollRect>();
        scrollViewHeight = scrollRect.GetComponent<RectTransform>().rect.height;
        
        content = GameUtils.GetChildGameObject(gameObject, "Content", true).GetComponent<RectTransform>();
        
        PopulateItemList();
    }

    private void PopulateItemList()
    {
        buttonList = new List<Button>();
        ScrollViewPrimeItem[] items = GetComponentsInChildren<ScrollViewPrimeItem>();
        for (int i = 0; i < items.Length; ++i)
        {
            buttonList.Add(items[i].GetComponent<Button>());
            items[i].enabled = false;
        }
    }

    private void Update()
    {
        SlowUpdate();
    }
    
    private void SlowUpdate()
    {
        slowUpdateTimer -= Time.deltaTime;
        if (slowUpdateTimer < 0)
        {
            slowUpdateTimer = SLOW_UPDATE_SPAN;

            RefreshButtonList();
        }
    }
    
    private void RefreshButtonList(bool disable = false)
    {
        for (int i = 0; i < buttonList.Count; ++i)
        {
            if (buttonList[i] == null) continue;

            float topValue = ((i / itemPerRow) * itemTemplateHeight) + topOffset;
            float bottomValue = topValue - scrollViewHeight - bottomOffset;
            
            if (content.offsetMax.y > topValue || content.offsetMax.y < bottomValue)
            {
                buttonList[i].enabled = false;
            }
            else
            {
                buttonList[i].enabled = true;
            }
        }
    }
}

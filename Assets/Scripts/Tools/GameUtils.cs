using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;
using System.Linq;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class GameUtils
{
    public static readonly Color COLOR_ORANGE = new Color(1f, 0.55f, 0f);
    public static readonly Color COLOR_YELLOW = new Color(0.914f, 0.792f, 0f, 1f);
    public static readonly Color COLOR_GREEN_RADAR = new Color(0f, 0.796f, 0.439f, 1f);
    public static readonly Color COLOR_CYAN_CREDITS = new Color(0.035f, 0.757f, 0.886f, 1f);
    public static readonly Color COLOR_YELLOW_GOLD = new Color(0.914f, 0.792f, 0f, 1f);
    public static readonly Color COLOR_GREEN_BOOST = new Color(0.161f, 0.659f, 0.518f, 1f);
    public static readonly Color COLOR_RED_SUPPORT = new Color(0.824f, 0.447f, 0.031f, 1f);
    // Color building and weapon is in the animation
    public static readonly Color COLOR_BLUE_BUILDING = new Color(0.204f, 0.608f, 0.984f, 1f);
    public static readonly Color COLOR_STAT_BOOSTED = new Color(0.192f, 0.933f, 0.768f, 1f);


    // GOOGLE ACHIEVEMENTS
    public const int GOOGLE_ACHIEVEMENTS_SUPPLIER = 20;
    public const int GOOGLE_ACHIEVEMENTS_INVESTOR = 100000;

    // Multiple kill rewards
    public const int DOUBLE_KILL_CREDIT_REWARD = 100;
    public const int TRIPLE_KILL_CREDIT_REWARD = 200;
    public const int QUADRA_KILL_CREDIT_REWARD = 500;
    public const int PENTA_KILL_CREDIT_REWARD = 1000;
    public const int HEXA_KILL_CREDIT_REWARD = 2000;
    public const int HEPTA_KILL_CREDIT_REWARD = 5000;
    public const int OCTA_KILL_CREDIT_REWARD = 10000;
    public const int NONA_KILL_CREDIT_REWARD = 20000;
    public const int DECA_KILL_CREDIT_REWARD = 50000;

    public const float DIRECT_HIT_DAMAGE_MULTIPLIER = 1.2f;
    public const float HIDDEN_KILL_MULTIPLIER = 1.5f;

    public const int BOSS_LVL_AT_EACH = 4;

    public const float STANDARD_HEIGHT = 968f; // 10 : 6 ratio
    public const float STANDARD_WIDTH = 1618f;
    public const float STANDARD_SCREEN_HEIGHT = 1080; // 16 :9 ratio
    public const float STANDARD_SCREEN_WIDTH = 1920;
    public const float LINE_THICKNESS = 5f;
        
    public const float HEALTH_SQUARE = 5f;
    public const float HEALTH_SPACE = 2f;
    public const float HEALTH_OFFSET = 60f;
    
    public const int HOMING_MISSILE_COUNT = 3;

    public const int DEFAULT_TRAIL_COUNT = 10;
    public const float TRAIL_DISTANCE = 12f;
    public const float TRAIL_DISTANCE_SQR = TRAIL_DISTANCE * TRAIL_DISTANCE;
    
    public const float ALERT_FIELD_TIMER = 3f;
    
    public const int MAX_UPGRADE_LEVEL = 10;
    public const int BASE_HEALTH = 5;

    public const float CURVE_AMPLITUDE_ARTILLERY = 50f;
    public const int ROCKETS_COUNT = 5;
    public const float ROCKETS_AREA_RADIUS = 1.2f;
        
    // Weapon Upgrades
    public const float RADIUS_FACTOR = 50f;
    public const float SPEED_FACTOR = 10f;
    public const float SIGHT_RADIUS_FACTOR = 75f;
    
    public const float LASER_SHOT_TIME = 0.5f;
    public const float LASER_BASIC_WIDTH = 5f;

    public const int BATTLE_CONSUMABLE_AMOUNT_MAX = 5;
    
    public const float MISSILE_OPEN_ANGLE = 5f;
    public const float MISSILE_OPEN_ANGLE_CLOSE = 30f;
    public const float MISSILE_CLOSE_DISTANCE = 150f;
    public const float MISSILE_OPEN_ANGLE_EPSILON = 1.5f;
	// Watch ads
    public const int WATCHED_AD_COOLDOWN_MINUTES = 45;
	//
    public const float WEAPON_SELECTION_COOLDOWN = 0.5f;

    public const int GET_MORE_CONSUMABLE_LIMIT_PER = 1;
    public const int CONSUMABLE_DEPLETED_GOLD_COST = 10;

    public const int REPAIR_UNLOCK_LVL = 0;
    public const int RELOAD_UNLOCK_LVL = 2;
    public const int PULSE_UNLOCK_LVL = 13;
    public const int SLOWER_UNLOCK_LVL = 25;
    public const int OHSHIT_UNLOCK_LVL = 37;

    public const int REPAIR_FREE_AMOUNT = 10;
    public const int RELOAD_FREE_AMOUNT = 10;
    public const int PULSE_FREE_AMOUNT = 10;
    public const int SLOWER_FREE_AMOUNT = 10;
    public const int OHSHIT_FREE_AMOUNT = 5;

    public const int REPAIR_BATTLE_AMOUNT_MAX = 3;
    public const int RELOAD_BATTLE_AMOUNT_MAX = 3;
    public const int PULSE_BATTLE_AMOUNT_MAX = 3;
    public const int SLOWER_BATTLE_AMOUNT_MAX = 3;
    public const int OHSHIT_BATTLE_AMOUNT_MAX = 3;

    public const float TAP_CONTINUE_DELAY = 1.5f;

    public const int NORMAL_LAST_MISSION_NUMBER = 24;

    public const float DAMAGED_WEAPON_COOLDOWN_MULTIPLIER = 2f;

    public const int MAX_GRIND_CARDS_AMOUNT = 6;

    public const int RARITY_LEVEL_RARE_MAX_VALUE = 10;
    public const int RARITY_LEVEL_UNCOMMON_MAX_VALUE = 15;

    public const int BOSS_STAR_REWARDS_MULTIPLIER = 2;

    public const float AMMO_REGEN_MULTIPLIER_AT_ZERO = 2f;

    public enum ETimeType
    {
        Seconds = 0,
        Minutes,
        Hours
    }

    public enum EMovementType
    {
        FollowPathToBase = 0,
        FollowPathToExit,
        StraightToBase,
        Straight
    }

    public enum ETargetBaseType
    {
        All = 0,
        City,
    }

    public enum EBossSpecial
    {
        None = 0,
        Repair,
        Shield,
        Bomb,
        JamRadar
    }

    public enum EGameDifficulty
    {
        Normal,
        Hard,
        Insane,
        Custom
    }

    public enum BaseID
    {
        NONE = -1,
        MISSILE = 0,
        ARTILLERY = 1,
        LASER = 2,
        RAILGUN = 3,
        ROCKETS = 4,
        FLAK = 5,
        CITY = 6,
        FACTORY = 7,
        POWERPLANT = 8,
        DEPOT = 9,
        RESEARCHCENTER = 10
    }

    public enum EConsumable
    {
        Slower = 0,
        OhShit,
        Pulse,
        Reload,
        Repair,
        Count
    }

    public enum EWeaponUpgrade
    {
        SPEED = 0,
        RELOAD,
        REGEN,
        AIR,
        GROUND,
        EXPLOSION_RADIUS,
        VISIBILITY_RADIUS,
        HEALTH,
        COUNT
    }

    public enum ConsumableUpgrades
    {
        PULSE_SPEED = 0,
        PULSE_TIME,
        REPAIR,
        AMMUNITION,
        HOMING_MISSILE,
        DRONE_TIME,
        DRONE_RANGE,
        COUNT
    }

    public enum GeneralUpgrades
    {
        SCANNER_TIME,
        SCANNER_WAIT,
        COUNT
    }

    // WARNING :: Only add to the end of the list!
    // Otherwise all the waves data will be incoherent.
    public enum UnitType
    {
        NONE = -1,
        INFANTRY = 0,
        LAV,
        TANK,
        HEAVYTANK,
        CONVOY,
        HELICOPTER,
        FIGHTERJET,
        BOMBER,
        TRANSPORTPLANE,
        BALLISTICMISSILE,
        JEEP,
        DRONE,
        ASSAULT_HELICOPTER,
        BUGGY,
        PARATROOPER_PLANE,
        PARATROOPER,
        MOBILE_MAINTENANCE,
        MOBILE_SHIELD,
        TACTICAL_BOMBER,
        MOBILE_GROUND_ECM,
        MOBILE_AIR_ECM,
        OFFROAD_BUGGY,
        PARATROOPER_INFANTRY,
        UCAV_DRONE,
        COUNT
    }
    
    // Make sure to add currency type to 
    // Pack controller transaction event too!
    public enum ECurrencyType
    {
        Credit,
        Gold,
        Cash,
        Achievement,
        WatchAd,
        StarReward,
        GrindCardReward,
        DailyFree,
        DailyMission,
        FreeURL,
        None
    }

    public enum CreditsSource
    {
        EnemyDestroyed,
        LootReward,
        ThreeStarReward,
        MatchReward,
        Credits2Pack,
        Pack2Credits,
        WeaponUnlocked,
        WeaponUpgraded,
        AchievementReward,
        WaveNextPressed,
        MultiKillReward,
        LastStandBonusReward,
        BoosterEnemeyDestroyed,
        BoosterLootReward,
        BoosterMatchReward,
        BoosterWaveNextPressed,
        BoosterMultiKillReward,
        DailyMission,
        Debug
    }

    public enum GoldSource
    {
        LootReward,
        OneStarReward,
        Gold2Pack,
        Pack2Gold,
        ConsumableDepleted,
        AchievementReward,
        Debug
    }

    public enum ELoadType
    {
        DynamicData,
        StaticData,
        Override
    }

    [Serializable]
    public class LineParameters
    {
        public Material lineMaterial;
        public Color lineLargeColor;
        public int lineLargeWidth;
        public Color lineSmallColor;
        public int lineSmallWidth;
    }

    // Json file names
    public const string CURRENCY_JSON = "currency1_21.json";
    public const string CONSUMABLES_JSON = "consumables1_21.json";
    public const string CHALLENGES_JSON = "challenges1_21.json";
    public const string LAST_STANDS_JSON = "laststands1_21_2.json";
    public const string OPERATIONS_JSON = "operations1_29.json";
    public const string ACHIEVEMENTS_JSON = "achievements1_21.json";
    public const string WEAPON_CARDS_JSON = "weaponcards1_21.json";
    public const string WEAPON_UPG_JSON = "weaponupgrades1_21.json";
    public const string WEAPON_CTR_JSON = "weaponcontroller1_21.json";
    public const string OPTIONS_JSON = "options1_21.json";
    public const string AUDIO_SETTINGS_JSON = "audioSettings1_21.json";
    public const string GRAPHIC_SETTINGS_JSON = "graphicSettings1_21.json";
    public const string WATCHED_ADS_JSON = "watchedads1_21.json";
    public const string PACK_DATA_JSON = "packdata1_21.json";
    public const string LEADERBOARD_DATA_JSON = "leaderboards1_21.json";
    public const string DAILY_JSON = "daily1_30.json";
    public const string LOCALINFO_JSON = "localinfo1_29.json";

    // Tags
    public const string BASE_TAG = "Base";
    public const string GROUND_TAG = "Ground";
    public const string AIR_TAG = "Air";
    public const string RADAR_SCAN_TAG = "RadarScan";
    public const string PULSE_TAG = "Pulse";
    public const string SLOWER_TAG = "Slower";

    // Google Play Service
    public const string CLOUD_SAVE_GAME_FILENAME = "SaveGame1_21";

    public static bool IsSpecialUnit(UnitType enemyType)
    {
        return enemyType == UnitType.PARATROOPER_PLANE
               || enemyType == UnitType.MOBILE_AIR_ECM
               || enemyType == UnitType.MOBILE_GROUND_ECM
               || enemyType == UnitType.TACTICAL_BOMBER
               || enemyType == UnitType.MOBILE_MAINTENANCE
               || enemyType == UnitType.MOBILE_SHIELD
               || enemyType == UnitType.OFFROAD_BUGGY
               || enemyType == UnitType.PARATROOPER
               || enemyType == UnitType.PARATROOPER_INFANTRY
               || enemyType == UnitType.UCAV_DRONE;
    }

    public static bool IsBossUnit(UnitType enemyType)
    {
        return enemyType == UnitType.PARATROOPER_PLANE
               || enemyType == UnitType.MOBILE_AIR_ECM
               || enemyType == UnitType.MOBILE_GROUND_ECM
               || enemyType == UnitType.TACTICAL_BOMBER
               || enemyType == UnitType.MOBILE_MAINTENANCE
               || enemyType == UnitType.MOBILE_SHIELD;
    }

    public static GameObject GetChildGameObject(GameObject fromGameObject, string withName, bool includeInactive = false)
    {

		List<Transform> childrenList = new List<Transform>();
        fromGameObject.transform.GetComponentsInChildren(includeInactive, childrenList);

        for (int i = 0; i < childrenList.Count; ++i)
        {
            if (childrenList[i].gameObject.name == withName) return childrenList[i].gameObject;
        }

        return null;
    }

    public static bool CompareVectors(Vector3 a, Vector3 b, float precision)
    {
        float dist = Vector3.SqrMagnitude(a - b);
        //Debug.Log("dist = " + dist);
        return dist <= precision;
    }

    public static float GetSqrDistanceBetweenVectors(Vector3 a, Vector3 b)
    {
        return Vector3.SqrMagnitude(a - b);
    }

    // Design - TTK values
    public static float GetTTK(EGameDifficulty difficulty, ChallengeData.EChallengeType challengeType)
    {
        return 2f;

        //if (challengeType == ChallengeData.EChallengeType.LastStand)
        //{
        //    return 1f;
        //}
        //
        //switch(difficulty)
        //{
        //    case EGameDifficulty.Normal:
        //        return 4f;
        //    case EGameDifficulty.Hard:
        //        return 3f;
        //    case EGameDifficulty.Insane:
        //        return 2f;
        //    case EGameDifficulty.Custom: // DESIGN ::
        //        return 2f;
        //    default:
        //        return 4f;
        //}
    }

    // Design - Consumable Production Time Multipliers
    public static float GetConsumableProductionTimeMultiplier(EGameDifficulty difficulty)
    {
        switch (difficulty)
        {
            case EGameDifficulty.Normal:
                return 1f;
            case EGameDifficulty.Hard:
                return 1f;
            case EGameDifficulty.Insane:
                return 1f;
            case EGameDifficulty.Custom: // DESIGN ::
                return 1f;
            default:
                return 1f;
        }
    }

    public static Sprite GetCurrencyIcon(ECurrencyType currencyType)
    {
        string spritePath = "";
        switch (currencyType)
        {
            case ECurrencyType.Credit:
                spritePath = "Images/LootCreditsIcon";
                break;
            case ECurrencyType.Gold:
                spritePath = "Images/LootGoldIcon";
                break;
            // TODO :: Add cash icon
            case ECurrencyType.Cash:
                spritePath = "Images/LootGoldIcon";
                break;
            default:
                spritePath = "Images/LootCreditsIcon";
                break;
        }
        return (Sprite)Resources.Load(spritePath, typeof(Sprite));
    }
    
    public static bool LineLineIntersection(out Vector3 intersection, Vector3 linePoint1, Vector3 lineVec1, Vector3 linePoint2, Vector3 lineVec2)
    {

        Vector3 lineVec3 = linePoint2 - linePoint1;
        Vector3 crossVec1and2 = Vector3.Cross(lineVec1, lineVec2);
        Vector3 crossVec3and2 = Vector3.Cross(lineVec3, lineVec2);

        float planarFactor = Vector3.Dot(lineVec3, crossVec1and2);

        //is coplanar, and not parrallel
        if (Mathf.Abs(planarFactor) < 0.0001f && crossVec1and2.sqrMagnitude > 0.0001f)
        {
            float s = Vector3.Dot(crossVec3and2, crossVec1and2) / crossVec1and2.sqrMagnitude;
            intersection = linePoint1 + (lineVec1 * s);
            return true;
        }
        else
        {
            intersection = Vector3.zero;
            return false;
        }
    }

    public static string UppercaseFirst(string s)
    {
        // Check for empty string.
        if (string.IsNullOrEmpty(s))
        {
            return string.Empty;
        }
        // Return char and concat substring.
        return char.ToUpper(s[0]) + s.Substring(1).ToLower();
    }

    public static Vector3 FindBorderPosition(Vector3 position, Vector3 direction)
    {
        Vector3 intersection;
        // set position and size of laser
        if (direction.x < 0 && direction.y >= 0)
        {
            // intersect left, top
            Vector3 left;
            Vector3 top;
            GameUtils.LineLineIntersection(out left, position, direction, PlayfieldController.Instance.BottomLeft, Vector3.up);
            GameUtils.LineLineIntersection(out top, position, direction, PlayfieldController.Instance.TopRight, -Vector3.right);
            if (((Vector3.SqrMagnitude(position - left) < Vector3.SqrMagnitude(position - top)) && left != Vector3.zero) || top == Vector3.zero)
            {
                intersection = left;
            }
            else
            {
                intersection = top;
            }
        }
        else if (direction.x < 0 && direction.y < 0)
        {
            // intersect left, bottom
            Vector3 left;
            Vector3 bottom;
            GameUtils.LineLineIntersection(out left, position, direction, PlayfieldController.Instance.BottomLeft, Vector3.up);
            GameUtils.LineLineIntersection(out bottom, position, direction, PlayfieldController.Instance.BottomLeft, Vector3.right);
            if (((Vector3.SqrMagnitude(position - left) < Vector3.SqrMagnitude(position - bottom)) && left != Vector3.zero) || bottom == Vector3.zero)
            {
                intersection = left;
            }
            else
            {
                intersection = bottom;
            }
        }
        else if (direction.x >= 0 && direction.y >= 0)
        {
            // intersect right, top
            Vector3 right;
            Vector3 top;
            GameUtils.LineLineIntersection(out right, position, direction, PlayfieldController.Instance.TopRight, -Vector3.up);
            GameUtils.LineLineIntersection(out top, position, direction, PlayfieldController.Instance.TopRight, -Vector3.right);
            if (((Vector3.SqrMagnitude(position - right) < Vector3.SqrMagnitude(position - top)) && right != Vector3.zero) || top == Vector3.zero)
            {
                intersection = right;
            }
            else
            {
                intersection = top;
            }
        }
        else
        {
            // intersect right, bottom
            Vector3 right;
            Vector3 bottom;
            GameUtils.LineLineIntersection(out right, position, direction, PlayfieldController.Instance.TopRight, -Vector3.up);
            GameUtils.LineLineIntersection(out bottom, position, direction, PlayfieldController.Instance.BottomLeft, Vector3.right);
            if (((Vector3.SqrMagnitude(position - right) < Vector3.SqrMagnitude(position - bottom)) && right != Vector3.zero) || bottom == Vector3.zero)
            {
                intersection = right;
            }
            else
            {
                intersection = bottom;
            }
        }
        return intersection;
    }

    public static Vector2 Shake2D(PlayfieldController.Shake camShake)
    {
        float valX = 0;
        float valY = 0;

        float iAmplitude = 1;
        float iFrequency = camShake.frequency;
        float maxAmplitude = 0;

        // Burst frequency
        float burstCoord = Time.time / (1 - camShake.burstFrequency);

        // Sample diagonally trough perlin noise
        float burstMultiplier = Mathf.PerlinNoise(burstCoord, burstCoord);

        //Apply contrast to the burst multiplier using power, it will make values stay close to zero and less often peak closer to 1
        burstMultiplier = Mathf.Pow(burstMultiplier, camShake.burstContrast);

        for (int i = 0; i < camShake.octaves; i++) // Iterate trough octaves
        {
            float noiseFrequency = Time.time / (1 - iFrequency) / 10;

            float perlinValueX = Mathf.PerlinNoise(noiseFrequency, 0.5f);
            float perlinValueY = Mathf.PerlinNoise(0.5f, noiseFrequency);

            // Adding small value To keep the average at 0 and   *2 - 1 to keep values between -1 and 1.
            perlinValueX = (perlinValueX + 0.0352f) * 2 - 1;
            perlinValueY = (perlinValueY + 0.0345f) * 2 - 1;

            valX += perlinValueX * iAmplitude;
            valY += perlinValueY * iAmplitude;

            // Keeping track of maximum amplitude for normalizing later
            maxAmplitude += iAmplitude;

            iAmplitude *= camShake.persistance;
            iFrequency *= camShake.lacunarity;
        }

        valX *= burstMultiplier;
        valY *= burstMultiplier;

        // normalize
        valX /= maxAmplitude;
        valY /= maxAmplitude;

        valX *= camShake.amplitude;
        valY *= camShake.amplitude;

        return new Vector2(valX, valY);
    }

    public static bool IsAnimDonePlaying(Animator animator, string animName)
    {
        return animator.GetCurrentAnimatorStateInfo(0).IsName(animName)
            && animator.GetCurrentAnimatorStateInfo(0).normalizedTime > 1
            && !animator.IsInTransition(0);
    }

    public static string GetWeaponLocalizedName(BaseID weaponID)
    {
        string localizedTextID;
        switch (weaponID)
        {
            case BaseID.ARTILLERY:
                localizedTextID = LocalizationManager.ARTILLERY_TEXT;
                break;
            case BaseID.FLAK:
                localizedTextID = LocalizationManager.FLAK_TEXT;
                break;
            case BaseID.MISSILE:
                localizedTextID = LocalizationManager.MISSILE_TEXT;
                break;
            case BaseID.RAILGUN:
                localizedTextID = LocalizationManager.RAILGUN_TEXT;
                break;
            case BaseID.ROCKETS:
                localizedTextID = LocalizationManager.ROCKETS_TEXT;
                break;
            case BaseID.LASER:
                localizedTextID = LocalizationManager.LASER_TEXT;
                break;
            default:
                localizedTextID = "NULL";
                break;
        }

        return LocalizationManager.Instance.GetLocalizedString(localizedTextID);
    }

    public static string GetWeaponLocalizedDesc(BaseID weaponID)
    {
        string localizedTextID;
        switch (weaponID)
        {
            case BaseID.ARTILLERY:
                localizedTextID = LocalizationManager.ARTILLERY_DESC;
                break;
            case BaseID.FLAK:
                localizedTextID = LocalizationManager.FLAK_DESC;
                break;
            case BaseID.MISSILE:
                localizedTextID = LocalizationManager.MISSILE_DESC;
                break;
            case BaseID.RAILGUN:
                localizedTextID = LocalizationManager.RAILGUN_DESC;
                break;
            case BaseID.ROCKETS:
                localizedTextID = LocalizationManager.ROCKETS_DESC;
                break;
            case BaseID.LASER:
                localizedTextID = LocalizationManager.LASER_DESC;
                break;
            default:
                localizedTextID = "NULL";
                break;
        }

        return LocalizationManager.Instance.GetLocalizedString(localizedTextID);
    }

    public static string GetWeaponUpgradeLocalizedName(BaseID weaponID, EWeaponUpgrade weaponUpgrade)
    {
        string localizedTextID;
        switch (weaponUpgrade)
        {
            case EWeaponUpgrade.AIR:
                localizedTextID = LocalizationManager.AIR_DAMAGE_TEXT;
                break;
            case EWeaponUpgrade.EXPLOSION_RADIUS:
                if (weaponID == BaseID.LASER)
                {
                    localizedTextID = LocalizationManager.LASER_RADIUS_TEXT;
                }
                else
                {
                    localizedTextID = LocalizationManager.EXPLOSION_RADIUS_TEXT;
                }
                break;
            case EWeaponUpgrade.GROUND:
                localizedTextID = LocalizationManager.GROUND_DAMAGE_TEXT;
                break;
            case EWeaponUpgrade.HEALTH:
                localizedTextID = LocalizationManager.HITPOINTS_TEXT;
                break;
            case EWeaponUpgrade.REGEN:
                localizedTextID = LocalizationManager.REGENERATION_TEXT;
                break;
            case EWeaponUpgrade.RELOAD:
                localizedTextID = LocalizationManager.RELOAD_TIME_TEXT;
                break;
            case EWeaponUpgrade.SPEED:
                if (weaponID == BaseID.LASER)
                {
                    localizedTextID = LocalizationManager.LASER_SPEED_TEXT;
                }
                else if (weaponID == BaseID.MISSILE)
                {
                    localizedTextID = LocalizationManager.MISSILE_SPEED_TEXT;
                }
                else if (weaponID == BaseID.ROCKETS)
                {
                    localizedTextID = LocalizationManager.ROCKET_SPEED_TEXT;
                }
                else
                {
                    localizedTextID = LocalizationManager.BULLET_SPEED_TEXT;
                }
                break;
            case EWeaponUpgrade.VISIBILITY_RADIUS:
                localizedTextID = LocalizationManager.SIGHT_RADIUS_TEXT;
                break;
            default:
                localizedTextID = "NULL";
                break;
        }

        return LocalizationManager.Instance.GetLocalizedString(localizedTextID);
    }

    public static Sprite GetWeaponIcon(BaseID weaponID)
    {
        Sprite weaponIcon = null;
        switch (weaponID)
        {
            case BaseID.ARTILLERY:
                weaponIcon = (Sprite)Resources.Load("Images/cards_artillery", typeof(Sprite));
                break;
            case BaseID.FLAK:
                weaponIcon = (Sprite)Resources.Load("Images/cards_flak", typeof(Sprite));
                break;
            case BaseID.MISSILE:
                weaponIcon = (Sprite)Resources.Load("Images/cards_missileLauncher", typeof(Sprite));
                break;
            case BaseID.LASER:
                weaponIcon = (Sprite)Resources.Load("Images/cards_laser", typeof(Sprite));
                break;
            case BaseID.RAILGUN:
                weaponIcon = (Sprite)Resources.Load("Images/cards_railgun", typeof(Sprite));
                break;
            case BaseID.ROCKETS:
                weaponIcon = (Sprite)Resources.Load("Images/cards_rocketLauncher", typeof(Sprite));
                break;
        }

        return weaponIcon;
    }

    public static Sprite GetGrindCardWeaponIcon(BaseID weaponID)
    {
        Sprite weaponIcon = null;
        switch (weaponID)
        {
            case BaseID.ARTILLERY:
                weaponIcon = (Sprite)Resources.Load("Images/gs_cardSmall_artillery", typeof(Sprite));
                break;
            case BaseID.FLAK:
                weaponIcon = (Sprite)Resources.Load("Images/gs_cardSmall_flak", typeof(Sprite));
                break;
            case BaseID.MISSILE:
                weaponIcon = (Sprite)Resources.Load("Images/gs_cardSmall_missile", typeof(Sprite));
                break;
            case BaseID.LASER:
                weaponIcon = (Sprite)Resources.Load("Images/gs_cardSmall_laser", typeof(Sprite));
                break;
            case BaseID.RAILGUN:
                weaponIcon = (Sprite)Resources.Load("Images/gs_cardSmall_railgun", typeof(Sprite));
                break;
            case BaseID.ROCKETS:
                weaponIcon = (Sprite)Resources.Load("Images/gs_cardSmall_rocket", typeof(Sprite));
                break;
        }

        return weaponIcon;
    }

    public static string GetConsumableLocalizedName(EConsumable consumable)
    {
        string localizedTextID;
        switch (consumable)
        {
            case EConsumable.OhShit:
                localizedTextID = LocalizationManager.OHSHIT_TEXT;
                break;
            case EConsumable.Pulse:
                localizedTextID = LocalizationManager.PULSE_TEXT;
                break;
            case EConsumable.Reload:
                localizedTextID = LocalizationManager.RELOAD_TEXT;
                break;
            case EConsumable.Repair:
                localizedTextID = LocalizationManager.REPAIR_TEXT;
                break;
            case EConsumable.Slower:
                localizedTextID = LocalizationManager.SLOWER_TEXT;
                break;
            default:
                localizedTextID = "NULL";
                break;
        }

        return LocalizationManager.Instance.GetLocalizedString(localizedTextID);
    }

    public static string GetConsumableLocalizedDesc(EConsumable consumableType)
    {
        string localizedTextID;
        switch (consumableType)
        {
            case EConsumable.OhShit:
                localizedTextID = LocalizationManager.OHSHIT_DESC;
                break;
            case EConsumable.Pulse:
                localizedTextID = LocalizationManager.PULSE_DESC;
                break;
            case EConsumable.Reload:
                localizedTextID = LocalizationManager.RELOAD_DESC;
                break;
            case EConsumable.Repair:
                localizedTextID = LocalizationManager.REPAIR_DESC;
                break;
            case EConsumable.Slower:
                localizedTextID = LocalizationManager.SLOWER_DESC;
                break;
            default:
                localizedTextID = "NULL";
                break;
        }

        return LocalizationManager.Instance.GetLocalizedString(localizedTextID);
    }

    public static string GetConsumableLocalizedStats(EConsumable consumableType)
    {
        string localizedTextID;
        switch (consumableType)
        {
            case EConsumable.OhShit:
                localizedTextID = LocalizationManager.OHSHIT_STATS;
                break;
            case EConsumable.Pulse:
                localizedTextID = LocalizationManager.PULSE_STATS;
                break;
            case EConsumable.Reload:
                localizedTextID = LocalizationManager.RELOAD_STATS;
                break;
            case EConsumable.Repair:
                localizedTextID = LocalizationManager.REPAIR_STATS;
                break;
            case EConsumable.Slower:
                localizedTextID = LocalizationManager.SLOWER_STATS;
                break;
            default:
                localizedTextID = "NULL";
                break;
        }

        return LocalizationManager.Instance.GetLocalizedString(localizedTextID);
    }

    public static Sprite GetConsumableIcon(EConsumable consumable, bool withColor)
    {
        Sprite consumableIcon = null;
        switch (consumable)
        {
            case EConsumable.OhShit:
                if (withColor)
                {
                    consumableIcon = (Sprite)Resources.Load("Images/consumable_homingMissileColorIcon", typeof(Sprite));
                }
                else
                {
                    consumableIcon = (Sprite)Resources.Load("Images/consumable_homingMissileIcon", typeof(Sprite));
                }
                break;
            case EConsumable.Pulse:
                if (withColor)
                {
                    consumableIcon = (Sprite)Resources.Load("Images/consumable_pulseColorIcon", typeof(Sprite));
                }
                else
                {
                    consumableIcon = (Sprite)Resources.Load("Images/consumable_pulseIcon", typeof(Sprite));
                }
                break;
            case EConsumable.Reload:
                if (withColor)
                {
                    consumableIcon = (Sprite)Resources.Load("Images/consumable_reloadColorIcon", typeof(Sprite));
                }
                else
                {
                    consumableIcon = (Sprite)Resources.Load("Images/consumable_reloadIcon", typeof(Sprite));
                }                
                break;
            case EConsumable.Repair:
                if (withColor)
                {
                    consumableIcon = (Sprite)Resources.Load("Images/consumable_repairColorIcon", typeof(Sprite));
                }
                else
                {
                    consumableIcon = (Sprite)Resources.Load("Images/consumable_repairIcon", typeof(Sprite));
                }
                break;
            case EConsumable.Slower:
                if (withColor)
                {
                    consumableIcon = (Sprite)Resources.Load("Images/consumable_slowerColorIcon", typeof(Sprite));
                }
                else
                {
                    consumableIcon = (Sprite)Resources.Load("Images/consumable_slowerIcon", typeof(Sprite));
                }
                break;
        }

        return consumableIcon;
    }

    public static T[] GetEnumValues<T>()
    {
        return (T[])Enum.GetValues(typeof(T));
    }

    public static string[] GetEnumNames<T>()
    {
        return Enum.GetNames(typeof(T));
    }

    //returns -1 when to the left, 1 to the right, and 0 for forward/backward
    public static float AngleDir(Vector3 a, Vector3 b)
    {
        var dot = a.x * -b.y + a.y * b.x;
        if (dot > 0)
            return 1f;
        else if (dot < 0)
            return -1f;
        else
            return 0;
    }

#if UNITY_ANDROID
    public static bool CheckPackageAppIsPresent(string package)
    {
        AndroidJavaClass up = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject ca = up.GetStatic<AndroidJavaObject>("currentActivity");
        AndroidJavaObject packageManager = ca.Call<AndroidJavaObject>("getPackageManager");

        //take the list of all packages on the device
        AndroidJavaObject appList = packageManager.Call<AndroidJavaObject>("getInstalledPackages", 0);
        int num = appList.Call<int>("size");
        for (int i = 0; i < num; i++)
        {
            AndroidJavaObject appInfo = appList.Call<AndroidJavaObject>("get", i);
            string packageNew = appInfo.Get<string>("packageName");
            if (packageNew.CompareTo(package) == 0)
            {
                return true;
            }
        }
        return false;
    }
#endif


#if UNITY_EDITOR
    //	This makes it easy to create, name and place unique new ScriptableObject asset files.
    public static void CreateAsset<T>() where T : ScriptableObject
    {
        T asset = ScriptableObject.CreateInstance<T>();

        string path = AssetDatabase.GetAssetPath(Selection.activeObject);
        if (path == "")
        {
            path = "Assets";
        }
        else if (Path.GetExtension(path) != "")
        {
            path = path.Replace(Path.GetFileName(AssetDatabase.GetAssetPath(Selection.activeObject)), "");
        }

        string assetPathAndName = AssetDatabase.GenerateUniqueAssetPath(path + "/New " + typeof(T).ToString() + ".asset");

        AssetDatabase.CreateAsset(asset, assetPathAndName);

        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
        EditorUtility.FocusProjectWindow();
        Selection.activeObject = asset;
    }
#endif
}

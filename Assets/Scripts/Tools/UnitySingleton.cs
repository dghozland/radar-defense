using UnityEngine;
using System.Collections.Generic;

public class BaseSingleton : MonoBehaviour
{    
    private bool initialized = false;
    public bool Initialized
    {
        get { return initialized; }
    }

    public BaseSingleton()
    {
        initialized = false;
    }

    public virtual void Awake()
    {
        initialized = true;
    }
}

public class UnitySingleton<T> : BaseSingleton where T : BaseSingleton
{
    private static GameObject controllers;
    private static T instance;
    private List<BaseSingleton> dependencies = new List<BaseSingleton>();

    public static T Instance
    {
        get
        {
            // If the instance has not been set yet
            if (instance == null)
            {
                // If there's more than 1 object of the type T
                if (FindObjectsOfType(typeof(T)).Length > 1)
                {
                    Debug.LogError("[UnitySingleton] Something went really wrong - there should never be more than 1 singleton!");
                    return instance;
                }

                // Try to find an object of the type T
                instance = (T)FindObjectOfType(typeof(T));

                // If there's still no instance set
                if (instance == null)
                {
                    // if the object was destroyed because the application is quitting
                    // we should not create a new object!
                    if (applicationIsQuitting)
                    {
                        Debug.LogWarning("[UnitySingleton] Instance '" + typeof(T) + "' already destroyed on application quit." + " Won't create again - returning null.");
                        return null;
                    }

                    // Create the singleton T
                    GameObject singleton = new GameObject();
                    instance = singleton.AddComponent<T>();
                    SetupController();
                    singleton.name = "(S) " + typeof(T).ToString();
                    singleton.transform.parent = controllers.transform;

                    Debug.Log("[UnitySingleton] An instance of " + typeof(T) + " created.");
                }
            }
            // as long we have an instance this cannot be true!
            applicationIsQuitting = false;

            return instance;
        }
    }

    protected void RegisterDependency(BaseSingleton dependency)
    {
        if (!dependencies.Contains(dependency))
        {
            dependencies.Add(dependency);
        }
    }

    public override void Awake()
    {
        base.Awake();
        if (instance == null)
        {
            instance = Instance;
        }
        Object[] objects = FindObjectsOfType(typeof(T));
        if (objects.Length > 1)
        {
            for (int i = 0; i < objects.Length; ++i)
            { 
                // IMPORTANT :: We are keeping the old one and destroying any new ones
                // by checking the condition if the current objects[i] >IS< this.
                if (objects[i] == this)
                {
                    Destroy(((T)objects[i]).gameObject);
                }
            }
        }
    }

    public virtual bool Update()
    {
        for (int i = 0; i < dependencies.Count; ++i)
        {
            if (!dependencies[i].Initialized)
            {
                return false;
            }
        }
        return true;
    }

    public virtual bool FixedUpdate()
    {
        for (int i = 0; i < dependencies.Count; ++i)
        {
            if (!dependencies[i].Initialized)
            {
                return false;
            }
        }
        return true;
    }


    private static void SetupController()
    {
        // Get the game object containing all controllers
        controllers = GameObject.FindGameObjectWithTag("Controllers");

        // If it could not be find
        if (controllers == null)
        {
            //controllers = new GameObject("Controllers");
            //controllers.tag = "Controllers";
            Debug.LogWarning("[UnitySingleton] Controllers GameObject could not be found. Make sure to have Scene Setup correctly!");
        }
    }

    private static bool applicationIsQuitting = false;

    public static bool ApplicationIsQuitting
    {
        get { return applicationIsQuitting; }
    }

    public virtual void OnDestroy()
    {
        applicationIsQuitting = true;
    }
}

public static class GCConstants
{
    public const string achievement_captain = "achievement_captain";
    public const string achievement_fire_power = "achievement_firepower";  
    public const string achievement_supplier = "achievement_supplier";
    public const string achievement_born_ready = "achievement_bornready";   
    public const string achievement_arsenal = "achievement_arsenal";  
    public const string achievement_investor = "achievement_investor";

    public const string leaderboard_skirmish_board = "skirmish_board";
    public const string leaderboard_raid_board = "raid_board";
    public const string leaderboard_onslaught_board = "onslaught_board";
}

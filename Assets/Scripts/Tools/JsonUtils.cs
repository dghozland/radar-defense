using UnityEngine;
using System.IO;
using Newtonsoft.Json;
using System;
using System.Collections;

public class JsonUtils <T>
{
    // -> Application.persistentDataPath    to have individual save.
    private static readonly string persistentAssets = Application.persistentDataPath;

    public static System.Exception Save(string fileName, T value, bool overridePath = false)
    {
        try
        {
            string path = overridePath ? fileName : persistentAssets + "/" + fileName;
            using (StreamWriter file = File.CreateText(path))
            {
                JsonSerializer serializer = new JsonSerializer()
                {
                    Formatting = Formatting.Indented
                };
                serializer.Serialize(file, value);

                return null;
            }
        }
        catch(System.Exception ex)
        {
            Debug.LogError("Exception : " + ex.ToString());
            return ex;
        }
    }
    
    public static System.Exception Load(string fileName, out T value, GameUtils.ELoadType type)
    {
        value = default(T);
        try
        {
            string path = GetPath(fileName, type);  

            string fileContent = GetTextFromPath(path, type);
            if (!string.IsNullOrEmpty(fileContent))
            {
                JsonSerializerSettings settings = new JsonSerializerSettings()
                {
                    Formatting = Formatting.Indented,
                };
                value = (T)JsonConvert.DeserializeObject<T>(fileContent, settings);
            }
            else
            {
                throw new FileLoadException();
            }
            return null;
        }
        catch(System.Exception ex)
        {
            // Note :: Disabling the warning since Radar is only
            // creating the JSON files when they don't exist.
            if (ex.GetType() != typeof(FileLoadException))
            {
                Debug.LogError("Exception : " + ex.ToString());
            }

            return ex;
        }
    }

    private static string GetPath(string fileName, GameUtils.ELoadType type)
    {
        switch (type)
        {
            case GameUtils.ELoadType.DynamicData:
                return persistentAssets + "/" + fileName;
            case GameUtils.ELoadType.StaticData:
                return fileName;
            case GameUtils.ELoadType.Override:
            default:
                return fileName;
        }
    }

    private static string GetTextFromPath(string path, GameUtils.ELoadType type)
    {
        string text = string.Empty;

        if (type == GameUtils.ELoadType.StaticData)
        {
			Debug.Log ("Loading static data: " + path);
            TextAsset txtAsset = (TextAsset)Resources.Load(path, typeof(TextAsset));
			if (txtAsset != null) 
			{
				Debug.Log ("Success loading static data: " + path);
				text = txtAsset.text;
			}
        }
        else
        {
            if (File.Exists(path))
            {
                text = File.ReadAllText(path);
            }
        }
        return text;
    }

    public void Load<T1>(T1 currentLanguage)
    {
        throw new NotImplementedException();
    }

    public static System.Exception Delete(string fileName)
    {
        try
        {
            if (File.Exists(persistentAssets + "/" + fileName))
            {
                File.Delete(persistentAssets + "/" + fileName);
                return null;
            }
            else
            {
                throw new FileLoadException();
            }
        }
        catch (System.Exception ex)
        {
            return ex;
        }
    }
}

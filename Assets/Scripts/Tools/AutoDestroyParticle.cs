﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(ParticleSystem))]
public class AutoDestroyParticle : MonoBehaviour
{
    private ParticleSystem visualEffect;

    private void Awake()
    {
        visualEffect = GetComponent<ParticleSystem>();
        if (visualEffect.loop)
        {
            Debug.LogWarning("Won't destroy a looping particle system. Set the looping to false.");
        }
    }

    private void Update()
    {
        if (!visualEffect.IsAlive())
        {
            Destroy(gameObject);
        }
    }
}

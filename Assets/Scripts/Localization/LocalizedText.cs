using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class LocalizedText : MonoBehaviour
{
    [SerializeField]
    private string localizedTextID = "";

    [Tooltip("This is used when we have to have other text written with the localized text. (i.e. 'UNLOCK_SPECIAL_TEXT 5' = 'Unlocks at level 5'")]
    // Note :: When this is not empty, the 'localizedTextID' will not be use anymore!
    [SerializeField]
    private string localizedFullTextID = "";

    private Text textComponent;

    public string LocalizedTextID
    {
        get { return localizedTextID; }
        set
        {
            localizedTextID = value;
            RefreshText();
        }
    }
    
    public string LocalizedFullText
    {
        get { return localizedFullTextID; }
        set
        {
            localizedFullTextID = value;
            RefreshText();
        }
    }

    // Note :: Needs to be done in Awake instead of Start, otherwise elements that
    // are disabled on starting the game won't run enter the Start function.
    private void Awake()
    {
        //// Make sure to have the Localization Manager initialized
        //LocalizationManager.Instance.Awake();
        LocalizationManager.Instance.RegisterLanguageChanged(gameObject, OnLanguageChanged);
        
        textComponent = GetComponent<Text>();

        if (localizedTextID.Equals(""))
        {
            // Note :: In Radar, achievement texts receive their 
            // localized text ID from the achievement controller.
            //Debug.LogWarning(gameObject.name + " has a localized text ID not set.");
        }

        RefreshText();
    }

    private void OnDestroy()
    {
        // IMPORTANT :: Not to forget to remove the delegate once it's destroyed.
        // Otherwise an object from another scene that has been deleted could awake.
        // Note :: For some reasons, the OnDestroy() seem to be not always called
        // when loading a new scene. This has the potential to crash when changing language
        // since the event is still attached and will try to call the RefreshText().
        if (!LocalizationManager.ApplicationIsQuitting)
        {
            LocalizationManager.Instance.UnRegisterLanguageChanged(gameObject, OnLanguageChanged);
        }
    }

    public void OnLanguageChanged()
    {
        RefreshText();
    }

    public void RefreshText()
    {
        if (textComponent)
        {
            string localizedString = LocalizationManager.Instance.GetLocalizedString(localizedTextID);
            // If there's an additional text with the localized text
            if (localizedFullTextID != "")
            {
                string localizedFullString = localizedFullTextID;
                // For each possible localized ingame texts that requires additional texts
                for (int i = 0; i < (int)LocalizationManager.ELocalizedInGameTextID.COUNT; ++i)
                {
                    // Get the localized text ID
                    LocalizationManager.ELocalizedInGameTextID eachLocalizedInGameTextID = (LocalizationManager.ELocalizedInGameTextID)i;
                    string eachLocalizedInGameTextIDString = eachLocalizedInGameTextID.ToString();

                    // If the full text contains the localized text ID
                    if (localizedFullTextID.Contains(eachLocalizedInGameTextIDString))
                    {
                        localizedFullString = localizedFullString.Replace(eachLocalizedInGameTextIDString, LocalizationManager.Instance.GetLocalizedString(eachLocalizedInGameTextIDString));
                    }
                }
                textComponent.text = localizedFullString;
            }            
            else
            {
                textComponent.text = localizedString;
            }
        }
        else
        {
            // Safeguard
            // Note :: Should never happen due to LocalizationManager
            // cleaning up the delegates on Awake().
            if (this == null)
            {
                Debug.LogWarning("A localizedText Object has been destroyed and did not get clean up for the event.");
                return;
            }

            // Note :: Happens when an external entity changes
            // the localized text ID before the awake of this.
            Awake();
        }        
    }
}

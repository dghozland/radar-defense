using UnityEngine;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Text;
using System.IO;
using System;
using System.Linq;
using Steamworks;

// TODO :: Have the LocalizationManager always survive through each scene changes!!
// BUG #1 :: A new localization manager is loaded, with bad timing,
// its trying to Awake a localized text that has been destroyed..
// or try catch in the awake of localized text..

// BUG #2 :: Go in Battle scene, go back in Menu scene, change language, crash.
public class LocalizationManager : UnitySingleton<LocalizationManager>
{
    #region In-code static Text IDs

    // Note :: This enum is currently used for localized texts
    // that have more than only the localized text ID in it.
    // REFACTOR :: Put all the static text IDs below into
    // this enum and always use the localizedFullText variable
    // in the LocalizedText class.
    public enum ELocalizedInGameTextID
    {
        UNLOCK_LVL_TEXT,
        UPGRADES_TEXT,
        CONSUMABLES_TEXT,
        BONUS_BOOSTER_TEXT,
        LEVEL_CLEARED_TEXT,
        LEVEL_FAILED_TEXT,
        LEVEL_RETREAT_TEXT,
        WAVE_TEXT,
        WEAPON_MAX_LVL_TEXT,
        RECOMMANDED_WEAPON_LVL_TEXT,
        COUNT
    }

    public static readonly string LEVEL_TEXT = "LEVEL_TEXT";
    public static readonly string NORMAL_LAST_STAND_TITLE = "NORMAL_LAST_STAND_TITLE";
    public static readonly string HARD_LAST_STAND_TITLE = "HARD_LAST_STAND_TITLE";
    public static readonly string INSANE_LAST_STAND_TITLE = "INSANE_LAST_STAND_TITLE";
    public static readonly string WAVE_TEXT = "WAVE_TEXT";
    public static readonly string LEVEL_CLEARED_TEXT = "LEVEL_CLEARED_TEXT";
    public static readonly string LEVEL_FAILED_TEXT = "LEVEL_FAILED_TEXT";
    public static readonly string LEVEL_RETREAT_TEXT = "LEVEL_RETREAT_TEXT";
    public static readonly string BOOST_DAMAGE_SPEED_TEXT = "BOOST_DAMAGE_SPEED_TEXT";
    public static readonly string BOOST_REGEN_SPEED_TEXT = "BOOST_REGEN_SPEED_TEXT";
    public static readonly string BOOST_RELOAD_SPEED_TEXT = "BOOST_RELOAD_SPEED_TEXT";
    public static readonly string BOOST_PROJECTILE_SPEED_TEXT = "BOOST_PROJECTILE_SPEED_TEXT";
    public static readonly string BOOST_RADAR_FREQUENCY_TEXT = "BOOST_RADAR_FREQUENCY_TEXT";
    public static readonly string CREDITS_TEXT = "CREDITS_TEXT";
    public static readonly string GOLD_TEXT = "GOLD_TEXT";
    public static readonly string INCOMING_MISSILE_TEXT = "INCOMING_MISSILE_TEXT";
    public static readonly string ALERT_ECM_TEXT = "ALERT_ECM_TEXT";
    public static readonly string ALERT_SHIELD_TEXT = "ALERT_SHIELD_TEXT";
    public static readonly string ALERT_REPAIR_TEXT = "ALERT_REPAIR_TEXT";
    public static readonly string DOUBLE_KILL_TEXT = "DOUBLE_KILL_TEXT";
    public static readonly string TRIPLE_KILL_TEXT = "TRIPLE_KILL_TEXT";
    public static readonly string QUADRA_KILL_TEXT = "QUADRA_KILL_TEXT";
    public static readonly string PENTA_KILL_TEXT = "PENTA_KILL_TEXT";
    public static readonly string HEXA_KILL_TEXT = "HEXA_KILL_TEXT";
    public static readonly string HEPTA_KILL_TEXT = "HEPTA_KILL_TEXT";
    public static readonly string OCTA_KILL_TEXT = "OCTA_KILL_TEXT";
    public static readonly string NONA_KILL_TEXT = "NONA_KILL_TEXT";
    public static readonly string DECA_KILL_TEXT = "DECA_KILL_TEXT";
    public static readonly string ARTILLERY_TEXT = "ARTILLERY_TEXT";
    public static readonly string FLAK_TEXT = "FLAK_TEXT";
    public static readonly string MISSILE_TEXT = "MISSILE_TEXT";
    public static readonly string RAILGUN_TEXT = "RAILGUN_TEXT";
    public static readonly string ROCKETS_TEXT = "ROCKETS_TEXT";
    public static readonly string LASER_TEXT = "LASER_TEXT";
    public static readonly string ARTILLERY_DESC = "ARTILLERY_DESC";
    public static readonly string FLAK_DESC = "FLAK_DESC";
    public static readonly string MISSILE_DESC = "MISSILE_DESC";
    public static readonly string RAILGUN_DESC = "RAILGUN_DESC";
    public static readonly string ROCKETS_DESC = "ROCKETS_DESC";
    public static readonly string LASER_DESC = "LASER_DESC";
    public static readonly string PULSE_TEXT = "PULSE_TEXT";
    public static readonly string RELOAD_TEXT = "RELOAD_TEXT";
    public static readonly string REPAIR_TEXT = "REPAIR_TEXT";
    public static readonly string SLOWER_TEXT = "SLOWER_TEXT";
    public static readonly string OHSHIT_TEXT = "OHSHIT_TEXT";
    public static readonly string CAMPAIGN_TAB = "CAMPAIGN_TAB";
    public static readonly string LAST_STAND_TAB = "LAST_STAND_TAB";
    public static readonly string EVENTS_TAB = "EVENTS_TAB";
    public static readonly string PACKS_TAB = "PACKS_TAB";
    public static readonly string OPTIONS_TAB = "OPTIONS_TAB";
    public static readonly string UPGRADES_TEXT = "UPGRADES_TEXT";
    public static readonly string CONSUMABLES_TEXT = "CONSUMABLES_TEXT";
    public static readonly string BULLET_SPEED_TEXT = "BULLET_SPEED_TEXT";
    public static readonly string MISSILE_SPEED_TEXT = "MISSILE_SPEED_TEXT";
    public static readonly string ROCKET_SPEED_TEXT = "ROCKET_SPEED_TEXT";
    public static readonly string RELOAD_TIME_TEXT = "RELOAD_TIME_TEXT";
    public static readonly string LASER_SPEED_TEXT = "LASER_SPEED_TEXT";
    public static readonly string REGENERATION_TEXT = "REGENERATION_TEXT";
    public static readonly string AIR_DAMAGE_TEXT = "AIR_DAMAGE_TEXT";
    public static readonly string GROUND_DAMAGE_TEXT = "GROUND_DAMAGE_TEXT";
    public static readonly string EXPLOSION_RADIUS_TEXT = "EXPLOSION_RADIUS_TEXT";
    public static readonly string LASER_RADIUS_TEXT = "LASER_RADIUS_TEXT";
    public static readonly string SIGHT_RADIUS_TEXT = "SIGHT_RADIUS_TEXT";
    public static readonly string HITPOINTS_TEXT = "HITPOINTS_TEXT";
    public static readonly string CARDS_TEXT = "CARDS_TEXT";
    public static readonly string WATCH_AD_TEXT = "WATCH_AD_TEXT";
    public static readonly string FREE_TEXT = "FREE_TEXT";
    public static readonly string NOT_AVAILABLE_WATCH_AD_TEXT = "NOT_AVAILABLE_WATCH_AD_TEXT";
    public static readonly string NEXT_WAVE_IN_TEXT = "NEXT_WAVE_IN_TEXT";
    public static readonly string CLAIM_NOW_TEXT = "CLAIM_NOW_TEXT";
    public static readonly string WEAPON_UPGRADE_TEXT = "WEAPON_UPGRADE_TEXT";
    public static readonly string CONSUMABLE_TEXT = "CONSUMABLE_TEXT";
    public static readonly string EPIC_TEXT = "EPIC_TEXT";
    public static readonly string RARE_TEXT = "RARE_TEXT";
    public static readonly string COMMON_TEXT = "COMMON_TEXT";
    public static readonly string GOOGLE_LOGOUT_TEXT = "GOOGLE_LOGOUT_TEXT";
    public static readonly string GOOGLE_LOGIN_TEXT = "GOOGLE_LOGIN_TEXT";
    public static readonly string UNLOCK_LVL_TEXT = "UNLOCK_LVL_TEXT";
    public static readonly string WEAPON_POSITION_NAME = "WEAPON_POSITION_NAME";
    public static readonly string BONUS_BOOSTER_TEXT = "BONUS_BOOSTER_TEXT";
    public static readonly string OHSHIT_DESC = "OHSHIT_DESC";
    public static readonly string PULSE_DESC = "PULSE_DESC";
    public static readonly string RELOAD_DESC = "RELOAD_DESC";
    public static readonly string REPAIR_DESC = "REPAIR_DESC";
    public static readonly string SLOWER_DESC = "SLOWER_DESC";
    public static readonly string OHSHIT_STATS = "OHSHIT_STATS";
    public static readonly string PULSE_STATS = "PULSE_STATS";
    public static readonly string RELOAD_STATS = "RELOAD_STATS";
    public static readonly string REPAIR_STATS = "REPAIR_STATS";
    public static readonly string SLOWER_STATS = "SLOWER_STATS";
    public static readonly string MISSION_FIRST_OBJECTIVE_DESC_TEXT = "MISSION_FIRST_OBJECTIVE_DESC_TEXT";
    public static readonly string MISSION_SECOND_OBJECTIVE_DESC_TEXT = "MISSION_SECOND_OBJECTIVE_DESC_TEXT";
    public static readonly string MISSION_THIRD_OBJECTIVE_DESC_TEXT = "MISSION_THIRD_OBJECTIVE_DESC_TEXT";
    public static readonly string LAST_STAND_FIRST_OBJECTIVE_DESC_TEXT = "LAST_STAND_FIRST_OBJECTIVE_DESC_TEXT";
    public static readonly string LAST_STAND_SECOND_OBJECTIVE_DESC_TEXT = "LAST_STAND_SECOND_OBJECTIVE_DESC_TEXT";
    public static readonly string LAST_STAND_THIRD_OBJECTIVE_DESC_TEXT = "LAST_STAND_THIRD_OBJECTIVE_DESC_TEXT";
    public static readonly string WEAPON_MAX_LVL_TEXT = "WEAPON_MAX_LVL_TEXT";
    public static readonly string WEAPON_UNLOCKED_TEXT = "WEAPON_UNLOCKED_TEXT";
    public static readonly string CONSUMABLE_UNLOCKED_TEXT = "CONSUMABLE_UNLOCKED_TEXT";
    public static readonly string RECOMMANDED_WEAPON_LVL_TEXT = "RECOMMANDED_WEAPON_LVL_TEXT";
    public static readonly string LASTSTAND_POPUP_GC_DESC = "LASTSTAND_POPUP_GC_DESC";
    public static readonly string LASTSTAND_POPUP_GP_DESC = "LASTSTAND_POPUP_GP_DESC";
    public static readonly string USED_TEXT = "USED_TEXT";
    #endregion

    private static bool isInitialized = false;
    private static readonly string LOCALIZATION_DATA_PATH = "Localization/";

	//Steam
	private string steam_uiLanguage;
	//

    private static Dictionary<GameObject, Action> onLanguageChangedDelegated = new Dictionary<GameObject, Action>();

    public void RegisterLanguageChanged(GameObject gameObject, Action registerFunction)
    {
        if (!onLanguageChangedDelegated.ContainsKey(gameObject))
        {
            onLanguageChangedDelegated.Add(gameObject, registerFunction);
        }
    }

    public void UnRegisterLanguageChanged(GameObject gameObject, Action registerFunction)
    {
        if (onLanguageChangedDelegated.ContainsKey(gameObject))
        {
            onLanguageChangedDelegated.Remove(gameObject);
        }
    }

    private void HandleLanguageChanged()
    {
        for (int i = onLanguageChangedDelegated.Count - 1; i >= 0; i--)
        {
            var keyValuePair = onLanguageChangedDelegated.ElementAt(i);
            if (keyValuePair.Key != null)
            {
                // call it
                keyValuePair.Value();
            }
            else
            {
                // remove it
                onLanguageChangedDelegated.Remove(keyValuePair.Key);
            }
        }
    }

    [SerializeField]
    private SystemLanguage currentLanguage = SystemLanguage.English;
    private List<Dictionary<string, string>> localizationData;

    public SystemLanguage CurrentLanguage
    {
			get { return currentLanguage; }
	        set
	        {
	            currentLanguage = value;
	            LanguageChanged();
	        }
    }


    public override void Awake()
    {
        base.Awake();

        // IMPORTANT :: Always clean up remaining delegates when awaken!
        // Note :: Otherwise, this has the potential to crash when changing language
        // since the event is still attached and will try to call the RefreshText()
        // for each LocalizedText objects.
        // TEMP :: Not working because it's gonna remove wanted delegates at first.
        //foreach (Delegate d in onLanguageChanged.GetInvocationList())
        //{
        //    //Debug.Log("Delegate : " + d.ToString());
        //    onLanguageChanged -= (LanguageEventHandler)d;
        //}

        if (!isInitialized)
        {
            // Always keep the flow state controller loaded through the scene changes
            DontDestroyOnLoad(gameObject);
            isInitialized = true;

			//Steam
			if(SteamManager.Initialized)
			{
				steam_uiLanguage = SteamUtils.GetSteamUILanguage();
				Debug.Log ("STEAM LANGUAGE  " + steam_uiLanguage);
				if (steam_uiLanguage == "english") CurrentLanguage = SystemLanguage.English;
				if (steam_uiLanguage == "french")  CurrentLanguage = SystemLanguage.French;
				else CurrentLanguage = SystemLanguage.English;
			}
			//

            LoadLanguage();
            SetupHints();
        }
    }

    private void SetupHints()
    {
        HintController.Instance.RegisterHint(Instance.GetLocalizedString("HINT_1"));
        HintController.Instance.RegisterHint(Instance.GetLocalizedString("HINT_2"));
        HintController.Instance.RegisterHint(Instance.GetLocalizedString("HINT_3"));
        HintController.Instance.RegisterHint(Instance.GetLocalizedString("HINT_4"));
        HintController.Instance.RegisterHint(Instance.GetLocalizedString("HINT_5"));
        HintController.Instance.RegisterHint(Instance.GetLocalizedString("HINT_6"));
        HintController.Instance.RegisterHint(Instance.GetLocalizedString("HINT_7"));
        HintController.Instance.RegisterHint(Instance.GetLocalizedString("HINT_8"));
        HintController.Instance.RegisterHint(Instance.GetLocalizedString("HINT_9"));
        HintController.Instance.RegisterHint(Instance.GetLocalizedString("HINT_10"));
    }

    private void SaveLanguage()
    {
		JsonUtils<SystemLanguage>.Save(GameUtils.OPTIONS_JSON, currentLanguage);
    }

    private void LoadLanguage()
    {
        System.Exception ex = JsonUtils<SystemLanguage>.Load(GameUtils.OPTIONS_JSON, out currentLanguage, GameUtils.ELoadType.DynamicData);
        if (ex != null
            && ex.GetType() == typeof(FileLoadException))
        {
            ResetLanguage();
        }

        else
        {
            LanguageChanged();
        }
    }

    private void ResetLanguage()
    {
        // IMPORTANT :: Here's the list of supported languages
        // Maybe find a way to have better maintenance 
        // from updating the code here

        if (Application.systemLanguage == SystemLanguage.English
            || Application.systemLanguage == SystemLanguage.French
            || Application.systemLanguage == SystemLanguage.German)
        {
            CurrentLanguage = Application.systemLanguage;
        } 
        // Default language if needed
        else
        {
            CurrentLanguage = SystemLanguage.English;
        } 
    }

    private void LanguageChanged()
    {
        // Reload localization data
        LoadLocalizationData();

        // Trigger event that will refresh all localized texts
        HandleLanguageChanged();

        // Save the language in JSON
        SaveLanguage();
    }
    
    private void LoadLocalizationData()
    {
        string path = LOCALIZATION_DATA_PATH + currentLanguage.ToString();
        JsonUtils<List<Dictionary<string, string>>>.Load(path, out localizationData, GameUtils.ELoadType.StaticData);
        // Warning :: Language file name must fit the SystemLanguage name
       

        //TextAsset txt = (TextAsset)Resources.Load(path, typeof(TextAsset));
        //if (txt != null)
        //{
        //    string content = txt.text;
        //    JsonSerializerSettings settings = new JsonSerializerSettings()
        //    {
        //        Formatting = Formatting.Indented,
        //        TypeNameHandling = TypeNameHandling.All
        //    };            
        //    localizationData = JsonConvert.DeserializeObject<>(content, settings);
        //}
        //else
        //{
        //    Debug.LogWarning("Couldn't load " + path);
        //}
    }

    public string GetLocalizedString(string id)
    {
        if (localizationData == null)
        {
            // TEMP :: If this is seen, solution would be to make sure to have 
            // the Localization Manager initialized before LocalizedText Awake().
            return "INVALID LOCALIZATION DATA";
        }

        for (int i = 0; i < localizationData.Count; ++i)
        {
            if (localizationData[i]["Key"] == id)
            {
                return localizationData[i]["Value"] as string;
            }
        }
        
        return "INVALID LOCALIZED TEXT ID";
    }
}
